## Descripción

Este repositorio contiene el blog Freak Spot, ubicado en
<https://freakspot.net/> y
<http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/>.

## Generar el blog

Tenemos un [vídeo mostrando cómo se genera Freak
Spot](https://freakspot.net/como-generar-freakspot/). Sigue leyendo si
prefieres una explicación escrita.

### Instalación de dependencias

Puedes generar este blog en tu ordenador. Para ello necesitas tener
los siguientes programas instalados:
* [Pelican](https://getpelican.com/). Es el generador de páginas
* [Python](http://python.org/). Es el lenguaje de programación en el que
  está escrito Pelican.
* [Markdown](https://pypi.python.org/pypi/Markdown/). Markdown
  es el lenguaje de marcado ligero en el que están escritos los
  artículos y páginas.
* [Babel](https://pypi.org/project/Babel/). Para la traducción del tema
  de la página.
* [BeautifulSoup4](https://pypi.python.org/pypi/beautifulsoup4/).
  Requerido por el complemento tipue-search.

GNU/Linux y macOS suelen traer Python instalado por defecto. Para
instalar los requisitos de Python en estos sistemas operativos basta con
ejecutar la siguiente instrucción: `pip install -U Babel beautifulsoup4
markdown pelican`.

No es realmente necesario, pero es muy recomendable instalar [GNU
Make](https://gnu.org/software/make). En la siguiente sección se asume
que tienes Make instalado. En sistemas operativos basados en Debian se
puede instalar con la instrucción `apt install make`.

### Generar el contenido

Tras instalar las dependencias, puedes generar el blog ejecutando las
siguientes órdenes:
1. `git clone https://notabug.org/Freak-Spot/Freak-Spot`
2. `cd Freak-Spot/freak-theme && make compile`
3. `cd .. && make serve`

Tras completar estos pasos, el blog estará disponible en la carpeta
`output`. Abre el archivo `output/index.html` con tu navegador favorito
para ver el blog.

## Colaboración

### Escribir un artículo

Si quieres publicar un artículo en Freak Spot, puedes realizar un *pull
request* o [mandarme el artículo por correo
electrónico](mailto:jorgesumle@freakspot.net). Si el artículo es
interesante, puede que lo acepte.

Si no sabes cómo funciona Pelican, puedes mándame el artículo por correo
sin preocuparte por el formato. Si quieres hacer un *pull request*,
debes utilizar los metadatos de Pelican y escribir el artículo en
[Markdown](https://es.wikipedia.org/wiki/Markdown). A continuación
se muestra un ejemplo.

```
Author: Nombre del autor
Category: Python
Date: 2017-12-03 10:20
Slug: titulo-del-artículo
Tags: pelican, publishing
Title: Título del artículo

Este es el contenido del artículo. Puedo usar la sintaxis de
[Markdown](https://es.wikipedia.org/wiki/Markdown)
```

El contenido de Freak Spot se encuentra bajo dominio público
([CC0 1.0 Universal](https://creativecommons.org/publicdomain/mark/1.0/)).
Si quieres publicar tu artículo usando otra licencia, indícalo.

Cuando escribas un artículo intenta utilizar
[etiquetas](https://freakspot.net/tags/) y
[categorías](https://freakspot.net/categories/) que ya existan, no
utilices el nombre de la categoría del artículo también como etiqueta.

Si necesitas alguna funcionalidad especial (como un vídeo, una imagen de
cabecera...) intenta utilizar complementos o funcionalidades que ya
estén presentes en el blog. Consulta ejemplos de artículos que hacen uso
de estos.

### Hacer una traducción

Copia el archivo que quieres traducir y añade el código del idioma (por
ejemplo `.de` para el alemán) antes de la extensión `.md`. Traduce el
contenido del artículo o la página. Cuando termines, cambia los
metadatos que hagan falta (título, autor, etc.) y añade el metadato
`Lang` con el valor del código del idioma de la traducción. A
continuación se muestra un ejemplo.

```
Author: Jorge
Date: 2017-02-28 01:11
Lang: de
Slug: reflexión-sobre-los-medios-de-comunicación
Tags: Bücher, Fernsehen, freie Software, Internet, Kommentar, Medien
Title: Kommentar zu den Medien

Heutzutage gibt es viele Medien, die vorher nicht existierten. Das [...]
```

### Mejora de la página

También puedes mejorar la apariencia y funcionalidad de la página.
Simplemente crea un *pull request*. Antes de hacerlo, te recomendamos
conocer [cómo se organiza la estructura de
directorios](https://freakspot.net/pages/estructura-de-directorios.html).
También es
recomendable comprobar si el código
<abbr title="HyperText Markup Language">HTML</abbr>
es válido ejecutando `make validate` en la carpeta del proyecto (debes
tener
<a href="https://www.gnu.org/software/make/"><abbr title="GNU's Not Unix">GNU</abbr> Make</a>
y [html5validator](https://pypi.python.org/pypi/html5validator)
instalados).

## Información de licencias

El contenido de este sitio web se encuentra bajo dominio público,
excepto donde se especifique lo contrario.

Todo el código es software libre; se encuentra bajo la licencia <a
href="https://www.gnu.org/licenses/agpl-3.0.html" rel="license"><abbr
title="Affero General Public License version 3">AGPLv3</abbr></a>, salvo
las siguientes excepciones:

- Licencias de JavaScript. La información sobre las licencias de
  JavaScript se encuentra en el archivo `content/pages/libreJS.md` en
  forma de [tabla preparada para ser leída por LibreJS](https://www.gnu.org/software/librejs/manual/librejs.html#JavaScript-Web-Labels).
- Todo lo que hay en el directorio `freak-theme` se encuentra bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.
- UglifyCSS, bajo la [licencia Expat](https://spdx.org/licenses/MIT.html).
- UglifyJS, bajo la [licencia FreeBSD](https://www.freebsd.org/copyright/freebsd-license.html).
- Los siguientes complementos (se encuentran en el directorio
  `plugins`):

    - **another_read_more_link**. Bajo la licencia [Apache License, Version 2.0](https://notabug.org/Freak-Spot/Freak-Spot/raw/master/plugins/another_read_more_link/LICENSE).
    - **pelican-css**. Bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.
    - **pelican-js**. Bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.
    - **minify-css-js.py**. Bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.

## Preguntas frecuentes

### ¿Dónde veo si las pruebas han fallado?

- [En GitLab](https://gitlab.com/Freak-Spot/Freak-Spot).

### ¿Y el sistema de comentarios?

Freak Spot utiliza el sistema de comentarios
[Hashover](https://github.com/jacobwb/hashover-next), el cual es
software libre:
- [Código fuente del lado del servidor](https://freakspot.net/hashover-next/backend/source-viewer.php)
- [Código JavaScript](https://freakspot.net/hashover-next/comments.php)
- [Diseño](https://notabug.org/Freak-Spot/estoso-de-hashover)

### ¿Y los vídeos?

Los vídeos que contiene Freak Spot no se encuentran en este repositorio
por varias razones:

- No es nada útil añadir los vídeos al repositorio, pues Git no está
  hecho para controlar los cambios en los vídeos. Además, normalmente no
  se suelen modificar los vídeos ya creados.
- Solo harían que este repositorio fuera más pesado aún.
- Algunos vídeos no se encuentran alojados en el servidor de Freak Spot.

La siguiente lista contiene todos los vídeos que se encuentran en Freak
Spot:

- [¿Cómo generar Freak Spot?](https://archive.org/download/libreweb/freakspot.webm)
- [My Moon (Blender short film)](https://freakspot.net/temporal/My_Moon_(Blender_short_film).mp4)
- [Divide la página del navegador con Tile Tabs](https://freakspot.net/temporal/Tile_Tabs.webm) ([720p](https://freakspot.net/temporal/Tile_Tabs1280x720.webm), [480p](https://freakspot.net/temporal/Tile_Tabs854x480.webm), [360p](https://freakspot.net/temporal/Tile_Tabs640x360.webm))
- [Locutus de GNU](https://ia903102.us.archive.org/7/items/libreweb/locutus.webm)
- [¿Cómo poner una imagen sobre un fondo con GIMP?](https://freakspot.net/video/Como-poner-una-imagen-sobre-un-fondo-con-GIMP.mp4)
- [Caracoles](https://freakspot.net/temporal/caracoles.mp4)
- [Sin parar](https://freakspot.net/temporal/Sin_parar.webm)
- [Escribir en griego antiguo en GNU/Linux](https://freakspot.net/video/escribiendo-en-griego-antiguo-con-onboard.webm)
- [Myopia: A Modern Yet Reversible Disease — Todd Becker, M.S. (AHS14)](https://freakspot.net/video/Myopia%20-%20A%20Modern%20Yet%20Reversible%20Disease%20%E2%80%94%20Todd%20Becker,%20M.S.%20(AHS14).webm)
- [Super Bombinhas (en esperanto)](https://freakspot.net/video/super-bombinhas.webm)
- [Super Bombinhas (en español)](https://freakspot.net/video/super-bombinhas-es.webm)
- [Super Bombinhas (en portugués)](https://freakspot.net/video/super-bombinhas-pt.webm)
- [Super Bombinhas (en inglés)](https://freakspot.net/video/super-bombinhas-en.webm)
- [Tráiler de SuperTuxKart 1.3](https://freakspot.net/video/trailer-supertuxkart.mp4)
- [Cómo eliminar celdas de tablas en LibreOffice Writer](https://freakspot.net/video/quitar-celdas-LibreOffice-Writer.webm)
- [Como eliminar células de tabelas no LibreOffice Writer](https://freakspot.net/video/eliminar-células-de-tabelas-no-LibreOffice-Writer.webm)

## Compresión de JavaScript y CSS

Cuando se genera la página que estará disponible en Internet con `make
publish` se usan los siguientes programas:

* [UglifyCSS](https://github.com/fmarcia/UglifyCSS). Compresor de código
  <abbr title="Cascading Style Sheets">CSS</abbr>.
* [UglifyJS](http://lisperator.net/uglifyjs/). Compresor de código
  JavaScript.
