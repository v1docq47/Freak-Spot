Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2020-04-06
Lang: es
Slug: autoevaluación-covid-19
Tags: aplicación, COVID-19, España, Google, página web, privacidad, seguridad
Title: El Gobierno de España promete liberar el programa Autoevaluación COVID-19
Image: <img src="/wp-content/uploads/2020/04/1024px-Electron_micrograph_of_two_coronaviruses.jpg" alt="" width="1024" height="731" srcset="/wp-content/uploads/2020/04/1024px-Electron_micrograph_of_two_coronaviruses.jpg 1024w, /wp-content/uploads/2020/04/1024px-Electron_micrograph_of_two_coronaviruses-512x365.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

Ha lanzado recientemente el Gobierno de España una [página
web](https://web.archive.org/web/20200406151517/https://asistencia.covid19.gob.es/) y una aplicación para el
autodiagnóstico del COVID-19, software privativo, al menos por ahora.
Dicen:

> Nuestra intención es liberar el código fuente para que otros organismos
> oficiales puedan implementar sus propias versiones de la aplicación,
> cada uno adaptando la herramienta a su protocolo sanitario de
> autoevaluación y triaje, asegurando la accesibilidad y la igualdad de
> oportunidades poniendo tecnología al servicio de la lucha contra la
> pandemia.

Según he visto, parte del
código de la página enlaza a librerías externas que se encuentran en
servidores de Google. El código que se carga, aunque pertenece a una
librería libre llamada [Web Font
Loader](https://github.com/typekit/webfontloader), está ofuscado.

Ya que tienen un correo de contacto, les he hecho llegar el siguiente
mensaje con el asunto «Sugerencia de seguridad y cuestiones»:

> Le he echado un vistazo al código de la página principal. No se
> debería ejecutar código de servidores externos, sino alojarlo todo en
> el propio servidor. Hablo de recursos externos como
> https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js, que
> debería ser alojado en un servidor bajo el control del Gobierno de
> España, no en el de una empresa de EE. UU., si es que se toman la
> seguridad en serio. Si no, desde el servidor de Google pueden cambiar
> el código en cualquier momento.

> A propósito, ¿cuánto dinero público se ha invertido para el
> desarrollo, de esta aplicación y página web? ¿Y en los otros proyectos
> redundantes de las demás Comunidades Autónomas? ¿Dónde se publicará el
> código fuente? ¿Por qué no se ha hecho aún?

Como mencioné en [un artículo anterior](/rastreo-exclusión-y-censura-con-la-excusa-del-covid19/),
esta aplicación no ofrece nada nuevo: toda la información sobre el
COVID-19 se encuentra fácilmente en Internet.

Las palabras en relación a la privacidad en este caso son más bonitas
que las de la aplicación de la Comunidad de Madrid, que daba a empresas
privadas datos públicos de salud:

> el único responsable de los datos es el Ministerio de Sanidad
