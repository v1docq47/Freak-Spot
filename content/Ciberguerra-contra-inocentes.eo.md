Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2022-04-14 23:00
Lang: eo
Save_as: reteja-milito-kontraŭ-senkulpaj-pri-la-fiprogramaro-enigita-en-nodeipc/index.html
URL: reteja-milito-kontraŭ-senkulpaj-pri-la-fiprogramaro-enigita-en-nodeipc/
Slug: ciberguerra-contra-inocentes
Tags: npm, pako-administrilo, politiko, Rusio
Title: Reteja milito kontraŭ senkulpaj: pri la fiprogramaro enigita en node-ipc

La provizoĉena atako uzita de la programo [node-ipc](https://github.com/RIAEvangelist/node-ipc),
dependaĵo de multaj aliaj programoj, sonigis la alarmilojn en la
komunumo de la libera programaro. La fiprogramaro kreis dosieron kun
protestmesaĝon en la dosierujo kaj forigis ĉiujn dosierojn de la
komputiloj, kiuj havis rusan aŭ belorusan IP-adreson.

Kompreneble, ĉar la programo estis libera, oni povis kontroli tion, kion
ĝi faris, antaŭ elŝuti ĝin, sed multaj homoj ne kutimas praktike fari
tion. La atako montris, ke la aŭtomataj ĝisdatigoj de programoj ne estas
sekuraj en pakaĵaj administriloj de programlingvoj, speciale de npm, kiu
jam havis similajn problemojn estintece.

La atako kaŭzis perdon de konfido de la komunumo de la libera programaro
al programisto Brandon Nozaki Miller. La atako kun la paca mesaĝo estas
vere sabotigo, ne kontraŭ la militaj aŭtoritatoj de Rusio kaj Belorusio,
sed kontraŭ homoj, kiuj loĝas en tiuj landoj aŭ kiuj uzas [virtualan
privatan reton](https://eo.wikipedia.org/wiki/VPN) kun IP-adreso de tiuj
landoj, kaj faras programojn. Ĉu montri kiel malbonaj kaj ataki ĉiujn
loĝantojn de lando kondukos al paco?
