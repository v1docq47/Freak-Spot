Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2019-12-29
Modified: 2020-10-21 21:04
Image: <img src="/wp-content/uploads/2019/12/DuckDuckGo-HTML-en-español.png" alt="Habla el pato de DuckDuckGo en castellano en la versión HTML">
Lang: es
Slug: duckduckgo-html-sin-javascript
Tags: Abrowser, buscador, código fuente, complemento, DuckDuckGo, Firefox, JavaScript, instalar, privacidad, software libre, software privativo, Trisquel, Trisquel 8, Trisquel Flidas
Title: DuckDuckGo HTML en Firefox con resultados en español

Suelo usar «el buscador que no te rastrea», DuckDuckGo. Como el código
JavaScript de la versión estándar de DuckDuckGo es privativo, yo uso la
[versión sin JavaScript](https://html.duckduckgo.com/html) (que también
existe [como servicio oculto de
Tor](https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/html)).

Lo malo es que la versión HTML de DuckDuckGo no detecta las preferencias
de idioma del navegador. Esto puede ser un inconveniente si estoy
buscando cosas en español, ya que siempre me va a mostrar
antes páginas en inglés.

<a href="/wp-content/uploads/2019/12/calistenia-DuckDuckGo-HTML.png">
<img src="/wp-content/uploads/2019/12/calistenia-DuckDuckGo-HTML.png" alt="Busco «calistenia» y me muestra resultados en inglés">
</a>

Así que he modificado el anterior complemento de búsqueda para que
busque en español. No ha sido muy difícil, la verdad. Solo he añadido el
parámetro de URL `kl=es-es` (para el español de España) tras consultar
la [lista de parámetros](https://duckduckgo.com/duckduckgo-help-pages/settings/params/). De esta forma
salen resultados en español.

<!-- more -->

<a href="/wp-content/uploads/2019/12/calistenia-DuckDuckGo-HTML-español.png">
<img src="/wp-content/uploads/2019/12/calistenia-DuckDuckGo-HTML-español.png" alt="Busco «calistenia», y me muestra resultados en español" width="1000" height="718" srcset="/wp-content/uploads/2019/12/calistenia-DuckDuckGo-HTML-español.png 1000w, /wp-content/uploads/2019/12/calistenia-DuckDuckGo-HTML-español-500x359.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

El
[código
fuente](/wp-content/uploads/2019/12/duckduckgo_html_es.xml) es
muy sencillo. Si te sirve, puedes instalar este complemento clicando
<a href="#" onclick="window.external.AddSearchProvider('/wp-content/uploads/2019/12/duckduckgo_html_es.xml')">aquí</a>. <noscript>(es necesario tener JavaScript
habilitado.)</noscript>
