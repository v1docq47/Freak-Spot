Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-02-19 00:06
Lang: eo
Slug: github-copilot-y-el-blanqueo-de-código-abierto
Save_as: GitHub-Copilot-kaj-senspurigo-de-malfermita-kodo/index.html
URL: GitHub-Copilot-kaj-senspurigo-de-malfermita-kodo/
Tags: maŝinlernado, GitHub, Microsoft, liberaj programoj
Title: GitHub Copilot kaj senspurigo de malfermita kodo

<span style="text-decoration:underline">Ĉi tiu artikolo estas traduko de la artikolo
«[GitHub Copilot and open source laundering](https://drewdevault.com/2022/06/23/Copilot-GPL-washing.html)»
publikigita de Drew Devault sub la
[CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.eo)-permesilo.</span>

*Averto: mi estas la fondinto de firmao, kiu konkuras kun GitHub. Mi
ankaŭ estas longatempa defendanto kaj disvolvanto de liberajn
programojn, kun vasta kompreno de la permesiloj kaj filozofio de liberaj
programoj. Mi ne nomos mian firmaon por malpliigi la atingon de mia
konflikto de interesoj.*

Ni vidis eksplodon pri maŝina lernado en la lasta jardeko apud eksplodo
de populareco de la libera programaro. Samtempe kiam liberaj programoj
superregis la programaron kaj trovis siajn lokojn en preskaŭ ĉiuj novaj
programaj produktoj, maŝina lernado multege pliigis sian malsimplecon,
ebligante pli naturajn interagojn inter homoj kaj komputiloj. Tamen,
malgraŭ ilia paralela leviĝo en komputiko, ĉi tiuj du fakoj restas
filozofie disaj.

Kvankam iuj aŭdace nomitaj firmaoj povas sugesti la malon, la fako de la
maŝina lernado ne ĝuis preskaŭ neniun el la liberecoj donitaj de la
movado de la libera programaro. Granda parto de la kodo, kiu rilatas
al maŝina lernado estas publike disponebla, kaj estas multe da publikaj
esploradaj artikoloj de libera aliro disponeblaj, por ke ĉiu ajn legu.
Tamen la ŝlosilo por maŝina lernado <!-- more --> estas aliro al
altkvalita datenaro kaj multege da komputila povo por prilabori tiujn
datenojn, kaj ĉi tiuj du rimedoj estas kaŝe konservitaj de preskaŭ ĉiuj
partoprenantoj en ĉi tiu fako[^1].

[^1]: Dankon al Mozilla Common Voices, unu el la malmultaj esceptoj el
    ĉi tiu regulo, kiu estas bonega projekto, kiu produktis altkvalitan
    libere disponeblan datenaron de voĉaj montroj kaj uzis ĝin por
    disvolvi liberajn modelojn kaj programojn por parola rekono kaj
    aliformigo el teksto al parolo.

La esenca baro al la aliro por projektoj de maŝina lernado estas
trapasante ĉi tiujn du problemojn, kiuj kutimas esti tre multekostaj.
Produkti altkvalitan bone etikeditan datenaron kutime postulas milojn da
horoj de laboro[^2], tasko kiu povas kosti milionojn da dolaroj. Do ĉia
paŝo, kiu malpliigas ĉi tiun nombron, estas tre dezirinda, eĉ se la
kosto faras etikajn kompromisojn. Per Amazon ĝi havas la formon de
ekonomia ekspluatado pere de fi-laboretoj. Per GitHub ĝi havas la formon
ignori la kondiĉoj de la liberaj permesiloj. Per ĉi tiu agmaniero ili
kreis ilon, kiu faciligas la grandskalan senspurigon de liberaj
programoj, ĉar ĝiaj klientoj, al kiuj GitHub oferas eblan neadon per
neesplorebla algoritmo, igas ilin neliberaj programoj

[^2]: Kutime ekspluata laboro de malalte disvolvitaj landoj, kiun la
    teknologia industrio kutime ŝajnigas, ke ne estas preskaŭ sklaveco.

Liberaj programoj ne estas senkondiĉa donaco. Estas kondiĉoj por
iliaj uzoj kaj reuzoj. Eĉ la tiel nomitaj «liberalaj» programaj
permesiloj devigas postulojn de reuzo, kiel la atribuon. Citante la
MIT-permesilo:

> La rajtigo estas per ĉi tio donita [...] sub la jenaj kondiĉoj:

> La antaŭa aŭtorrajta sciigo kaj ĉi tiu permesa sciigo devas esti
> inkluditaj en ĉiuj la kopioj aŭ gravaj partoj de la Programo.

Aŭ la same «liberala» BSD-permesilo:

> Oni permesas la redistribuon kaj la reuzadon per fontaj kaj duumaj
> formoj, kun aŭ sen modifo, kondiĉe ke la jenaj kondiĉoj estu
> plenumitaj:

> La redistribuo de la fontkodo devas konservi la antaŭan aŭtorrajtan
> atentigon, ĉi tiun liston de kondiĉoj kaj la jenan malgarantion.

En la alia fino de la spektro, rajtocedaj permesiloj kiel GNU General
Public License aŭ Mozilla Public License iras plu, postulante ne nur
atribuon por derivitaj verkoj, sed ke tiuj derivitaj verkoj estu *ankaŭ
publikigitaj* per la sama permesilo. Citante la GPL-n:

> Vi povas transdoni verkon bazitan sur la Programo, aŭ la modifojn por
> produkti ĝin bazite sur la Programo, per la formo de fontkodo sub la
> kondiĉoj de la 4-a sekcio, kondiĉe ke vi ankaŭ plenumu ĉi ĉiujn
> kondiĉojn:

> […]

> Vi devas licenci la tutan verkon, kiel tuton, sub ĉi tiu Permesilo al
> ĉiu homo, kiu iĝu posedanto de kopio.

Kaj la MPL-n:

> Ĉiu distribuo de Protektita Programo en Formo de Fontkodo, inkludante
> ĉiujn ajn Modifojn, kiujn Vi kreas aŭ al kiuj vi kontribuas, devas esti
> sub la kondiĉoj de ĉi tiu Permesilo. Vi devos informi al la ricevantoj,
> ke la Formo de Fontkodo de la Protektita Programo estas regita per la
> kondiĉoj de ĉi tiu Permesilo, kaj kiel ili povas akiri kopion de ĉi tiu
> Permesilo. Vi ne povas klopodi ŝanĝi aŭ limigi la rajtojn de la
> ricevantoj en la Fontkoda Formo.

La permesiloj de liberaj programoj devigas devojn al la uzantoj per
kondiĉoj, kiuj regas la atribuon, sublicencadon, distribuon, patentojn,
registritajn markojn kaj rilatojn kun leĝoj kiel la Digital Millennium
Copyright Act. La komunumo de la libera programaro ne ignoras la
malfacilaĵojn pri la obeo de ĉi tiuj devoj, kiujn kelkaj grupoj vidas kiel
tro premajn. Sed sendepende de kiom da premaj oni povus rigardi ĉi tiujn
devojn necesas, ke oni plenumu ilin. Se vi kredas, ke la forto de la
aŭtorrajto devas protekti vian proprietan programon, vi devas konsenti,
ke ĝi same protektu malfermitkodajn verkojn, malgraŭ la ĝeno aŭ kosto
rilataj al ĉi tiu vero.

GitHub Copilot estas trejnita per programoj regataj per ĉi tiuj
kondiĉoj, kaj ne plenumas ilin, kaj lasas al la uzantoj akcidente ne
sekvu ĉi tiujn kondiĉojn. Iuj argumentas pri la riskoj de «aŭtorrajta
surprizo», kiam iu inkludas verkon licencitan sub la GPL en produkton
kaj surpriziĝas trovante, ke ili ankaŭ estas devigita eldoni sian
produkton sub la kondiĉoj de la GPL. Copilot instituciigas ĉi tiun
riskon kaj ĉiu ajn uzanto, kiu deziras uzi ĝin por disvolvi neliberajn
programojn, ne devus fari tion, aŭ ili povus esti laŭleĝe devigita
eldoni siajn verkojn sub la kondiĉoj de permesilo, kiu estas nedezirinda
por iliaj celoj.

Esence la diskuto povas esti resumita, ĉu aŭ ĉu ne la modelo konstituas
derivitan verkon de sia eniga informo. Microsoft argumentas, ke ĝi ne
tion konstituas. Tamen ĉi tiuj permesiloj ne estas specifaj rilataj al la
rimedo de derivado; la klasika agmaniero kopii kaj alglui de unu
projekto al alia ne devas esti la sola rimedo, por ke ĉi tiuj kondiĉoj
aplikiĝu. La modelo ekzistas kiel rezulto apliki algoritmon al ĉi tiuj
enigaj informoj, kaj tiel la modelo mem estas derivita verko de siaj
enigaĵoj. La modelo, poste uzita por krei novajn programojn, sendas
siajn devojn al tiuj verkoj.

Ĉio ĉi supozas la plej bonan interpreton de la argumento de Microsoft,
tre dependante de la fakto, ke la modelo iĝas ĝenerala programisto,
signife lerninte de siajn enigaĵojn kaj aplikante ĉi tiun scion al la
produktado de originalaj verkoj. Se homa programisto farus la samon,
studante liberajn programojn kaj aplikante tiujn lecionojn, sed ne la
kodon mem, al originalaj projektoj, mi konsentus, ke ria aplikita scio
ne kreas derivitajn verkojn. Tamen tiel ne estas kiel maŝina lernado
funkcias. Maŝina lernado estas esence adorita maŝino de rekonado kaj
reproduktado kaj ne reprezentas veran ĝeneraligon de la lernado. Eble ĝi
havas kapablon de limigita kvanto de origineco, sed ankaŭ kapablas
degradiĝi al simple okazo kopii kaj alglui. Jen estas ekzemplo de
Copilot reproduktante, laŭtekste, funkcion, kiu estas regata de la GPL
kaj kiu estus tiel regata sub ĝiaj kondiĉoj:

<!-- more -->

<figure>
    <video src="/video/copilot-squareroot.mp4" muted autoplay controls></video>
    <figcaption class="wp-caption-text">Fonto: <a href="https://nitter.snopyta.org/mitsuhiko/status/1410886329924194309">Armin Ronacher</a> per Twitter</figcaption>
</figure>

La permesilo reproduktita per Copilot ne estas ĝusta, nek forme nek
funkcie. Ĉi tiu kodo ne estis skribita de V. Petkov kaj la GPL devigas
multe pli fortajn devojn ol tiuj sugestitaj de la komento. Ĉi tiu
malgranda ekzemplo estis intence kaŭzita per sugestiva instigo (ĉi tiu
fama funkcio estas konata kiel la [«rapida inversa kvadrata
radiko»](https://es.wikipedia.org/wiki/Algoritmo_de_la_ra%C3%ADz_cuadrada_inversa_r%C3%A1pida))
kaj la «float Q_», sed ne estas troigo supozi, ke iu povas hazarde fari
ion similan per iu ajn speciale malbonŝanca angla priskribo de sia celo.

Kompreneble la uzo de sugestiva instigo por konvinki al Copilot presi
GPL-licencitan kodon sugestas alian uzon: intence senspurigi liberan
kodon. Se la argumento de Microsoft tenas sin, ja la sola afero, kio
necesas por laŭleĝe eviti permesilon de libera programo, estas instrui al
maŝinlernada algoritmo regurgiti funkcion, kiun vi volas uzi.

Ĉi tio estas problemo. Mi proponas du sugestojn al du legantaroj: unu
al GitHub kaj alian al la disvolvistoj de liberaj programoj, kiuj zorgas
pri Copilot.

Al GitHub: ĉi tiu estas via momento de Oracle kontraŭ Google. Vi
investis por konstrui platformon, sur kio la malfermitkoda revolucio
estis konstruita, kaj ekspluati ĉi tiun platformon per ĉi tiu movo estas
ega perfido de la komunuma fido. La leĝo aplikeblas al vi, kaj baziĝi
sur la fakto, ke la sencentra malfermitkoda komunumo ne povos organizi
efikan juran defion al via militkesto de 7,5 miliardoj da dolaroj, ne
ŝanĝas tion. La malfermitkoda komunumo miras, kaj la miro malrapide sed
certe koleriĝas dum niaj zorgoj estas tute ignoritaj kaj vi puŝas antaŭen
la publikigon de Copilot. Mi esperas, ke se la situacio ne ŝanĝas, vi
trovos grupon sufiĉe motivitan por ŝanĝi tion. La legitimeco de la
ekosistemo de la liberaj programoj povas dependi de ĉi tiu problemo, kaj
estas multaj firmaoj, kiuj estas finance instigitaj certigi, ke ĉi tiu
legitimeco daŭru. Mi ja estas preta aliĝi al kolektiva proceso kiel
prizorganto aŭ kune kun aliaj firmaoj interesitaj pri liberaj
programoj uzante niajn monajn rimedojn por ebligi proceson.

La ilo povas esti plibonigita, eble ankoraŭ ĝustatempe eviti la efikojn
plej damaĝajn (tio estas damaĝajn al via entrepreno) de Copilot. Jen
miaj sugestoj:

1. Ebligi al la uzantoj kaj deponejoj de GitHub elektu ne esti aldonitaj
   al modelo. Pli bone, ebligi, ke ili elektu partopreni. Ne ligu ĉi
   tiun opcion kun nerilataj projektoj kiel Software Heritage kaj
   Internet Archive.
2. Sekvi la programajn permesilojn, kiuj estu aldonita al la modelo kaj
   informi al la uzantoj pri iliaj devoj rilate al tiuj permesiloj.
3. Tute forigi rajtocedan kodon de la modelo, krom se vi volas ankaŭ igi
   la modelon kaj ĝian subtenan kodon liberaj.
4. Konsideri kompensi la aŭtorrajtajn proprietulojn de la projektoj de
   liberaj programoj aldonitaj al la modelo per marĝeno de la
   uzadokotizoj de Copilot, kontraŭ permesilo, kiu permesas ĉi tiun uzon.

Via nuntempa modelo eble devas esti forĵetita. La GPL-kodo inkludita en
ĝi rajtigas al ĉiu ajn kiu uzu ĝin ricevi GPL-igitan kopion de la
modelo por sia propra uzo. Ĝi rajtigas al ĉi tiuj homoj komercan uzadon,
por krei konkurantan produkton per ĝi. Sed, ĝi supozeble ankaŭ inkludas
verkojn sub nekongruaj permesiloj, kiel la CDDL, kio estas... problema. La
tuta afero estas jura malordo.

Mi ne povas paroli nome de la alia parto de la komunumo, kiu estis
malprofitita per ĉi tiu projekto, sed miaflanke mi ne serĉus la
respondojn al iuj el ĉi tiuj aferoj en tribunalo, se vi konsentus solvi
ĉi tiujn problemojn nun.

Kaj mia konsilo al la prizorgantoj de liberaj programoj, al kiuj
ĝenas, ke iliaj permesiloj estu ignoritaj. Unue, ne uzu GitHub-on kaj via
kodo (ankoraŭ) ne eniros en la modelon. [Mi antaŭe
skribis](/eo/gravas-ke-liberaj-programoj-uzu-liberan-infrastrukturon/)
pri, kial kutime gravas, ke projektoj de liberaj programoj uzu liberan
infrastrukturon, kaj ĉi tio nur fortigas tiun fakton. Krome la malnova
«voĉdonu per via monujo» agmaniero estas bona maniero montri vian
malaprobon. Tion dirite, se vi pensas, ke vi *ne* pagas por GitHub, vi
eble volas preni momenton por pripensi, ĉu la instigoj kreitaj per tiu
rilato klarigas ĉi tiun evoluon kaj povus konduki al pli malfavoraj
rezultoj por vi en la estonteco.

Vi ankaŭ povos esti allogita solvi ĉi tiun problemon ŝanĝante viajn
programajn permesilojn por malpermesi ĉi tiun agmanieron. Mi ja diros, ke
laŭ la interpreto de Microsoft pri la situacio (alvokante la justan uzon),
ne gravas al ili, kiun permesilon vi uzas: spite ili uzos vian kodon.
Fakte [iuj proprietaj kodoj](https://nitter.snopyta.org/ChrisGr93091552/status/1539731632931803137) estis inkluditaj en la modelon. Tamen mi ankoraŭ
subtenas viajn klopodojn trakti ĉi tion en viaj programaj permesiloj, ĉar
ĝi donas pli fortan juran bazon, sur kiu ni povas rifuzi Copilot.

Mi avertas al vi, ke la maniero kiel vi traktas tiun klaŭzon de via
permesilo gravas. Ĉiam, kiam vi skribas aŭ ŝanĝas permesilon de libera
programo, vi devus konsideri, ĉu ĝi ankoraŭ estas agnoskita kiel libera
aŭ malfermita kodo post viaj ŝanĝoj. Por esti specifa, klaŭzo kiu
absolute malpermesas la uzon de via kodo por trejni maŝinlernadan
modelon igos vian programon *nelibera*, kaj mi ne rekomendas ĉi tiun
agon. Anstataŭe mi ĝisdatigus viajn permesilojn por klarigi, ke inkludi la
kodon en maŝinlernadan modelon estas konsiderita kiel formo de derivita
laboro kaj ke la kondiĉoj de viaj permesiloj aplikas al la modelo kaj ĉiuj
ajn verkoj produktitaj per tiu modelo.

Resume mi pensas, ke GitHub Copilot estas malbona ideo kiel ĝi estis
desegnita. Ĝi reprezentas senhontan malrespekton al la licencado per si
mem kaj ĝi ebligas similan malobservon &dash; volan aŭ nevolan &mdash;
fare de siaj uzantoj. Mi esperas, ke ili aŭskultu miajn sugestojn, kaj
mi esperas, ke miaj vortoj al la komunumo de liberaj programoj oferu
iujn konkretajn manierojn por antaŭeniri pri ĉi tiu problemo.
