Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-02-06 12:00
Lang: pt
Slug: github-copilot-y-el-blanqueo-de-código-abierto
Save_as: GitHub-Copilot-e-branqueamento-de-código-aberto/index.html
URL: GitHub-Copilot-e-branqueamento-de-código-aberto/
Tags: aprendizagem automática, GitHub, Microsoft, software livre
Title: GitHub Copilot e branqueamento de código aberto

<span style="text-decoration:underline">Este artigo é uma tradução do
artigo «[GitHub Copilot and open source laundering](https://drewdevault.com/2022/06/23/Copilot-GPL-washing.html)»
publicado por Drew Devault sob a licencia
[CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.pt).</span>

*Aviso: sou o fundador de uma empresa que compete com o GitHub. Sou
também um defensor de longa data e desenvolvedor de <i lang="en"
style="font-style: normal">software</i> livre, com um amplo entendimento
de licenciamento e filosofia de
<i lang="en" style="font-style: normal">software</i> livre. Não vou
nomear a minha empresa neste artigo para reduzir o alcance do meu
conflito de interesses.*

Assistimos a uma explosão na aprendizagem automática na última década,
acompanhada de uma explosão na popularidade do <i lang="en">software</i>
livre. Ao mesmo tempo que o <i lang="en">software</i> livre passou a
dominar o <i lang="en">software</i> e encontrou o seu lugar em quase
todos os novos produtos de <i lang="en">software</i>, a aprendizagem
automática aumentou drasticamente em sofisticação, facilitando interações
mais naturais entre seres humanos e computadores. Contudo, apesar do seu
aumento paralelo na computação, estes dois domínios permanecem
filosoficamente distantes.

Embora algumas empresas de nome audacioso possam sugerir o contrário, o
âmbito da aprendizagem automática não gozou de quase nenhuma das
liberdades transmitidas pelo movimento do <i lang="pt">software</i>
livre. Grande parte do código em si relacionado com a aprendizagem
automática está disponível ao público, e há muitos documentos de
investigação de acesso público disponíveis para quem quer que seja que
os leia. Contudo, a chave para a aprendizagem automática <!-- more -->é
o acesso a um conjunto de dados de alta qualidade e a montes de poder
computacional para processar esses dados, e estes dois recursos ainda
são mantidos sob chave por quase todos os participantes nesta área[^1].

[^1]: Temos de agradecer ao Mozilla Common Voices, uma das poucas
  excepções a esta regra, que é um excelente projecto que produziu um
  conjunto de dados de alta qualidade e livremente disponíveis de
  amostras de voz, e o utilizou para desenvolver modelos e programas
  livres para o reconhecimento de texto-para-fala e fala.

A barreira à entrada essencial para projectos de aprendizagem automática
está a ultrapassar estes dois problemas, que são muitas vezes muito
dispendiosos de conseguir. Um conjunto de dados de alta qualidade
e bem marcado requer geralmente milhares de horas de trabalho para
produzir[^2], uma tarefa que pode potencialmente custar milhões de
dólares.  Qualquer abordagem que reduza este número é, portanto, muito
desejável, mesmo que o custo for fazendo compromissos éticos. Com a
Amazon, assume a forma de exploração económica gigantesca. Com GitHub,
assume a forma de desconsideração dos termos das licenças de
<i lang="en">software</i> livre. No processo, construíram uma ferramenta
que facilita o branqueamento em grande escala de <i
lang="en">software</i> livre em <i lang="en">software</i> não-livre
pelos seus clientes, a quem o GitHub oferece uma negação plausível
através de um algoritmo impenetrável.

[^2]: Tipicamente mão-de-obra explorada de países de baixo
  desenvolvimento, que a indústria tecnológica costuma fingir que não
  está perto da escravatura.

O <i lang="en">software</i> livre não é um presente sem reservas.
Existem termos para a sua utilização e reutilização. Mesmo as chamadas
licenças «liberais» de <i lang="en">software</i> impõem requisitos de
reutilização, tais como a atribuição. Para citar a licença MIT:

> A autorização é concedida [...] sob as seguintes condições:

> O anterior aviso de direitos de autor e este aviso de permissão devem
> ser incluídos em todas as cópias ou partes substanciais do Programa.

Ou a igualmente «liberal» licença BSD:

> É permitida a redistribuição e utilização em formas fonte e binárias,
> com ou sem modificação, desde que sejam cumpridas as seguintes
> condições:

> A redistribuição do código-fonte deve manter o aviso de direitos de
> autor acima referido, esta lista de condições e a seguinte declaração
> de exoneração de responsabilidade.

No outro extremo do espectro, licenças <i lang="en">copyleft</i> tais
como a GNU General Public License ou a Mozilla Public License vão mais
longe, exigindo não só a atribuição de obras derivadas, mas que tais
obras derivadas sejam *também lançadas* com a mesma licença. Citando a
GPL:

> Pode transmitir um trabalho baseado no Programa, ou as modificações
> para o produzir a partir do Programa, sob a forma de código-fonte nos
> termos da secção 4, desde que também cumpra todas estas condições:

> […]

> Deve licenciar toda a obra, como um todo, sob esta Licença a qualquer
> pessoa que entre na posse de uma cópia.

E a Mozilla Public License:

> Toda a distribuição do <i lang="en">Software</i> Protegido em Forma de Código Fonte,
> incluindo quaisquer Modificações que Você criar ou para as quais
> contribuir, deve estar sob os termos desta Licença. Deverá informar os
> destinatários que a Forma de Código Fonte do <i lang="en">Software</i> Protegido é
> regida pelos termos desta Licença, e como podem obter uma cópia da
> presente Licença. Não poderá tentar alterar ou restringir os direitos
> dos destinatários no Forma de Código-Fonte.

As licenças de <i lang="en">software</i> livre impõem obrigações ao
utilizador através de termos que regem a atribuição, sublicenciamento,
distribuição, patentes, marcas registadas e relações com leis como a
Digital Millennium Copyright Act. A comunidade do
<i lang="en">software</i> livre não é desconhecedora das dificuldades em
impor o cumprimento destas obrigações, que alguns grupos consideram
demasiado onerosas. Mas por muito onerosas que se possam considerar
estas obrigações, exige-se, no entanto, o seu cumprimento. Se acredita
que a força dos direitos de autor deve proteger o seu
programa proprietário, então tem de concordar que ele
protege igualmente as obras de código aberto, apesar dos inconvenientes
ou custos associados a esta verdade.

O GitHub Copilot é treinado com programas regidos por estes termos, e
não os cumpre, e permite aos clientes não cumprirem acidentalmente
estes termos por si próprios. Alguns argumentam sobre os riscos de uma
«surpresa <i lang="en">copyleft</i>», em que alguém incorpora uma obra
licenciada pela GPL no seu produto e fica surpreendido ao descobrir que
é obrigado a lançar o seu produto também sob os termos da GPL. O
Copilot institucionaliza este risco e qualquer utilizador que deseje
utilizá-lo para desenvolver <i lang="en">software</i> não livre deveria
ser aconselhado a não o fazer, caso contrário poderá ver-se legalmente
obrigado a cumprir estes termos, sendo talvez em última análise obrigado
a publicar as suas obras sob os termos de uma licença que é indesejável
para os seus objetivos.

Essencialmente, a discussão resume-se a se o modelo constitui ou não uma
obra derivada da sua informação de entrada. A Microsoft argumenta que
não o constitui. No entanto, estas licenças não são específicas quanto
aos meios de derivação; a abordagem clássica de copiar e colar de um
projecto para outro não precisa de ser o único meio para que estes
termos se apliquem. O modelo existe como resultado da aplicação de um
algoritmo a estas informações de entrada, e portanto o modelo em si é um
trabalho derivado das suas informações de entrada. O modelo, então
utilizado para criar novos programas, transmite as suas obrigações para
essas obras.

Tudo isto pressupõe a melhor interpretação do argumento da Microsoft,
com uma forte dependência do facto de o modelo se tornar um programador
de propósito geral, tendo aprendido significativamente com a sua
informação de entrada e aplicando este conhecimento para produzir
trabalho original. Se um programador humano adoptar a mesma abordagem,
estudando programas livres e aplicando essas lições, mas não o código em
si, a projectos originais, eu concordaria que o seu conhecimento
aplicado não está a criar obras derivadas. No entanto, não é assim que
funciona a aprendizagem automática. A aprendizagem automática é
essencialmente um motor glorificado de reconhecimento e reprodução de
padrões e não representa uma generalização genuína do processo de
aprendizagem. É talvez capaz de uma quantidade limitada de
originalidade, mas também é capaz de se degradar ao simples caso de
copiar e colar. Aqui está um exemplo de Copilot a reproduzir,
literalmente, uma função que é regida pela GPL, e que seria, portanto,
regida pelos seus termos:

<!-- more -->

<figure>
    <video src="/video/copilot-squareroot.mp4" muted autoplay controls></video>
    <figcaption class="wp-caption-text">Fonte: <a href="https://nitter.snopyta.org/mitsuhiko/status/1410886329924194309">Armin Ronacher</a> via Twitter</figcaption>
</figure>

A licença reproduzida pelo Copilot não é correcta, nem na forma nem na
função. Este código não foi escrito por V. Petkov e a GPL impõe
obrigações muito mais fortes do que as sugeridas pelo comentário. Este
pequeno exemplo foi deliberadamente provocado com uma sugestão de
informação de entrada (esta famosa função é conhecida como a [«raiz quadrada inversa
rápida»](https://es.wikipedia.org/wiki/Algoritmo_de_la_ra%C3%ADz_cuadrada_inversa_r%C3%A1pida))
e o «float Q_», mas não é um exagero assumir que alguém pode
acidentalmente fazer algo semelhante com qualquer descrição em língua
inglesa particularmente azarada do seu objetivo.

Naturalmente, o uso de uma entrada sugestiva para convencer o Copilot a
imprimir o código licenciado sob a GPL sugere outro uso: branquear
deliberadamente o código-fonte livre. Se o argumento da Microsoft for
válido, então de facto a única coisa que é necessária para contornar
legalmente uma licença de <i lang="en">software</i> livre é ensinar um
algoritmo de aprendizagem automática para regurgitar uma função que se
pretende utilizar.

Isto é um problema. Tenho duas sugestões a oferecer a dois públicos: uma
para o GitHub, e outra para programadores de <i lang="en">software</i>
livre que estão preocupados com o Copilot.

Para o GitHub: este é o teu momento Oracle contra Google. Investiste na
construção de uma plataforma sobre a qual foi construída a revolução do
código aberto, e aproveitar esta plataforma para esta ação é uma traição
profunda à confiança da comunidade. A lei aplica-se a ti, e apostar no
fato de a comunidade descentralizada de código aberto não ser capaz de
montar um desafio legal eficaz ao teu cofre de guerra da Microsoft de
7,5 mil milhões de dólares não muda isto. A comunidade de código aberto está espantada, e o
espanto está lenta mas seguramente a ferver em fúria à medida que as
nossas preocupações caem em ouvidos surdos e avanças com o lançamento do
Copilot. Espero que, se a situação não mudar, encontres um grupo
suficientemente motivado para desafiar esta situação. A legitimidade do
ecossistema de <i lang="en">software</i> livre pode depender deste
problema, e há muitas empresas que são financeiramente incentivadas a
assegurar que esta legitimidade se mantenha. Estou decididamente preparado
para participar num processo de ação colectiva como mantenedor, ou ao
lado de outras empresas com interesses no <i lang="en">software</i>
livre, fazendo uso dos nossos recursos financeiros para facilitar um
processo judicial.

A ferramenta pode ser melhorada, provavelmente ainda a tempo de evitar
os efeitos mais prejudiciais (prejudiciais ao teu negócio, ou seja) do
Copilot. Ofereço as seguintes sugestões específicas:

1. Permitir aos utilizadores e repositórios do GitHub que optem por não
   ser incorporados no modelo. Melhor, permite-lhes que optem por
   aderir. Não ligar esta opção a projectos não relacionados, como o
   Software Heritage e a Internet Archive.
2. Acompanhar as licenças de software incorporadas no modelo e informar
   os utilizadores das suas obrigações no que diz respeito a essas
   licenças.
3. Remover totalmente o código <i lang="en">copyleft</i> do modelo, a
   menos que desejes tornar o modelo e o seu código de suporte também
   <i lang="en">software</i> livre.
4. Considerar compensar os proprietários dos direitos de autor dos
   projectos de <i lang="en">software</i> livre incorporados no modelo
   com uma margem das taxas de utilização do Copilot, em troca de uma
   licença que permita essa utilização.

O teu modelo atual provavelmente precisa de ser deitado fora. O código
GPL incorporado no mesmo dá direito a qualquer pessoa que o utilize a
receber uma cópia GPL do modelo para seu próprio uso. Dá a estas pessoas
o direito a uma utilização comercial, para construir um produto
concorrente com ele. Mas, presumivelmente também inclui obras sob
licenças incompatíveis, tais como a CDDL, o que é... problemático. Tudo
isto é uma desarranjo legal.

Não posso falar em nome do resto da comunidade que foi prejudicada por
este projecto, mas pela minha parte, não me importaria de não procurar
as respostas a nenhuma destas questões em tribunal se concordasse em
resolver estes problemas agora.

E, o meu conselho aos detentores de <i lang="en">software</i> livre que
estão chateados por as suas licenças estarem a ser ignoradas. Primeiro,
não usem GitHub e o vosso código não chegará ao modelo (por enquanto). [Já
escrevi anteriormente](/es-importante-que-el-software-libre-use-infraestructuras-de-software-libre/) sobre o porquê de ser geralmente importante que os
projectos de <i lang="en">software</i> livre utilizem infra-estruturas de <i lang="en">software</i> livre,
e isto só reforça esse facto. Além disso, a velha abordagem «vota com a
tua carteira» é uma boa forma de mostrar o vosso desagrado. Dito isto, se
vos ocorrer que *não* pagam realmente por GitHub, então talvez queiram
tirar um momento para considerar se os incentivos criados por essa
relação explicam este desenvolvimento e podem levar a resultados mais
desfavoráveis para vocês no futuro.

Poderão também sentir-se tentados a resolver este problema alterando as
suas licenças de <i lang="en">software</i> para proibir este comportamento. Direi desde
já que, de acordo com a interpretação da Microsoft da situação
(invocando a utilização justa), não lhes interessa qual a licença que
utilizam: utilizarão o vosso código independentemente. De fato,
descobriu-se que algum [código proprietário](https://nitter.snopyta.org/ChrisGr93091552/status/1539731632931803137) foi incorporado no modelo. No
entanto, continuo a apoiar os vossos esforços para abordar esta questão
nas vossas licenças de software, uma vez que proporciona uma base legal
ainda mais forte sobre a qual podemos rejeitar o Copilot.

Advirto-vos que a forma como abordam essa cláusula da vossa licença é
importante. Sempre que escrever ou alterar uma licença de
<i lang="en">software</i> livre e de código aberto, deveram considerar
se esta ainda se qualificará ou não como livre ou de código aberto após
as suas alterações. Para ser específico, uma cláusula que proíbe
totalmente a utilização do vosso código para formação de um modelo de
aprendizagem automática tornará *não livre* o vosso <i lang="en">software</i>, e não
recomendo esta abordagem. Em vez disso, gostaria de actualizar as vossas
licenças para esclarecer que a incorporação do código num modelo de
aprendizagem automática é considerada uma forma de trabalho derivado, e
que os seus termos de licença se aplicam ao modelo e a quaisquer
trabalhos produzidos com esse modelo.

Para resumir, penso que o GitHub Copilot é uma má ideia tal como foi
concebido. Representa um flagrante desrespeito do licenciamento do <i lang="en">software</i> livre em
si mesmo, e permite um desrespeito semelhante &mdash; deliberado ou não
&mdash;
entre os seus utilizadores. Espero que eles tenham em conta as minhas
sugestões e espero que as minhas palavras à comunidade do <i lang="en">software</i>
livre ofereçam algumas formas concretas de avançar com este problema.
