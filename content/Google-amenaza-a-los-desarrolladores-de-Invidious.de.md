Author: Jorge Maldonado Ventura
Category: Nachrichten
Date: 2023-06-10 13:24
Lang: de
Slug: google-amenaza-a-los-desarrolladores-de-invidious
Tags: Google, Invidious, Privatsphäre, freie Software, YouTube
Save_as: YouTube-droht-Invidious-Entwickler/index.html
URL: YouTube-droht-Invidious-Entwickler/
Title: YouTube droht Invidious-Entwickler
Image: <img src="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png" alt="" width="1200" height="740" srcset="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png 1200w, /wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious-600x370.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Die Entwickler von Invidious [haben eine Email von YouTube
erhalten](https://github.com/iv-org/invidious/issues/3872), in der sie
aufgefordert werden, das Programm nicht mehr anzubieten. Laut der
Rechtsabteilung von YouTube verstoßen sie gegen die
[API](https://de.wikipedia.org/wiki/Programmierschnittstelle)-Nutzungsbedingungen
von YouTube, was unmöglich ist, da Invidious die API von YouTube nicht
nutzt.

Sie behaupten auch, dass Invidious „auf
[invidious.io](https://invidious.io/) angeboten wird“, was ebenfalls
nicht stimmt, da diese Webseite keine Version von Invidious hostet.
Zurzeit gibt es 40 Webseiten, die öffentliche Versionen von Invidious
hosten, über die das Invidious-Team keine Kontrolle hat, weil Invidious
die freie [AGPL](https://www.gnu.org/licenses/agpl-3.0.html)-Lizenz
verwendet. Selbst wenn Invidious in den Vereinigten Staaten illegal
wäre, wird es im [Tor-Netzwerk](https://de.wikipedia.org/wiki/Tor_(Netzwerk)), dem [I2P-Netzwerk](https://geti2p.net/de/) und in vielen Ländern
gehostet, was es praktisch unmöglich macht, Invidious verschwinden zu
lassen. Außerdem ist sein Code auf mehreren Entwicklungsplattformen und
auf vielen Computern zu finden.

Invidious hat weder den Nutzungsbedingungen von YouTube-API noch den
Nutzungsbedingungen von YouTube zugestimmt. YouTube erlaubt den Zugriff
auf Inhalte, die auf seinen Servern gehostet werden, über das
HTTP-Protokoll, so dass Invidious kein Computerverbrechen begeht; es
schützt einfach das Recht auf Privatsphäre und Freiheit.

Google (das Unternehmen, das YouTube kontrolliert) hingegen [verletzt
die Privatsphäre](https://stallman.org/google.html#surveillance), [zensiert](https://stallman.org/google.html#censorship), [verlangt die Verwendung proprietärer
Software](https://stallman.org/google.html#nonfree-software), [beutet seine Nutzer aus](/de/wie-beutet-Google-mit-CAPTCHAs-aus/), [entwickelt KI-Software für militärische Zwecke](/de/du-hilfst-zu-töten-indem-du-ein-Google-reCAPTCHA-ausfüllst/) und [hat enorme ökologische
Auswirkungen](https://time.com/5814276/google-data-centers-water/), um
nur ein paar Beispiele zu nennen. Aus diesen Gründen gibt es Leute, die
meinen, [Google muss zerstört werden](/de/wie-zerstört-man-Google/).

Glücklicherweise (selbst wenn Invidious verschwinden sollte) gibt es
andere freie Projekte wie
[Piped](/de/YouTube-mit-Privatsphäre-mit-Piped/),
[NewPipe](https://newpipe.net/) und
[`youtube-dl`](https://youtube-dl.org/). Wird Google auch die Entwickler
dieser Projekte und ihre Millionen von Nutzern bedrohen?
