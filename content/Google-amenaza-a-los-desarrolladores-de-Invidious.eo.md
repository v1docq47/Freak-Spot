Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2023-06-10 09:00
Lang: eo
Slug: google-amenaza-a-los-desarrolladores-de-invidious
Tags: Google, Invidious, privateco, liberaj programoj, YouTube
Save_as: YouTube-minacas-la-disvolvistojn-de-Invidious/index.html
URL: YouTube-minacas-la-disvolvistojn-de-Invidious/
Title: YouTube minacas la disvolvistojn de Invidious
Image: <img src="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png" alt="" width="1200" height="740" srcset="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png 1200w, /wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious-600x370.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

La disvolvistoj de Invidious [ricevis retmesaĝon el
YouTube](https://github.com/iv-org/invidious/issues/3872), kiu petas, ke
ili ne plu havigu la programon. Laŭ la jura teamo de YouTube, ili
estus malobservantaj la uzkondiĉoj de la [API](https://eo.wikipedia.org/wiki/Aplikprograma_interfaco) de YouTube, kio ne eblas, ĉar Invidious ne uzas la API de YouTube.

Ili ankaŭ diras, ke Invidious «estas havigata en
[invidious.io](https://invidious.io/)», kio ankaŭ ne veras, ĉar tiu
retejo ne gastigas neniun version de Invidious. Nuntempe estas 40 retejoj,
kiuj gastigas publikajn versiojn de Invidious, sur kiuj la
Invidious-skipo neniun regpovon havas, ĉar Invidious uzas la liberan
permesilon [AGPL](https://www.gnu.org/licenses/agpl-3.0.html). Eĉ se
Invidious estu kontraŭleĝa en Usono, estas versioj gastigitaj en la
[Tor-reto](https://eo.wikipedia.org/wiki/Tor_(programaro)), en la
[I2P-reto](https://geti2p.net) kaj en multaj landoj, kio igas, ke
efektive ne eblu malaperigi Invidious. Krome, ĝia kodo troviĝas en kelkaj
platformoj por disvolvistoj kaj en multege da komputiloj.

Invidious akceptis nek la kondiĉojn de uzado de la YouTube-API nek tiujn
de YouTube. YouTube ebligas la aliron al la enhavo gastigita en siaj
serviloj per la HTTP-protokolo, do Invidious neniel ciferece krimis; ĝi
simple defendas la rajton al la privateco kaj libereco.

Google (la firmao, kiu regas YouTube-n), aliflanke, [ne respektas la
privatecon](https://stallman.org/google.html#surveillance),
[cenzuras](https://stallman.org/google.html#censorship), [postulas uzi
proprietajn programojn](https://stallman.org/google.html#nonfree-software),
[ekspluatas la uzantojn](/eo/kiel-Google-ekspluatas-per-CAPTCHA-j/) kaj
[ege damaĝas la
naturon](https://time.com/5814276/google-data-centers-water/) (por citi
nur kelkajn ekzemplojn). Pro tio estas homoj, kiuj opinias, ke [Google
devas esti detruita](/eo/kiel-detrui-Google/).

Bonŝance, eĉ se Invidious malaperas, ekzistas aliaj liberaj projektoj
kiel [Piped](/eo/YouTube-privatece-per-Piped/),
[NewPipe](https://newpipe.net/) kaj
[`youtube-dl`](https://youtube-dl.org/). Ĉu Google ankaŭ minacos la
disvolvistojn de tiuj projektoj kaj iliajn milionojn da uzantoj?
