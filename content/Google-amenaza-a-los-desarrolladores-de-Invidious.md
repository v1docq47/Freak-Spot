Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2023-06-10 00:59
Lang: es
Slug: google-amenaza-a-los-desarrolladores-de-invidious
Tags: Google, Invidious, privacidad, software libre, YouTube
Title: YouTube amenaza a los desarrolladores de Invidious
Image: <img src="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png" alt="" width="1200" height="740" srcset="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png 1200w, /wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious-600x370.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Los desarrolladores de Invidious [han recibido un correo de
YouTube](https://github.com/iv-org/invidious/issues/3872) pidiéndoles
que dejen de ofrecer el programa. Según el equipo legal de YouTube,
estarían infringiendo los términos de uso de la
[API](https://es.wikipedia.org/wiki/API) de YouTube, lo cual es
imposible, pues Invidious no usa la API de YouTube.

Dicen, además, que Invidious «está siendo ofrecido en
[invidious.io](https://invidious.io/)», lo cual tampoco es cierto,
porque ese sitio web no aloja ninguna versión de Invidious. A día de hoy hay
40 sitios web que alojan versiones de Invidious públicas sobre los que el equipo de Invidious no
tiene ningún control, ya que Invidious usa la licencia libre
[AGPL](https://www.gnu.org/licenses/agpl-3.0.html). Incluso aunque
Invidious fuera ilegal en Estados Unidos, está alojado en la [red
Tor](https://es.wikipedia.org/wiki/Tor_(red_de_anonimato)), en la [red
I2P](https://geti2p.net/es/) y en multitud de países, lo que hace que
sea prácticamente imposible hacer desaparecer Invidious. Asimismo, su
código se encuentra en varias plataformas de desarrollo y en un montón
de ordenadores.

Invidious no ha aceptado ni los términos de servicio de la API de
YouTube ni los de YouTube. YouTube permite el acceso al contenido
alojado en sus servidores mediante el protocolo HTTP, así que Invidious
no está realizando ningún crimen informático; simplemente está
salvaguardando el derecho a la privacidad y a la libertad.

Google (la empresa que controla YouTube), por otro lado, [no
respeta la privacidad](https://stallman.org/google.html#surveillance),
[censura](https://stallman.org/google.html#censorship), [requiere el uso
de programas
privativos](https://stallman.org/google.html#nonfree-software), [explota
a sus usuarios](/como-explota-Google-con-CAPTCHAs/), [desarrolla
programas de inteligencia artificial con fines
militares](/al-completar-un-recaptcha-de-google-ayudas-a-matar/),
[tiene un enorme impacto
ecológico](https://time.com/5814276/google-data-centers-water/), por citar
solo algunos ejemplos. Por estos motivos hay quienes piensan que [Google
debe ser destruido](/C%C3%B3mo-destruir-Google/).

Afortunadamente, incluso si Invidious desapareciera, existen otros
proyectos libres como [Piped](/youtube-con-privacidad-con-piped/),
[NewPipe](https://newpipe.net/) y [`youtube-dl`](https://youtube-dl.org/).
¿Amenazará Google también a los desarrolladores de esos proyectos y a
sus millones de usuarios?
