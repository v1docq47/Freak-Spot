Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2024-01-10 11:00
Lang: es
Slug: internet-tiene-inquilinos-y-propietarios-¿qué-eres-tú
Tags: Fediverso, Internet, Lemmy, Mastodon, PeerTube, Tor
Title: Internet tiene inquilinos y propietarios, ¿qué eres tú?
Image: <img src="/wp-content/uploads/2024/01/propiedad-virtual.png" alt="" width="1169" height="945" srcset="/wp-content/uploads/2024/01/propiedad-virtual.png 1169w, /wp-content/uploads/2024/01/propiedad-virtual-584x472.png 584w" sizes="(max-width: 1169px) 100vw, 1169px">

Muchos se quejan de que son censurados en Internet, de que no
pueden hablar abiertamente sobre ciertos temas por miedo a perder
alcance e ingresos, etc. Dependen de plataformas sobre las que no tienen control,
son como *inquilinos* a los que pueden echar en cualquier momento. Cuando
publicas en X, Instagram, TikTok, YouTube..., estás usando la plataforma (la
*vivienda*) de otra persona. En el momento en que dejes de ser
rentable y le caigas mal a los anunciantes o a los propietarios de la
empresa, te censuran, te echan... Y no puedes hacer nada para evitarlo.
¿O puedes?

La forma más barata de protegerte contra la arbitrariedad de una
plataforma es usar varias. Si una plataforma te falla, tienes todavía otras.
Lo mejor, en mi opinión, es usar una plataforma [libre](https://freakspot.net/escribe-programas-libres/aprende/) que forme parte de una red descentralizada.
Si no te convence ninguna plataforma, puedes montarte una. ¿Eso es caro?
No. Gracias al protocolo [ActivityPub](https://es.wikipedia.org/wiki/ActivityPub),
es muy barato crear un nodo dentro del [Fediverso](https://es.wikipedia.org/wiki/Fediverso). El Fediverso es
una gran red descentralizada con la que podrás llegar a millones de usuarios.
Puedes elegir [PeerTube](https://joinpeertube.org/es) para
subir vídeos, [Mastodon](https://joinmastodon.org/es) para crear y
compartir publicaciones breves, [Lemmy](https://join-lemmy.org/?lang=es)
para discutir con otras personas...

También puedes comprar un dominio por muy poco dinero y montarte un sitio web en
un servidor que alquiles o que te montes en casa. <!-- more -->Para
mayor protección contra la censura puedes [crear también un servicio
oculto de Tor](/crear-servicio-oculto-para-sitio-web-en-nginx/)[^1].

Asimismo, puedes crear una lista de correo o una lista de difusión en
algún programa de mensajería. Con este método, tus publicaciones llegan
directamente a tus destinatarios, sin depender de algoritmos arbitrarios
que puedan limitar tu alcance. De nuevo, si el servidor desde donde
mandas los correos te pertenece, tendrás más control, serás el soberano.

En resumen, el truco para evitar la censura y la arbitrariedad de los
grandes propietarios es tener una plataforma o canal propio. Si no es
posible, lo mejor es tener varias cuentas repartidas en diferentes
plataformas. De esta forma, si te censuran en una, sigues teniendo las
otras.

[^1]: Como complemento al artículo enlazado, recomiendo también el artículo [«¿Cómo
    crear un página web en la red Tor?»](/como-crear-un-pagina-web-en-la-tor/).
