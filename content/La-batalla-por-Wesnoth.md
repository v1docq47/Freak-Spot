Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2017-05-14 02:16
Image: <img src="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-combate.png" alt="Combate en la batalla de Wesnoth" width="1920" height="1080" srcset="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-combate.png 1920w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-combate-960x540.png 960w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-combate-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
Lang: es
Slug: la-batalla-por-wesnoth
Tags: 2D, estrategia, fantasía, GNU/Linux, Mac OS, Wesnoth, Windows
Title: <cite>La batalla por Wesnoth</cite>

[<cite>La batalla por Wesnoth</cite>](https://www.wesnoth.org/) es un videojuego de
estrategia por turnos con temática de fantasía. Se encuentra disponible
para GNU/Linux, Mac OS y Windows.

<a href="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-principal.png">
<img src="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-principal.png" alt="Menú principal de La Batalla por Wesnoth" width="1920" height="1080" srcset="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-principal.png 1920w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-principal-960x540.png 960w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-principal-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

Se trata de un juego muy completo y cuidado en detalles. Deberemos
planificar cuidadosamente la estrategia para llevar a nuestras tropas a
la victoria. Varios factores influyen en la batalla: el terreno, el
momento del día, el tipo de ataque, habilidades, etc.

<!-- more -->

<a href="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-diálogo.png">
<img src="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-diálogo.png" alt="Diálogo en una campaña de La batalla por Wesnoth" width="1920" height="1080" srcset="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-diálogo.png 1920w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-diálogo-960x540.png 960w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-diálogo-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

Para aprender los conceptos básicos del juego, podemos jugar el tutorial
o leer el [manual del
juego](https://www.wesnoth.org/manual/stable/manual.es.html). En mi
opinión, lo mejor es completar el tutorial y un par de campañas. Aprender
a jugar es bastante fácil. Con la práctica uno va mejorando las
estrategias y conociendo más sobre la historia del Gran Continente,
el continente del mundo fantástico donde se encuentra Wesnoth.

<a href="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-de-campañas.png">
<img src="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-de-campañas.png" alt="Selección de campañas en La batalla por Wesnoth" width="1920" height="1080" srcset="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-de-campañas.png 1920w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-de-campañas-960x540.png 960w, /wp-content/uploads/2017/05/La-batalla-por-Wesnoth-menú-de-campañas-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

<cite>La batalla por Wesnoth</cite> tiene bastantes campañas oficiales. También
existen otras campañas y mapas que se pueden descargar. Se puede jugar
durante mucho tiempo completando campañas, puesto que hay un montón.
Pero mucha gente prefiere jugar con otras personas en línea. <cite>La batalla
por Wesnoth</cite> cuenta con un modo multijugador, en el que podemos hablar en
tiempo real con nuestros aliados y con nuestros oponentes.

Los aspectos que más me han gustado del juego son la banda sonora y los
gráficos: la interfaz durante la batalla es sencilla y completa a la
vez; la banda sonora es muy variada, y no cansa escucharla durante mucho
tiempo.

<a href="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-preparando-el-ataque.png">
<img src="/wp-content/uploads/2017/05/La-batalla-por-Wesnoth-preparando-el-ataque.png" alt="Preparando un ataque en La batalla por Wesnoth">
</a>

Cuantas más campañas juguemos, más descubriremos sobre el mundo de
fantasía de <cite>La batalla por Wesnoth</cite>. Nos podremos poner en la piel de
troles, elfos, seres humanos, enanos, orcos, dracos, saurios, sirénidos,
nagas y muchas otras razas. Con ellos deberemos forjar estrategias
explotando las debilidades de nuestros adversarios y aprovechando al
máximo nuestras fortalezas.
