Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2017-12-09 13:12
Image: <img src="/wp-content/uploads/2017/12/montón-de-dólares.jpeg" alt="Un montón de dólares." width="1200" height="800" srcset="/wp-content/uploads/2017/12/montón-de-dólares.jpeg 1200w, https://wp-content/uploads/2017/12/montón-de-dólares-600x400.jpeg 600w" sizes="(max-width: 1200px) 100vw, 1200px">
Lang: es
Modified: 2017-12-14 01:51
Slug: liberapay-ya-acepta-dolares-americanos
Tags: código abierto, divisas, donaciones, Estados Unidos de América, funcionalidades
Title: Liberapay ya acepta dólares americanos

*Este artículo es una traducción de [Liberapay now supports US dollars](https://medium.com/liberapay-blog/liberapay-now-supports-us-dollars-f9484e3008f8)*.

La internacionalización ha sido una parte importante del proyecto de
Liberapay desde el principio, y hoy nos complace anunciar un importante
paso hacia delante en este área: la transición hacia una plataforma
multidivisa, con el dólar americano uniéndose al euro en nuestros libros
de cuentas.

<!-- more -->

No habíamos planeado trabajar en esto en 2017, pero cuando oímos que
Gratipay se iba a [cerrar](https://gratipay.news/the-end-cbfba8f50981?gi=3d4bb3f52b92)
a final del año decidimos dar un paso hacia delante. Tras tres semanas
de intenso trabajo hemos conseguido implementar el soporte básico para
el dólar americano, ¡así como una herramienta para ayudar a usuarios de
Gratipay a migrar sus cuentas a Liberapay!

Modificar una plataforma como la nuestra para manejar múltiples monedas
no es una tarea pequeña, y si te gustan las estadísticas de Git, estarás
interesado en saber que este trabajo inicial ha constado de
[38 commits, 97 archivos cambiados, 2261 inserciones y 1161
eliminaciones](https://github.com/liberapay/liberapay.com/compare/227...0b5ad1123ca5ef4834bfd446af902fbdabaf2caf).
Es uno de los cambios más grandes al código fuente de Liberapay desde
que el servicio se lanzó en febrero de 2016.

<a href="/wp-content/uploads/2017/12/montón-de-dólares.jpeg">
<img src="/wp-content/uploads/2017/12/montón-de-dólares.jpeg" alt="Un montón de dólares." width="1200" height="800" srcset="/wp-content/uploads/2017/12/montón-de-dólares.jpeg 1200w, https://wp-content/uploads/2017/12/montón-de-dólares-600x400.jpeg 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

## ¿Cómo funciona?

Cada cuenta de Liberapay tiene ahora una moneda «principal», así como un
interruptor que determina si son aceptadas o rechazadas donaciones en
otras monedas (lo último es lo predeterminado).

Aceptar monedas extranjeras puede complicarte las cosas, pero
rechazarlas puede desalentar a algunos donantes; es tu elección. Por
supuesto, un gran factor a considerar es si tus mecenas están esparcidos
por todo el mundo o concentrados en la misma zona monetaria que tú.

<figure>
    <a href="/wp-content/uploads/2017/12/selector-de-moneda-de-Liberapay.png">
    <img src="/wp-content/uploads/2017/12/selector-de-moneda-de-Liberapay.png" alt="" width="733" height="210" srcset="/wp-content/uploads/2017/12/selector-de-moneda-de-Liberapay.png 733w, /wp-content/uploads/2017/12/selector-de-moneda-de-Liberapay-366x105.png 366w" sizes="(max-width: 733px) 100vw, 733px">
    </a>
    <figcaption class="wp-caption-text">Captura de pantalla del
    formulario de inscripción con el nuevo selector de moneda</figcaption>
</figure>

## Limitaciones

El soporte de múltiples monedas es todavía un trabajo en proceso:

- Todavía no soportamos la conversión de divisas, así que no puedes
  convertir tus ingresos antes de retirarlos a tu cuenta bancaria.
  Sabemos por experiencia que enviar euros a una cuenta de Estados
  Unidos no siempre funciona, por ejemplo. Para aliviar estos problemas
  estamos pensando en implementar conversiones de divisas automáticas sin comisiones dentro de Liberapay.
- [Los equipos](https://liberapay.com/explore/teams) están limitados a
  una sola moneda, y no es posible cambiarla si el equipo ya está
  recibiendo donaciones. Estas son limitaciones técnicas temporales.
- Nuestro procesador de pagos no soporta aún la gama completa de métodos
  de pago utilizados en los Estados Unidos, por ejemplo, faltan todavía
  las tarjetas American Express y ACH (pagos domiciliados de Estados
  Unidos). Esto debería mejorar con el tiempo, pero puede llevar algún
  tiempo.

## ¿Y qué hay de todas las demás monedas?

Nuestro socio de pagos admite actualmente otras 9 monedas que podrían
ser añadidas a Liberapay: la libra británica (GBP), el dólar canadiense
(CAD), la corona sueca (SEK), la corona noruega (NOK), la corona danesa
(DKK), el franco suizo (CHF), el rand sudafricano (ZAR), el esloti
polaco (PLN) y el dólar australiano. Por favor, contacta con nosotros si
estás interesado en recibir donaciones en una de estas monedas.

No aceptamos criptomonedas, y no pensamos cambiar eso en un futuro
próximo.

## ¿Necesito hacer algo?

Si prefieres euros y no quieres aceptar dólares, entonces no tienes que
hacer nada. Si prefieres dólares, o si te gustaría permitir que tus
mecenas te envíen dólares aunque prefieras euros, entonces deberías
[modificar los ajustes de tu perfil](https://liberapay.com/about/me/edit).

Si solo usas Liberapay para donar dinero, no para recibirlo, no hay
mucho que puedas hacer en este momento. Por favor, se paciente mientras
los creadores deciden cómo quieren manejar las monedas extranjeras, y
mientras trabajamos en pulir el lado del mecenas de esto.

Por último, deberías [registrarte](https://liberapay.com/sign-up) si aún
no tienes una cuenta, ¡por supuesto!
