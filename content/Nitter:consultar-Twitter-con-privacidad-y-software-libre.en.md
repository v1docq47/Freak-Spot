Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2019-11-30 18:13
Lang: en
Modified: 2020-10-08 11:20
Slug: consultar-twitter-con-privacidad-y-software-libre
Save_as: Check-Twitter-with-free-software-and-privacy-with-Nitter/index.html
Tags: free software, privacy, program, social networks, Twitter, website
Title: Check Twitter with free software and privacy, with Nitter
URL: Check-Twitter-with-free-software-and-privacy-with-Nitter/
Image: <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">

Twitter is a centralized social network that requires the use of
proprietary software. It's almost impossible to use Twitter without
giving up your privacy or freedom... unless we use another front-end,
such as [Nitter](https://nitter.net/), which I describe in this article.

I think its name is an acronym from
<em><strong>n</strong>ot tw<strong>itter</strong></em>.
But who cares? The thing is that it works well and also the interface is
lightweight, it prevents Twitter from getting your IP, you can customize
its look, it has native RSS feeds and it's responsive.

It's now in its infancy, so they are expected to include more features,
such a login system to admin accounts you follow from its web.

<!-- more -->

<figure>
    <a href="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png">
    <img src="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png" alt="Searching 'free software' with Nitter" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-cuentas-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">We can search accounts</figcaption>
</figure>

<figure>
  <a href="/wp-content/uploads/2019/11/Nitter-cuenta.png">
  <img src="/wp-content/uploads/2019/11/Nitter-cuenta.png" alt="Twitter account seen with Nitter" width="1438" height="840" srcset="/wp-content/uploads/2019/11/Nitter-cuenta.png 1438w, /wp-content/uploads/2019/11/Nitter-cuenta-719x420.png 719w" sizes="(max-width: 1438px) 100vw, 1438px">
  </a>
  <figcaption class="wp-caption-text">We can check accounts</figcaption>
</figure>

<figure>
   <a href="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png">
     <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="Here we exclude the retweets from the results" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">And we can filter tweets</figcaption>
</figure>

Because it is free software, you can install Nitter on your server (if
you have one) or use [someone else's
Nitter](https://github.com/zedeus/nitter/wiki/Instances).

Does it feel inconvenient to change the Twitter URL with Nitter ones?
The Firefox plugin [Privacy
Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/?src=search)
replaces Twitter links with links to Nitter instances. It also
replaces Instagram links with [Bibliogram](/en/check-Instagram-with-privacy-and-free-software/)
links and YouTube links with [Indivious](/en/youtube-con-privacidad-con-invidious/) links.
