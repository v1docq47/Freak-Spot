Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2020-11-28
Lang: eo
Slug: consultar-twitter-con-privacidad-y-software-libre
Save_as: konsulti-Tviteron-per-libera-programaro-kaj-privatece/index.html
URL: konsulti-Tviteron-per-libera-programaro-kaj-privatece/
Tags: libera programaro, privateco, programo, sociaj retoj, Tvitero, retejo
Title: Konsulti Tviteron per libera programaro kaj privatece
Image: <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">

Tvitero estas centra socia reto, kiu bezonas la uzon de proprieta
programaro. Estas preskaŭ maleble uzi Tvitero sen forlasi vian
privatecon aŭ liberecon... krom se ni uzas alian fasadon, kiel
[Nitter](https://nitter.net/)-on, pri kiu mi skribas en ĉi tiu artikolo.

Mi pensas, ke ĝia nomo estas akronimo de <em><strong>n</strong>ot
tw<strong>itter</strong></em> (Esperante «ne Tvitero»). Sed al kiu
gravas tion? Ĝi bone funkcias, ĝia interfaco estas malpeza, vi povas
agordi ĝian nodon, ĝi havas proprajn RSS-fluojn, estas sinadapta,
evitas, ke Tvitero konu vian IP-adreson: tio gravas al mi.

<!-- more -->

<figure>
    <a href="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png">
    <img src="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png" alt="Searching 'free software' with Nitter" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-cuentas-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">Ni povas serĉi kontojn</figcaption>
</figure>

<figure>
  <a href="/wp-content/uploads/2019/11/Nitter-cuenta.png">
  <img src="/wp-content/uploads/2019/11/Nitter-cuenta.png" alt="Twitter account seen with Nitter" width="1438" height="840" srcset="/wp-content/uploads/2019/11/Nitter-cuenta.png 1438w, /wp-content/uploads/2019/11/Nitter-cuenta-719x420.png 719w" sizes="(max-width: 1438px) 100vw, 1438px">
  </a>
  <figcaption class="wp-caption-text">Ni povas konsulti kontojn</figcaption>
</figure>

<figure>
   <a href="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png">
     <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="Here we exclude the retweets from the results" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">Kaj ni povas filtri pepaĵojn</figcaption>
</figure>

Ĉar ĝi estas libera programaro, vi povas instali je Nitter en via
servilo (se vi tion havas) aŭ uzi je la [Nitter de
aliulo](https://github.com/zedeus/nitter/wiki/Instances).

Ĉu estas malkomforte anstataŭigi la Tviterajn URL-ojn per Nitter-aj?  La
kromaĵo de Firefox [Privacy
Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/?src=search)
anstataŭigas Tviterajn ligilojn per ligiloj al nodoj de Nitter. Ĝi
ankaŭ anstataŭigas ligilojn de Instagram per
[Bibliogram](/eo/Instagram-per-libera-programaro-kaj-privatece/) kaj
ligiloj de YouTube per
[Indivious](/eo/YouTube-privatece-per-Invidious/).
