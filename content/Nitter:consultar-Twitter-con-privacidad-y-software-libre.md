Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2019-11-30 18:13
Modified: 2020-05-02 07:00
Lang: es
Slug: consultar-twitter-con-privacidad-y-software-libre
Tags: privacidad, página web, programa, redes sociales, software libre, Twitter
Title: Consultar Twitter con privacidad y <i lang="en">software</i> libre, con Nitter
Image: <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">

Twitter es una red social centralizada y que requiere el uso de <i lang="en">software</i>
privativo. Consultar Twitter con el navegador de forma decente sin
perder privacidad o libertad es prácticamente imposible... a no ser que
usemos otra interfaz, como [Nitter](https://nitter.net/), la que
describo en este artículo.

Creo que su nombre es un acrónimo de
<em><strong>n</strong>ot tw<strong>itter</strong></em> («no
Twitter»). Bueno, ¿qué más da? El caso es que funciona bien y además la
interfaz es ligera, evita que Twitter obtenga tu dirección IP, puedes
personalizar su apariencia, tiene fuentes RSS propias y es responsiva.

Está aún en su infancia, así que se espera que incluyan más
funcionalidades, como un sistema de inicio de sesión para administrar
las cuentas a las que sigues desde la web y traducciones a otros
idiomas.

<!-- more -->
<figure>
    <a href="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png">
    <img src="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png" alt="Buscando «software libre» con Nitter" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-cuentas-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">Podemos buscar cuentas</figcaption>
</figure>

<figure>
  <a href="/wp-content/uploads/2019/11/Nitter-cuenta.png">
  <img src="/wp-content/uploads/2019/11/Nitter-cuenta.png" alt="Cuenta de Twitter vista con Nitter" width="1438" height="840" srcset="/wp-content/uploads/2019/11/Nitter-cuenta.png 1438w, /wp-content/uploads/2019/11/Nitter-cuenta-719x420.png 719w" sizes="(max-width: 1438px) 100vw, 1438px">
  </a>
  <figcaption class="wp-caption-text">Podemos consultar cuentas</figcaption>
</figure>

<figure>
   <a href="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png">
     <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="Filtrando los retuits de una búsqueda con Nitter" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">Y podemos filtrar tuits</figcaption>
</figure>

Como es software libre, puedes instalar Nitter en tu servidor (si tienes
uno) o usar el
[Nitter de otra gente](https://github.com/zedeus/nitter/wiki/Instances).

¿Te resulta incómodo cambiar las URLs de Twitter a las de Nitter? El
complemento para Firefox [Privacy
Redirect](https://addons.mozilla.org/es/firefox/addon/privacy-redirect/?src=search)
convierte los enlaces a Twitter a enlaces a instancias de Nitter.
También reemplaza los enlaces de Instagram por los de
[Bibliogram](/consultar-instagram-con-software-libre-y-privacidad/)
y los de YouTube por los de
[Invidious](/youtube-con-privacidad-con-invidious/).
