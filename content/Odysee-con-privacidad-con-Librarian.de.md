Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2022-06-19 13:03
Modified: 2023-11-30 19:00
Lang: de
Slug: odysee-con-privacidad-con-librarian
Save_as: Odysee-mit-Privatsphäre-mit-librarian/index.html
URL: Odysee-mit-Privatsphäre-mit-librarian/
Tags: JavaScript, Odysee, Privatsphäre, YouTube
Title: Odysee mit Privatsphäre mit librarian
Image: <img src="/wp-content/uploads/2022/06/librarian.svg" alt="">

Odysee ist eine Video-Website, die ein freies Protokoll namens LBRY
verwendet, das eine Blockchain nutzt, um Dateien zu teilen und Schöpfer
mit ihrer eigenen Kryptowährung zu entlohnen. Mit LBRY ist es nicht
möglich, ein Video zu zensieren, wie es bei vielen aktuellen Plattformen
der Fall ist.

Das Problem ist, dass die Odysee-Plattform nicht sehr
datenschutzfreundlich ist und JavaScript erfordert &ndash; obwohl [ihr Code
frei ist](https://github.com/OdyseeTeam/odysee-frontend). Als Alternative gibt es [librarian](https://codeberg.org/librarian/librarian). <https://lbry.projectsegfau.lt/> ist eine empfohlene Instanz, aber [es gibt auch andere](https://codeberg.org/librarian/librarian#instances).
