Author: Jorge Maldonado Ventura
Category: Nachrichten
Date: 2023-06-18 11:26
Lang: de
Slug: reddit-pierde-usuarios-y-dinero-debido-a-la-protesta-de-sus-usuarios
Save_as: Reddit-verliert-Nutzer-und-Geld-Unzufriedenheit-ignoriert/index.html
URL: Reddit-verliert-Nutzer-und-Geld-Unzufriedenheit-ignoriert/
Tags: Reddit, Lemmy
Title: Reddit verliert Nutzer und Geld, weil es die Unzufriedenheit der eigenen Nutzer ignoriert
Image: <img src="/wp-content/uploads/2023/06/reddit-en-llamas.jpg" alt="" width="763" height="639" srcset="/wp-content/uploads/2023/06/reddit-en-llamas.jpg 763w, /wp-content/uploads/2023/06/reddit-en-llamas-381x319.jpg 381w" sizes="(max-width: 763px) 100vw, 763px">

Reddit zahlt den Preis dafür, dass es den [Protest seiner Nutzer gegen
die jüngsten Änderungen des Unternehmens](/de/Schluss-mit-Reddit/) ignoriert hat.

Einerseits sind viele Menschen zu Plattformen wie
[Lemmy](https://join-lemmy.org/) gewechselt, das seine Nutzerzahlen in
kurzer Zeit verdreifacht hat:

<a href="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png">
<img src="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png" alt="" width="1130" height="320" srcset="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png 1130w, /wp-content/uploads/2023/06/usuarios-totales-Lemmy-565x160.png 565w" sizes="(max-width: 1130px) 100vw, 1130px">
</a>

Andererseits wurde die Reddit-Website aufgrund von Protesten
heruntergefahren, woraufhin mehr als 7000 <!-- more -->Foren privat
wurden[^1] und vier Millionen Menschen die Nutzung der Website
einstellten[^2]. In einem Versuch, den Schaden zu minimieren, entfernte
Reddit mehrere protestierende Moderatoren aus den Moderationsteams und
ersetzte sie durch willfährige Moderatoren[^3].

Es gibt jedoch immer noch Foren, die geschlossen sind. Außerdem gibt es
Nutzer, die irrelevante Inhalte auf die Plattform hochladen, um den Wert
des Unternehmens zu mindern und den Protest fortzusetzen. Ein Beispiel
dafür ist das [r/pic-Forum](https://safereddit.com/r/pics), das jetzt
ein „Ort für Bilder von John Oliver, die sexy aussehen“ ist. Darüber
hinaus gibt es Menschen, die die Plattform mit Lärmvideos füllen[^4].

<a href="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png">
<img src="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png" alt="">
</a>

All diese Unzufriedenheit ist mehreren Werbefirmen, die
ihre Kampagnen eingestellt haben, nicht entgangen[^5].

Die Moderatoren arbeiten unentgeltlich für das Unternehmen; man hat
ihnen nicht zugehört. Auch den Nutzern, die ebenfalls wichtig sind,
wurde nicht zugehört. Das Ergebnis ist für Reddit katastrophal: Verlust
von Nutzern, Verlust von Werbeeinnahmen und Zunahme von Junk-Inhalten.

[^1]: Peters, J. (12. Juni 2023). <cite>Reddit crashed because of the growing subreddit blackout</cite>. <https://www.theverge.com/2023/6/12/23758002/reddit-crashing-api-protest-subreddit-private-going-dark>
[^2]: Bonifacic, I. (17. Juni 2023). <cite>Reddit’s average daily traffic fell during blackout, according to third-party data</cite>. <https://www.engadget.com/reddits-average-daily-traffic-fell-during-blackout-according-to-third-party-data-194721801.html>
[^3]: Divided by Zer0 (17. Juni 2023). <cite>Mates, today without warning, the reddit royal navy attacked. I’ve been demoded by the admins.</cite>. <https://lemmy.dbzer0.com/post/35555>.
[^4]: Horizon (15. Juni 2023). <cite>Since the blackout isn't working, everyone upload 1gb large videos of noise to reddit servers</cite>.
    <https://bird.trom.tf/TheHorizon2b2t/status/1669270005010288640>.
[^5]: Perloff, C. (14. Juni 2023). <cite>Ripples Through Reddit as Advertisers Weather Moderators Strike</cite>. <https://www.adweek.com/social-marketing/ripples-through-reddit-as-advertisers-weather-moderators-strike/amp/>.
