Author: Jorge Maldonado Ventura
Category: News
Date: 2023-06-18 11:26
Lang: en
Slug: reddit-pierde-usuarios-y-dinero-debido-a-la-protesta-de-sus-usuarios
Save_as: Reddit-loses-users-and-money-for-ignoring-the-complaints-of-its-users/index.html
URL: Reddit-loses-users-and-money-for-ignoring-the-complaints-of-its-users/
Tags: Reddit, Lemmy
Title: Reddit loses users and money for ignoring the complaints of its own users
Image: <img src="/wp-content/uploads/2023/06/reddit-en-llamas.jpg" alt="" width="763" height="639" srcset="/wp-content/uploads/2023/06/reddit-en-llamas.jpg 763w, /wp-content/uploads/2023/06/reddit-en-llamas-381x319.jpg 381w" sizes="(max-width: 763px) 100vw, 763px">

Reddit is paying the price for ignoring the users' [protest against
latest changes made by the company](/en/stop-using-Reddit/).

On the one hand, many people have moved to platforms such as [Lemmy](https://join-lemmy.org/),
which has tripled its user count in a short time:

<a href="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png">
<img src="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png" alt="" width="1130" height="320" srcset="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png 1130w, /wp-content/uploads/2023/06/usuarios-totales-Lemmy-565x160.png 565w" sizes="(max-width: 1130px) 100vw, 1130px">
</a>

On the other hand, the Reddit site was taken offline by protests in
which more than 7000 <!-- more -->forums went private[^1] and four
million people stopped using the website[^2]. In an attempt to
minimise the damage, Reddit removed several protesting moderators from
moderation teams, replacing them with compliant moderators[^3].

However, some forums are still closed. There are also users who are
uploading irrelevant content to the platform in order to diminish the
value of the company and to continue the protest. One example is the
[r/pic forum](https://safereddit.com/r/pics), which is now a "A place
for images of John Oliver looking sexy". In addition, some people are
filling the platform with videos of noise[^4].

<a href="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png">
<img src="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png" alt="">
</a>

All this discontent has not gone unnoticed by several advertising
companies, which have stopped their campaigns[^5].

Moderators work for free for the company; they have not been
listened to. The users, who are also essential, have not been listened
to either. The result has been disastrous for Reddit: loss of users,
loss of ad revenue and increase in junk content.

[^1]: Peters, J. (June 12, 2023). <cite>Reddit crashed because of the growing subreddit blackout</cite>. <https://www.theverge.com/2023/6/12/23758002/reddit-crashing-api-protest-subreddit-private-going-dark>
[^2]: Bonifacic, I. (June 17, 2023). <cite>Reddit’s average daily traffic fell during blackout, according to third-party data</cite>. <https://www.engadget.com/reddits-average-daily-traffic-fell-during-blackout-according-to-third-party-data-194721801.html>
[^3]: Divided by Zer0 (June 17, 2023). <cite>Mates, today without warning, the reddit royal navy attacked. I’ve been demoded by the admins.</cite>. <https://lemmy.dbzer0.com/post/35555>.
[^4]: Horizon (June 15, 2023). <cite>Since the blackout isn't
    working, everyone upload 1gb large videos of noise to reddit
    servers</cite>.
    <https://bird.trom.tf/TheHorizon2b2t/status/1669270005010288640>.
[^5]: Perloff, C. (June 14, 2023). <cite>Ripples Through Reddit as Advertisers Weather Moderators Strike</cite>. <https://www.adweek.com/social-marketing/ripples-through-reddit-as-advertisers-weather-moderators-strike/amp/>.
