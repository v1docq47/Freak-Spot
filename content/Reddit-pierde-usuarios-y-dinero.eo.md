Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2023-06-18 14:00
Lang: eo
Slug: reddit-pierde-usuarios-y-dinero-debido-a-la-protesta-de-sus-usuarios
Save_as: Reddit-perdas-uzantojn-kaj-monon-pro-ignori-la-plendojn-de-siaj-uzantoj/index.html
URL: Reddit-perdas-uzantojn-kaj-monon-pro-ignori-la-plendojn-de-siaj-uzantoj/
Tags: Reddit, Lemmy
Title: Reddit perdas uzantojn kaj monon, ĉar ĝi ignoris la plendojn de siaj uzantoj
Image: <img src="/wp-content/uploads/2023/06/reddit-en-llamas.jpg" alt="" width="763" height="639" srcset="/wp-content/uploads/2023/06/reddit-en-llamas.jpg 763w, /wp-content/uploads/2023/06/reddit-en-llamas-381x319.jpg 381w" sizes="(max-width: 763px) 100vw, 763px">

Reddit pagas la prezon ignori la [proteston de siaj uzantoj kontraŭ siaj
lastaj ŝanĝoj](/eo/ĉesu-uzi-Reddit/).

Unuflanke multaj homoj moviĝis al aliaj platformoj kiel
[Lemmy](https://join-lemmy.org/),
kiu triobligis siajn uzantojn en mallonga tempo:

<a href="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png">
<img src="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png" alt="" width="1130" height="320" srcset="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png 1130w, /wp-content/uploads/2023/06/usuarios-totales-Lemmy-565x160.png 565w" sizes="(max-width: 1130px) 100vw, 1130px">
</a>

Aliflanke la retejo de Reddit ĉesis funkcii pro protestoj, en kiuj pli
ol 7000 diskutejoj <!-- more -->iĝis nealireblaj[^1] kaj kvar milionoj
da homoj ĉesis uzi la retejon[^2]. Klopodante minimumigi la damaĝon,
Reddit forigis el la teamoj de administrantoj kelkajn administrantojn,
kiuj protestis, anstataŭigante ilin per obeemaj administrantoj[^3].

Tamen iuj diskutejoj ankoraŭ estas malfermitaj. Estas ankaŭ uzantoj,
kiuj alŝutas senrilatan enhavon al la retejo por malpliigi la valoron de
la firmao kaj daŭrigi la proteston. Ekzemplo estas la
[diskutejo r/pic](https://safereddit.com/r/pics), kiu estas nun «ejo por
seksallogaj bildoj de John Oliver». Krome, kelkaj homoj plenigas la
retejon per hazardaj videoj[^4].

<a href="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png">
<img src="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png" alt="">
</a>

Ĉi tiu malkontenteco ne estis ignorita de kelkaj reklamfirmaoj, kiuj
ĉesigis siajn kampanjojn[^5].

Administrantoj senpage laboras por la firmaoj; ili ne estis aŭskultitaj.
La uzantoj, kiuj estas esencaj, ankaŭ ne estis aŭskultitaj. La rezulto
estas katastrofa por Reddit: perdo de uzantoj, perdo de reklam-enspezo
kaj pliiĝo de senutila enhavo.

[^1]: Peters, J. (12-a de junio de 2023). <cite>Reddit crashed because of the growing subreddit blackout</cite>. <https://www.theverge.com/2023/6/12/23758002/reddit-crashing-api-protest-subreddit-private-going-dark>
[^2]: Bonifacic, I. (17-a de junio de 2023). <cite>Reddit’s average daily traffic fell during blackout, according to third-party data</cite>. <https://www.engadget.com/reddits-average-daily-traffic-fell-during-blackout-according-to-third-party-data-194721801.html>
[^3]: Divided by Zer0 (17-a de junio de 2023). <cite>Mates, today without warning, the reddit royal navy attacked. I’ve been demoded by the admins.</cite>. <https://lemmy.dbzer0.com/post/35555>.
[^4]: Horizon (15-a de junio de 2023). <cite>Since the blackout isn't
    working, everyone upload 1gb large videos of noise to reddit
    servers</cite>.
    <https://bird.trom.tf/TheHorizon2b2t/status/1669270005010288640>.
[^5]: Perloff, C. (14-a de junio de 2023). <cite>Ripples Through Reddit as Advertisers Weather Moderators Strike</cite>. <https://www.adweek.com/social-marketing/ripples-through-reddit-as-advertisers-weather-moderators-strike/amp/>.
