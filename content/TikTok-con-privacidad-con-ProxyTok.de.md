Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2022-10-14 19:20
Modified: 2022-10-29 14:00
Lang: de
Slug: TikTok-con-privacidad-con-ProxiTok
Save_as: TikTok-mit-Privatsphäre-mit-ProxyTok/index.html
URL: TikTok-mit-Privatsphäre-mit-ProxyTok/
Tags: TikTok
Title: TikTok mit Privatsphäre mit ProxiTok
Image: <img src="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">

TikTok ist ein zentralisiertes soziales Netzwerk und erfordert die
Nutzung von proprietärer Software. Es ist praktisch unmöglich, TikTok
mit dem Browser bequem einzusehen, ohne Privatsphäre oder Freiheit
zu verlieren, ... es sei denn, du verwendest eine andere Schnittstelle,
wie [ProxyTok](https://proxitok.pabloferreiro.es/), die ich in diesem Artikel beschreibe.

Die ProxiTok-Schnittstelle ist einfach. Du kannst beliebte Inhalte
(**<i lang="en">Trending</i>**) aufrufen, Nutzer entdecken
(**<i lang="en">Discover</i>**) und nach Benutzername, Tag, TikTok-URL,
Musik- und Videokennzeichen suchen.

<figure>
<a href="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Nach Benutzername suchen</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/10/Profil-von-reneerde-auf-ProxiTok.png">
<img src="/wp-content/uploads/2022/10/Profil-von-reneerde-auf-ProxiTok.png" alt="" width="1012" height="826" srcset="/wp-content/uploads/2022/10/Profil-von-reneerde-auf-ProxiTok.png 1012w, /wp-content/uploads/2022/10/Profil-von-reneerde-auf-ProxiTok-506x413.png 506w" sizes="(max-width: 1012px) 100vw, 1012px">
</a>
    <figcaption class="wp-caption-text">Profil von <a href="https://proxitok.herokuapp.com/@reneerde">@reneerde</a>. Es kann auch per RSS verfolgt werden.</figcaption>
</figure>

Das Projekt wurde am 1. Januar 2022 ins Leben gerufen, ist also noch
recht jung, und es ist zu erwarten, dass weitere Funktionen hinzugefügt
werden. Um automatisch auf ProxiTok umzuleiten, kannst du die
[LibRedirect-Erweiterung](https://libredirect.github.io/) installieren,
die auch andere datenschutzschädliche Websites vermeidet.

Es gibt [mehrere öffentliche
Instanzen](https://github.com/pablouser1/ProxiTok/wiki/Public-instances),
und es ist möglich, eine auf deinem eigenen Server zu installieren.
