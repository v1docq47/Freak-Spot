Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2022-10-29 13:00
Lang: pt
Slug: TikTok-con-privacidad-con-ProxiTok
Save_as: TikTok-com-privacidade-com-o-ProxyTok/index.html
URL: TikTok-com-privacidade-com-o-ProxyTok/
Tags: TikTok
Title: TikTok com privacidade com o ProxiTok
Image: <img src="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">

O TikTok é uma rede social centralizada que exige o uso de
<i lang="en">software</i> privativo. É quase impossível consultar o
TikTok de forma aceitável sem perder privacidade ou liberdade... a não
ser que usemos uma outra interface, como o
[ProxyTok](https://proxitok.pabloferreiro.es/), que eu descrevo neste
artigo.

A interface do ProxiTok é simples. Podes ir ao conteúdo popular
(**<i lang="en">Trending</i>**), descobrir utilizadores
(**<i lang="en">Discover</i>**) e pesquisar por nome de utilizador, etiqueta,
URL do TikTok, identificador musical e identificador de vídeo.

<figure>
<a href="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Pesquisar por nome de utilizador</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Perfil de <a href="https://proxitok.herokuapp.com/@recetasveganas">@recetasveganas</a>. Também pode ser seguido por RSS.</figcaption>
</figure>

O projecto foi lançado em 1 de Janeiro de 2022, portanto é bastante jovem
e espera-se que sejam adicionados mais recursos. Para
redireccionar automaticamente para o ProxiTok, podes instalar a extensão
[LibRedirect](https://libredirect.github.io/), que também evita outros
sites prejudiciais à privacidade.

Existem [várias instâncias
públicas](https://github.com/pablouser1/ProxiTok/wiki/Public-instances),
e é possível instalar uma no teu próprio servidor.
