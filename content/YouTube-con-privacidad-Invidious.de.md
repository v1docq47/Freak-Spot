Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2022-11-18 17:00
Lang: de
Slug: youtube-con-privacidad-con-invidious
Save_as: YouTube-mit-Privatsphäre-mit-Invidious/index.html
URL: YouTube-mit-Privatsphäre-mit-Invidious/
Tags: Erweiterung, Firefox, Google, Invidious, Privatsphäre, YouTube
Title: YouTube mit Privatsphäre: mit Invidious
Image: <img src="/wp-content/uploads/2022/11/Hauptseite-Invidious.png" alt="" width="1438" height="1014" srcset="/wp-content/uploads/2022/11/Hauptseite-Invidious.png 1438w, /wp-content/uploads/2022/11/Hauptseite-Invidious-719x507.png 719w" sizes="(max-width: 1438px) 100vw, 1438px">

Bekanntlich ist YouTube keine freie Software und respektiert deine
Privatsphäre nicht, aber leider gibt es Videos, die nur da sind. In
diesem Artikel stelle ich Invidious vor, eine einfache Möglichkeit,
YouTube-Videos anzusehen, ohne die proprietäre Software von Google
auszuführen.

Invidious ist eine kostenlose und leichtgewichtige Schnittstelle für
YouTube, die im Sinne der Softwarefreiheit entwickelt wurde. Hier sind
einige seiner Funktionen:

- Es hat keine Werbung.
- Es handelt sich um freie Software, deren [Quellcode](https://github.com/iv-org/invidious) unter der [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)-Lizenz steht.
- Sie hat eine Suchmaschine.
- Kein Google-Konto zum Speichern von Abonnements erforderlich.
- Ermöglicht die Anzeige von Untertiteln.
- Es ist in hohem Maße anpassbar.
- Ermöglicht es Ihnen, Videos von Invidious in Ihre Seite einzubetten, wie das untenstehende...

<!-- more -->

<iframe id='ivplayer' width='640' height='360' src='https://inv.zzls.xyz/embed/q1ZbSFFrits' style='border:none;'></iframe>

weil es freie Software ist, gibt es keine einheitliche Version. Das
bedeutet, dass du Invidious auf deinem Server installieren oder aus den
[öffentlich verfügbaren Versionen](https://api.invidious.io/) wählen kannst.

Ich lade dich ein, [auszuprobieren, wie es
funktioniert](https://invidious.snopyta.org/). Nachfolgend siehst du
einige der Möglichkeiten, die Sie mit Invidious nutzen können.

<figure>
<a href="/wp-content/uploads/2022/11/anmelden-Invidious.png">
<img src="/wp-content/uploads/2022/11/anmelden-Invidious.png" alt="" width="1012" height="640" srcset="/wp-content/uploads/2022/11/anmelden-Invidious.png 1012w, /wp-content/uploads/2022/11/anmelden-Invidious-506x320.png 506w" sizes="(max-width: 1012px) 100vw, 1012px">
</a>
    <figcaption class="wp-caption-text">Du kannst ohne ein Google-Konto
    anmelden.</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/11/Abonnements-verwalten-Invidious.png">
<img src="/wp-content/uploads/2022/11/Abonnements-verwalten-Invidious.png" alt="" width="1920" height="1032" srcset="/wp-content/uploads/2022/11/Abonnements-verwalten-Invidious.png 1920w, /wp-content/uploads/2022/11/Abonnements-verwalten-Invidious-960x516.png 960w, /wp-content/uploads/2022/11/Abonnements-verwalten-Invidious-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">Du kannst dir die neueste Videos
    deiner Abonemments anschauen.</figcaption>
</figure>

<figure>

<a href="/wp-content/uploads/2022/11/Dunkelmodus-Invidious.png">
<img src="/wp-content/uploads/2022/11/Dunkelmodus-Invidious.png" alt="" width="1920" height="1032" srcset="/wp-content/uploads/2022/11/Dunkelmodus-Invidious.png 1920w, /wp-content/uploads/2022/11/Dunkelmodus-Invidious-960x516.png 960w, /wp-content/uploads/2022/11/Dunkelmodus-Invidious-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">Es hat einen Dunkelmodus.</figcaption>
</figure>

Es gibt Firefox Erweiterungen, die YouTube-Links in Invidious-Links umwandeln.
[Privacy Redirect](https://addons.mozilla.org/de/firefox/addon/privacy-redirect/)
funktioniert auch für
[Nitter](/en/Check-Twitter-with-free-software-and-privacy-with-Nitter/).
