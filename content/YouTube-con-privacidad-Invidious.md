Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-01-04
Modified: 2022-10-29 21:00
Lang: es
Slug: youtube-con-privacidad-con-invidious
Tags: complemento, Firefox, Google, Invidious, privacidad, YouTube
Title: YouTube con privacidad: con Invidious
Image: <img src="/wp-content/uploads/2020/01/buscando-vídeos-con-Invidious.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2020/01/buscando-vídeos-con-Invidious.png 1000w, /wp-content/uploads/2020/01/buscando-vídeos-con-Invidious-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">

Como ya es bien sabido, YouTube no es <i lang="en">software</i> libre ni respeta tu
privacidad, pero lamentablemente hay vídeos que solo están allí. En este
artículo os presento Invidious, una forma sencilla de ver vídeos de
YouTube sin ejecutar <i lang="en">software</i> privativo de Google.

Invidious es una interfaz libre y ligera para YouTube que está hecha
pensando en la libertad de <i lang="en">software</i>. Estas son algunas de sus
características:

- No tiene anuncios
- Es <i lang="en">software</i> libre, [código fuente](https://github.com/iv-org/invidious) bajo licencia [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)
- Tiene un buscador
- No requiere cuenta de Google para guardar suscripciones
- Permite ver subtítulos
- Es muy personalizable
- Permite insertar vídeos desde Invidious en tú página, como el que
  sigue...

<!-- more -->

<iframe id='ivplayer' width='640' height='360' src='https://inv.zzls.xyz/embed/CvMK0sg-Ac8' style='border:none;'></iframe>

Como es <i lang="en">software</i> libre, no hay una única versión. Esto quiere decir que
puedes instalar Invidious en tu servidor o elegir entre las [versiones
públicas](https://api.invidious.io/) que hay disponibles.

Te invito a que [pruebes cómo funciona](https://invidious.snopyta.org/). Abajo dejo
imágenes de algunas cosas que puedes hacer con Invidious.

<figure>
<a href="/wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious.png">
<img src="/wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious.png 1000w, /wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">Puedes iniciar sesión sin cuenta de
    Google</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/01/suscripciones-Invidious.png">
<img src="/wp-content/uploads/2020/01/suscripciones-Invidious.png" alt="" width="1438" height="840" srcset="/wp-content/uploads/2020/01/suscripciones-Invidious.png 1438w, /wp-content/uploads/2020/01/suscripciones-Invidious-719x420.png 719w" sizes="(max-width: 1438px) 100vw, 1438px">
</a>
    <figcaption class="wp-caption-text">Puedes ver los últimos vídeos de los
    canales a los que estás suscrito</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/01/apariencia-de-invidious.png">
<img src="/wp-content/uploads/2020/01/apariencia-de-invidious.png" alt="" width="1438" height="840" srcset="/wp-content/uploads/2020/01/apariencia-de-invidious.png 1438w, /wp-content/uploads/2020/01/apariencia-de-invidious-719x420.png 719w" sizes="(max-width: 1438px) 100vw, 1438px">
</a>
    <figcaption class="wp-caption-text">Tiene un modo oscuro</figcaption>
</figure>

Hay complementos para Firefox que convierten los enlaces a YouTube a
enlaces a instancias de Invidious. [Privacy Redirect](https://addons.mozilla.org/es/firefox/addon/privacy-redirect/) en concreto también funciona
para [Nitter](/consultar-twitter-con-privacidad-y-software-libre/) y
[Bibliogram](/consultar-instagram-con-software-libre-y-privacidad/).
