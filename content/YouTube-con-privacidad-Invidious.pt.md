Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2022-10-30 12:30
Lang: pt
Slug: youtube-con-privacidad-con-invidious
Tags: extensão, Firefox, Google, Invidious, privacidade, YouTube
Title: YouTube com privacidade com o Invidious
Save_as: YouTube-com-privacidade-com-o-Invidious/index.html
URL: YouTube-com-privacidade-com-o-Invidious/
Image: <img src="/wp-content/uploads/2022/10/pesquisar-no-Invidious.png" alt="" width="1517" height="983" srcset="/wp-content/uploads/2022/10/pesquisar-no-Invidious.png 1517w, /wp-content/uploads/2022/10/pesquisar-no-Invidious-758x491.png 758w, /wp-content/uploads/2022/10/pesquisar-no-Invidious-379x245.png 379w" sizes="(max-width: 1517px) 100vw, 1517px">

Como é bem sabido, o YouTube não é um programa livre nem respeita a tua
privacidade, mas infelizmente há alguns vídeos que só estão lá. Neste
artigo apresento o Invidious, uma forma simples de ver vídeos do YouTube
sem correr o <i lang="en">software</i> privativo do Google.

Invidious é uma interface livre e leve para o YouTube que é feita com a
liberdade de software em mente. Aqui estão algumas das suas
características:

- Sem anúncios
- <i lang="en">Software</i> livre, [código fuente](https://github.com/iv-org/invidious)
  sob a licença [APGv3](https://www.gnu.org/licenses/agpl-3.0.html).
- Tem buscador
- Não precisa de conta de Google para guardar subscrição
- Permite ver legendas
- É muito personalizável
- Permite colocar vídeos do Invidious em tua página, como o seguinte:

<!-- more -->

<iframe id='ivplayer' width='640' height='360' src='https://inv.zzls.xyz/embed/K_Fj2_8BXzc' style='border:none;'></iframe>

Como se trata de <i lang="en">software</i> libre, não existe uma versão
única. Isto significa que podes instalar o Invidious no teu servidor ou
escolher a partir das [versões públicas disponíveis](https://api.invidious.io/).

Convido-te a experimentar como funciona. Abaixo estão imagens de algumas
das coisas que podes fazer com o Invidious.

<figure>
<a href="/wp-content/uploads/2022/10/iniciar-sessão-Invidious.png">
<img src="/wp-content/uploads/2022/10/iniciar-sessão-Invidious.png" alt="" width="1492" height="531" srcset="/wp-content/uploads/2022/10/iniciar-sessão-Invidious.png 1492w, /wp-content/uploads/2022/10/iniciar-sessão-Invidious-746x265.png 746w" sizes="(max-width: 1492px) 100vw, 1492px">
</a>
    <figcaption class="wp-caption-text">Podes iniciar sessão sem conta
    do Google.</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/10/subscrições-no-Invidious.png">
<img src="/wp-content/uploads/2022/10/subscrições-no-Invidious.png" alt="" width="1517" height="983" srcset="/wp-content/uploads/2022/10/subscrições-no-Invidious.png 1517w, /wp-content/uploads/2022/10/subscrições-no-Invidious-758x491.png 758w, /wp-content/uploads/2022/10/subscrições-no-Invidious-379x245.png 379w" sizes="(max-width: 1517px) 100vw, 1517px">
</a>
    <figcaption class="wp-caption-text">Podes ver os últimos vídeos dos canais aos quais estás inscrito.</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/10/reproduzir-vídeo-no-Invidious.png">
<img src="/wp-content/uploads/2022/10/reproduzir-vídeo-no-Invidious.png" alt="" width="1517" height="983" srcset="/wp-content/uploads/2022/10/reproduzir-vídeo-no-Invidious.png 1517w, /wp-content/uploads/2022/10/reproduzir-vídeo-no-Invidious-758x491.png 758w, /wp-content/uploads/2022/10/reproduzir-vídeo-no-Invidious-379x245.png 379w" sizes="(max-width: 1517px) 100vw, 1517px">
</a>
    <figcaption class="wp-caption-text">Tem modo escuro.</figcaption>
</figure>

Existem extensões do Firefox que convertem ligações do YouTube em
ligações para instâncias do Invidious.
[Privacy Redirect](https://addons.mozilla.org/pt-PT/firefox/addon/privacy-redirect/)
em particular também funciona para [Nitter](/pt/consultar-Twitter-com-software-livre-e-privacidade-com-Nitter/).
