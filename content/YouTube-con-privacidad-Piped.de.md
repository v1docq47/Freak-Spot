Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2022-11-26 11:20
Lang: de
Save_as: YouTube-mit-Privatsphäre-mit-Piped/index.html
URL: YouTube-mit-Privatsphäre-mit-Piped/
Slug: youtube-con-privacidad-con-piped
Tags: Google, Piped, Privatsphäre, YouTube
Title: YouTube mit Privatsphäre mit Piped
Image: <img src="/wp-content/uploads/2022/11/video-de-Piped-con-capítulos.png" alt="" width="1911" height="984" srcset="/wp-content/uploads/2022/11/video-de-Piped-con-capítulos.png 1911w, /wp-content/uploads/2022/11/video-de-Piped-con-capítulos-955x492.png 955w, /wp-content/uploads/2022/11/video-de-Piped-con-capítulos-477x246.png 477w" sizes="(max-width: 1911px) 100vw, 1911px">

Wie [Invidious](/de/YouTube-mit-Privatsphäre-mit-Invidious/) bietet auch
[Piped](https://piped.kavin.rocks/) eine freie und
datenschutzfreundliche Schnittstelle zu YouTube.

Der Vorteil von Piped ist, dass es mit
[SponsorBlock](https://sponsor.ajay.app/) funktioniert, so dass du
keine Zeit damit verschwendest muss, gesponserte Teile von Videos
reinzuziehen. Ich habe nur die Funktionen erwähnt, die ich am nützlichsten
finde;
[eine ausführlichere Liste](https://github.com/TeamPiped/Piped/#features)
findest du auf der Projektseite.


<figure>
<a href="/wp-content/uploads/2022/11/channel-view-Jisi-on.Piped.png">
<img src="/wp-content/uploads/2022/11/channel-view-Jisi-on.Piped.png" alt="" width="1920" height="1032" srcset="/wp-content/uploads/2022/11/channel-view-Jisi-on.Piped.png 1920w, /wp-content/uploads/2022/11/channel-view-Jisi-on.Piped-960x516.png 960w, /wp-content/uploads/2022/11/channel-view-Jisi-on.Piped-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">YouTube-Kanal von Piped gezeigt</figcaption>
</figure>

Einige Nachteile im Vergleich zu Invidious sind, dass man die Videos
eines Kanals nicht nach Alter oder Beliebtheit sortieren kann, sondern
nur die neuesten Videos des Kanals angezeigt werden; es gibt keine
Schaltfläche zum Herunterladen von Videos und Audio; man sieht keine
Miniaturansicht des Frames, wenn man mit der Maus über die Zeitleiste
fährt; die Miniaturansicht des Videos erscheint nicht, wenn man einen
Link teilt...


<figure>
<a href="/wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video.png">
<img src="/wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video.png" alt="" width="1437" height="1010" srcset="/wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video.png 1437w, /wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video-718x505.png 718w" sizes="(max-width: 1437px) 100vw, 1437px">
</a>

    <figcaption class="wp-caption-text">Du kannst die Videos in eine
    Schleife legen, Kommentare ansehen, Videobeschreibungen lesen...</figcaption>
</figure>

<!-- more -->

Nachfolgend gibt es ein mit Piped hochgeladenes Video:

<iframe id='ivplayer' width='640' height='360' src='https://piped.kavin.rocks/embed/Ag1AKIl_2GM' style='border:none;'></iframe>

Es ist großartig, dass es kostenlose Alternativen zu freier Software
gibt. Ich bin mit der Funktionsweise von Piped zufrieden, aber ich mag
auch Invidious sehr. Beides sind gute Optionen, sicherlich viel besser
als die direkte Nutzung von YouTube, denn du sparst Zeit, indem du
Werbung vermeidest, du schützt deine Privatsphäre und du gibst YouTube
keinen Cent.
