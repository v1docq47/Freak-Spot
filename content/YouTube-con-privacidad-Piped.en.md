Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2022-11-15 14:30
Lang: en
Save_as: YouTube-privately-with-Piped/index.html
URL: YouTube-privately-with-Piped/
Slug: youtube-con-privacidad-con-piped
Tags: Google, Piped, Pricacy, YouTube
Title: YouTube privately with Piped
Image: <img src="/wp-content/uploads/2022/11/video-de-Piped-con-capítulos.png" alt="" width="1911" height="984" srcset="/wp-content/uploads/2022/11/video-de-Piped-con-capítulos.png 1911w, /wp-content/uploads/2022/11/video-de-Piped-con-capítulos-955x492.png 955w, /wp-content/uploads/2022/11/video-de-Piped-con-capítulos-477x246.png 477w" sizes="(max-width: 1911px) 100vw, 1911px">

Just like [Invidious](/en/youtube-con-privacidad-con-invidious/),
[Piped](https://piped.kavin.rocks/) offers a free and privacy-friendly
to YouTube.

The advantage of Piped is that it works with
[SponsorBlock](https://sponsor.ajay.app/), so you don't
waste time putting up with sponsored parts of videos. I've only mentioned the
features I find most useful; [a more detailed
list](https://github.com/TeamPiped/Piped/#features) is available on the
project page.


<figure>
<a href="/wp-content/uploads/2022/11/channel-view-Jisi-on.Piped.png">
<img src="/wp-content/uploads/2022/11/channel-view-Jisi-on.Piped.png" alt="" width="1920" height="1032" srcset="/wp-content/uploads/2022/11/channel-view-Jisi-on.Piped.png 1920w, /wp-content/uploads/2022/11/channel-view-Jisi-on.Piped-960x516.png 960w, /wp-content/uploads/2022/11/channel-view-Jisi-on.Piped-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">YouTube channen shown by Piped</figcaption>
</figure>

Some disadvantages compared to Invidious are that it does not allow you
to sort the videos of a channel according to age or popularity, but
simply shows the latest videos of the channel; there is no button to
download videos and audio; you do not see a thumbnail of the frame when
hovering the mouse over the timeline; the thumbnail of the video does
not appear when sharing a link...

<figure>

<a href="/wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video.png">
<img src="/wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video.png" alt="" width="1437" height="1010" srcset="/wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video.png 1437w, /wp-content/uploads/2022/11/Piped-comments-videos-suggestions-moving-video-718x505.png 718w" sizes="(max-width: 1437px) 100vw, 1437px">
</a>

    <figcaption class="wp-caption-text">You can move the video, read
    comments and descritions...</figcaption>
</figure>

<!-- more -->

I show a video loaded with Piped below:

<iframe id='ivplayer' width='640' height='360' src='https://piped.kavin.rocks/embed/Ag1AKIl_2GM' style='border:none;'></iframe>

It's great that there are free alternatives to free programs. I'm
satisfied with the performance of Piped, but I also really like
Invidious. Both are good options, certainly much better than using
YouTube directly, as you save time by avoiding ads, protecting your
privacy, and not you don't give a penny to YouTube.
