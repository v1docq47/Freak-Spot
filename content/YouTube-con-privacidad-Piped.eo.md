Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2022-10-31 14:00
Lang: eo
Save_as: YouTube-privatece-per-Piped/index.html
URL: YouTube-privatece-per-Piped/
Slug: youtube-con-privacidad-con-piped
Tags: Google, Piped, privateco, YouTube
Title: YouTube privatece per Piped
Image: <img src="/wp-content/uploads/2022/10/YouTube-video-en-Piped.png" alt="" width="1492" height="960" srcset="/wp-content/uploads/2022/10/YouTube-video-en-Piped.png 1492w, /wp-content/uploads/2022/10/YouTube-video-en-Piped-746x480.png 746w" sizes="(max-width: 1492px) 100vw, 1492px">

Same kiel [Invidious](/eo/YouTube-privatece-per-Invidious/),
[Piped](https://piped.kavin.rocks/) disponebligas alian fasadon por
YouTube, kiu liberas kaj respektas vian privatecon.

La avantaĝo de Piped estas, ke ĝi funkcias per
[SponsorBlock](https://sponsor.ajay.app/), do vi ne perdas vian tempon
senvole vidante sponsoritajn partojn de videoj. Mi nur menciis la
funkciojn, kiujn mi konsideras plej utilaj; en la projekta retejo estas
[pli detala listo](https://github.com/TeamPiped/Piped/#features).


<figure>
<a href="/wp-content/uploads/2022/10/YouTube-kanalo-en-Piped.png">
<img src="/wp-content/uploads/2022/10/YouTube-kanalo-en-Piped.png" alt="" width="1920" height="1032" srcset="/wp-content/uploads/2022/10/YouTube-kanalo-en-Piped.png 1920w, /wp-content/uploads/2022/10/YouTube-kanalo-en-Piped-960x516.png 960w, /wp-content/uploads/2022/10/YouTube-kanalo-en-Piped-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">YouTube-kanalo vidita per Piped</figcaption>
</figure>

Kelkaj malavantaĝoj rilate al Invidious estas, ke ĝi ne permesas ordigi
la videojn de kanalo laŭ iliaj malnoveco aŭ populareco, sed ĝi simple
montras la lastajn videojn de la kanalo; ne estas butono por elŝuti
videojn kaj aŭdaĵojn; ne eblas vidi filmeran bildeton per movado de la
muŝo sur la templinion; ne aperas la videa bildeto, kiam vi konigas
ligilon...

<figure>
<a href="/wp-content/uploads/2022/10/video-en-Piped-priskribo-komentoj-ripetadi.png">
<img src="/wp-content/uploads/2022/10/video-en-Piped-priskribo-komentoj-ripetadi.png" alt="" width="1920" height="1032" srcset="/wp-content/uploads/2022/10/video-en-Piped-priskribo-komentoj-ripetadi.png 1920w, /wp-content/uploads/2022/10/video-en-Piped-priskribo-komentoj-ripetadi-960x516.png 960w, /wp-content/uploads/2022/10/video-en-Piped-priskribo-komentoj-ripetadi-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">Vi povas ripetadi videojn, vidi
    komentojn, legi videajn priskribojn...</figcaption>
</figure>

<!-- more -->

Jen mi montras videon ŝarĝitan per Piped:

<iframe id='ivplayer' width='640' height='360' src='https://piped.kavin.rocks/embed/530Y4a6jomI' style='border:none;'></iframe>

Malaĉas, ke ekzistas liberaj alternativoj por liberaj programoj. Mi
estas sata pri la funkciado de Piped, sed mi ankaŭ ŝategas Invidious.
Ambaŭ estas bonaj opcioj, multe pli bonaj ol rekte uzi YouTube, ĉar vi
ŝparas tempon evitante reklamojn, vi protektas vian privatecon kaj vi
donas nenion da mono al YouTube.
