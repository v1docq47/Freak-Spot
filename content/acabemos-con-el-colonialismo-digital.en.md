Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2023-05-29 15:00
Lang: en
Slug: acabemos-con-el-colonialismo-digital
Tags: digital colonialism, decentralization, United States of America, privacy, free software, surveillance
Title: Let's end digital colonialism
Save_as: let's-end-digital-colonialism/index.html
URL: let's-end-digital-colonialism/
Image: <img src="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png 1920w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-960x540.png 960w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

*We are colonized*.
[The Tech Giants](https://en.wikipedia.org/wiki/Big_Tech) control the digital world with the complicity of the
United States. Most computers (those using AMD or Intel processors) can
be spied on and remotely controlled thanks to a universal<!-- more --> backdoor[^1],
with the legal backing of the USA PATRIOT Act[^2]. We are also spied on
through mobile phones[^3], surveillance cameras, through the banking
system, etc.

## We are the enemy


WikiLeaks[^4] and Edward Snowden[^5] showed the world the spying
and other atrocities committed, especially by publishing confidential
files. The creator of WikiLeaks is in prison for reporting the
situation, and Snowden is in exile in Russia. But you don't have to
commit a *crime* to be imprisoned or persecuted, as free software
programmer and privacy advocate Ola Bini knows well[^6].

If you are reading this article, you are probably considered an
extremist by the US National Security Agency, as happened to the readers
of <cite>Linux Journal</cite>[^7]. Fortunately there are more GNU/Linux
and privacy extremists today. Companies like Microsoft used to say
GNU/Linux was a cancer[^8]; now they “love Linux”[^9]. Free software and
privacy have gained popularity and followers, but most of the problems
mentioned above are still present.


## Decolonization

Although most people live colonized by proprietary and spying
technologies, there are free alternatives that allow us to have more
privacy and control, there are free hardware and free software projects
that give us the freedoms that big companies and governments deny us.
These technologies should be supported and adopted, but for that we need
to make the problem visible and convince the population of their
advantages.

If we want to achieve more privacy and freedom, we must also [put an end
to companies like Google](/en/how-to-destroy-Google/), Meta, Apple and
Microsoft, which only pay lip service to privacy and free software. To
do this we must replace their spyware and proprietary programs with free
and decentralized alternatives. We must also support free hardware
projects.

However, the system is against us. In most countries, tax money is used
to pay for spyware licenses and to create a digital surveillance system.
One way to fight against that is to pay as little taxes as
possible (paying in cash and using anonymous cryptocurrencies like
Monero[^10], for instance) and to expose this issue.

The Roman Empire did not fall in two days. Neither will China's nor
the U.S. digital empire. It is in our hands to create and foster
liberating options.


[^1]: The Hated One (2019, April 7). <cite>How Intel wants to backdoor
    every computer in the world | Intel Management Engine
    explained</cite>. <https://inv.zzls.xyz/watch?v=Lr-9aCMUXzI>.
[^2]: Leiva A. (2021, November 26). <cite>26 de octubre de 2001: George W. Bush firma la ley USA Patriot, o Patriot Act</cite>. <https://elordenmundial.com/hoy-en-la-historia/26-octubre/26-de-octubre-de-2001-george-w-bush-firma-la-ley-usa-patriot-o-patriot-act/>.
[^3]: Rosenzweig, A. (2017, October 18). <cite>No Cellphones Beyond This Point</cite>.
    <https://freakspot.net/no-m%C3%A1s-celulares-a-partir-de-ahora/>.
[^4]: Wikipedia authors (2013, May 15). <cite>List of material
    published by WikiLeaks</cite>.
    <https://en.wikipedia.org/wiki/List_of_material_published_by_WikiLeaks>.
[^5]: BBC News Mundo (2022, September 26). <cite>Snowden: Putin
    otorga la nacionalidad rusa al exagente estadounidense</cite>.
    <https://www.bbc.com/mundo/noticias-internacional-63039060>.
[^6]: Wikipedia authors (2023, February 1). <cite>Ola Bini</cite>. <https://es.wikipedia.org/wiki/Ola_Bini>.
[^7]: Rankin, K. (2014, July 3). <cite>NSA: Linux Journal is an
    "extremist forum" and its readers get flagged for extra
    surveillance</cite>.
    <https://www.linuxjournal.com/content/nsa-linux-journal-extremist-forum-and-its-readers-get-flagged-extra-surveillance>.
[^8]: Greene, T. C. (2001, June 2). <cite>Ballmer: 'Linux is a
    cancer'</cite>. <https://www.theregister.com/2001/06/02/ballmer_linux_is_a_cancer/>.
[^9]: Wikipedia authors (2023, April 7). <cite>Microsoft and open source</cite>.
    <https://en.wikipedia.org/wiki/Microsoft_and_open_source>.
[^10]: Maldonado Ventura, J. (2023, February 20).
    <cite>Criptomonedas, anonimato, economía descentralizada</cite>.
    <https://freakspot.net/dinero-an%C3%B3nimo-digital-monero/>.
