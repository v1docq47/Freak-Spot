Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2023-05-29 07:00
Lang: es
Slug: acabemos-con-el-colonialismo-digital
Tags: colonialismo digital, descentralización, Estados Unidos de América, privacidad, software libre, vigilancia
Title: Acabemos con el colonialismo digital
Image: <img src="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png 1920w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-960x540.png 960w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

*Estamos colonizados*.
[Los gigantes tecnológicos](https://es.wikipedia.org/wiki/Gigantes_tecnol%C3%B3gicos)
controlan el mundo digital con la complicidad de Estados Unidos. La
mayoría de ordenadores (los que usan procesadores AMD o Intel)
pueden ser espiados y controlados remotamente gracias a una puerta
trasera <!-- more -->universal[^1], con el respaldo legal de la ley USA PATRIOT
Act[^2]. También somos espiados mediante teléfonos móviles[^3], cámaras
de vigilancia, mediante el sistema bancario, etc.

## Somos el enemigo

WikiLeaks[^4] y Edward Snowden[^5] mostraron al mundo el espionaje y otras
atrocidades cometidas, sobre todo publicando archivos confidenciales. El
creador de WikiLeaks se encuentra en prisión por informar de la
situación y Snowden está exiliado en Rusia. Pero no hace falta cometer
un *crimen* para ser encarcelado o perseguido, como sabe bien el
programador de <i lang="en">software</i> libre y defensor de la
privacidad Ola Bini[^6].

Si estás leyendo este artículo, probablemente la Agencia de Seguridad
Nacional (de EE.&nbsp;UU.) te considera un extremista, como le sucedió
a los lectores de <cite>Linux Journal</cite>[^7]. Afortunadamente a día
de hoy hay más extremistas de GNU/Linux y de la privacidad. Las
empresas como Microsoft decían que GNU/Linux era un cáncer[^8]; ahora
«aman Linux»[^9]. El <i lang="en">software</i> libre y la privacidad han
ganado popularidad y adeptos, pero la mayoría de los problemas antes
mencionados siguen presentes.

## Descolonización

Aunque la mayoría de las personas vive colonizada por tecnologías
privativas y que espían, existen alternativas libres que nos permiten tener
más privacidad y control, existen proyectos de <i lang="en">hardware</i>
libre y de <i lang="en">software</i> libre que nos brindan las
libertades que las grandes tecnológicas y gobiernos nos niegan. Esas
tecnologías deben ser apoyadas y adoptadas, pero para ello hace falta
visibilizar el problema y convencer a la población de sus ventajas.

Si queremos lograr más privacidad y libertad también debemos [acabar con
empresas como Google](/Cómo-destruir-Google/), Meta, Apple y Microsoft,
que solo defienden la privacidad y el <i lang="en">software</i> libre de
boquilla. Para ello debemos reemplazar sus programas espías y privativos
por alternativas libres y descentralizadas. Debemos apoyar también
proyectos de <i lang="en">hardware</i> libre.

Sin embargo, el sistema está contra nosotros. En la mayoría de países se
usa el dinero obtenido mediante impuestos para pagar licencias de
programas espías y crear un sistema de vigilancia digital. Una forma de
luchar contra eso es pagar los menos impuestos posibles &mdash;pagando
en efectivo y usando criptomonedas anónimas como Monero[^10], por
ejemplo&mdash; y denunciar la situación.

El Imperio romano no cayó en dos días. Tampoco lo hará el imperio
digital de Estados Unidos ni el de China. Está en nuestras manos
crear y fomentar opciones liberadoras.

[^1]: The Hated One (7 de abril de 2019). <cite>How Intel wants to backdoor
    every computer in the world | Intel Management Engine
    explained</cite>. <https://inv.zzls.xyz/watch?v=Lr-9aCMUXzI>.
[^2]: Leiva A. (26 de noviembre de 2021). <cite>26 de octubre de 2001: George W. Bush firma la ley USA Patriot, o Patriot Act</cite>. <https://elordenmundial.com/hoy-en-la-historia/26-octubre/26-de-octubre-de-2001-george-w-bush-firma-la-ley-usa-patriot-o-patriot-act/>.
[^3]: Rosenzweig, A. (16 de enero de 2021). <cite>No más celulares a
    partir de ahora</cite>.
    <https://freakspot.net/no-m%C3%A1s-celulares-a-partir-de-ahora/>.
[^4]: Autores de Wikipedia (15 de mayo de 2023). <cite>List of material
    published by WikiLeaks</cite>.
    <https://en.wikipedia.org/wiki/List_of_material_published_by_WikiLeaks>.
[^5]: BBC News Mundo (26 de septiembre de 2022). <cite>Snowden: Putin
    otorga la nacionalidad rusa al exagente estadounidense</cite>.
    <https://www.bbc.com/mundo/noticias-internacional-63039060>.
[^6]: Autores de Wikipedia (1 de febrero de 2023). <cite>Ola Bini</cite>. <https://es.wikipedia.org/wiki/Ola_Bini>.
[^7]: Rankin, K. (3 de julio de 2014). <cite>NSA: Linux Journal is an
    "extremist forum" and its readers get flagged for extra
    surveillance</cite>.
    <https://www.linuxjournal.com/content/nsa-linux-journal-extremist-forum-and-its-readers-get-flagged-extra-surveillance>.
[^8]: Greene, T. C. (2 de junio de 2001). <cite>Ballmer: 'Linux is a
    cancer'</cite>. <https://www.theregister.com/2001/06/02/ballmer_linux_is_a_cancer/>.
[^9]: Autores de Wikipedia (7 de abril de 2023). <cite>Microsoft and open source</cite>.
    <https://en.wikipedia.org/wiki/Microsoft_and_open_source>.
[^10]: Maldonado Ventura, J. (20 de febrero de 2023).
    <cite>Criptomonedas, anonimato, economía descentralizada</cite>.
    <https://freakspot.net/dinero-an%C3%B3nimo-digital-monero/>.
