Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-06-17 10:45
Lang: pt
Slug: acabemos-con-el-colonialismo-digital
Tags: colonialismo digital, descentralização, Estados Unidos da América, privacidade, software livre, vigilância
Title: Acabemos com o colonialismo digital
Save_as: acabemos-com-o-colonialismo-digital/index.html
URL: acabemos-com-o-colonialismo-digital/
Image: <img src="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png 1920w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-960x540.png 960w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

*Estamos colonizados*. Os gigantes tecnológicos controlam o mundo
digital com a cumplicidade dos Estados Unidos. A maioria dos
computadores (os que utilizam processadores AMD ou Intel) podem ser
espiados e controlados à distância graças a uma porta traseira
universal[^1], legalmente respaldada pela Lei Patriótica dos EUA[^2].
Também somos espiados através dos telemóveis[^3], das câmaras de
vigilância, do sistema bancário, etc.

## Somos os inimigos

WikiLeaks[^4] e Edward Snowden[^5] mostraram ao mundo a espionagem e
outras atrocidades cometidas, nomeadamente através da publicação de
ficheiros confidenciais. O criador da WikiLeaks está na prisão por ter
denunciado a situação e Snowden está exilado na Rússia. Mas não é
preciso cometer um *crime* para ser preso ou perseguido, como bem sabe o
programador de <i lang="pt">software</i> livre e defensor da privacidade
Ola Bini[^6].

Se estás a ler este artigo, provavelmente és considerado um extremista
pela Agência de Segurança Nacional (dos EUA), como aconteceu com os
leitores do <cite>Linux Journal</cite>[^7]. Felizmente há mais extremistas do
GNU/Linux e da privacidade hoje em dia. Empresas como a Microsoft
costumavam dizer que o GNU/Linux era um cancro[^8]; agora eles «adoram o
Linux»[^9]. O <i lang="en">software</i> livre e a privacidade ganharam
popularidade e seguidores, mas a maioria dos problemas mencionados
anteriormente ainda estão presentes.

## Descolonização

Embora a maioria das pessoas viva colonizada por tecnologias
proprietárias e espiãs, existem alternativas livres que nos permitem ter
mais privacidade e controlo, existem projectos de
<i lang="en">hardware</i> e <i lang="en">software</i> livres que nos dão as
liberdades que a grandes empresas e os governos nos negam. Estas
tecnologias devem ser apoiadas e adoptadas, mas para isso temos de
tornar o problema visível e convencer a população das suas vantagens.

Se queremos mais privacidade e liberdade, temos também de [acabar com
empresas como a Google](/pt/como-destruir-o-Google/), a Meta, a Apple e
a Microsoft, que só defendem a privacidade e <i lang="en">software</i>
livre da boca para fora. Para isso, temos de substituir o seus programas
espiões e proprietários por alternativas livres e descentralizadas.
Temos também de apoiar projectos de <i lang="en">hardware</i> livre.

No entanto, o sistema está contra nós. Na maioria dos países, o dinheiro
dos impostos é utilizado para pagar licenças de programas espiões e para
criar um sistema de vigilância digital. Uma maneira de lutar contra isso
é pagar o mínimo possível de impostos &mdash; pagando em dinheiro e
usando criptomoedas anónimas como o Monero[^10], por exemplo &mdash; e
protestar.

O Império Romano não caiu em dois dias. Também não o fará o império
digital dos Estados Unidos nem o da China. Cabe-nos a nós criar e
promover opções libertadoras.

[^1]: The Hated One (7 de abril de 2019). <cite>How Intel wants to backdoor
    every computer in the world | Intel Management Engine
    explained</cite>. <https://inv.zzls.xyz/watch?v=Lr-9aCMUXzI>.
[^2]: Leiva A. (26 de novembro de 2021). <cite>26 de octubre de 2001: George W. Bush firma la ley USA Patriot, o Patriot Act</cite>. <https://elordenmundial.com/hoy-en-la-historia/26-octubre/26-de-octubre-de-2001-george-w-bush-firma-la-ley-usa-patriot-o-patriot-act/>.
[^3]: Rosenzweig, A. (18 de outubro de 2017). <cite>No Cellphones Beyond
    This Point</cite>. <https://freakspot.net/en/no-cellphones/>.
[^4]: Autores da Wikipédia (5 de junho de 2023). <cite>List of material
    published by WikiLeaks</cite>.
    <https://en.wikipedia.org/wiki/List_of_material_published_by_WikiLeaks>.
[^5]: BBC News Mundo (26 de setembro de 2022). <cite>Snowden: Putin
    otorga la nacionalidad rusa al exagente estadounidense</cite>.
    <https://www.bbc.com/mundo/noticias-internacional-63039060>.
[^6]: Autores da Wikipédia (13 de abril de 2019). <cite>Ola Bini</cite>. <https://pt.wikipedia.org/wiki/Ola_Bini>.
[^7]: Rankin, K. (3 de julho de 2014). <cite>NSA: Linux Journal is an
    "extremist forum" and its readers get flagged for extra
    surveillance</cite>.
    <https://www.linuxjournal.com/content/nsa-linux-journal-extremist-forum-and-its-readers-get-flagged-extra-surveillance>.
[^8]: Greene, T. C. (2 de junho de 2001). <cite>Ballmer: 'Linux is a
    cancer'</cite>. <https://www.theregister.com/2001/06/02/ballmer_linux_is_a_cancer/>.
[^9]: Autores da Wikipédia (7 de abril de 2023). <cite>Microsoft and open source</cite>.
    <https://en.wikipedia.org/wiki/Microsoft_and_open_source>.
[^10]: Maldonado Ventura, J. (20 de fevereiro de 2023).
    <cite>Criptomoedas, anonimato, economia descentralizada</cite>.
    <https://freakspot.net/pt/Criptomoedas-anonimato-economia-descentralizada/>.
