Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2017-01-18 15:05
Lang: es
Modified: 2018-05-04 19:30
Slug: agua-insomne
Status: published
Tags: poesía, verso
Title: Agua insomne

<p>Ríos de pesadillas colapsan al despertar.<br>
En furia y letargo,<br>
bajo cascadas de mentiras,<br>
despierto dormido y absorto<br>
escuchando su flujo.</p>
<p>No me ahogan mis pesadillas;<br>
no me ponen de rodillas;<br>
la contracorriente renace;<br>
su fuente desbocada me transporta;<br>
su revolución me fortalece.</p>
