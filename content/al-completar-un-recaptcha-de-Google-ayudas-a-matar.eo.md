Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2018-03-17 23:55
Image: <img src="/wp-content/uploads/2018/03/recaptcha-usado-para-matar.png" alt="Completing a Google reCAPTCHA to train an artificial intelligence that will help US drones to kill">
Lang: eo
Slug: al-completar-un-recaptcha-de-google-ayudas-a-matar
Save_as: Plenumante-reCAPTCHA-n-de-Google-vi-helpas-mortigi/index.html
URL: Plenumante-reCAPTCHA-n-de-Google-vi-helpas-mortigi/
Tags: United States of America, Google, Pentagono, reCAPTCHA
Title: Plenumante reCAPTCHA-n de Google vi helpas mortigi

Google lastatempe asociis sin kun la
[Pentagono](https://eo.wikipedia.org/wiki/Pentagono_(Usono)) por helpi
ĝin disvolvi artefaritan intelekton. La projekto, nomita Maven,
konsistas en disvolvo de sistemo por identigi aĵojn per bildoj de
senpilotaj aviadiloj.

Tio signifas, ke la usona imperio uzos la povon de Google super la homoj
en la tuta mondo por siaj timigaj interesoj. Nun plenumi reCAPTCHA de
Google ne nur [signifas etikajn
danĝerojn](/eo/kiel-Google-ekspluatas-per-CAPTCHA-j/) rilatajn al ekonomia
ekspluatado kaj al la uzado de proprieta programaro, sed ankaŭ proksima
kunlaborado pri la mortigado de homoj. Mortoj, kiuj ŝajnas malhumanigitaj,
sed fine estas nekonsciaj (kaj konsciaj) homoj, kiuj trejnas maŝinojn por
mortigi.
