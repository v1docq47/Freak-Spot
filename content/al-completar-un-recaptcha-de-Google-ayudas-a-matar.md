Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2018-03-17 23:55
Image: <img src="/wp-content/uploads/2018/03/recaptcha-usado-para-matar.png" alt="Completando un reCAPTCHA de Google para entrenar a una inteligencia artificial que ayudará a drones estadounidendes a matar">
Lang: es
Slug: al-completar-un-recaptcha-de-google-ayudas-a-matar
Tags: Estados Unidos de América, Google, Pentágono, reCAPTCHA
Title: Al completar un reCAPTCHA de Google ayudas a matar

Recientemente, Google se ha asociado con [El
Pentágono](https://es.wikipedia.org/wiki/El_Pent%C3%A1gono) para
ayudarle en el desarrollo de inteligencia artificial. El proyecto, cuyo
nombre es Maven, plantea el desarrollo de un sistema para identificar
objetos a través de las imágenes de drones.

Esto significa que el poder que tiene Google sobre la gente en el mundo
entero lo utilizará el imperio estadounidense para sus oscuros
intereses. Completar un reCAPTCHA de Google ya no solo [supone peligros
éticos](/como-explota-Google-con-CAPTCHAs/)
relacionados con la explotación económica y el uso de <i lang="en">software</i>
privativo, sino también la estrecha colaboración en el asesinato de
personas humanas. Muertes que parecen deshumanizadas, pero al fin y al
cabo son personas inconscientes (y conscientes también) las que entrenan
a las máquinas para matar.
