Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-12-28 17:12
Lang: en
Slug: aliases-para-agilizar-tareas-en-bash
Save_as: aliases-to-streamline-tasks-in-Bash/index.html
URL: aliases-to-streamline-tasks-in-Bash/
Tags: .bashrc, .bash_aliases, alias, Bash settings
Title: Aliases to streamline tasks in Bash

Aliases, as the name suggests, are used to call a command by another
name. The command to which an alias is applied will work as if it had
been called directly. For example, if I want to go to the parent
directory with the command `..`, I only have to create an alias from the
terminal with the following command: `alias ..='cd ...'`.

You probably already have several aliases created and don't know it. If
you run `alias`, you will see the aliases you have already defined. These
aliases are defined in the `.bashrc` file, where you can add your own
aliases (remember to [reload the Bash configuration](/en/reload-Bash-configuration/) after adding them so that
you can start using them without restarting the computer). But if you
want to add a lot of them and you want to distinguish which ones are
yours, it is advisable to have them in a separate file.

In the `.bashrc` file you will probably find these lines or some similar ones:

    :::bash
    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.

    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

This means that every time you start Bash, it loads the aliases found in
the `~/.bash_aliases` file if it exists. If you don't have this file yet,
create it and add some aliases to help you in your day-to-day work. They
will save you a lot of time in the long run.

Here are some useful aliases:

    :::bash
    alias ....='cd ../../..'
    alias ...='cd ../..'
    alias ..='cd ..'
    alias install='sudo apt-get install'
    alias search='apt-cache search'
    alias update='sudo apt-get update && sudo apt-get upgrade'

I have a repository at
<https://notabug.org/jorgesumle/bash_aliases> with all my aliases, take a
look at it and copy the ones you find useful.
