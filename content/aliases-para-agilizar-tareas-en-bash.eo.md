Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-11-02 19:30
Lang: eo
Slug: aliases-para-agilizar-tareas-en-bash
Save_as: kromnomoj-por-rapidigi-laborojn-en-Bash/index.html
URL: kromnomoj-por-rapidigi-laborojn-en-Bash/
Tags: .bashrc, .bash_aliases, alias, alias Bash, kromnomo, Bash-agordaro
Title: Kromnomoj por rapidigi laborojn en Bash

La kromnomoj utilas por alvoki komandon per alia nomo. La komando, al
kiu oni aplikas kromnomon, funkcios kvazaŭ ĝi estus rekte alvokita.
Ekzemple, se mi volas iri al patra dosierujo per la komando `..`, mi nur
devas krei kromnomon en la terminalo per la jena komando:
`alias ..='cd ..'`.

Verŝajne vi jam havas kelkajn kromnomojn kaj vi ne scias pri tio. Se vi
plenumas `alias`, vi povos vidi la kromnomojn, kiuj jam estas difinitaj
por vi. Ĉi tiuj kromnomoj troviĝas difinitaj en la `.bashrc`-dosiero,
tie vi povas aldoni viajn proprajn kromnomojn (memoru [reŝarĝi la
agordaron de Bash](/eo/reŝarĝi-la-agordaron-de-Bash/), post kiam vi aldonis ilin, por ke vi povas ekuzi
ilin sen restartigi la komputilon). Sed se vi volas aldoni multajn kaj
volas distingi, kiuj estas viaj, rekomendindas havi ilin en aparta
dosiero.

En la `.bashrc`-dosiero vi verŝajne trovos ĉi tiujn liniojn aŭ iujn
similajn:

    :::bash
    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.

    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

Ĉi tio signifas, ke ĉiufoje, kiam vi komencas Bash, la kromnomoj
trovitaj en la dosiero `~/.bash_aliases` estos ŝarĝitaj, se ili ekzistas.
Se vi ankoraŭ ne havas ĉi tiun dosieron, kreu ĝin kaj aldonu kelkajn
kromnomojn, kiuj helpu vin en via ĉiutaga laboro. Longtempe ili ŝparos
al vi multege da tempo.

Sekve mi montras al vi kelkajn utilajn kromnomojn. Mi havas deponejon en
<https://notabug.org/jorgesumle/bash_aliases> kun ĉiuj miaj kromnomoj,
vidu ĝin kaj kopiu tiujn kromnomojn utilajn por vi.

    :::bash
    alias ....='cd ../../..'
    alias ...='cd ../..'
    alias ..='cd ..'
    alias install='sudo apt-get install'
    alias search='apt-cache search'
    alias update='sudo apt-get update && sudo apt-get upgrade'
