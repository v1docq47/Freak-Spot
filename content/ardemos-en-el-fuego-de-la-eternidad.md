Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2022-12-23 22:12
Lang: es
Slug: arderemos-en-el-fuego-de-la-eternidad
Tags: poesía, verso
Title: Arderemos en el fuego de la eternidad
Image: <img src="/wp-content/uploads/2022/12/fuego.webp" alt="" width="1200" height="800" srcset="/wp-content/uploads/2022/12/fuego.webp 1200w, /wp-content/uploads/2022/12/fuego.-600x400. 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Tú y yo somos el bosque,<br>
con sus ramas, con sus sombras,<br>
con sus luces, con el viento,<br>
que desencadena el huracán.

Tú y yo, en abrazo eterno...<br>
arderemos en el fuego de la eternidad.<br>

`git commit -m "Te quiero"`

<small>Porque seremos recordados por nuestras acciones. 🔥</small>
