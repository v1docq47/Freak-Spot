Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2020-02-07
Lang: de
Slug: arreglar-o-matar-el-javascript-instalado-automáticamente
Save_as: das-automatisch-installierte-JavaScript-reparieren-oder-töten/index.html
Tags: Abrowser, Rat, Kritik, Programmierer, Google Chrome, Firefox, Icecat, Iceweasel, Internet, JavaScript, Browser, Webseiten, Freie Software, Richard Stallman, Trisquel, Windows
Title: Das automatisch installierte JavaScript reparieren oder töten?
URL: das-automatisch-installierte-JavaScript-reparieren-oder-töten/

*Diese Artikel ist eine Übersetzung von dem englischen Artikel
[«Fix or Kill Automatically Installed JavaScript?»](https://web.archive.org/web/20180614034938/https://onpon4.github.io/articles/kill-js.html),
von Julie Marchant unter einer
<a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de"><abbr title="Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International">CC BY-SA 4.0</abbr></a>
Lizenz veröffentlicht.*

Der Aufsatz von Richard Stallman „Die JavaScript Falle“ (auf English
«*The Javascript trap*») zeigt, dass die Menschen proprietäre Programme
ausführen, die täglich automatisch, heimlich in ihren Computer
installiert sind.  Eigentlich herunterspielte er sehr das Problem; nicht
nur führen täglich die meisten Nutzer proprietäre Software nur beim
Surfen aus, sondern auch führen sie jeden Tag Dudzende or sogar Hunderte
von solchen Programmen aus. Die JavaScript Falle ist ganz real und
fruchtbar; man sagt, die Netz sie so kaputt ohne diesen nicht
standarisierten, normalerweise proprietäre Erweiterung von HTML, dass
Browser jetzt keine eindeutige Option verfügen, um JavaScript zu
deaktivieren; die Deaktivierung von JavaScript, behauptet man, wird nur
Verwirrung stiften.

Es ist klar, dass wir dieses Problem lösen sollen. Trotzdem, wenn er
darauf konzentriert, ob die Scripts „triviale“ oder freie sind, Hr.
Stallman verfehlt das Ziel: dieses Verhalten von automatischer,
heimlicher Software-Installierung ist selbst das Hauptproblem. Dass fast
alle Software proprietär ist, ist nur ein Nebeneffekt.

<!-- more -->

Als Antwort auf dem Artikel von Hr. Stallman, eine Erweiterung für
Firefox und Firefox abgeleitete Browser, gennant LibreJS, wurde
entwickelt. Diese Erweiterung analisiert automatisch alle
JavaScript von einer Webseite, um zu bestimmen, ob es trivial oder libre
ist; wenn eine dieser Voraussetzungen als richtig bestimmt ist, die
Software ist ausgeführt. Sonsts ist es blockiert. Ich schätze das
LibreJS Projekt und was es versucht. Aber ich denke, dass LibreJS der
falsche Ansatz ist, um das Problem zu lösen.

Nun scheitert LibreJS, weil es ein Format braucht, das nicht überall
erkannt ist, aber theoretisch könnte das in der Zukunft gelöst werden,
nehmen wir mal an es wird. Nehmen wir mal noch weiter an, dass LibreJs
so erforlreich ist, dass ein Großteil der Netz Scripts unter freie
Lizenzen veröffentlicht und die Lizenzen in einem Format, das LibreJS
versteht, dokumentiert.

Es scheint gut nach außen hin, aber was folgt darauf ist, dass Software
ist noch heimlich installiert in unseren Browser jeden Tag. Die einzige
Unterschied ist, dass LibreJS denkt, die Programme sind frei.

Ich möchte nicht die Wichtigkeit herunterspielen, dass alle Software
frei sei. Trotztdem wenn irgendeine Software automatisch in unserer
Computer auf Wunsch anderer Partei installiert wird, macht es fast
unmöglich, die Freiheit 0 auszuüben. Es wird angenehmt, dass du alle
diese JavaScript Programme willst, die betragen Hundert von Scripts
jeden Tag, die in deiner Computer ausgeführt werden, normalerweise bevor
du sogar die Gelegenheit hast, die Quellcode zu überprüfen.

Noch schlimmer, das System automatisierter JavaScript-Installierung
installiert nur das Software zeitweilig, um einmal ausgeführt zu werden.
In der Tat, jedes Mal, wenn das Server aktualisiert einen JavaScript
Programm, wird diese Aktualisierung gegen die Nutzer durchgesetzt. Auch
wenn das Script frei wäre, es ist, als ob es eine Hintertür eingebaut
hätte.

Das ist sehr vergleichbar mit dem Fall von Tivoisierung, in dem du
magst theoretisch die Freiheit haben, zu kontrollieren, was ein Programm
macht, aber du kannst das nicht wegen der Umstände in der Praxis machen.
Es ist nicht genug theoretische Kontrolle zu haben. Richtige Kontrolle
ist auch nötig. In der JavaScript Fall ist nicht diese Mangel an
Kontrolle die Folge der Böswilligkeit, sondern eher eine Folge der
nachlässige Annahme, dass der Nutzer die Ausführung jeder
Script wünscht, die eine Webseite empfehlen könnte. Das ist nicht
unbedingt richtig. Es wäre als ob Windows in meiner Maschine installiert
würde, jeden Tag ich einen Artikel lese, die der Nutz von Windows
emphfehlt, oder als ob jeder Blogeintrag die spricht über, wie toll
Chrome ist, die automatische Installierung von Chrome verursachen würde.

Was können wir machen? Ich erkenne zwei möglichen Lösungen.

## 1. Lösung: JavaScrpit zu reparieren

Die erste mögliche Lösung, und die klarste, ist das Verhalten des
Browsers gegenüber die Anfragen von JavaScript Programmen zu ändern. Ich
schlage vor, dass damit das System akzeptabel ist, müssen die folgende
Voraussetzungen erfüllen werden:

- Das Browser muss die JavaScript Code dauerhaft installieren, und nur
  wenn der Nutzer erlaubt das irgendwie.
- Das Browser muss den Nutzer erlauben, Scripts und Programme zu
  installieren, nicht nur die Scripts von der Webseite angefragt.
- Das Browser muss keine JavaScript Code automatisch aktualisieren,
  außer wenn der Nutzer bestimmt das, und der Nutzer muss in der Lage
  sein, wovon solche Aktualisierungen kommen, zu wählen.

Du wirst anmerken, dass die automatische Lizenz-Detektion ist nicht
da enthalten. Wie bekommt denn ein Nutzer nur freie JavaScript Code,
ohne jede Quellcode Datei zu kontrollieren? Die Lösung ist eigentlich
sehr einfach: wie andere freie Software. Ich traue die Entwickler von
Trisquel nur freie Software ohne schädliche Funktionen im Repository von
Trisquel hinzuzufügen. Übrigens können die Programmierer von Trisquel
die Nutzer von Trisquel gegen Schadsoftware, proprietäre oder nicht,
schützen; LibreJS nicht. Ebenso können wir ein Repository von freie
JavaScript Quellcode erstellen und warten.

Um das zu schaffen, die installierte JavaScript Programme müssten auch
in jeder Webseite funktionieren, die das anfragt, nicht nur in einer
Seite. Welche schon installierte JavaScript Code zu nutzen, kann man
bestimmen, in dem man einem Hash von verkleinerte installierte
JavaScript Code bekommt und dann einem Hash von angefragte Scripts
bekommt, danach sie ebenso verkleinert sind. Falls das keine
Entsprechung liefert, kann der Dateiname von Scripts nach Entsprechungen
oder quasi-Entschprechungen suchen, und kann der Nutzer gefragt werden,
ob man diese Scripts genutzt werden sollten. Eine Art von Datenbank im
Browser des Nutzers, die bestimmte Webseiten angibt, wo man Scripts
benutzen soll, könnte auch hilfreich sein.

Ich nehme an, dass diese Vorgehen viel Mühe brauchen würde, und darum
hat es nicht möglicherweise der Entwickler von LibreJS versucht. Es
hilft nicht, dass das *verlässlich* durchzuziehen, würde ständige Arbeit
bedeuten, um die Änderungen der Webseiten auf dem neuesten Stand zu
halten.

## 2. Lösung: JavaScript zu töten

Als ich schlägte etwas wie die 1. Lösung in der bug-gnuzilla
Mailingliste vor, eine Antwort sagte, dass es eine einfachere Lösung
gibt: anstatt JavaScript zu reparieren, könnten wir einfach die
JavaScript Ausführung in unseren Browser völlig deaktivieren (anders
gesagt, JavaScript zu töten). Natürlich meine ich eigentlich automatisch
installierte JavaScript. Es gibt nichts schlecht mit dem heutigen Nutz
von JavaScript, um neue Firefox Erweiterungen zu entwickeln, zum
Beispiel. Nutzer-Scripts und Erweiterungen können sogar entwickelt
werden, um wichtige proprietäre JavaScript Code zu ersetzen.

Trotzdem ist diese Lösung nicht problemlos. Insbesondere erfordert das
zwar eine riesige soziale Veränderung, aber kleiner als was LibreJS
versucht. Browser, die entfernen JavaScript-Unterstützung, können
bei diesem Plan helfen, aber es gibt ein Henne-Ei-Problem, in dem Sinne,
dass Browser ohne JavaScript-Unterstützung nur als unterlegen betrachtet
werden, insofern viele Webseite benötigen es.

Ein möglicher Mittelweg zu diesem Zweck kann sein, ein Browser unterstützt
JavaScript, hat es aber standardmäßig ausgeschaltet und gibt den Nutzern
ein einfachstes Mittel, vorläufig JavaScript auszuführen auf einer
Seite; so wird der Nutzer eine freie von JavaScript Erfahrung, aber noch
die Möglichkeit, JavaScript zu nützen auf Webseiten, die es benötigen,
ohne so viele Unannehmlichkeiten, dass das den Browser unbrachbar
verwandelt. Es würde sogar den netten Nebeneffekt, die Browser-Erfahrung
generell reibungsloser für Nutzer machen; viele Webseiten haben enorme
JavaScript Scheiße, die nur beim deaktivierung von JavaScript ganz
vermeiden werden kann.

## Fazit

Alle diser Maßnahmen haben Vor- und Nachteile.

Die erste Lösung kann sofort für Dinge wie Diaspora und Reddit zu guten
Ergebnissen führen: Webseiten dessen JavaScript-Code benötigt ist, aber
meistens frei. Es würde vermutlich kleine oder gar keine Änderung in das
Web bringen, aber es bracht es nicht, um zu funktionieren. Trotzdem es
würde etwas Arbeit, das Browser Benehmen in bezug zu JavaScript zu
einstellen, und es würde viel mehr mühsam ein Repository freier
JavaScript-Programmen zu unterhalten.

Die zweite Lösung ist gar ähnlich zu das, was LibreJS nun versucht, aber
in einem viel kleineren Umfang. Es hängt von einer Änderung des Webs:
man muss die Mehrheit von Web-Entwickler überzeugen, keine
JavaScript-Code mehr vorauszusetzen. Wenn es funktioniert, kann es
spektakulär funktionieren. Andererseits kann es spektakulär schietern
oder nur dazu führen, noch eine andere Methode automatisch, heimlich
Software in Browser der Nutzer zu installieren, die populär wird.

Ich bin mir nicht sicher, welche besser ist, aber LibreJS ist noch eine
Lösung weder ein Patch, weder auch ein Schritt in die richtige Richtung.
Bis ein freies Browser, das richtig JavaScript repariert, verfügbar ist,
wer auch immer, der Freiheit in seinem Computer will, muss jede normale
JavaScript-Ausführung in seinem Browser deaktivieren, auch wenn die Code
frei ist, und Web-Entwicker, die wollen die Freiheit ihrer Nutzer
respektieren, müssen arbeiten, um jede JavaScript-Voraussetzung von
ihrer Webseiten zu entfernen.
