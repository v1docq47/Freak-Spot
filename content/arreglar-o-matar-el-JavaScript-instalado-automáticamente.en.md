Author: Julie Marchant
Category: Opinion
Date: 2019-10-20
Lang: en
Slug: arreglar-o-matar-el-javascript-instalado-automáticamente
Save_as: fix-or-kill-automatically-installed-JavaScript/index.html
Tags: Abrowser, advice, criticism, programmers, Google Chrome, Firefox, Icecat, Iceweasel, Internet, JavaScript, browsers, webpages, free software, Richard Stallman, Trisquel, Windows
Title: Fix or kill automatically installed JavaScript?
URL: fix-or-kill-automatically-installed-JavaScript/

*This article was
[first published](https://web.archive.org/web/20180614034938/https://onpon4.github.io/articles/kill-js.html)
by Julie Marchant under the license <a href="https://creativecommons.org/licenses/by-sa/4.0/"><abbr title="Creative Commons Attribution-ShareAlike 4.0 International">CC BY-SA 4.0</abbr></a>.*

In Richard Stallman's essay, "The JavaScript Trap", it is pointed out that people run proprietary software which is silently, automatically installed into their browsers every day. In fact, he very much downplayed the problem; not only are most users running proprietary programs every day merely by browsing the Web, they are running dozens or even hundreds of such programs each day. The JavaScript Trap is very real and prolific; the Web is said to be so broken without these non-standard, usually proprietary extensions to HTML that Web browsers have moved toward not even offering an obvious option to disable JavaScript; disabling JavaScript, it is argued, will only cause confusion.

It's obvious that we need to solve this problem. However, in focusing on whether or not scripts are "trivial" or libre, Mr. Stallman misses a crucial point: this behavior of automatic, silent software installation is, itself, the main problem. That most of the software in question is proprietary is merely a side-effect.

<!-- more -->

In response to Mr. Stallman's article, an extension for Firefox and
Firefox-derived browsers called LibreJS was developed. This extension
automatically analyzes all JavaScript on a page to determine whether or
not they are either trivial or libre; if one of these conditions is
determined to be true, the software is executed. Otherwise, it is
blocked. I appreciate the LibreJS project and what it's trying to do.
But I think that LibreJS is fundamentally the wrong approach to solving
the problem.

Right now, LibreJS is failing because it requires a format that isn't
recognized anywhere, but theoretically, this could be solved in the
future, so let's suppose that it does. Let's suppose even further that
LibreJS succeeds so much that it causes a large portion of the Web to
release scripts under libre licenses and document the licenses in a
format LibreJS can understand.

It seems great on the surface, but what follows from this is that
software is still being silently installed into our browsers every day.
The only difference is that LibreJS thinks the programs are libre.

I don't want to downplay the importance of all software being libre.
However, when any software is automatically installed onto our computers
by the request of another party, it makes exercising freedom 0 properly
impossible. It is assumed that you want all of these JavaScript
programs, which can easily amount to hundreds of new scripts each day,
to be executed on your computer, typically before you even have a chance
to check the source code.

Even worse, the system of automatic JavaScript software installation
only installs the software temporarily, to be executed once. In effect,
whenever the server updates a JavaScript program that is sent to users'
Web browsers, that update is imposed on the users. Even if the script is
libre, it's as if it has a backdoor built-in.

This is very similar to the case of tivoization, where you may
theoretically have the freedom to control what a program does, but
cannot do so in practice because of the circumstances. It's not enough
to have theoretical control. Actual control is also needed. In the case
of JavaScript, this lack of control is not a result of malice, but
rather a result of Web browsers carelessly assuming that the user
desires the execution of every script a Web page might recommend. This
is not necessarily true. It would be like if Windows was installed on my
machine every time I read an article recommending the use of Windows, or
if every blog post talking about how great Chrome is resulted in the
automatic installation of Chrome onto my system.

So what can we do? I know of two possible solutions.

## Solution 1: Fix JavaScript

The first possible solution, and the most obvious one, is to modify the
behavior of Web browsers with regard to JavaScript software requests. I
propose that for the system to be acceptable, all of the following
conditions MUST be met:

- The browser must install JavaScript code permanently, and only when
  the user explicitly authorizes it in some fashion.
- The browser must give the user the ability to install any arbitrary
  script, not just the script requested by the Web page.
- The browser must not upgrade any JavaScript code automatically, unless
  the user has specified that it should be, and the user must be able to
  choose where such updates come from.

You will notice that automatic license detection isn't included in any
of this. So how does a user get only libre JavaScript without manually
checking every source file? The solution is actually quite simple: the
same way as any other libre software. I trust the developers of Trisquel
to only include libre software with no malicious features in the
Trisquel repository. Incidentally, Trisquel's developers can actually
protect Trisquel's users from malware, proprietary or not; LibreJS
can't. Similarly, we can create and maintain a repository of libre
JavaScript code.

For this to work, installed JavaScript programs should also work for all
Web pages that request it, not just one page. What already installed
JavaScript code to use by default can be determined by getting a hash of
minified versions of installed scripts, and then getting a hash of
requested scripts after minifying them the same way. If that doesn't
return a match, the file names of the scripts can be checked for matches
or near-matches, and the user can be asked if these scripts should be
used. Some kind of database in the user's browser specifying websites
certain scripts should be used on would also be helpful.

I suppose this approach would take a considerable amount of effort, and
this is probably why the developer of LibreJS hasn't attempted it. It
doesn't help that getting this to work *reliably* would involve constant
work keeping up with changing Web pages.

## Solution 2: Kill JavaScript


When I proposed something like Solution 1 on the bug-gnuzilla mailing
list, one reply noted that there's a much simpler solution: rather than
trying to fix JavaScript, we could just disable JavaScript execution in
our browsers entirely (in otherwords, kill JavaScript). Of course, I
really mean automatically installed JavaScript. There is nothing wrong
with the current use of JavaScript to develop Firefox extensions, for
example. User scripts and extensions can even be developed to replace
important proprietary JavaScript code.

Still, this solution is not without its problems. In particular, this
requires a massive social change, albeit less of a change than what
LibreJS is attempting. Browsers removing JavaScript support can help
this plan along, but there's a chicken and egg problem in the sense that
browsers without JavaScript support will just be seen as inferior while
many websites require it.

One possible middle ground to this end can be for a browser to support
JavaScript, but have it turned off by default and give the user an easy
way to temporarily enable JavaScript execution on a single page; that
way, the user will get a JavaScript-free experience, but still have the
option to use JavaScript for pages that demand it without so much
inconvenience that it renders the browser unwieldy. It would even have a
nice side effect of making the browsing experience generally smoother
for users; a lot of websites have tremendous JavaScript bloat that can
be avoided entirely just by disabling JavaScript.

## Conclusion

Each of these approaches has strengths and weaknesses.

The first solution can yield good results immediately for things like
Diaspora and Reddit: websites whose JavaScript code is required, but
mostly libre. It would probably not bring about much if any change in
the Web, but it doesn't need to in order to work. However, it would
require some work to adjust a browser's behavior with respect to
JavaScript appropriately, and it would be a lot more work to maintain a
repository of libre JavaScript programs.

The second solution is rather similar to what LibreJS is currently
trying to do, albeit on a much smaller scale. It depends on changing the
Web: convincing the majority of Web developers to stop requiring
JavaScript code. If it works, it could work spectacularly. On the other
hand, it could easily fail spectacularly, or just result in yet another
method to automatically, silently install software into users' browsers
becoming popular.

I'm not sure which is better, but LibreJS is neither a good solution,
nor a good temporary fix or even a step in the right direction. Until a
libre browser which properly fixes JavaScript becomes available, anyone
who wants freedom in their computing must disable all normal JavaScript
execution in their browsers, even if the code is libre, and Web
developers who want to respect their users' freedom should work to
eliminate all JavaScript requirements on their websites.
