Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2019-01-02 19:35
Lang: eo
Slug: arreglar-o-matar-el-javascript-instalado-automáticamente
Save_as: ĉu-ripari-aŭ-forigi-la-aŭtomate-instalitan-Ĝavoskripton/index.html
Tags: Abrowser, konsilo, kritiko, programistoj, Google Chrome, Firefox, Icecat, Iceweasel, Interreto, Ĝavoskripto, retiloj, retejoj, libera programaro, Richard Stallman, Triskelo, Vindovo
Title: Ĉu ripari aŭ forigi la aŭtomate instalitan Ĝavoskripton?
URL: ĉu-ripari-aŭ-forigi-la-aŭtomate-instalitan-Ĝavoskripton/

*Ĉi tiu artikolo estas traduko el angla de artikolo [«Fix or Kill
Automatically Installed
JavaScript?»](https://web.archive.org/web/20180614034938/https://onpon4.github.io/articles/kill-js.html),
publikigita
de Julie Marchant sub permesilo
<a href="https://creativecommons.org/licenses/by-sa/4.0/"><abbr title="Creative Commons Atribuite-Samkondiĉe 4.0 Tutmonda">CC BY-SA 4.0</abbr></a>.*

En la eseo de Richard Stallman «La kaptilo de Ĝavoskripto» (angle «*The
Javascript trap*») estas montrita, ke la homoj plenumas proprietajn
programojn, kiuj estas aŭtomate, silente instalitaj en iliaj retiloj
ĉiutage. Fakte li grande reduktis gravecon al problemo; ne nur la
plejparto el la uzuloj plenumas proprietajn programojn ĉiutage nur
retumante en la Reto. La Kaptilo de Ĝavoskripto estas tre reala kaj
abunda; oni diras, ke la Reto estas tiel difektita sen tiuj mallaŭnormaj
HTMLaj kromaĵoj, normale proprieta, kiel la retiloj jam ne eĉ oferas
videblan opcion por malebligi Ĝavoskripto; malebligi Ĝavoskripton, oni
argumentas, nur kaŭzos konfuzon.

Estas evidenta, ke ni bezonas solvi tiun problemon. Tamen koncentrante
sin pri ĉu skriptoj estas «triviala» aŭ libera, S-ro Stallman forgesas
kernan aspekton: ĉi tiu aŭtomata, silenta agmaniero de programa
instalado mem estas la kerna problemo. Ke la plej da koncernaj programoj
estas proprieta, estas nure kromefiko.

<!-- more -->

Respondante al artikolo de la S-ro Stallman, kromprogramo por Firefox
kaj retiloj devenitaj de Firefox nomita LibreJS estis programita. Ĉi tiu
kromprogramo analizas aŭtomate la tutan Ĝavoskripton en unu paĝo por
determini, se ĝi estas triviala aŭ libera; se unu el ĉiu kondicioj estas
determinita kiel vera, la programo estas plenumita. Kontraŭe ĝi estas
blokita. Mi aprezas la projekton de LibreJS kaj kion ĝi klopodas. Sed mi
pensas, ke LibreJS estas fundamente la erara maniero por solvi la
problemon.

Nun LibreJS fiaskas, ĉar ĝi bezonas aranĝon, kiu ne estas rekonita ie
ajn, sed teorie tio povus solviĝi estonte, do ni supozu tion. Iru ni eĉ
pli fore kaj supozu, ke LibreJS havas tiel sukceson, ke kaŭzas, ke
granda parte de la Reto eldonu skripton sub liberaj permesiloj kaj
dokumentas la permesilojn en aranĝo komprenebla de LibreJS.

Ĝi ŝajnas bonege en simpla vido, sed la sekvo estas, ke la programoj
estadas instalitaj silente en niaj retiloj ĉiutage. La nura diferenco
estas, ke LibreJS pensas, ke la programoj estas liberaj.

Mi ne volas minimumigi la gravecon pri, ke la tutaj programoj estu
liberaj. Tamen, kiam ajn programaro estas instalita aŭtomate en niaj
komputiloj laŭ la peto de alia persono, tio faras ekzerci la liberano
0 dece malebla. Oni premisas, ke vi volas ĉiom da tiuj Ĝavoskriptaj
programoj, kiuj povas facile sumi centojn de novaj skriptoj ĉiutage, por
esti plenumita en via komputilo, tipe antaŭ vi eĉ havas okazon por
kontroli la fontkodon.

Eĉ pli malbone, la aŭtomata sistemo de instalado de programaroj nur
instalas la programaro kelktempe, por esti plenumita unufoje. Efike,
kiam ajn la servilo ĝisdatigas Ĝavoskriptan programon, kiu estas sendita
al la uzulaj retiloj, tiu ĝisdatigo estas impozita al la uzuloj. Eĉ
se la skripto estas libera, kiel se ĝi havas integritan kaŝenirejon.

Tio estas simile al afero pri "tiviĝo" (angle tivoization), kie vi povas
teorie havi la liberecon de kontroli, kion faras la programon, sed vi ne
povas praktike kaŭze de la cirkonstancoj. Ne estas sifuĉa havi la
teorian kontrolon. La reala kontrolo estas ankaŭ bezonata. En la afero
de Ĝavoskripto, ĉi tiu manko de kontrolo ne estas maliĉe farita, sed pli
ĝuste rezulto de retaj retiloj supozante malzorge, ke la uzulo deziras
la plenumo de ĉiu skripto, kiun Reto-paĝaro povas rekomendi. Tio ne
estas necese vera. Ĝi estus kiel se Vindovo estu instalita en mia
maŝino ĉiukaze mi legu artikolon, ke rekomendu la uzon de Vindovo, aŭ
kiel se ĉiu blogaĵo, kiu parolas pri kiel bona estas Chrome okazigu
aŭtomate la instaladon de Chrome en mia sistemo.

Kion ni povas fari? Mi konas du eblajn solvojn.

## 1a Solvo: Repari Ĝavoskripto

La unua ebla solvo, kaj la pli evidenta, estas aliigi la agmanieron de
la Retaj retiloj respekte pri la petoj de Ĝavoskripta programaro. Mi
proponas, ke, por ke la sistemo estas akceptebla, **devas** esti
plenumitaj la sekvajn kondiĉojn:

- La retilo devas instali la Ĝavaskriptan kodon daŭre, kaj nur se la
  uzulo permesas ĝin specife iamaniere.
- La retilo devas ebligi al uzulo instali ajn skripton, ne nur la
  skripton petita de la retpaĝo.
- La retilo ne debas ĝisdatigi neniun Ĝavoskriptan kodon aŭtomate,
  escepte se la uzulo precizigis tion, kaj la uzulo devas povi elekti el
  kie venas tiuj ĝisdatigoj.

Vi rimarkos, ke la aŭtomata permesila detekto ne estas inkludita en neniu
el tiuj. Kiel uzulo havigas nur liberan Ĝavoskripton sen kontroli
permane ĉiun fontkodan dosieron? La solvo estas fakte tre simpla: same
kiel ajn alia libera programaro. Mi konfidas, ke la programistoj de
Triskelo nur inkludos liberan programaron sen fiaj funkciaroj en la
deponejo de Triskelo. Kunokaze la programistoj de Triskelo povas
protekti la Triskelajn uzulojn kontraŭ fiprogramoj, proprieta aŭ ne;
LibreJS ne. Simile, ni povas krei kaj funkciteni deponejon de libera
Ĝavoskipta kodo.

Por funkciigi tion, la Ĝavoskriptaj instalitaj programoj devus ankaŭ
funkcii en ĉiu Retpaĝo, kiu ĝin petas, ne nur en unu paĝo. Kion jam
instalitan Ĝavoskriptan kodon uzi defaŭlte povas esti determinita
akirante haketon de la malpliigitaj instalitaj skriptoj, kaj poste
akirante haketon de petitaj skriptoj post malpliigi ilin same. Se tio ne
liveras trafon, la dosiernomoj de la skriptoj povas esti kontrolita
serĉante trafojn aŭ kvazaŭtrafojn, kaj oni povas demandi al la uzulo, se
devas la skriptoj esti uzitaj. Ajn tipo de datumbazo en la uzula retilo
specifante retejojn, en kiuj iajn skriptojn devas esti uzitaj, estus
ankaŭ utila.

Mi supozas, ke ĉi tiu maniero kaŭzu konsiderindan kiomon da penon,
kaj tio estas verŝajne la kialo, por ke la programisto de LibreJS ne
klopodis ĝin. Ne helpas, ke funkciigi tion *fideble* signifu daŭran
laboron por ĝisdatigi la aliigojn de la Retejoj.

## 2a Solvo: Forigi Ĝavoskripton

Kiam mi proponis ion kiel 1a solvo en la dissendolisto bug-gnuzilla,
respondo diris, ke ekzistas solvo tre pli simpla: anstataŭ ripari
Ĝavoskripton, ni povus simple malŝalti la plenumon de Ĝavoskripto tute
(alidire forigi Ĝavoskripton). Kompreneble, vere mi povas diri la
*aŭtomate instalitan* Ĝavoskripton. Ne estas nenio malbona en la ĉitaga
uzo de Ĝavoskripto por programi kromaĵojn de Firefox, ekzemple. Oni
povas eĉ programi skriptojn kaj kromaĵojn de uzuloj por anstataŭigi
gravan Ĝavoskriptan proprietan kodon.

Tamen en ĉi tiu solvo ne mankas problemoj. Konkrete, tio bezonas masivan
socian aliigon, sed pli malgranda ol tiu, kiun LibreJS klopodas. Retiloj
forigantaj subtenon de Ĝavoskripto povas helpi efektivigi la planon, sed
estas problemo de ovo kaj kokino en la racio, ke multaj retiloj sen
subteno por Ĝavoskripto estos rigarditaj kiel malsupera, dum multaj
retejoj bezonas ĝin.

Ebla intermeza punkto por tiu fino povas esti, ke retilo subtenu
Ĝavoskripton, sed ĝi havu ĝin maleblita defaŭlte kaj permesu al la
uzulo facilan manieron de ebligi portempe la plenumon de Ĝavoskripto en
unu sola pâgo; tiel la uzulo havos sperton sen Ĝavoskripto, sed ankoraŭ
havos la opcion de uzi Ĝavoskripton por paĝoj, kiuj bezonas ĝin sen tiel
granda maloportunaĵo kiel ĝi iĝas la retilo malmulte uzebla. Eĉ ĝi havus
la bonan kromefekton iĝi la retuma sperto ĝenerale glata por uzuloj;
multaj retejoj havas ega Ĝavoskripta ŝvelo, kiu povas esti evitita plene
nur malebligante Ĝavoskripton.

## Konkludo

Ĉiuj el tiuj agmanieroj havas avantaĝojn kaj malavantaĝojn.

La unua solvo povas doni bonajn rezultojn tuj por aĵoj kiel Diaspora
kaj Reddit: retejoj kies Ĝavoskripta kodo estas bezonata, sed plejparte
libera. Eble tio ne estigus grandan aliigon en la Reto (se eĉ ion), sed
ĝi ne bezonas fari tion por funkcii. Tamen bezonus laboron agordi la
agmanieron de retilo pri Ĝavoskripto laŭcele, kaj estus tre pli laborema
ĝisdatigi deponejon de Ĝavoskriptaj liberaj programoj.

La dua solvo estas tre simila ol kiu LibreJS klopodas fari nune, sed en
malpli grandeco. Tio dependas de aliigi la Reton: konvinkante al la
plejparto da Retaj programistoj pri ĉesi bezonigi Ĝavoskriptan kodon. Se
tio funkcius, ĝi povus funkcii spektakle. Aliflanke tio povus fiaski
senpene spektakle aŭ simple krei alian ekstran manieron de instali
programaron silente kaj aŭtomate en la retiloj de uzuloj, kiu iĝas
populara.

Mi ne estas certa pri kio estas pli bona, sed LibreJS ne estas nek bona
solvo, nek bona tempa riparo, nek eĉ paŝo al la korekta direkto. Ĝis
libera retilo, kiu riparu Ĝavoskripton adekvate estu disponebla, ĉiu
kiu volas liberecon en sia komputilo devos malebligi ĉiun normalan
plenumon de Ĝavoskripto en siaj retiloj, eĉ se la kodo estas libera, kaj
la Retaj programistoj, kiuj volas respekti la liberecon de siaj uzuloj,
devos labori por forigi ĉiujn bezonaĵojn de Ĝavoskripto el siaj paĝaroj.
