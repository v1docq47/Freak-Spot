Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2018-05-02 01:52
Modified: 2018-09-04 02:49
Lang: es
Slug: arreglar-o-matar-el-javascript-instalado-automáticamente
Tags: Abrowser, consejos, crítica, desarrolladores, Google Chrome, Firefox, Icecat, Iceweasel, Internet, JavaScript, navegadores, páginas web, software libre, Richard Stallman, Trisquel, Windows
Title: ¿Arreglar o matar el JavaScript instalado automáticamente?

*Este artículo es una traducción del inglés del artículo
[«Fix or Kill Automatically Installed JavaScript?»](https://web.archive.org/web/20180614034938/https://onpon4.github.io/articles/kill-js.html) publicado
por Julie Marchant bajo la licencia
<a href="https://creativecommons.org/licenses/by-sa/4.0/"><abbr title="Creative Commons Attribution-ShareAlike 4.0 International">CC BY-SA 4.0</abbr></a>.*

En el ensayo de Richard Stallman, «La Trampa de JavaScript», se señala
que la gente ejecuta <i lang="en">software</i> privativo que es silenciosamente,
automáticamente instalado en sus navegadores cada día. De hecho, él
restó importancia en gran medida al problema; no solo la mayoría de
usuarias está ejecutando programas privativos cada día meramente
navegando la Red, están ejecutando docenas o incluso cientos de tales
programas cada día. La Trampa de JavaScript es muy real y prolífica; se
dice que la Red está tan rota sin estas extensiones de HTML no
estándares, normalmente privativas, que los navegadores han pasado a ni
siquiera ofrecer una opción obvia para deshabilitar JavaScript;
deshabilitar JavaScript, se argumenta, solo causará confusión.

Es obvio que necesitamos resolver este problema. Sin embargo, al
centrarse en si los guiones son «triviales» o libres, el señor Stallman
olvida un punto importante: este comportamiento de instalación de
<i lang="en">software</i> automático, silencioso es, en sí, el principal problema. Que la
mayoría del <i lang="en">software</i> en cuestión sea privativo es meramente un efecto
secundario.

<!-- more -->

En respuesta al artículo del señor Stallman, se desarrolló una extensión
para Firefox y navegadores derivados de Firefox llamada LibreJS. Esta
extensión analiza automáticamente todo el JavaScript en una página para
determinar si es trivial o libre; si se determina que una de estas
condiciones es verdadera, se ejecuta el <i lang="en">software</i>. De lo contrario, se
bloquea. Aprecio el proyecto LibreJS y lo que está intentando hacer.
Pero creo que LibreJS es fundamentalmente el enfoque incorrecto para
resolver el problema.

Ahora, LibreJS está fallando porque requiere un formato que no es
reconocido en cualquier sitio, pero teóricamente, esto podría resolverse
en el futuro, así que supongamos que se hace. Vayamos incluso más lejos
y supongamos que LibreJS tiene tanto éxito que hace que una gran parte
de la Red publique guiones bajo licencias libres y documenta las
licencias en un formato que LibreJS puede entender.

Parece genial a simple vista, pero la consecuencia de esto es que el
<i lang="en">software</i> sigue siendo instalado silenciosamente en nuestros navegadores
cada día. La única diferencia es que LibreJS piensa que los programas
son libres.

No quiero restar importancia a que todo el <i lang="en">software</i> sea libre. Sin
embargo, cuando cualquier <i lang="en">software</i> se instala automáticamente en
nuestros ordenadores por la petición de otra parte, hace ejercer la
libertad 0 correctamente imposible. Se asume que quieres todos esos
programas JavaScript, los cuales pueden ascender a cientos de nuevos
guiones cada día, que serán ejecutados en tu ordenador, típicamente
antes de que incluso tengas ocasión de comprobar el código fuente.

Aún peor, el sistema de instalación de <i lang="en">software</i> automático solo instala
el <i lang="en">software</i> temporalmente, para ser ejecutado una vez. En efecto, cada
vez que el servidor actualiza un programa JavaScript que es enviado a
los navegadores de las usuarias, esa actualización es impuesta a las
usuarias. Incluso si el guion es libre, es como si tuviera una puerta
trasera integrada.

Esto es muy similar al caso de tivoización, donde puedes tener en teoría
la libertad de controlar lo que hace un programa, pero no puedes en
práctica por las circunstancias. No es suficiente tener control teórico.
Se necesita también control real. En el caso de JavaScript, esta
carencia de control no es producto de malicia, sino más bien una
consecuencia de navegadores web asumiendo despreocupadamente que la
usuaria desea la ejecución de cada guion que una página Web pueda
recomendar. Esto no es necesariamente cierto. Sería como si Windows
fuera instalado en mi máquina cada vez que leo un artículo que
recomienda el uso de Windows, o como si cada publicación de blog que
habla de lo genial que es Chrome resultara en la instalación automática
de Chrome en mi sistema.

¿Qué podemos hacer? Conozco dos posibles soluciones.

## Solución 1: Arreglar JavaScript

La primera posible solución, y la más obvia, es modificar el
comportamiento de los navegadores web con respecto a las peticiones de
<i lang="en">software</i> JavaScript. Propongo que para que el sistema sea aceptable,
**deben** cumplirse todas las siguientes condiciones:

- El navegador debe instalar el código JavaScript permanentemente, y
  solo si la usuaria lo autoriza explícitamente de alguna forma.
- El navegador debe darle a la usuaria la habilidad de instalar
  cualquier guion arbitrario, no solo el guion solicitado por la página
  Web.
- El navegador no debe actualizar ningún código JavaScript
  automáticamente, a no ser que la usuaria haya especificado que debería
  hacerlo, y la usuaria deberá ser capaz de elegir de dónde vienen tales
  actualizaciones.

Te darás cuenta de que la detección de licencia automática no está
incluida en ninguna de estas. ¿Así cómo obtiene la usuaria solo
JavaScript libre sin comprobar manualmente cada archivo fuente? La
solución en realidad es bastante simple: de la misma forma que con
cualquier otro <i lang="en">software</i> libre. Confío en que las desarrolladoras de
Trisquel solo incluyan <i lang="en">software</i> libre sin funcionalidades maliciosas en
el repositorio de Trisquel. Incidentalmente, las desarrolladoras de
Trisquel pueden proteger a las usuarias de Trisquel de *malware*,
privativo o no; LibreJS no. Similarmente, podemos crear y mantener un
repositorio de código JavaScript libre.

Para que esto funcione, los programas JavaScript instalados deberían
también funcionar para todas las páginas Web que los soliciten, no solo
una página. Qué código JavaScript ya instalado usar por defecto puede
ser determinado obteniendo un hash de las versiones minificadas de
guiones instalados, y después obteniendo un hash de guiones solicitados
después de minificarlos de la misma manera. Si eso no devuelve una
coincidencia, los nombres de archivo de los guiones pueden comprobarse
en busca de coincidencias o cuasicoincidencias, y se puede preguntar a
la usuaria si deberían usarse esos guiones. Algún tipo de base de datos
en el navegador de la usuaria especificando páginas en que deberían
usarse ciertos guiones serían también útil.

Supongo que este enfoque tomaría una cantidad de esfuerzo considerable,
y esto es probablemente por lo que la desarrolladora de LibreJS no lo ha
intentado. No ayuda que hacer que esto funcione de forma *fiable*
implicaría trabajo constante para estar al día con los cambios de
páginas Web.

## Solución 2: Matar JavaScript

Cuando propuse algo como Solución 1 en la lista de correo bug-gnuzilla,
una respuesta apuntó que hay una solución mucho más simple: en vez de
tratar de arreglar JavaScript, podríamos simplemente deshabilitar la
ejecución de JavaScript en nuestros navegadores completamente (en otras
palabras, matar JavaScript). Por supuesto, realmente quiero decir el
JavaScript *instalado automáticamente*. No hay nada malo en el uso
actual de JavaScript para desarrollar extensiones de Firefox, por
ejemplo. Pueden desarrollarse incluso guiones y extensiones de usuarias
para reemplazar código JavaScript privativo importante.

Aun, esta solución no está libre de problemas. En particular, esto
requiere un cambio social masivo, si bien un cambio menor que lo que
LibreJS está intentando. Navegadores eliminando soporte de JavaScript
pueden ayudar a llevar el plan adelante, pero hay un problema de la
gallina y el huevo en el sentido de que navegadores sin soporte para
JavaScript serán vistos como inferiores mientras que muchas páginas web
lo requieran.

Un posible punto intermedio para este fin puede ser que un navegador
soporte JavaScript, pero lo tenga desactivado por defecto y dé a la
usuaria una forma fácil de activar temporalmente la ejecución de
JavaScript en una única página; de esa forma, la usuaria obtendrá una
experiencia libre de JavaScript, pero aún tendrá la opción de usar
JavaScript para páginas que lo requieran sin tanta inconveniencia de que
vuelva el navegador poco manejable. Incluso tendría el bonito efecto
secundario de hacer la experiencia de navegación más fluida para
usuarias; un montón de páginas tienen una tremenda hinchazón de
JavaScript que puede ser evitada completamente con solo deshabilitar
JavaScript.

## Conclusión

Cada uno de estos enfoques tiene fortalezas y debilidades.

La primera solución puede traer buenos resultados inmediatamente para
cosas como Diaspora y Reddit: páginas web cuyo código JavaScript es
requerido, pero en su mayoría libre. Posiblemente no traería un gran
cambio en la Red (si acaso alguno), pero no necesita hacerlo para
funcionar. Sin embargo, requeriría algún trabajo ajustar el
comportamiento del navegador con respecto a JavaScript adecuadamente, y
sería mucho más trabajo mantener un repositorio de programas JavaScript
libres.

La segunda solución es bastante similar a lo que LibreJS está tratando
de hacer actualmente, si bien en una escala mucho menor. Depende de
cambiar la Red: convenciendo a la mayoría de desarrolladoras Web de
dejar de requerir código JavaScript. Si funciona, podría funcionar
espectacularmente. Por otro lado, podría fallar fácilmente de forma
espectacular o simplemente resultar en otro método más de instalar
<i lang="en">software</i> silenciosa y automáticamente en los navegadores de usuarias
que se vuelve popular.

No estoy segura de cuál es mejor, pero LibreJS no es ni una buena
solución ni un buen arreglo temporal ni siquiera un paso en la dirección
adecuada. Hasta que un navegador libre que arregle el problema
adecuadamente esté disponible, cualquiera que quiera libertad en su
computación deberá deshabilitar toda la ejecución normal de JavaScript
en sus navegadores, incluso si el código es libre, y las desarrolladoras
Web que quieran respetar las libertades de sus usuarias deberán trabajar
para eliminar todos los requerimientos de JavaScript en sus páginas.
