Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2018-05-15 10:47
Lang: es
Modified: 2018-05-16 09:24
Slug: las-arrobas-barras-y-equis-no-se-entienden
Tags: arroba, barra, espeak, equis, español, feminismo, lector de pantalla, lenguaje
Title: Las arrobas, barras y equis no se entienden

Hay gente que usa arrobas, barras, equis o incluso asteriscos con el
objetivo de usar un
[lenguaje no sexista](https://es.wikipedia.org/wiki/Lenguaje_no_sexista).
Para que os hagáis una idea muestro algunos ejemplos:

- Lxs interesadxs pueden contactar conmigo
- Saludos a tod@s l@s asistentes
- Trabaja de camarer*
- El cambio fue positivo para las/os trabajadoras/es

Usar estas formulas en formato digital es una mala idea porque
dificulta mucho la escucha a personas que usan lectores de pantalla.

Como ejemplo, he generado varios audios con el programa
[espeak](https://en.wikipedia.org/wiki/ESpeak). El primer texto
proporcionado es «Lxs interesadxs pueden contactar conmigo». El
resultado no es muy comprensible...

<!-- more -->

<audio controls src="/wp-content/uploads/2018/05/espeak-equis.wav"></audio>

Con arrobas pasa lo mismo. El siguiente audio es «saludos a tod@s l@s
asistentes»...

<audio controls src="/wp-content/uploads/2018/05/espeak-arroba.wav"></audio>

El uso de barras también hace difícil la comprensión. Este texto es «el
cambio fue positivo para las/os trabajadoras/es»...

<audio controls src="/wp-content/uploads/2018/05/espeak-con-barras.wav"></audio>

Hay otras propuestas que no les dificultan la lectura a quienes usan
lectores de pantalla:

- Unas personas abogan por el uso de un solo género, que haga referencia
  a toda persona: «Las [personas] trabajadoras se quejaron de las
  condiciones laborales».
- Otras prefieren usar la e como alternativa neutral a la a y la o. «Les
  humanes son animales».
- Otras prefieren usar la i como alternativa neutral a la a y la o: «Lis
  humanis son animales».
- ... (y otras más que no conozco)

Si vas a escribir un texto para algún dispositivo digital, ten en cuenta
que mucha gente no lo entenderá si usas caracteres inpronunciables. No
excluyas a las personas con problemas visuales o de otro tipo.
