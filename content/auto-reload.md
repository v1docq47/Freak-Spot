Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2017-05-02 02:25
Image: <img src="/wp-content/uploads/2017/05/Auto-Reload.png" alt="Auto Reload en acción" class="attachment-post-thumbnail wp-post-image" width="1920" height="144" srcset="/wp-content/uploads/2017/05/Auto-Reload.png 1920w, /wp-content/uploads/2017/05/Auto-Reload-960x72.png 960w, /wp-content/uploads/2017/05/Auto-Reload-480x36.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
Lang: es
Slug: recarga-las-páginas-web-automáticamente-cuando-las-modificas
Tags: automatismo, CSS, Firefox, HTML, Icecat, Iceweasel, JavaScript, páginas web, programación, recarga
Title: Recarga las páginas web automáticamente cuando las modificas

A la hora de desarrollar páginas web, muchas veces necesitamos probar
los cambios que hemos realizado. Para ello tenemos que abrir la página
web con el navegador y recargarla si ya estaba abierta. Pero esto es
algo molesto si lo tenemos que hacer muchas veces.

<!-- more -->

Para automatizar el proceso de recarga podemos utilizar [Auto
Reload](https://addons.mozilla.org/es/firefox/addon/auto-reload/): un
complemento para Firefox y navegadores derivados de Firefox que detecta
cuándo se ha modificado algún archivo de la página localmente y la
recarga automáticamente. No hace falta configurar nada si solo vamos a
abrir archivos locales, como un archivo HTML con la URL
`file:///tmp/index.html`. Si dicho archivo HTML tiene asociados archivos
<abbr title="Cascading Style Sheets">CSS</abbr> o JavaScript, estos se
recargarán automáticamente cada vez que se modifiquen.

<ins cite="actualización" datetime="2018-07-11T17:57:00+02:00">Este
complemento solo funciona en versiones de Firefox y navegadores
derivados de Firefox anteriores a la versión 57, debido a que las
*WebExtensions* no tienen acceso al sistema de archivos.</ins>

Si estamos desarrollando en un servidor, puede que necesitemos
establecer algunas reglas para que funcione este complemento.
Podemos recargar la página si la URL de nuestro navegador tiene un
patrón y determinados archivos o los contenidos de un directorio han
cambiado. El patrón se establece con una expresión regular con la
sintaxis de JavaScript desde el menú de preferencias.

Por ejemplo, si quiero que la página se recargue cuando la URL del
navegador comience por 'http://localhost:8000', puedo utilizar la
expresión regular `http://localhost:8000.*`.

<a href="/wp-content/uploads/2017/05/Auto-Reload-preferencias.png">
<img src="/wp-content/uploads/2017/05/Auto-Reload-preferencias.png" alt="Menú de preferencias de Auto Reload" class="size-fill" width="1920" height="841" srcset="/wp-content/uploads/2017/05/Auto-Reload-preferencias.png 1920w, /wp-content/uploads/2017/05/Auto-Reload-preferencias-960x420.png 960w, /wp-content/uploads/2017/05/Auto-Reload-preferencias-480x210.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

Desde el menú de preferencias podemos acceder a otras opciones más
avanzadas.
