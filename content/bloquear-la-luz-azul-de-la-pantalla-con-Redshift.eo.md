Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2023-01-31 16:01
Image: <img src="/wp-content/uploads/2019/08/pantalla-luz-azul-Redshift.jpg" alt="" width="1023" height="681" srcset="/wp-content/uploads/2019/08/pantalla-luz-azul-Redshift.jpg 1023w, /wp-content/uploads/2019/08/pantalla-luz-azul-Redshift-511x340.jpg 511w" sizes="(max-width: 1023px) 100vw, 1023px">
Lang: eo
Slug: reducir-la-luz-azul-de-la-pantalla-con-Redshift
Save_as: malpliigi-la-ekranan-bluan-lumon-per-Redshift/index.html
URL: malpliigi-la-ekranan-bluan-lumon-per-Redshift/
Tags: koordinatoj, GPLv3, blua lumo, okuloj, ekrano, programo, Redshift, sano, terminalo, Trisquel, Debiano
Title: Malpliigi la ekranan bluan lumon per Redshift

Blua lumo estas emisiita de naturaj fontoj, kiel la suno kaj ekranoj de
elektraj aparatoj. Oni konsideras, ke ĉirkaŭ triono de ĉiu la lumo
videbla de homoj estas blua. Tro da ricevado de tia lumo kaŭzas gravajn
sanajn problemojn.

<!-- more -->
<figure>
    <img alt="" id="img1" src="/wp-content/uploads/2019/08/Espectro_de-luz-colores-precisos.svg">
    <figcaption class="wp-caption-text">Luma spektro en nanometroj</figcaption>
</figure>

La intervalo de la onda longeco de la blua lumo estas inter 380 kaj 500
nanometroj. Oni povas siavice dividi ĝin en blua-purpura lumo (380-450
nm) kaj blua-turkisa lumo (450-500 nm). La unua estas tiu, kiu plej da
energio havas, ĉar ĝi havas malpli da onda longeco, kaj povas kaŭzi
[astenopion](https://es.wikipedia.org/wiki/Astenop%C3%ADa)[^1], sonĝaj
problemoj[^2], la frua apero de aĝ-rilata makula degenero[^3][^4]... La
dua estas malpli damaĝa al la homa okulo.

Por preventi sanajn problemojn esencas malpliigi la longedaŭra ricevado
de blua lumo. Ne bonas ricevi artefaritan lumon de ekranoj dum longaj
tempoperiodoj. Kiam oni uzas aparatojn kun ekrano rekomendindas, ke
ĝia lumo estu plej simila kiel eblas al la natura lumo de la ĉirkaŭaĵo.

La programo Redshift ebligas agordi la koloran temperaturon de la ekrano
por adapti ĝin al la natura lumo. La kolora temperaturo rekte rilatas al
sia onda longeco[^2]; do la programo malpliigas la bluan lumon. Ĉi tiu
programo utilas, inter aliaj aferoj, por ke la okuloj estu malpli
incitita, kiam vi nokte laboras antaŭ la komputilo. En distribuoj
bazitaj sur Debiano oni povas instali ĝin plenumante...

    ::bash
    sudo apt install redshift

Kiam instalita, oni povas plenumi ĝin en la terminalo specifante nian
lokon (la latitudon kaj longitudon). Ekzemple, se vi estas en Vigo (en
Hispanio), vi devas skribi `redshift -l 42.13:8.43`, ĉar ĉi tiuj estas
ĝiaj koordinatoj.

<figure>
    <img src="/wp-content/uploads/2023/01/koordinatoj-de-Vigo.png" alt="">
    <figcaption class="wp-caption-text">Koordinatoj de Vigo, laŭ Vikipedio.</figcaption>
</figure>

Se ni ne volas skribi la lokon ĉiam, kiam ni plenumas la programon, ni
povas konservi ĝin en agordan dosieron en
`~/.config/redshift.conf`.

    ::INI
    [redshift]
    location-provider=manual

    [manual]
    lat=42.13
    lon=8.43

Redshift uzas kolortemperaturon pli altan dum la tago (pli da blua lumo)
ol dum la nokto. Oni povas ŝanĝi la implicitajn valorojn per la
argumento <code>-t <em>TAGO:NOKTO</em></code> kaj ĝiaj respektivaj
agordaj opcioj, `temp-day` kaj `temp-night`.

En distribuoj bazitaj sur Debiano la pako `redshift-gtk` provizas
grafikan interfacon kaj bildeton en la lanĉobreto, kio ege faciligas
ĝian uzon. Ni povas komenci la programon el la menuo de programoj aŭ
plenumante `redshift-gtk` en la terminalo.

Ni povas ankaŭ igi, ke la programo aŭtomate plenumu ĉiam, kiam ni ŝaltas
la komputilon...

<figure>
<a href="/wp-content/uploads/2023/01/aldoni-Redshift-al-aplikaĵoj-post-startigo.png">
<img src="/wp-content/uploads/2023/01/aldoni-Redshift-al-aplikaĵoj-post-startigo.png" alt="" width="799" height="640" srcset="/wp-content/uploads/2023/01/aldoni-Redshift-al-aplikaĵoj-post-startigo.png 799w, /wp-content/uploads/2023/01/aldoni-Redshift-al-aplikaĵoj-post-startigo-399x320.png 399w" sizes="(max-width: 799px) 100vw, 799px">
</a>
    <figcaption class="wp-caption-text">Kiel aŭtomate plenumi Redshift-on post la startiĝo en Debian testing.</figcaption>
</figure>

Redshift estas bona maniero limigi la ricevadon de artefarita blua lumo;
alia pli simpla maniero estas pasigi malpli daŭrajn labortagojn antaŭ
komputilo.

<small>La [bildo de la luma spektro](#img1) estis publikigita de Fulvio314 en [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Light_spectrum_(precise_colors).svg) sub la [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)-permesilo.</small>

[^1]: Lin, J. B., Gerratt, B. W., Bassi, C. J., & Apte, R. S. (2017). Short-wavelength light-blocking eyeglasses attenuate symptoms of eye fatigue. *Investigative ophthalmology & visual science, 58*(1), 442-447.
[^2]: Chellappa, S. L., Steiner, R., Oelhafen, P., Lang, D., Götz, T., Krebs, J., & Cajochen, C. (2013). Acute exposure to evening blue‐enriched light impacts on human sleep. *Journal of sleep research, 22*(5), 573-580.
[^3]: Villegas-Pérez, M. P. (2005). Exposición a la luz, lipofuschina y degeneración macular asociada a la edad. *Archivos de la Sociedad Española de Oftalmología, 80*(10), 565-567.
[^4]: Taylor, H. R., Munoz, B., West, S., Bressler, N. M., Bressler, S. B., & Rosenthal, F. S. (1990). Visible light and risk of age-related macular degeneration. *Transactions of the American Ophthalmological Society*, 88, 163.
[^5]: Ley de desplazamiento de Wien (s.f.). En *Wikipedia*. Konsultita je la 17-a de aŭgusto de 2019, de <https://es.wikipedia.org/wiki/Ley_de_desplazamiento_de_Wien>
