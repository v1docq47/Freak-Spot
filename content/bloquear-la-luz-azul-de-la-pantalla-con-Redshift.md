Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2019-08-17
Image: <img src="/wp-content/uploads/2019/08/pantalla-luz-azul-Redshift.jpg" alt="" width="1023" height="681" srcset="/wp-content/uploads/2019/08/pantalla-luz-azul-Redshift.jpg 1023w, /wp-content/uploads/2019/08/pantalla-luz-azul-Redshift-511x340.jpg 511w" sizes="(max-width: 1023px) 100vw, 1023px">
Lang: es
Modified: 2023-01-31 12:00
Slug: reducir-la-luz-azul-de-la-pantalla-con-Redshift
Tags: coordenadas, GPLv3, luz azul, ojos, pantalla, programa, Redshift, salud, terminal, Trisquel, Trisquel 8
Title: Reducir la luz azul de la pantalla con Redshift

La luz azul es emitida por fuentes naturales, como el sol y pantallas
de dispositivos electrónicos. Se considera que aproximadamente un tercio
de toda la luz visible por el ser humano es azul. La excesiva exposición
a este tipo de luz causa serios problemas de salud.

<!-- more -->
<figure>
    <img alt="" id="img1" src="/wp-content/uploads/2019/08/Espectro_de-luz-colores-precisos.svg">
    <figcaption class="wp-caption-text">Espectro de luz en nanómetros</figcaption>
</figure>

El rango de la longitud de onda de la luz azul está entre los 380 y los
500 nanómetros. Se puede dividir a su vez en luz azul-morada (380-450
nm) y luz azul-turquesa (450-500 nm). La primera es la que más energía
tiene, al tener una menor longitud de onda, y puede provocar
astenopía[^1], trastornos del sueño[^2], la aparición precoz de
degeneración macular asociada a la edad[^3][^4]... La segunda es menos
dañina para el ojo humano.

Para prevenir problemas de salud es fundamental reducir la exposición
prolongada a la luz azul. No es bueno recibir la luz artificial de
pantallas durante largos periodos de tiempo. Cuando se usan dispositivos
con pantalla es recomendable que su luz se asemeje lo más posible a la
luz natural del entorno.

El programa Redshift permite ajustar la temperatura de color de la
pantalla para adaptarla a la luz natural. La
temperatura de color de está directamente relacionada con su longitud de
onda[^5]; por tanto, el programa reduce la luz azul. Este programa
servirá, entre otras cosas, para que los ojos estén menos irritados al
trabajar frente al ordenador por la noche. En distribuciones de
GNU/Linux basadas en Debian se puede instalar ejecutando...

    ::bash
    sudo apt install redshift

Una vez instalado se puede ejecutar desde la terminal especificando
nuestra ubicación (la latitud y la longitud). Por ejemplo, si estamos en
Vigo, escribiremos `redshift -l 42.14:8.43`, pues estas son sus
coordenadas.

<figure>
    <img alt="" src="/wp-content/uploads/2019/08/coordenadas-de-Vigo-Wikipedia.png">
    <figcaption class="wp-caption-text">Coordenadas de Vigo, según Wikipedia.</figcaption>
</figure>

Si no queremos escribir la ubicación cada vez que iniciemos el programa,
podemos guardarla en un archivo de configuración en
`~/.config/redshift.conf`.

    ::INI
    [redshift]
    location-provider=manual

    [manual]
    lat=42.14
    lon=8.43

Redshift usa una temperatura de color más alta durante el día (más luz
azul) que durante la noche. Se pueden cambiar los valores
predeterminados con el argumento <code>-t <em>DÍA:NOCHE</em></code> y
sus correspondientes opciones de configuración `temp-day` y
`temp-night`.

En distribuciones derivadas de Debian el paquete `redshift-gtk`
proporciona una interfaz gráfica y un icono en la barra de inicio, lo
cual facilita mucho su uso. Podemos iniciar este programa desde el menú
de aplicaciones o ejecutando `redshift-gtk` desde la terminal.

También podemos hacer que el programa se ejecute cada vez que encendamos
el ordenador...

<figure>
    <a href="/wp-content/uploads/2019/08/ejecutar-automáticamente-Redshift-al-inicio.png">
    <img src="/wp-content/uploads/2019/08/ejecutar-automáticamente-Redshift-al-inicio.png" alt="" width="1440" height="900" srcset="/wp-content/uploads/2019/08/ejecutar-automáticamente-Redshift-al-inicio.png 1440w, /wp-content/uploads/2019/08/ejecutar-automáticamente-Redshift-al-inicio-720x450.png 720w" sizes="(max-width: 1440px) 100vw, 1440px">
    </a>
    <figcaption class="wp-caption-text">Cómo ejecutar Redshift automáticamente al inicio en Trisquel 8.</figcaption>
</figure>

Redshift es una buena manera de mitigar la exposición a la luz azul
artificial; otra más sencilla es pasar jornadas de trabajo más cortas
frente al ordenador.

<small>La [imagen del espectro de luz](#img1) fue publicada por Fulvio314 en [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Light_spectrum_(precise_colors).svg) con la licencia [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0).</small>

[^1]: Lin, J. B., Gerratt, B. W., Bassi, C. J., & Apte, R. S. (2017). Short-wavelength light-blocking eyeglasses attenuate symptoms of eye fatigue. *Investigative ophthalmology & visual science, 58*(1), 442-447.
[^2]: Chellappa, S. L., Steiner, R., Oelhafen, P., Lang, D., Götz, T., Krebs, J., & Cajochen, C. (2013). Acute exposure to evening blue‐enriched light impacts on human sleep. *Journal of sleep research, 22*(5), 573-580.
[^3]: Villegas-Pérez, M. P. (2005). Exposición a la luz, lipofuschina y degeneración macular asociada a la edad. *Archivos de la Sociedad Española de Oftalmología, 80*(10), 565-567.
[^4]: Taylor, H. R., Munoz, B., West, S., Bressler, N. M., Bressler, S. B., & Rosenthal, F. S. (1990). Visible light and risk of age-related macular degeneration. *Transactions of the American Ophthalmological Society*, 88, 163.
[^5]: Ley de desplazamiento de Wien (s.f.). En *Wikipedia*. Consultado el 17 de agosto del 2019, de <https://es.wikipedia.org/wiki/Ley_de_desplazamiento_de_Wien>
