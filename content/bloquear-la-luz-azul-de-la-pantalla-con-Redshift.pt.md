Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2023-02-01 10:59
Image: <img src="/wp-content/uploads/2019/08/pantalla-luz-azul-Redshift.jpg" alt="" width="1023" height="681" srcset="/wp-content/uploads/2019/08/pantalla-luz-azul-Redshift.jpg 1023w, /wp-content/uploads/2019/08/pantalla-luz-azul-Redshift-511x340.jpg 511w" sizes="(max-width: 1023px) 100vw, 1023px">
Lang: pt
Slug: reducir-la-luz-azul-de-la-pantalla-con-Redshift
Save_as: Reduzir-a-luz-azul-no-ecrã-com-o-Redshift/index.html
URL: Reduzir-a-luz-azul-no-ecrã-com-o-Redshift/
Tags: coordenadas, GPLv3, luz azul, olhos, ecrã, programa, Redshift, saúde, terminal, Trisquel, Debian
Title: Reduzir a luz azul no ecrã com o Redshift

A luz azul é emitida por fontes naturais, tais como o sol e ecrãs de dispositivos electrónicos. Aproximadamente um terço de toda a luz visível para os seres humanos é considerada azul. A exposição excessiva à luz azul causa graves problemas de saúde.

<!-- more -->
<figure>
    <img alt="" id="img1" src="/wp-content/uploads/2019/08/Espectro_de-luz-colores-precisos.svg">
    <figcaption class="wp-caption-text">Espectro de luz em nanómetros</figcaption>
</figure>

O intervalo de comprimento de onda da luz azul situa-se entre 380 e 500
nanómetros. Pode ser ainda dividida em luz azul-púrpura (380-450 nm) e
luz azul-turquesa (450-500 nm). A primeira é a que mais energia tem,
pois tem um comprimento de onda mais curto, e pode causar astenopia[^1],
distúrbios do sono[^2], o início precoce da degeneração macular
relacionada com a idade[^3][^4].... A segunda é menos prejudicial para o
olho humano.

Para prevenir problemas de saúde, é essencial reduzir a exposição
prolongada à luz azul. Não é bom receber luz artificial dos ecrãs
durante longos períodos de tempo. Ao utilizar dispositivos com ecrãs,
recomenda-se que a sua luz se assemelhe o mais possível à luz natural
do ambiente.

O programa Redshift permite ajustar a temperatura da cor do ecrã para
corresponder à luz natural. A temperatura da cor do ecrã está
directamente relacionada com o seu comprimento de onda[^5]; por
conseguinte, o programa reduz a luz azul. Entre outras coisas, este
programa irá ajudar a reduzir a irritação dos olhos quando se trabalha
ao computador à noite. Em distribuições de GNU/Linux baseadas no Debian,
pode ser instalado executando...

    ::bash
    sudo apt install redshift

Uma vez instalado, podes executá-lo a partir do terminal, especificando a
sua localização (latitude e longitude). Por exemplo, se estivermos em
Vigo (Espanha), escreveremos `redshift -l 42.14:8.43`, pois estas são as
suas coordenadas.

<figure>
    <a href="/wp-content/uploads/2023/02/coordenadas-de-Vigo.png">
    <img src="/wp-content/uploads/2023/02/coordenadas-de-Vigo.png" alt="">
    </a>
    <figcaption class="wp-caption-text">Coordenadas de Vigo, segundo Wikipédia.</figcaption>
</figure>

Se não quisermos escrever a localização cada vez que iniciarmos o
programa, podemos guardá-lo num ficheiro de configuração em
`~/.config/redshift.conf`.

    ::INI
    [redshift]
    location-provider=manual

    [manual]
    lat=42.14
    lon=8.43

O Redshift utiliza uma temperatura de cor mais elevada durante o dia
(mais luz azul) do que durante a noite. As predefinições podem ser
alteradas com o argumento <code>-t <em>DIA:NOITE</em></code> e as suas
correspondentes opções de configuração `temp-day` e `temp-night`.

Nas distribuições derivadas do Debian, o pacote `redshift-gtk` fornece
uma interface gráfica e um ícone de [barra de tarefas](https://pt.wikipedia.org/wiki/Barra_de_tarefas), o que o torna
muito fácil de usar. Podemos iniciar este programa a partir do menu de
aplicações ou executando `redshift-gtk` a partir do terminal.

Também podemos fazer o programa funcionar sempre que ligamos o
computador...

<figure>
<a href="/wp-content/uploads/2023/02/executar-automaticamente-o-Redshift-no-arranque-no-Debian-testing.png">
<img src="/wp-content/uploads/2023/02/executar-automaticamente-o-Redshift-no-arranque-no-Debian-testing.png" alt="" width="801" height="636" srcset="/wp-content/uploads/2023/02/executar-automaticamente-o-Redshift-no-arranque-no-Debian-testing.png 801w, /wp-content/uploads/2023/02/executar-automaticamente-o-Redshift-no-arranque-no-Debian-testing-400x318.png 400w" sizes="(max-width: 801px) 100vw, 801px">
</a>
    <figcaption class="wp-caption-text">Como executar o Redshift
    automaticamente no arranque no Debian testing.</figcaption>
</figure>

O Redshift é uma boa forma de mitigar a exposição à luz azul artificial;
uma forma mais simples é passar dias de trabalho mais curtos em frente
ao computador.

<small>[A imagem do espectro da luz](#img1) foi publicada pela Fulvio314
no [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Light_spectrum_(precise_colors).svg)
sob a licença [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0).</small>

[^1]: Lin, J. B., Gerratt, B. W., Bassi, C. J., & Apte, R. S. (2017). Short-wavelength light-blocking eyeglasses attenuate symptoms of eye fatigue. *Investigative ophthalmology & visual science, 58*(1), 442-447.
[^2]: Chellappa, S. L., Steiner, R., Oelhafen, P., Lang, D., Götz, T., Krebs, J., & Cajochen, C. (2013). Acute exposure to evening blue‐enriched light impacts on human sleep. *Journal of sleep research, 22*(5), 573-580.
[^3]: Villegas-Pérez, M. P. (2005). Exposición a la luz, lipofuschina y degeneración macular asociada a la edad. *Archivos de la Sociedad Española de Oftalmología, 80*(10), 565-567.
[^4]: Taylor, H. R., Munoz, B., West, S., Bressler, N. M., Bressler, S. B., & Rosenthal, F. S. (1990). Visible light and risk of age-related macular degeneration. *Transactions of the American Ophthalmological Society*, 88, 163.
[^5]: Ley de desplazamiento de Wien (s.f.). Na *Wikipédia*. Consultado em 17 de agosto de 2019, de <https://es.wikipedia.org/wiki/Ley_de_desplazamiento_de_Wien>
