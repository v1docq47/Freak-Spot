Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2023-02-10 00:01
Lang: eo
Slug: bloquear-sitios-web-etchosts
Save_as: bloki-retejojn-hosts-dosiero/index.html
URL: bloki-retejojn-hosts-dosiero/
Tags: reklamoj, bloki, GNU/Linukso, retejoj, privateco
Title: Bloki retejojn: <code>hosts</code>-dosiero
Image: <img src="/wp-content/uploads/2023/02/ne-eblas-konektiĝi-al-retejo-blokita-per-la-hosts-dosiero.png" alt="" width="960" height="688" srcset="/wp-content/uploads/2023/02/ne-eblas-konektiĝi-al-retejo-blokita-per-la-hosts-dosiero.png 960w, /wp-content/uploads/2023/02/ne-eblas-konektiĝi-al-retejo-blokita-per-la-hosts-dosiero-480x344.png 480w" sizes="(max-width: 960px) 100vw, 960px">


Por bloki retejojn vi povas uzi retumilan kromprogramon (kiel
[Block Site](https://add0n.com/block-site.html)), prokurilan servilon
(kiel [Squid](https://en.wikipedia.org/wiki/Squid_(software))), sed
ankaŭ estas la opcio redakti la `hosts`-dosieron, metodo kiu konsumas
malmultege da ĉefmemoro kaj, malkiel la retumila kromprogramo, funkcios
por ĉiu retumilo aŭ programo, <!-- more --> kiu konektiĝas al la
Interreto[^1].


## Kiel funkcias ĉi tiu metodo?

Vi nur devas aldoni liniojn uzante la jenan aranĝon al la
`hosts`-dosiero (`etc/hosts` en GNU/Linukso) kun la paĝoj, kiujn vi
volas bloki:

    :::text
    0.0.0.0 example.com

Ĉi tiu linio igas, ke ĉiam, kiam vi klopodas konekti al la retejo
`example.com`, anstataŭe vi konektu al la IP-adreso `0.0.0.0.0`, kiu
estas ne-enkursigebla adreso, kiu estas uzita por indiki nekonatan,
nevalidan aŭ neaplikeblan adreson. La rezulto, post rekomenci la retan
servon (`sudo systemctl restart NetworkManager` en Debiano) kaj la
retumilon, estas jena:

<a href="/wp-content/uploads/2023/02/ne-eblas-konektiĝi-al-retejo-blokita-per-la-hosts-dosiero.png">
<img src="/wp-content/uploads/2023/02/ne-eblas-konektiĝi-al-retejo-blokita-per-la-hosts-dosiero.png" alt="" width="960" height="688" srcset="/wp-content/uploads/2023/02/ne-eblas-konektiĝi-al-retejo-blokita-per-la-hosts-dosiero.png 960w, /wp-content/uploads/2023/02/ne-eblas-konektiĝi-al-retejo-blokita-per-la-hosts-dosiero-480x344.png 480w" sizes="(max-width: 960px) 100vw, 960px">
</a>

Ankaŭ eblas alidirekti al alia retejo, kvankam la plejparto de la
retumiloj ne lasos al vi vidi la retejon, ĉar ĉi tio estis estintece
uzita por [rettrompado](https://eo.wikipedia.org/wiki/Rettrompado).

Se vi volas bloki multe da retejoj, aldoni centojn da paĝoj permane ne
estas praktika. Pro tio ekzistas homoj kaj projektoj, kiuj kompilas
listojn de paĝoj. Ekzemplo estas la [listo administrita de Steven
Black](https://github.com/StevenBlack/hosts/), kiu provizas bloklistojn
laŭ temoj, kiel pornografio, falsaj novaĵoj kaj vetoj. Sufiĉas kopii kaj
alglui el la listo, kiun vi volas, en vian `hosts`-dosieron.

[^1]: Ĉi tiu metodo ne funkcios por programoj, kiuj konektas al la
    Interreto per [Tor](https://eo.wikipedia.org/wiki/Tor_(programaro)),
    ĉar la domajna nomsistemo estas solvita ene de la Tor-reto.
