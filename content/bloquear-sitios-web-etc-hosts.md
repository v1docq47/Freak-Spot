Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-02-13 20:50
Lang: es
Slug: bloquear-sitios-web-etchosts
Tags: anuncios, bloqueo, GNU/Linux, páginas web, privacidad
Title: Bloquear sitios web: archivo <code>hosts</code>
Image: <img src="/wp-content/uploads/2022/02/archivo-hosts-ejemplo-de-bloqueo.png" alt="" width="952" height="548" srcset="/wp-content/uploads/2022/02/archivo-hosts-ejemplo-de-bloqueo.png 952w, /wp-content/uploads/2022/02/archivo-hosts-ejemplo-de-bloqueo-476x274.png 476w" sizes="(max-width: 952px) 100vw, 952px">

Para bloquear páginas web puedes usar una extensión
para el navegador (como [Block
Site](https://add0n.com/block-site.html)), un <a
href="https://es.wikipedia.org/wiki/Servidor_proxy">servidor <i
lang="en">proxy</i></a> (como
[Squid](https://es.wikipedia.org/wiki/Squid_(programa))), pero también
existe la opción de editar el [archivo
`hosts`](https://es.wikipedia.org/wiki/Archivo_hosts), método que apenas
consume RAM y nos servirá, a diferencia de la extensión del navegador,
para cualquier navegador o programa <!-- more --> que se conecte a Internet[^1].

## ¿Cómo funciona este método?

Basta con añadir líneas con el siguiente formato al archivo `hosts`
(`/etc/hosts` en GNU/Linux) con las páginas que se quieren bloquear:

    :::text
    0.0.0.0 ejemplo.com

Esta línea hace que cada vez que te intentes conectar a la página
<code>ejemplo.com</code> en su lugar te conectes a la dirección IP
<code>0.0.0.0</code>, que es una dirección no enrutable que se usa para
designar un destino desconocido, no válido o no aplicable. El resultado,
tras reiniciar el servicio de red (`sudo service network-manager
restart` en Debian) y el navegador web es el siguiente:

<a href="/wp-content/uploads/2022/02/archivo-hosts-ejemplo-de-bloqueo.png">
<img src="/wp-content/uploads/2022/02/archivo-hosts-ejemplo-de-bloqueo.png" alt="" width="952" height="548" srcset="/wp-content/uploads/2022/02/archivo-hosts-ejemplo-de-bloqueo.png 952w, /wp-content/uploads/2022/02/archivo-hosts-ejemplo-de-bloqueo-476x274.png 476w" sizes="(max-width: 952px) 100vw, 952px">
</a>

También es posible redirigir a otra página web, aunque la mayoría de navegadores no
te dejarán ver la página web, ya que esto ha sido utilizado en el pasado
para hacer
<a href="https://es.wikipedia.org/wiki/Phishing"><i lang="en">phishing</i></a>.

Si quieres bloquear muchas páginas web, añadir cientos de páginas a mano
no es práctico. Por ello existen personas y proyectos que recopilan
listas de páginas. Un ejemplo es la [lista administrada por Steven
Black](https://github.com/StevenBlack/hosts/), que proporciona listas de
bloqueo según una temática, como pornografía, noticias falsas y
apuestas. Basta con copiar y pegar de la lista que desees en tu archivo
`hosts`.

[^1]: Este método no funcionará para programas que se conecten a
  Internet usando
  [Tor](https://es.wikipedia.org/wiki/Tor_(red_de_anonimato)), puesto
  que el DNS se resuelve dentro de la red Tor.
