Author: Jorge Maldonado Ventura
Category: Bash
Date: 2019-09-05
Lang: de
Slug: busca-ayuda-para-bash-con-la-instruccion-help
Tags: Bash, Hilfe
Save_as: Such-Hilfe-für-Bash-mit-dem-Befehl-help/index.html
Title: Such Hilfe für Bash mit dem Befehl help
URL: Such-Hilfe-für-Bash-mit-dem-Befehl-help/

Der Befehl `help` nutzt man, um Hilfe über eingebauten Befehlen von der
Shell zu finden. Wenn wir den Befehl `help` ausführen, bekommen wir eine
kurze Übersicht von eingebauten Befehlen. `help` können wir auch
Parameter übergeben. Mit `help help` sehen wir die Bedienungsanleitung.
