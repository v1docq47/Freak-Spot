Author: Jorge Maldonado Ventura
Category: Cinema
Date: 2017-07-05 22:53
Image: <img src="/wp-content/uploads/2017/07/caracol.png" alt="" width="1366" height="768" srcset="/wp-content/uploads/2017/07/caracol.png 1366w, /wp-content/uploads/2017/07/caracol-683x384.png 683w" sizes="(max-width: 1366px) 100vw, 1366px">
Lang: en
Modified: 2017-07-09 03:56
Slug: caracoles-cortometraje
Tags: Audacity, Blender, dystopia, free culture, Garage Band, GIMP, Krita, short film, surrealism, Synfig
Title: Snails: a dystopian short film

<!-- more -->
<video controls poster="/wp-content/uploads/2017/07/caracol.png">
  <source src="/temporal/caracoles.mp4" type="video/mp4">
  <p>Sorry, your browser doesn't support HTML 5. Please change
  or update your browser</p>
</video>

Pepe is a young adult who after work, tired of the day, wants to go home
to rest. In their dystopian world full of surrealism, they gets on a
kind of bus-snail, where they inevitably gets lost in thought.

The video is under <a href="https://creativecommons.org/licenses/by/3.0/"><abbr title="Creative Commons Attribution 3.0 International">CC BY 3.0</abbr></a> and was
obtained from
[Goblin Refuge](https://goblinrefuge.com/mediagoblin/u/guayabitoca/m/caracoles-snails/).
