Author: Jorge Maldonado Ventura
Category: Cine
Date: 2017-07-05 22:10
Image: <img src="/wp-content/uploads/2017/07/caracol.png" alt="" width="1366" height="768" srcset="/wp-content/uploads/2017/07/caracol.png 1366w, /wp-content/uploads/2017/07/caracol-683x384.png 683w" sizes="(max-width: 1366px) 100vw, 1366px">
Lang: es
Slug: caracoles-cortometraje
Tags: Audacity, Blender, cortometraje, cultura libre, distopía, Garage Band, GIMP, Krita, surrealismo, Synfig
Title: Caracoles: un cortometraje distópico

<!-- more -->

<video controls poster="/wp-content/uploads/2017/07/caracol.png">
  <source src="/temporal/caracoles.mp4" type="video/mp4">
  <p>Lo siento, tu navegador no soporta HTML 5. Por favor, cambia o actualiza tu navegador</p>
</video>

Pepe es un joven adulto que al terminar su día laboral, cansado del día,
quiere ir a descansar a su casa. En su mundo distópico cargado de
surrealismo sube una especie de autobús-caracol, donde inevitablemente
se pierde en sus pensamientos.

El vídeo se encuentra bajo la licencia <a href="https://creativecommons.org/licenses/by/3.0/"><abbr title="Creative Commons Attribution 3.0 International">CC BY 3.0</abbr></a> y fue
obtenido de
[Goblin Refuge](https://goblinrefuge.com/mediagoblin/u/guayabitoca/m/caracoles-snails/).
