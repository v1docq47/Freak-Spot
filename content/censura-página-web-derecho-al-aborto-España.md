Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2020-04-25
Lang: es
Slug: censura-de-página-web-al-aborto-españa
Tags: censura, España, página web
Title: Censura de sitio web de Women on Web en España
Image: <img src="/wp-content/uploads/2020/04/censura-womenonweb.png" alt="" width="1294" height="732" srcset="/wp-content/uploads/2020/04/censura-womenonweb.png 1294w, /wp-content/uploads/2020/04/censura-womenonweb-647x366.png 647w" sizes="(max-width: 1294px) 100vw, 1294px">

Varios proveedores de acceso a Internet en España (Vodafone, Movistar,
Euskaltel y Yoigo) han bloqueado el acceso al sitio web de Women on Web.
Más detalle sobre el suceso en el artículo de Dones Tech [Censura a
l'estat espanyol de la web womenonweb.org (suport al dret a avortar
segures)](https://donestech.net/noticia/censura-lestat-espanyol-de-la-web-womenonweborg) (en catalán).

Como se ve en la imagen introductoria del artículo, la página se puede
ver desde fuera de España, pero no en España (solo es posible usando Tor
o una <abbr title="Red Privada Virtual"><a href="https://es.wikipedia.org/wiki/Red_privada_virtual">RPV</a></abbr>).
