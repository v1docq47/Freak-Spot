Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2019-04-20 16:00
Image: <img src="/wp-content/uploads/2019/04/grep-y-less-con-color.png" alt="">
Lang: eo
Save_as: kolore-kombini-je-Grep-kaj-less/index.html
Slug: combinar-grep-y-less-con-color
Tags: Bash, koloro, grep, komandlinia interfaco, less, ŝelo, terminalo
Title: Kolore kombini <code>grep</code> kaj <code>less</code>
URL: kolore-kombini-je-Grep-kaj-less/

Mi kutimas uzi [Grep](https://www.gnu.org/software/grep/) por serĉi
precizan tekston en dosieroj. Per `grep -R teksto` en dosierujo mi povas
trovi ĉiujn dosierojn en kiu tiu teksto estas.

<a href="/wp-content/uploads/2019/04/grep-con-color.png">
<img src="/wp-content/uploads/2019/04/grep-con-color.png" alt="">
</a>

Kiam estas multaj kongruoj, estas pli komforta uzi `less` por moviĝi tra
la rezultoj. La problemo estas, ke plenumante `grep -R teksto | less`
oni ne plu vidas la kolorojn.

<!-- more -->

<a href="/wp-content/uploads/2019/04/grep-y-less-sin-color.png">
<img src="/wp-content/uploads/2019/04/grep-y-less-sin-color.png" alt="">
</a>

Grep ne montras kolorojn defaŭlte, sed estas konfigurita en la
plejparto de GNU/Linuksaj sistemoj per alinomo en la dosiero `.bashrc`,
aŭ en la funkcie ekvivalenta en aliaj ŝeloj, por detekti kiam montri kaj
kiam ne montri kolorojn: `alias grep='grep --color=auto'`. Kiam la
rezulto de ĝia plenumo estas donita al alia programo per dukto, Grep
ĉesas montri kolorojn, ĉar la kodoj uzitaj por montri kolorojn en la
ŝeloj aldonas ne ekzistantajn signojn, kiuj povas krei neatenditajn
rezultojn. Por montri kolorojn ankaŭ tiukaze ni devas aldoni je
`--color=always`. Ankaŭ devas ni doni la opcion `-r` al `less`, por ke ĝi
montru la kolorajn signojn. Tiel estus la komando: `grep --color=always
-R teksto | less -r`.

<a href="/wp-content/uploads/2019/04/grep-y-less-con-color.png">
<img src="/wp-content/uploads/2019/04/grep-y-less-con-color.png" alt="">
</a>

Ĉar skribi tiom multe estas teda, estas bona ideo aldoni ĉi tiun
funkcion al la dosiero `.bashrc` (memoru, ke estas necese
[reŝarĝi la agordaron de Bash](/reŝarĝi-la-agordaron-de-Bash/)
por uzi la funkcion):

    :::bash
    lgrep ()
    {
      grep --color=always -R "$1" | less -r
    }

Tiumaniere nur necesas skribi `lgrep teksto` por montriĝi la koloraj
rezultoj en `less`.
