Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2023-11-21 10:50
Image: <img src="/wp-content/uploads/2019/04/grep-y-less-con-color.png" alt="">
Lang: pt
Save_as: combinar-grep-e-less-com-cor/index.html
URL: combinar-grep-e-less-com-cor/
Slug: combinar-grep-y-less-con-color
Tags: Bash, cor, grep, interfaz de comandos, less, terminal
Title: Combinar <code>grep</code> e <code>less</code> com cor

Normalmente utilizo [o Grep](https://www.gnu.org/software/grep/) para
procurar texto exato em ficheiros. Ao utilizar `grep -R texto` dentro de
um diretório, posso localizar todos os seus ficheiros onde esse texto se
encontra.

<a href="/wp-content/uploads/2019/04/grep-con-color.png">
<img src="/wp-content/uploads/2019/04/grep-con-color.png" alt="">
</a>

Quando há muitas correspondências, é mais conveniente usar o `less` para
percorrer os resultados. O problema é que quando se executa `grep -R text
| less` as cores deixam de ser visíveis.

<!-- more -->

<a href="/wp-content/uploads/2019/04/grep-y-less-sin-color.png">
<img src="/wp-content/uploads/2019/04/grep-y-less-sin-color.png" alt="">
</a>

O Grep não exibe cores por padrão, mas é configurado na maioria das
distribuções GNU/Linux com um alias no arquivo `.bashrc`, ou o equivalente em
outros interpretadores de comandos, para detetar quando exibir cores e
quando não: `alias grep='grep --color=auto`. Ao passar o resultado de sua
execução para outro programa através de um <i lang="en">pipe</i>, o Grep deixa de exibir
cores, pois os códigos usados para exibir cores em terminais adicionam
caracteres inexistentes que podem produzir resultados inesperados. Para
mostrar as cores também neste caso devemos acrescentar `--color=always`.
Temos também de passar a opção `-r` ao `less` para que este mostre os
caracteres coloridos. O comando tem o seguinte aspeto: `grep
--color=always -R text | less -r`.

<a href="/wp-content/uploads/2019/04/grep-y-less-con-color.png">
<img src="/wp-content/uploads/2019/04/grep-y-less-con-color.png" alt="">
</a>

Uma vez que é aborrecido escrever tanto, é uma boa ideia adicionar esta
função ao ficheiro `.bashrc` (lembra-te que é necessário [recarregar a
configuração do Bash](/pt/recarregar-a-configuração-do-Bash/) para poder
utilizar a função):

    :::bash
    lgrep ()
    {
      grep --color=always -R "$1" | less -r
    }

Desta forma, só tens de escrever `lgrep texto` para apresentar os
resultados com cor no `less`.
