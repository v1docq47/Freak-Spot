Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2020-03-30
Lang: eo
Slug: comentario
Tags: verso
Title: Komento
JS: komento.js (bottom)
Save_as: komento/index.html
URL: komento/

Nu, vi min petis,  
jam do mi venis.

<!-- more -->

<template id="rompita-ligilo">[Ne trovita](/pages/404.md). Ŝarĝante...</template>
<span id="ne-trovita"></span>

<span id="teksto"></span>
<noscript>
Paroli kun stilo povas mi.  
Kaŝi ion lude faris mi.  
Kiel komento  
nek aŭdebla  
nek <span style="color: black;">videbla</span><!--, sed legebla-->,  
kiel la fonto kies akvo  
kies sono kies varmo...  
Ĉesu! Ties ne estas.  
Nur al vi diri restas,  
ke pli simple ĝi estas.  
<!-- Mi estas retejo. -->
</noscript>
