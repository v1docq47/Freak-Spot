Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2020-03-30
Lang: es
Slug: comentario
Tags: verso
Title: Comentario
JS: komento.js (bottom)

Bueno, me pediste,  
así que ya vine.  

<!-- more -->

<template id="rompita-ligilo">[No encontrado](/pages/404.md). Cargando...</template>
<span id="ne-trovita"></span>

<span id="teksto"></span>
<noscript>
Hablar con estilo puedo.  
Esconder algo lúdicamente hice.  
Como un comentario  
ni audible  
ni <span style="color: black;">visible</span><!--, pero legible -->,  
como la fuente cuya agua  
cuyo sonido cuyo calor...  
¡Para! De eso no es.  
Solo decirte queda  
que más simple es.  
<!-- Soy una página web. -->
</noscript>
