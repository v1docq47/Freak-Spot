Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2016-10-21 18:51
Image: <img src="/wp-content/uploads/2016/10/ReTux-in-Trisquel-menu-1.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" width="483" height="490" alt="¿Cómo añadir un programa al menú de GNOME?" srcset="/wp-content/uploads/2016/10/ReTux-in-Trisquel-menu-1.png 483w, /wp-content/uploads/2016/10/ReTux-in-Trisquel-menu-1-296x300.png 296w" sizes="(max-width: 483px) 100vw, 483px">
Lang: es
Modified: 2017-02-25 17:32
Slug: como-anadir-un-programa-al-menu-de-gnome
Status: published
Tags: Alacarte, GNOME, Inicio, lanzador, launcher, menu, ReTux, Trisquel, Trisquel Belenos, Trisquel 7
Title: ¿Cómo añadir un programa al menú de GNOME?

El otro día compré un juego llamado *ReTux*. Pero como solo tenía el
código fuente era un poco incómodo de iniciar. Para ello decidí añadirlo
al menú de inicio. En este artículo enseñaré a hacer esto mismo. Utilizo
una distribución de GNU/Linux llamada Trisquel, con el entorno de
escritorio GNOME. En GNOME viene incluido el programa
[Alacarte](https://en.wikipedia.org/wiki/Alacarte), que es el que uso en
este artículo, así que estos pasos deberían funcionar para las
distribuciones con GNOME.<!-- more -->

El menú (barra de inicio o como queráis llamarlo) suele ubicarse en la
esquina inferior izquierda y permite acceder rápidamente a los programas
que tenemos instalados. Pero si solo disponemos del código fuente de un
programa y este se encuentra en nuestra carpeta personal (`$HOME`), no
aparecerá ahí listado a menos que se lo indiquemos. Para lograr este
objetivo debemos seguir los siguientes pasos:

1.  Ve a la sección *Menú pricipal* del menú de configuración del
    sistema (*Configuración del
    sistema*).<a href="/wp-content/uploads/2016/10/configuraciondelsistema.png"><img class="aligncenter size-full wp-image-523" src="/wp-content/uploads/2016/10/configuraciondelsistema.png" alt="configuraciondelsistema" width="232" height="483" srcset="/wp-content/uploads/2016/10/configuraciondelsistema.png 232w, /wp-content/uploads/2016/10/configuraciondelsistema-144x300.png 144w" sizes="(max-width: 232px) 100vw, 232px" /></a><a href="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-200601.png"><img class="aligncenter size-full wp-image-525" src="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-200601.png" alt="captura-de-pantalla-de-2016-10-21-200601" width="859" height="688" srcset="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-200601.png 859w, /wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-200601-300x240.png 300w, /wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-200601-768x615.png 768w" sizes="(max-width: 859px) 100vw, 859px" /></a>
2.  Abre el menú para crear un elemento nuevo pulsando el botón
    *Elemento nuevo* en la sección que deseemos. Si necesitamos una
    nueva sección para nuestro elemento, tenemos que crearla con el
    botón *Menú nuevo*.<a href="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201521.png"><img class="aligncenter size-full wp-image-528" src="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201521.png" alt="Configuración del Menú principal en Trisquel" width="675" height="530" srcset="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201521.png 675w, /wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201521-300x236.png 300w" sizes="(max-width: 675px) 100vw, 675px" /></a>
3.  Añade la información sobre el programa. En el campo *Name* pon el
    nombre del programa; en *Command*, la instrucción que necesitas
    ejecutar para lanzar el programa (en mi caso fue
    `python3 /home/jorge/Programas/retux/retux-1.1.2-gnulinux/retux.py`);
    en *Comment* debes poner el texto que quieres que se muestre cuando
    pases el ratón por encima del programa en el menú de inicio
    de Trisquel. Si necesitas lanzar tu programa desde la Terminal,
    marca la opción *Launch in Terminal?*. Puedes añadir también un
    icono, clicando en el cuadrado con la imagen por defecto (que se
    encuentra en la parte superior izquierda de la ventana) e indicando
    la ruta de la imagen que quieres
    utilizar.<a href="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201137.png"><img class="aligncenter size-full wp-image-529" src="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201137.png" alt="captura-de-pantalla-de-2016-10-21-201137" width="419" height="191" srcset="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201137.png 419w, /wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-201137-300x137.png 300w" sizes="(max-width: 419px) 100vw, 419px" /></a><a href="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-202757.png"><img class="aligncenter size-full wp-image-534" src="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-202757.png" alt="Añadiendo el lanzador en el menú de Trisquel para ReTux" width="387" height="191" srcset="/wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-202757.png 387w, /wp-content/uploads/2016/10/Captura-de-pantalla-de-2016-10-21-202757-300x148.png 300w" sizes="(max-width: 387px) 100vw, 387px" /></a>

A continuación, os muestro el resultado final.
<a href="/wp-content/uploads/2016/10/ReTux-in-Trisquel-menu-1.png"><img class="aligncenter size-full wp-image-540" src="/wp-content/uploads/2016/10/ReTux-in-Trisquel-menu-1.png" alt="ReTux en el menú de Trisquel" width="483" height="490" srcset="/wp-content/uploads/2016/10/ReTux-in-Trisquel-menu-1.png 483w, /wp-content/uploads/2016/10/ReTux-in-Trisquel-menu-1-296x300.png 296w" sizes="(max-width: 483px) 100vw, 483px" /></a>
