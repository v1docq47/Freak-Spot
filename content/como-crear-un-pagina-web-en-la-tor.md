Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2016-12-26 02:16
Lang: es
Modified: 2021-12-05
Slug: como-crear-un-pagina-web-en-la-tor
Status: published
Tags: .onion, Cybersy, Deep web, GLAMP, GNU/Linux, página web, privacidad, Tor, Tor Browser, torrc, Trisquel, Trisquel Belenos, Trisquel 7
Title: ¿Cómo crear un página web en la red Tor?

En este artículo enseño cómo crear un servicio oculto en la red Tor,
concretamente un sitio web.

Lo primero que debemos hacer es tener Tor funcionando. Lo más sencillo
es [descargarse el Tor
Browser](https://www.torproject.org/es/download/) de
[la página oficial de Tor](https://www.torproject.org/es/) e instalarlo.

Lo siguiente es instalar un servidor web. Puedes [instalar un servidor GLAMP](/instalacion-de-un-servidor-glamp/) o
cualquier otro tipo de servidor web. Debes configurar tu servidor web
para que no dé información sobre ti, sobre tu ordenador o sobre tu
ubicación si quieres contar con una mayor privacidad. Es una buena idea
usar una [máquina virtual](https://es.wikipedia.org/wiki/M%C3%A1quina_virtual).

Lo último es configurar el servicio oculto para que apunte al servidor
local anteriormente instalado. Para ello necesitamos editar un archivo
llamado `torrc`. La ruta de este archivo es la siguiente dentro de la
carpeta obtenida al descargar Tor Browser:
`Browser/TorBrowser/Data/Tor/torrc`. Si instalaste Tor de otra forma, el
archivo `torrc` se encontrará en otra ubicación (lee la siguiente
sección para saber donde encontrar el archivo:
<https://support.torproject.org/es/#tbb_tbb-editing-torrc>).

Debes añadir dos líneas a tu archivo `torrc`:

-   **HiddenServiceDir** es el directorio donde Tor guardará la
    información sobre el servicio oculto. En concreto, Tor creará un
    archivo aquí llamado `hostname` que contendrá la URL onion y otro
    llamado `private_key`. Este no debe ser el directorio de tu servidor
    web, pues este directorio contendrá información secreta. No
    necesitas añadir nada dentro de este directorio
-   **HiddenServicePort** te permite especificar un puerto virtual (esto
    es, el puerto que los usuarios del servicio oculto pensarán que
    están usando) y una dirección IP y un puerto para redirigir las
    conexiones a este puerto virtual

En mi caso añadí estas líneas:

    HiddenServiceDir /home/jorge/Documentos/tor/
    HiddenServicePort 80 127.0.0.1

 
<a href="/wp-content/uploads/2016/12/torrc.png"><img class="aligncenter size-full wp-image-595" src="/wp-content/uploads/2016/12/torrc.png" alt="El archivo &lt;code&gt;torrc&lt;/code&gt; configurado para ofrecer un servicio oculto" width="1025" height="723" srcset="/wp-content/uploads/2016/12/torrc.png 1025w, /wp-content/uploads/2016/12/torrc-300x212.png 300w, /wp-content/uploads/2016/12/torrc-768x542.png 768w, /wp-content/uploads/2016/12/torrc-1024x722.png 1024w" sizes="(max-width: 1025px) 100vw, 1025px" /></a>

En la anterior imagen puedes ver mi archivo `torrc`. Cuando elijas el
directorio HiddenServiceDir, no lo crees, es mejor que lo haga Tor
automáticamente para evitar problemas con los permisos. En mi caso el
directorio `/home/jorge/Documentos/tor` se creó cuando volví a iniciar
Tor tras haber guardado el nuevo contenido del archivo `torrc`.

Una vez escrita en el archivo `torrc` la información anteriormente
mencionada, debes abrir Tor Browser o ejecutar Tor de nuevo. Si no se
inicia Tor correctamente, es que has escrito algo mal en el archivo
`torrc`. Revísalo, corrígelo y prueba de nuevo. Si tienes problemas,
puedes dejar aquí un comentario para que os intentemos ayudar o
[contactar con el proyecto
Tor](https://www.torproject.org/es/contact/).

Si todo ha ido bien, cuando inicies Tor de nuevo, se crearán dos
archivos en el directorio que indicaste en HiddenServiceDir, en mi caso
en `/home/jorge/Documentos/tor`. Son los archivos `private_key` (tu
clave privada) y `hostname` (archivo de texto con tu URL onion).

Cuando te dirijas con el Tor Browser a esa URL, verás el contenido del
archivo `index.html` que está en la carpeta de tu servidor web, si es
que existe (en mi caso es `/var/www/html/index.html`). Ahora puedes
crear una página web sencilla y visualizarla en tu URL onion. Cualquiera
que conozca esa URL y use Tor podrá ver tu página web.

<a href="/wp-content/uploads/2016/12/deep-web-page.png"><img class="aligncenter size-full wp-image-596" src="/wp-content/uploads/2016/12/deep-web-page.png" alt="Mi primera página web en la Deep Web" width="1025" height="767" srcset="/wp-content/uploads/2016/12/deep-web-page.png 1025w, /wp-content/uploads/2016/12/deep-web-page-300x224.png 300w, /wp-content/uploads/2016/12/deep-web-page-768x575.png 768w, /wp-content/uploads/2016/12/deep-web-page-1024x766.png 1024w" sizes="(max-width: 1025px) 100vw, 1025px" /></a>

En
<https://riseup.net/en/security/network-security/tor/onionservices-best-practices>
puedes encontrar algunas recomendaciones más a la hora de crear
servicios ocultos.
