Author: Jorge Maldonado Ventura
Category: Reta programado
Date: 2023-01-10 17:50
Lang: eo
Save_as: kiel-krei-plurlingvan-retejon-per-Pelican/index.html
URL: kiel-krei-plurlingvan-retejon-per-Pelican/
Slug: cómo-crear-un-sitio-web-multilingüe-con-pelican
Tags: i18n_subsites, Pelican, lokigo
Title: Kiel krei plurlingvan retejon per Pelican
Image: <img src="/wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados.png" alt="" width="1920" height="1033" srcset="/wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados.png 1920w, /wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados-960x516.png 960w, /wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

[Pelican](https://getpelican.com/) estas tre agordebla kreilo de retejoj
programita per Python. Ĉi tiu retejo estas farita per Pelican kaj
disponeblas en kelkaj lingvoj<!-- more -->[^1]. Per Pelican estas du
opcioj por traduki retejoj:

1. [Traduki unuopajn artikolojn](#1).
2. [Krei tutan subretejon](#2) (ekzemple [freakspot.net/pt/](https://freakspot.net/pt/)) kun tradukitaj modeloj.

<h2 id="1">Unuopajn artikolojn</h2>

<a href="/wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo.png">
<img src="/wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo.png" alt="" width="845" height="286" srcset="/wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo.png 845w, /wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo-422x143.png 422w" sizes="(max-width: 845px) 100vw, 845px">
</a>

Por la unua opcio sufiĉas instali Pelican-on[^2] kaj krei du dosierojn
kun la sama `Slug` en la dosierujo `content/`. Ekzemple en la esperanta
dosiero (`kiel-detrui-Google.md`) ni skribus ion similan:

    :::text
    Author: Jorge Maldonado Ventura
    Category: Privateco
    Date: 2022-11-06 23:51
    Lang: eo
    Slug: Cómo-destruir-Google
    Tags: rekta agado, reklamoj, bojkoto, Google, privateco
    Title: Kiel detrui Google

    La komerca modelo de Google baziĝas sur la kolekto de personaj datenoj de
    uzantoj, la vendado de ili al aliaj firmaoj kaj la montrado de reklamoj. La firmaoj ankaŭ

Oni devas specifi lingvon malsaman (ekzemple `Lang: es`) ol tiu de la
tradukita dosiero en la dosiero de la alia lingvo (en ĉi tiu ekzemplo
`kiel-detrui-Google.es.md`). Laŭvole oni povas aldoni ankaŭ `Save_as`
kaj `URL` por internaciigi la URL-jn.

    :::text
    Author: Jorge Maldonado Ventura
    Category: Privacidad
    Date: 2022-11-06 19:00
    Lang: es
    Slug: Cómo-destruir-Google
    Save_as: cómo-destruir-Google/index.html
    URL: cómo-destruir-Google/
    Tags: acción directa, anuncios, boicot, Google, privacidad
    Title: Cómo destruir Google

    El modelo de negocio de Google se basa en recoger datos personales de
    usuarios, venderlos a terceros y servir anuncios. La empresa también

<h2 id="2">Subretejoj</h2>

Por krei subretejon necesas uzi la [i18n_subsites](https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites)-kromaĵon. Por tio oni devas fari jenon:

<ol>
    <li>Elŝuti la dosierojn de la kromaĵo en la dosierujo uzita por
    loĝigi la kromaĵojn. En mia okazo ĝi troviĝas en <code>plugins/i18n_subsites/</code>.</li>
    <li>Aktivigi la kromaĵon kaj agordi la subretejojn, kiujn ni volas
    krei. En la jena ekzemplo mi agordas nur unu subretejon en la hispana:</li>
</ol>

    :::python
    PLUGINS = ['i18n_subsites']

    I18N_SUBSITES = {
        'es': {
            'SITENAME': 'Sitio web sobre cultura libre',
            }
        }

Rekomendindas traduki la etoson de la retejo, tio estas, la menuojn, la
malsuban parton, ktp. Por fari tion rekomendindas uzi GNU gettext. La
kromaĵo inkludas [gvidilon pri kiel lokigi etosojn kun Jinja2](https://github.com/getpelican/pelican-plugins/blob/master/i18n_subsites/localizing_using_jinja2.rst) (en la angla), kiun vi povas sekvi.

Finfine, per la variabloj disponeblaj en la kromaĵo oni povas aldoni
falliston por ŝanĝi la lingvon kaj aliajn funkciojn. La metadatumoj de
la artikolo devas esti kiel tiuj, kiujn ni uzis [antaŭe](#1) kiel
ekzemplojn. Se vi perdiĝas, vi povas konsulti la fontkodon de retejoj,
kiuj uzas `i18n_subsites`; ekzemple la [fontkodo de ĉi tiu retejo](https://notabug.org/Freak-Spot/Freak-Spot) estas disponebla.


[^1]: En [Esperanto](/eo/), la [portugala](/pt/), [germana](/de/), [angla](/en/), [hispana](/) kaj [rumana](/ro/).
[^2]: En Debian oni plenumus ``sudo apt install pelican``. Por instali
    Pelican-on per Pip oni devas plenumi ``sudo pip3 install pelican``.
