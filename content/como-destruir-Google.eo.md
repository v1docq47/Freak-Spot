Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2022-11-06 23:51
Lang: eo
Slug: Cómo-destruir-Google
Save_as: kiel-detrui-Google/index.html
URL: kiel-detrui-Google/
Tags: rekta agado, reklamoj, bojkoto, Google, privateco
Title: Kiel detrui Google
Image: <img src="/wp-content/uploads/2022/11/aniquilar-a-Google.png" alt="" width="960" height="540" srcset="/wp-content/uploads/2022/11/aniquilar-a-Google.png 960w, /wp-content/uploads/2022/11/aniquilar-a-Google-480x270.png 480w" sizes="(max-width: 960px) 100vw, 960px">

La komerca modelo de Google baziĝas sur la kolekto de personaj datenoj de
uzantoj, la vendado de ili al aliaj firmaoj kaj la montrado de reklamoj. La firmaoj
ankaŭ [partoprenas en spionadaj projektoj](https://eo.wikipedia.org/wiki/PRISM),
[disvolvas programojn de artefarita intelekto kun militaj celoj](/eo/Plenumante-reCAPTCHA-n-de-Google-vi-helpas-mortigi/), [ekspluatas la uzantojn](/eo/kiel-Google-ekspluatas-per-CAPTCHA-j/).

Ĝi estas unu el la plej potencaj firmaoj el la planedo. Tamen Google
estas giganto kun argilaj piedoj, kiu povas esti neniigita.

## Forigi ĝiajn enspezojn per reklamoj

Google gajnas monon liverante tajloritajn reklamojn bazitajn sur la
informo, kiun ĝi kolektas de siaj uzantoj. Se la homoj ne vidas
reklamojn, Google ne gajnas monon. Bloki reklamojn estas maniero eviti la
spuradon kaj igi, ke Google perdu monon, sed se ni vizitas paĝojn de
Google, ĝi daŭre akiros informon, kiun ĝi povos vendi al reklamantoj. Do
plej simplas [bloki la
reklamojn](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/)
kaj eviti retejojn de Google.

Alia ideo estas alklaki ĉiujn la reklamojn per la
[AdNauseam](https://adnauseam.io/)-kromaĵo, kiu ankaŭ povas kaŝi ilin
por ni, por ke ili ne ĝenu. Ĉi tio rimedo igas, ke Google gajnu malpli
da mono per klakoj en reklamoj kaj ke la serviloj de Google havu iom pli
da laborŝarĝo (malgrandega, sed ĝi kontribuas al kreskado de ĝiaj
elspezoj).

## Plenigi la servilojn de Google per merdo

Google permesas alŝuti preskaŭ ĉion ajn al siaj serviloj (videojn,
dosierojn, ktp.). Se la enhavo alŝutita al ĝiaj serviloj okupas multe da
spaco kaj estas rubo, kiu forpelas al la homoj el ĝiaj serviloj (videoj
kun robotaj voĉoj, kiuj diras sensencajn aferojn, centoj da videoj, kiuj
okupas gigabajtojn kaj gigabajtojn), la bontenadaj kostoj de la serviloj
pliiĝas kaj la profito, kiun la firmao akiras, malpliiĝas.

Se estas tutmonda kunordigita klopodo de pluraj uzantoj, Google devos
komenci limigi la alŝuton de dosieroj, dungi homojn, por ke ili trovu
rubajn videojn, bloki homojn kaj ilian IP-adresojn, ktp., kio pliigus
ĝiajn malprofitojn kaj malpliigus ĝiajn profitojn.

Ekzemple, mi povas ĉiuhore krei 15-minutajn videojn kaj aŭtomate aŭ
semiaŭtomate alŝuti ilin al YouTube. La videoj devus okupi multe da
spaco. Ju pli da rezolucio, da koloroj, da sona diverseco, da kadroj por
sekundo, des pli da mono YouTube elspezos por teni tiujn videojn en siaj
serviloj.

[Mi aŭtomate kreis la jenan videon per `ffmpeg`](/eo/krei-hazardajn-videojn-per-FFmpeg/). Ĝi daŭras nun du
sekundojn, sed okupas 136 <abbr title="megabajtojn">MB</abbr>. 15-minuta
simila video okupus 61,2 <abbr title="gigabajtojn">GB</abbr>.

<!-- more -->

<video controls>
  <source src="/video/basura.mp4" type="video/webm">
  <p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>

Necesus krei modifojn de hazardaj videoj, rubajn videojn de robotoj
parolantaj en strangaj lingvoj... Samtempe necesus kuraĝigi la uzantojn
transloĝiĝi en aliajn platformojn kiel
[PeerTube](https://joinpeertube.org/eo/),
[Odysee](https://es.wikipedia.org/wiki/Odysee), ktp., kiuj
estas pli respektemaj al la privateco.

Same ni povus plenigi Google Drive per rubaj dosieroj. Ni eble
devus uzi plurajn kontojn kaj klopodi ne troe altiri la atenton de
Google. Oni povas uzi minoritatajn lingvojn, dosierojn, kiuj ŝajnas
laŭrajtaj, ktp.

Por ke la atako al Google havu kiel eble plej multe da efiko, pluraj
malsamaj kontoj devus esti uzitaj, kun malsamaj IP-adresoj, kaj necesus
kunordigi ĉion inter diversaj homoj. Ne estas ia leĝo, kiu malpermesas
alŝuti hazarde kreitajn videojn kaj dosierojn, kaj kvankam ĝi ekzistus,
oni povus fari tion anonime, por eviti esti identigita.

## Uzi ĝiajn servilojn per prokurilo pagante nenion

Oni povas uzi programojn kiel [Piped](/eo/YouTube-privatece-per-Piped/),
[Invidious](/eo/YouTube-privatece-per-Invidious/) aŭ
[NewPipe](https://newpipe.net/). YouTube elspezas monon sendante
videajn dosierojn trans la Interreto. Tiumaniere ĝi ne kolektas
niajn personajn datenojn, dum ĝi perdas monon, kaj ni ĝuas la videojn.
😆

## Bojkotu ĝiajn produktojn

Aĉetu nenion, kiu donu monon al Google. Se vi jam havas produkton de
Google, eblas agi, por ke ĝi sendu kiel eble plej malmulte da datenoj.
Ekzemple eblas
[ŝanĝi la agordojn de la kaptita portalo de Android](/no-digas-a-google-d%C3%B3nde-te-conectas-a-internet-en-android-portal-cautivo/).

Se vi bezonas poŝtelefonon, vi povas aĉeti malnovan telefonon, telefonon
kun GNU/Linukso aŭ iun kun [libera distribuo de Android](https://en.wikipedia.org/wiki/List_of_custom_Android_distributions).

## Propagando kontraŭ Google

Ĉi tiu artikolo estas tia ekzemplo. ✊
