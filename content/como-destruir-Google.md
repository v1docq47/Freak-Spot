Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2022-11-06 19:00
Lang: es
Slug: Cómo-destruir-Google
Tags: acción directa, anuncios, boicot, Google, privacidad, Invidious
Title: Cómo destruir Google
Image: <img src="/wp-content/uploads/2022/11/aniquilar-a-Google.png" alt="" width="960" height="540" srcset="/wp-content/uploads/2022/11/aniquilar-a-Google.png 960w, /wp-content/uploads/2022/11/aniquilar-a-Google-480x270.png 480w" sizes="(max-width: 960px) 100vw, 960px">

El modelo de negocio de Google se basa en recoger datos personales de
usuarios, venderlos a terceros y servir anuncios. La empresa también
[participa en programas de
espionaje](https://es.wikipedia.org/wiki/PRISM), [desarrolla programas de
inteligencia artificial con fines
militares](/al-completar-un-recaptcha-de-google-ayudas-a-matar/),
[explota a los usuarios](/como-explota-Google-con-CAPTCHAs/).

Es una de las empresas más poderosas del planeta. Sin embargo, Google es
un gigante con pies de barro que puede ser aniquilado.

## Acabar con sus ingresos por anuncios

Google gana dinero ofreciendo anuncios personalizados basados en la
información que recoge de sus usuarios. Si la gente no ve anuncios,
Google no gana dinero. Bloquear anuncios es una forma de evitar el
rastreo y hacer que Google pierda dinero, pero si visitamos las páginas
de Google, este seguirá obteniendo información que pueda vender a
anunciantes. Por tanto, lo más eficaz es [bloquear los anuncios](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/)
y no visitar sitios de Google.

Otra idea es clicar en todos los anuncios con la extensión
[AdNauseam](https://adnauseam.io/), que también nos los oculte para que
no nos resulte molesto. Este método hace que Google gane menos dinero
por clics en los anuncios y que los servidores de Google tengan algo más
de carga de trabajo (mínima, pero contribuye a aumentar sus gastos).

## Llenar los servidores de Google de mierda

Google deja subir casi cualquier cosa a sus servidores (vídeos,
archivos, etc.). Si el contenido subido a sus servidores ocupa mucho
espacio y es basura que espanta a la gente de sus servicios (vídeos con
voces de robots que dicen cosas sin sentido, cientos vídeos con ruido
que ocupan gigas y gigas), los costes de mantenimiento de los servidores
aumentan y el beneficio que obtiene la empresa se reduce.

Si se trata de un esfuerzo coordinado de forma global por varios
usuarios, Google tendría que empezar a restringir la subida de archivos,
contratar a gente para que encontrara vídeos basura, bloquear a gente y
direcciones IP, etc., con lo que aumentarían sus pérdidas y reduciría
sus beneficios.

Por ejemplo, puedo crear cada hora vídeos de 15 minutos y subirlos a
YouTube de forma automática o semiautomática. Los vídeos deberían
ocupar mucho espacio. Cuanta más resolución, más colores, más variedad
de sonido, más fotogramas por segundo, más dinero gastará YouTube para
mantener esos vídeos en sus servidores.

El vídeo que muestro a continuación [lo he generado de forma automática
con `ffmpeg`](/crear-vídeos-de-ruido-con-ffmpeg/). Dura solo dos segundos, pero ocupa
136 <abbr title="Megabytes">MB</abbr>. Un vídeo similar de 15 minutos ocuparía
61,2 <abbr title="Gigabytes">GB</abbr>.

<!-- more -->

<video controls>
  <source src="/video/basura.mp4" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Habría que hacer variaciones de vídeos aleatorios, vídeos basura de
robots hablando en idiomas extraños... Al mismo tiempo animar a los
usuarios a pasarse a otras plataformas como
[PeerTube](https://joinpeertube.org/es/),
[Odysee](https://es.wikipedia.org/wiki/Odysee), etc., que son más
respetuosas con la privacidad.

De la misma forma podríamos llenar Google Drive de archivos basura.
Habría que usar probablemente varias cuentas e intentar no llamar
demasiado la atención de Google. Se pueden usar idiomas minoritarios,
archivos que parecen legítimos, etc.

Para que el ataque a Google tenga el mayor impacto posible deben usarse
varias cuentas distintas, con diferentes IP, y coordinarlo todo entre
varias personas. No hay ninguna ley que prohíba subir vídeos y archivos
generados de forma aleatoria, y aunque la hubiera, se podría hacer de
forma anónima para evitar ser identificado.

## Usar sus servidores mediante un <i lang="en">proxy</i> sin pagar nada

Se pueden usar programas como
[Piped](/youtube-con-privacidad-con-piped/),
[Invidious](/youtube-con-privacidad-con-invidious/) o
[NewPipe](https://newpipe.net/). YouTube gasta dinero enviando archivos
de vídeo a través de Internet. De esta forma, no recoge datos personales
nuestros al mismo tiempo que pierde dinero, y nosotros disfrutamos de
los vídeos. 😆

## Boicot a sus productos

No comprar nada que dé dinero a Google. Si ya tienes un producto de
Google, se pueden tomar medidas para que le envíe el menor número de
datos posible. Por ejemplo, se pueden [cambiar los ajustes del portal
cautivo de Android](/no-digas-a-google-d%C3%B3nde-te-conectas-a-internet-en-android-portal-cautivo/).

Si necesitas un móvil, puedes comprar un móvil de teclas, un móvil con
GNU/Linux o uno con una [distribución libre de
Android](https://en.wikipedia.org/wiki/List_of_custom_Android_distributions).

## Propaganda contra Google

Este artículo es un ejemplo de ello. ✊
