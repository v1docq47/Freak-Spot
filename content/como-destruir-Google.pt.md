Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2022-11-17 10:00
Lang: pt
Save_as: como-destruir-o-Google/index.html
URL: como-destruir-o-Google/
Slug: Cómo-destruir-Google
Tags: ação direta, boicote, anúncios, Google, privacidade,
Title: Como destruir o Google
Image: <img src="/wp-content/uploads/2022/11/aniquilar-a-Google.png" alt="" width="960" height="540" srcset="/wp-content/uploads/2022/11/aniquilar-a-Google.png 960w, /wp-content/uploads/2022/11/aniquilar-a-Google-480x270.png 480w" sizes="(max-width: 960px) 100vw, 960px">

O modelo empresarial do Google é baseado na recolha dos dados pessoais
dos utilizadores, na sua venda a terceiros e na sua veiculação de
anúncios. A empresa também [se envolve em programas de espionagem](https://es.wikipedia.org/wiki/PRISM),
[desenvolve programas de inteligência artificial para fins militares](/pt/preencher-reCAPTCHA-do-Google-ajuda-a-matar/),
[explora utilizadores](/pt/Como-o-Google-explora-com-os-CAPTCHAs/).

É uma das empresas mais poderosas do planeta. No entanto, o Google é um
gigante com pés de barro que pode ser aniquilado.

## Acabar com as suas receitas publicitarias

O Google ganha dinheiro ao servir anúncios personalizados com base nas
informações que recolhe dos seus utilizadores. Se as pessoas não vêem
anúncios, o Google não ganha dinheiro. O bloqueio de anúncios é uma
forma de impedir o rastreio e fazer o Google perder dinheiro, mas se
visitar as páginas do Google, o Google continuará a obter informações
que pode vender aos anunciantes. Portanto, a coisa mais fácil a fazer é
[bloquear os anúncios](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/).

Outra ideia é clicar em todos os anúncios com a extensão [AdNauseam](https://adnauseam.io/), que
também os esconde de nós para que não os achemos irritantes. Este método
significa que a Google ganha menos dinheiro com os cliques dos anúncios
e que os servidores do Google têm um pouco mais de carga de trabalho
(mínimo, mas isso aumenta os seus custos).

## Encher os servidores do Google de porcaria

O Google permite-te carregar quase tudo nos seus servidores (vídeos,
ficheiros, etc.). Se o conteúdo carregado para os seus servidores ocupa
muito espaço e é lixo que afasta as pessoas dos seus serviços (vídeos
com vozes de robôs a dizer disparates, centenas de vídeos com ruído que
ocupam gigas e gigas), o custo de manutenção dos servidores aumenta e o
lucro da empresa é reduzido.

Se este for um esforço coordenado globalmente por vários utilizadores, a
Google teria de começar a restringir os carregamentos, contratar pessoas
para encontrar vídeos de lixo, bloquear pessoas e endereços IP, etc., o
que aumentaria as suas perdas e reduziria os seus lucros.

Por exemplo, posso criar vídeos de 15 minutos por hora e carregá-los
para o YouTube automaticamente ou semi-automaticamente. Os vídeos devem
ocupar muito espaço. Quanto mais resolução, mais cores, mais variedade
de som, mais quadros por segundo, mais dinheiro o YouTube vai gastar
para manter esses vídeos nos seus servidores.

O vídeo que mostro abaixo foi [gerado automaticamente com `ffmpeg`](/pt/criar-vídeos-de-ruído-com-FFmpeg/). Tem
apenas dois segundos de duração, mas ocupa 136
<abbr title="Megabytes">MB</abbr>. Um vídeo semelhante de 15 minutos
levaria 61,2 <abbr title="Gigabyes">GB</abbr>.

<!-- more -->

<video controls>
  <source src="/video/basura.mp4" type="video/webm">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

Deve haver variações de vídeos aleatórios, vídeos de lixo de robôs a
falar em línguas estranhas... Ao mesmo tempo, encorajar os utilizadores
a mudar para outras plataformas, tais como
[PeerTube](https://joinpeertube.org/pt_BR/), Odysee, etc., que são mais
respeitadoras da privacidade.

Da mesma forma, podíamos encher o Google Drive com ficheiros de lixo.
Teríamos provavelmente de usar várias contas e tentar não atrair
demasiada atenção do Google. Podem ser utilizadas línguas minoritárias,
ficheiros que pareçam legítimos, etc.

Para que o ataque ao Google tenha o maior impacto possível, devem ser
utilizadas várias contas diferentes, com diferentes IPs, e tudo deve ser
coordenado entre várias pessoas. Não há lei contra o carregamento de
vídeos e ficheiros gerados aleatoriamente, e mesmo que houvesse, poderia
ser feito de forma anónima para evitar ser identificado.

## Utilizar os seus servidores através de um <i lang="en">proxy</i> sem pagar nada

Podes utilizar programas tais como
[Piped](/pt/YouTube-com-privacidade-com-Piped/),
[Invidious](/pt/YouTube-com-privacidade-com-o-Invidious/) ou
[NewPipe](https://newpipe.net/). O YouTube gasta dinheiro enviando
ficheiros de vídeo através da Internet. Desta forma, não recolhe os
nossos dados pessoais ao mesmo tempo que perde dinheiro, e nós
desfrutamos os vídeos. 😆

## Boicotar os seus produtos

Não compres nada que dê dinheiro ao Google. Se já tens um produto de
Google, podes tomar medidas para enviar o mínimo de dados possível para o
Google. Por exemplo, é possível [alterar as definições do portal cativo
do Android](/no-digas-a-google-dónde-te-conectas-a-internet-en-android-portal-cautivo/).

Se precisar de um telemóvel, podes comprar um telemóvel burro, um
telefone com GNU/Linux ou um com uma distribuição gratuita Android.

## Propaganda contra o Google

Este artigo é um exemplo disso. ✊
