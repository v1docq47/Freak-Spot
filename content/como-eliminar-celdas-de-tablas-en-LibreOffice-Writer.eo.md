Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2023-01-30 10:00
Lang: eo
Slug: cómo-eliminar-celdas-de-tablas-en-libreoffice-writer
Save_as: kiel-forigi-tabelajn-ĉelojn-en-LibreOffice-Writer/index.html
URL: kiel-forigi-tabelajn-ĉelojn-en-LibreOffice-Writer/
Tags: LibreOffice Writer
Title: Kiel forigi tabelajn ĉelojn en LibreOffice Writer
Image: <img src="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png" alt="" width="786" height="421" srcset="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png 786w, /wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer-393x210.png 393w" sizes="(max-width: 786px) 100vw, 786px">

Por forigi ĉelojn la sola opcio estas forigi ĝiajn borderojn. Ĉi tie mi
havas tabelon, el kiu mi volas forigi tri el la plej supraj ĉeloj, kiuj
troas:

<a href="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png" alt="" width="791" height="419" srcset="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png 791w, /wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer-395x209.png 395w" sizes="(max-width: 791px) 100vw, 791px">
</a>

Por fari tion ni devas alklaki la **Borderoj**-butonon, kiu aperas
malsupre, kiam ni elektas la forigotajn ĉelojn, kaj poste mi alklakas la
butonon **Sen borderoj**, kiel ni faras en la jena bildo:

<a href="/wp-content/uploads/2023/01/forigas-tabelajn-borderojn-en-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/forigas-tabelajn-borderojn-en-LibreOffice-Writer.png" alt="" width="967" height="975" srcset="/wp-content/uploads/2023/01/forigas-tabelajn-borderojn-en-LibreOffice-Writer.png 967w, /wp-content/uploads/2023/01/forigas-tabelajn-borderojn-en-LibreOffice-Writer-483x487.png 483w" sizes="(max-width: 967px) 100vw, 967px">
</a>

Se vi perdiĝas, ĉi tie estas ankaŭ klariga video:

<video controls>
  <source src="/video/eliminar-células-de-tabelas-no-LibreOffice-Writer.webm" type="video/webm">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

Jen la rezulto:

<a href="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png" alt="" width="786" height="421" srcset="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png 786w, /wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer-393x210.png 393w" sizes="(max-width: 786px) 100vw, 786px">
</a>
