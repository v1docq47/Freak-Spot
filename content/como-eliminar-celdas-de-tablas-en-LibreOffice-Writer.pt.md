Author: Jorge Maldonado Ventura
Category: Edição de textos
Date: 2023-01-21 11:00
Lang: pt
Slug: cómo-eliminar-celdas-de-tablas-en-libreoffice-writer
Save_as: como-eliminar-células-de-tabelas-no-LibreOffice-Writer/index.html
URL: como-eliminar-células-de-tabelas-no-LibreOffice-Writer/
Tags: LibreOffice Writer
Title: Como eliminar células de tabelas no LibreOffice Writer
Image: <img src="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png" alt="" width="786" height="421" srcset="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png 786w, /wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer-393x210.png 393w" sizes="(max-width: 786px) 100vw, 786px">

Para remover células, a única opção é eliminar os seus contornos. Aqui
tenho uma tabela da qual quero remover três das células mais altas que
sobram:

<a href="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png" alt="" width="791" height="419" srcset="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png 791w, /wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer-395x209.png 395w" sizes="(max-width: 791px) 100vw, 791px">
</a>

Para o fazer carrego no botão **Contornos** que aparece no fundo quando
seleccionamos as células que queremos apagar e depois carrego no botão
**Sem contorno**, como faço na imagem seguinte:

<a href="/wp-content/uploads/2023/01/removendo-contornos-de-tabela-no-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/removendo-contornos-de-tabela-no-LibreOffice-Writer.png" alt="" width="961" height="974" srcset="/wp-content/uploads/2023/01/removendo-contornos-de-tabela-no-LibreOffice-Writer.png 961w, /wp-content/uploads/2023/01/removendo-contornos-de-tabela-no-LibreOffice-Writer-480x487.png 480w" sizes="(max-width: 961px) 100vw, 961px">
</a>

No caso de se perder, aqui está também uma demonstração em vídeo:

<video controls>
  <source src="/video/eliminar-células-de-tabelas-no-LibreOffice-Writer.webm" type="video/webm">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

O resultado é o seguinte:

<a href="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png" alt="" width="786" height="421" srcset="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png 786w, /wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer-393x210.png 393w" sizes="(max-width: 786px) 100vw, 786px">
</a>
