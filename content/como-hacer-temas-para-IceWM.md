Author: Ren Canteros Sousa
Category: GNU/Linux
Date: 2017-06-17 11:57
Image: <img src="/wp-content/uploads/2017/06/arkanos3.jpg" alt="Tema IceWM">
Lang: es
Slug: como-hacer-temas-para-icewm
Tags: gestor de ventanas, IceWM
Title: Cómo hacer temas para IceWM

Si eres usuario del gestor de ventanas [IceWM](http://www.icewm.org/)
tal vez te preguntes cómo se hacen los temas del mismo. Además, tal vez
hayas visto algún tema que te gustó pero que tiene algo que no te agrada
y deseas saber como modificarlo. O puede ser que encontraste un tema de
Metacity/XFW/Openbox/etcétera y tienes deseos de adaptarlo a IceWM. En
el siguiente tutorial te mostraré cómo se hace esto. Verás que no es tan
complicado como parece.

<!-- more -->

## Introducción

### ¿Qué es un tema de IceWM?

Un tema de IceWM esta hecho de las siguientes partes:

- Uno o más archivos de temas, incluyendo un archivo `default.theme`, que
  debe de especificar todas las opciones de apariencia de iceWM, en el
  formato de `opción=valor` en cada línea (algunos de los valores o
  preferencias no pueden cambiarse en la especificación del tema).
- Una colección de imágenes (archivos <abbr title="X PixMap">XPM</abbr>) que
  son usadas para construir la decoración de las ventanas.
- Una colección de imágenes (archivos <abbr title="X PixMap">XPM</abbr>) para
  punteros de ratón, iconos especiales, icono para la bandeja de correo
  (mediante mutt) y la barra de tareas.

Las imágenes que usaremos, tanto el archivo `default.theme`, debe
permanecer en una sola carpeta con el nombre del tema.  Y esta última
debe de estar en el directorio `$HOME` de nuestro usuario, dentro de la carpeta
`~/.icewm/themes`. De esta forma, podremos ver como va quedando nuestro tema
cada vez que hagamos alguna modificación al mismo.

### Tipos de temas

Hay varios tipos de temas, que no son más que distintos tipos de
apariencia que se pueden ver en IceWM con la opción "Look". La cual
soporta las siguientes: warp3, warp4, win95, motif, nice, pixmap, metal,
gtk y flat. Estos permiten diferentes tipos de modificación de la
apariencia y requieren diferentes números de imágenes. (Notemos que
pixmap, metal y gtk se ven casi igual.)

En este tutorial, usaremos el valor flat, que es el que más se asemeja a
la apariencia de los temas actuales, aunque si lo deseas puedes hacerlo
con otra variable. Para ver con cuál se ve mejor tu tema edita el
archivo `default.theme`, cambia la opción a la cual quieras probar. Por
ejemplo si tenemos un tema con la apariencia flat la podemos cambiar por
cualquier otra, de tal forma que lo tenemos así:

    :::bash
    Look=valor

Donde `valor` es la apariencia deseada.

### ¿Qué se necesia para hacer un tema?

Necesitamos un editor de imágenes, este puede ser <abbr title="GNU Image Manipulation Program">GIMP</abbr>, Mtpaint o el
que queramos siempre y cuando podamos manejar y guardar imágenes en
formato `.xpm`. No es necesario tener muchos conocimientos en estos, con
que sepamos lo básico nos bastará. También necesitamos un editor de
texto, yo uso Leafpad, o bien Ne. Usa el editor que mejor se adapte a
tus necesidades y donde te sientas más cómodo.

## Creando tu primer tema

Para empezar, necesitarás crear una nueva carpeta donde va a estar todos
los archivos `.xpm`, además de tres  o cuatro subcarpetas a las cuales
vas a llamar: `icons`, `taskbar`, `ledclock` (opcional) y `mailbox`. Tal
vez creas necesario ponerle nombre a tu tema, así que si se te ocurre
algo original renombra la carpeta principal con su nuevo nombre. O si lo
prefieres puedes dejarlo hasta el final y le pones como nombre «prueba»,
ahora corta y pega tu carpeta en `~/.icewm/themes`, y ahora
sí: ¡Manos a la obra!

### El archivo `default.theme`

Primero abrimos nuestro editor de textos favorito y en él escribimos lo
siguiente:

    :::bash
    ThemeName="Nombre del tema"
    ThemeDescription="Descripción"
    ThemeAuthor="Tu nombre  o nombres de los autores <tu-correo@algo>"
    License="Licencia"
    Look=flat

Donde `ThemeName` es el nombre del tema, `ThemeDescription` es para que
pongamos una descripción breve de nuestro tema, `ThemeAuthor` es para
que pongamos nuestro nombre o mote junto a nuestro correo de contacto
(esto es esencial si queremos que los usuarios de nuestros temas nos
envíen retroalimentación), y `Look` es la apariencia que escogimos para
nuestro tema. Respecto a la licencia yo elijo la
[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html) para mis temas,
aunque esto es personal, puedes usar la licencia que quieras. En este ejemplo
usaré un tema ya hecho de mi autoría llamado
[«arkanos»](https://www.box-look.org/p/1018102/). Ya que hayamos terminado de
nombrar nuestro tema y escrito la descripción correspondiente, guardamos
nuestro archivo de texto dentro de la carpeta de nuestro tema y lo llamaremos
`default.theme`. Entonces nos quedará algo así:

<a href="/wp-content/uploads/2017/06/editando-tema-IceWM.jpg">
<img alt="Creando tema de IceWM" src="/wp-content/uploads/2017/06/editando-tema-IceWM.jpg">
</a>

Después de esto, empezamos por armar las partes básicas del tema, por lo
pronto escribimos lo siguiente. Tengamos en cuenta que no importa el
orden de las opciones del tema, puedes ponerlos en el orden que quieras,
también recuerda que las líneas que contienen una almohadilla (#) al
principio son comentarios (no hace falta escribirlos en el tema).

    :::bash
    #Muestra un efecto en los botones al pasar el cursor por encima de ellos
    RolloverButtonsSupported=1
    #Tamaño de la barra de título
    TitleBarHeight=18
    # El texto de la barra de título aparece centrado.
    TitleBarCentered=1
    TitleBarJustify=50
    #Define que botones van a ir a la izquierda de la barra de título.
    TitleButtonsLeft="s"
    #Define que botones van a la derecha.
    TitleButtonsRight="xmi"
    #Define que botones soportará tu tema
    TitleButtonsSupported="xmis"
    #Muestra el icono de la aplicación en el botón del menú de la barra de título.
    ShowMenuButtonIcon=1 # 0/1

Notemos que los botones se representan con las siguientes letras: cerrar
(x), ocultar(h), maximizar y restaurar (m), botón de menú(s), minimizar
(i), encoger (r) y ocultar (d). En este ejemplo, mostraremos únicamente
los botones tradicionales (xmis), aunque puedes cambiar su posición en
la barra de título simplemente cambiando su ubicación. Si quisieramos
que los botones de cerrar, maximizar y minimizar estuvieran a la
izquierda, lo que escribiríamos sería esto:

    :::bash
    TitleButtonsLeft="xmi"
    TitleButtonsRight="s"

Ahora pasamos a definir el tamaño de los bordes y esquinas de nuestro
tema, entonces:

    :::bash
    #Tamaño de los bordes.
    BorderSizeX=4
    BorderSizeY=2
    #Tamaño de las esquinas.
    CornerSizeX=22
    CornerSizeY=22
    #Tamaño de los bordes de los cuadros de diálogo.
    DlgBorderSizeX=4
    DlgBorderSizeY=2

Ahora pasamos a configurar el aspecto del reloj de la barra de tareas,
en este caso sólo usaremos uno simple, aunque si lo quisiéramos podemos
crear imágenes `.xpm` para que estas se muestren en lugar del tipo de
letra que elijamos más adelante. Para esto tendremos que poner la opción
`TaskBarClockLeds` a `1`. En este ejemplo lo dejaremos en `0`.

    :::bash
    TaskBarClockLeds=0
    #El color del reloj, si dejamos está opción sin valor este será transparente.
    ColorClock=""
    #El color del texto del reloj.
    ColorClockText="black"

Si te fijas bien, en donde pusimos el color usamos la palabra «black»
(negro), puedes usar los nombres de los colores HTML. Por ejemplo:
`white`, `red`, `black`, `green`, `blue`, `magenta`, `cyan`, `brown`,
etc. Pero si queremos usar un color distinto a los básicos lo
representamos con su código HTML, por ejemplo para el negro lo
representamos así:

    :::bash
    ColorClockText="black"


¿Y cómo obtenemos el color/código deseado?, ¿o qué colores usar?, bueno
es cuestión de gustos. Si por ejemplo quisiéramos crear un tema oscuro,
lo que debemos de hacer es buscar colores oscuros que nos agraden.  Con
<abbr title="GNU Image Manipulation Program">GIMP</abbr>, sólo basta abrirlo y añadir el empotrable Colores, después vamos a
buscar el color deseado, y después simplemente copiamos el código a
nuestro tema. En este ejemplo, uso colores claros y algo de azul. Ahora
que, también puedes usar imágenes que te agraden y usarlas como base
para los colores de tu tema, abrimos la imagen en cuestión, y con la
herramienta de gotero seleccionamos el color o los colores que nos
gusten.

<a href="/wp-content/uploads/2017/06/selector-de-color-GIMP.jpg">
<img alt="Selector de color en GIMP" src="/wp-content/uploads/2017/06/selector-de-color-GIMP.jpg">
</a>

Si no tenemos muy claro qué aspecto va a tener nuestro tema, podemos
basarnos en uno ya hecho. Si lo haces, no olvides dar crédito al autor
original del tema y mencionar el nombre del tema. Claro, si vas a hacer
un tema exactamente igual al original, será más sencillo que le pongas
el mismo nombre o uno parecido para que así sea más fácil de ubicar por
los usuarios.

Bien, vamos a personalizar los colores de la barra de tareas.

    :::bash
    #Color por defecto de la barra de tareas.
    ColorDefaultTaskBar="#ebebeb"
    #Color normal de la aplicación.
    ColorNormalTaskBarApp="#ebebeb"
    #Color normal del texto de la aplicación.
    ColorNormalTaskBarAppText="#0e0e0e"
    #Color de la aplicación activa.
    ColorActiveTaskBarApp="#ebebeb"
    #Color del texto de la aplicación activa.
    ColorActiveTaskBarAppText="white"
    #Color de la aplicación minimizada.
    ColorMinimizedTaskBarApp="#ebebeb"
    #Color del texto de la aplicación minimizada.
    ColorMinimizedTaskBarAppText="#0e0e0e"
    #Color del texto del espacio de trabajo activo.
    ColorActiveWorkspaceButtonText="white"
    #Color del texto del espacio de trabajo normal o inactivo.
    ColorNormalWorkspaceButtonText="#0e0e0e"

En la configuración anterior vemos que he elegido el mismo color para
todos los estados posibles de la aplicación de la barra de tareas,
¿por qué?; ya que crearemos imágenes <abbr title="X PixMap">XPM</abbr>
después con el mismo color, excepto por el estado activo no es necesario
que este cambie, a menos que no vayamos a usar una imagen. De ser así,
ponemos un color distinto. Por ejemplo:

    :::bash
    ColorActiveTaskBarApp="#84c3eb"

Ahora es el turno de personalizar el cuadro que aparece al presionar
<kbd>Alt</kbd>+<kbd>Tab</kbd> para cambiar la ventana (QuickSwitch).

    :::bash
    #Color del cuadro.
    ColorQuickSwitch="#ebebeb"
    #Color del texto del cuadro.
    ColorQuickSwitchText="#525252"
    #Color del elemento seleccionado.
    ColorQuickSwitchActive="#84c3eb"

Las siguientes opciones son para el menú de IceWM.

    :::bash
    #Color normal del menú.
    ColorNormalMenu="#efefef"
    #Color del elemento seleccionado.
    ColorActiveMenuItem="#84c3eb"
    #Color del texto del menú.
    ColorNormalMenuItemText="#525252"
    #Color del texto seleccionado.
    ColorActiveMenuItemText="white"
    #Color del elemento desactivado del menú.
    ColorDisabledMenuItemText="#7f9ab7"

Y estas son para personalizar los colores de los medidores de consumo
del <abbr title="Central Processing Unit">CPU</abbr> y red de la barra
de tareas.

    :::bash
    #Color del uso del CPU del usuario.
    ColorCPUStatusUser="#6aa4e0"
    #Color del uso del CPU del sistema.
    ColorCPUStatusSystem="#007eff"
    #Color de los procesos.
    ColorCPUStatusNice="#6aa4e0"
    #Color del espacio inactivo del CPU.
    ColorCPUStatusIdle="black"
    ColorNet="#bdbcbc"
    ColorNetSend="#bdbcbc"
    ColorNetReceive="#007eff"
    ColorNetIdle="black"

En esta parte configuramos el fondo de pantalla que queramos, en este
ejemplo sólo usaremos un color para ello. Si quisieras incluir una
imagen, esta debe de estar dentro de la carpeta del tema, y para esto
debes de escribir su nombre completo con extensión. Simplemente
descomenta las tres últimas opciones.

    :::bash
    #Color de fondo.
    DesktopBackgroundColor="#484d6b"
    #La imagen de fondo centrada.
    #DesktopBackgroundCenter=1 # 0 / 1
    #Escala la imagen en la pantalla.
    #DesktopBackgroundScaled=1 # 0 / 1
    #La ruta a la imagen de fondo. Si esta dentro de la carpeta de tu tema con poner su nombre basta.
    #DesktopBackgroundImage=""

Siguen los colores de los bordes.

    :::bash
    #Color normal del borde.
    ColorNormalBorder="#cbcbcb"
    #Color del borde activo.
    ColorActiveBorder="#cbcbcb"

Notamos que hemos elegido el mismo color para el estado normal y activo,
ya que en este tema usaremos las mismas imágenes para las ventanas
activas y normales lo dejamos así. Toca entonces definir el color de la
barra de título y botones, en este caso lo que va a diferenciar va a ser
los colores del texto de la barra de título y los botones que le vamos a
poner. Entonces:

    :::bash
    #Color para la barra de titulo activa.
    ColorActiveTitleBar="#cbcbcb"
    #Color para la barra de título normal.
    ColorNormalTitleBar="#cbcbcb"
    #Color del botón normal.
    ColorNormalButton="#cbcbcb"
    #Color del botón activo.
    ColorActiveButton="#cbcbcb"
    #Color del texto de la barra de título normal.
    ColorNormalTitleBarText="#4f4c47"
    #Color del texto de la barra de título activa.
    ColorActiveTitleBarText="#191a1c"

Para cambiar los colores del pequeño cuadro de estado que nos muestra la
posición de la ventana seleccionada al moverla, añadimos lo siguiente:

    :::bash
    #Color del cuadro.
    ColorMoveSizeStatus="#84c3eb"
    #Color del texto.
    ColorMoveSizeStatusText="#white"

<a href="/wp-content/uploads/2017/06/propiedades-ventana-IceWM.png">
<img alt="Propiedades de la ventana en IceWM" src="/wp-content/uploads/2017/06/propiedades-ventana-IceWM.png">
</a>

Nos queda personalizar la lista de ventanas y sus elementos. Por lo tanto:

    :::bash
    #Color de la lista de ventanas.
    ColorListBox="#ebebeb"
    #Color del texto de lista de ventanas.
    ColorListBoxText="#939da9"
    #Color del elemento seleccionado.
    ColorListBoxSelection="#84c3eb"
    #Color del texto del elemento seleccionado.
    ColorListBoxSelectionText="white"
    #Color de la barra de desplazamiento.
    ColorScrollBar="#ebebeb"
    #Color del deslizador de la barra de desplazamiento.
    ColorScrollBarSlider="#ebebeb"
    #Color del botón de la barra de desplazamiento.
    ColorScrollBarButton="#ebebeb"
    #Color de la flecha del botón de la barra de desplazamiento.
    ColorScrollBarButtonArrow="#84c3eb"

Es el turno de las etiquetas de texto que aparecen al poner el puntero del ratón por encima de distintos elementos.

    :::bash
    ColorToolTip="#f9f9f9"
    ColorToolTipText="#525252"
    ColorLabel="#f9f9f9"
    ColorLabelText="#525252"

Con esta configuración se verá así:

<a href="/wp-content/uploads/2017/06/tooltip-personalizado-IceWM.png">
<img alt="Tooltip para IceWM" src="/wp-content/uploads/2017/06/tooltip-personalizado-IceWM.png">
</a>

Ya casi terminamos de editar el archivo `default.theme`. Por último es
el turno de los tipos de letras que usaremos, yo he elegido Droid Sans.
El tipo de letra le dará la marca personal a nuestro tema, hay muchos
tipos de letras, en este caso te recomiendo usar uno libre. Entonces:

    :::bash
    #Tipo de letra de la barra de título.
    TitleFontNameXft="droid sans:size=10:bold"
    #Tipo de letra del menú.
    MenuFontNameXft="droid sans:size=10"
    #Tipo de letra de los cuadros de estado.
    StatusFontNameXft="droid sans:size=10"
    #Tipo de letra para el cuadro de cambiar ventana.
    QuickSwitchFontNameXft="droid sans:size=10"
    #Tipo de letra del botón normal o inactivo.
    NormalButtonFontNameXft="droid sans:size=10"
    #Tipo de letra del botón activo.
    ActiveButtonFontNameXft="droid sans:size=10"
    #Tipo de letra de la barra de tareas normal.
    NormalTaskBarFontNameXft="droid sans:size=10"
    #Tipo de letra de la barra de tareas activa.
    ActiveTaskBarFontNameXft="droid sans:size=10"
    #Tipo de letra del botón de herramienta.
    ToolButtonFontNameXft="droid sans:size=10"
    #Tipo de letra de los espacios de trabajo inactivos.
    NormalWorkspaceFontNameXft="droid sans:size=10"
    #Tipo de letra del espacio de trabajo activo.
    ActiveWorkspaceFontNameXft="droid sans:size=10"
    #Tipo de letra de las ventanas minimizadas.
    MinimizedWindowFontNameXft="droid sans:size=10"
    #Tipo de letra de la lista de ventanas.
    ListBoxFontNameXft="droid sans:size=10"
    #Tipo de letra de las etiquetas.
    ToolTipFontNameXft="droid sans:size=10"
    LabelFontNameXft="droid sans:size=10"
    #Tipo de letra del reloj.
    ClockFontNameXft="droid sans:size=10:bold"
    ApmFontNameXft="droid sans:size=10"
    #Tipo de letra de entrada.
    InputFontNameXft="droid sans:size=10"

Perfecto, una vez hecho todo esto, nos queda probar nuestro tema, damos
clic en **Settings > Temas > «nuestro tema»** y este lucirá más o menos
así:

<a href="/wp-content/uploads/2017/06/tema-IceWM.jpg">
<img alt="Tema IceWM" src="/wp-content/uploads/2017/06/tema-IceWM.jpg">
</a>

Es el momento de pasar a la siguiente parte la creación de las imágenes
<abbr title="X PixMap">XPM</abbr> que formarán parte del tema y le darán
un mejor aspecto.

### La barra de tareas

Para empezar, recuerda que debemos de tener la carpeta `taskbar` en
nuestro tema. Vamos a abrir <abbr title="GNU Image Manipulation Program">GIMP</abbr> y crearemos una imagen nueva, esta debe
tener 4 de anchura por 25 de altura.

<a href="/wp-content/uploads/2017/06/crea-imagen-4x25.jpg">
<img src="/wp-content/uploads/2017/06/crea-imagen-4x25.jpg" alt="Crea imagen 4x25">
</a>

Una vez creada, vamos a ponerle un degradado, para esto seleccionamos los
colores que queramos, también recuerda usar el color que definiste para la
barra de tareas. Este último lo usas para que sea el color de frente, y usa uno
más oscuro para el de fondo. Algo como esto:

<a href="/wp-content/uploads/2017/06/selector-de-color-GIMP.jpg">
<img src="/wp-content/uploads/2017/06/selector-de-color-GIMP.jpg" alt="Selector de color en GIMP">
</a>

Una vez que tenemos los colores para el degradado, seleccionamos la herramienta
de mezcla o bien pulsamos «L».

![Botón de degradado en GIMP](/wp-content/uploads/2017/06/degradado-GIMP.jpg)

<a href="/wp-content/uploads/2017/06/creando-degradado-GIMP.jpg">
<img src="/wp-content/uploads/2017/06/creando-degradado-GIMP.jpg" alt="Selector de color en GIMP">
</a>

En el empotrable «Opciones de herramienta» nos mostrará las opciones (sic) de
la herramienta mezcla. Las dejamos tal cual y nos situamos en la parte inferior
de nuestra imagen, entonces presionamos «Ctrl» y movemos nuestro puntero hacia
arriba. Con el degradado hecho, sólo nos queda añadir dos líneas del tamaño de
4 pixel por 1 con la herramienta de selección de rectángulo o con presionar «R»...

<a href="/wp-content/uploads/2017/06/creando-degradado-GIMP.jpg">
<img src="/wp-content/uploads/2017/06/creando-degradado-GIMP.jpg" alt="Selector de color en GIMP">
</a>

y rellenar esos pequeños rectángulos con un color más oscuro que los que hemos
usado en el degradado (siendo el de arriba más claro que el de la parte de
abajo). Con la herramienta de relleno (Shift+ B)...

<a href="/wp-content/uploads/2017/06/herramienta-relleno-GIMP.jpg">
<img src="/wp-content/uploads/2017/06/herramienta-relleno-GIMP.jpg" alt="Herramienta de relleno de GIMP">
</a>

ponemos uno en la parte inferior de la imagen y otro en la parte superior. De
tal modo que nuestra imagen final se verá así:

<a href="/wp-content/uploads/2017/06/barra-tareas-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/barra-tareas-IceWM.jpg" alt="Diseño para personalizar la barra de tareas en IceWM">
</a>

La guardamos dentro de la carpeta `taskbar` con el nombre `taskbarbg.xpm`. Y
aquí va un truco: Para ahorrar espacio en el tema y cómo vamos a usar la misma
imagen para distintos estados de la barra de tarea, lo que haremos en vez de
hacer más imágenes es crear enlaces simbólicos que apunten a la que ya tenemos.

Para ello, nos situamos con la terminal en la carpeta taskbar y escribimos los
siguientes comandos:

    :::bash
    ln -s taskbarbg.xpm taskbuttonbg.xpm
    ln -s taskbarbg.xpm taskbuttonminimized.xpm

Con eso tenemos tanto las imágenes de la barra de tareas y sus estados
minimizado e inactivo, así como el botón normal o inactivo de los espacios de
trabajo.

Para la imagen de la aplicación activa, hacemos como ya hemos explicado
anteriormente, pero en este caso usaremos un color que contraste con el color
dominante de la barra de tareas. En este caso usé como base un tono de azul
(`83c1ea#`), recuerda que la imagen debe de ser 4x25 también.  Por lo que debe
de verse más o menos así:

<a href="/wp-content/uploads/2017/06/aplicacion-activa-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/aplicacion-activa-IceWM.jpg" alt="Diseño para aplicación activa en IceWM">
</a>

Una vez terminada nuestra imagen, la guardamos con el nombre
`taskbuttonactive.xpm` dentro de la carpeta `taskbar`.

Creamos una nueva imagen con el tamaño de 60 de anchura y 20 de altura, este
tamaño es variable y es a nuestro gusto, es para el botón de aplicaciones de
iceWM. Lo podemos hacer más pequeño de anchura o más grande que el aquí
indicado.

Bien, una vez hecho esto, nos vamos a **Archivo > Abrir como capas** y abrimos
la imagen `taskbarbg.xpm` sobre nuestra imagen. Duplicamos la imagen unas
cuatro o tres veces y las juntamos para tener una imagen más grande, es decir,
seleccionamos la capa que este más arriba, damos clic derecho, Combinar las
capas visibles y después Recortada según la imagen. Después duplicamos esta
imagen dos o tres veces más y llenamos el espacio en blanco de nuestra imagen.

Con esto, ahora podemos crear una imagen pequeña en el centro de nuestra
imagen, o bien, utilizar la herramienta de texto y nuestro tipo de fuente
favorita y escribimos: `Inicio`, `aplicaciones`, `menú`, `icewm`, etc. Nos
tiene que quedar así, notemos que he usado un icono de unos copos de nieve:

<a href="/wp-content/uploads/2017/06/inicio-iconos-de-nieve.png">
<img src="/wp-content/uploads/2017/06/inicio-iconos-de-nieve.png" alt="Inicio de IceWM con diseño de copos de nieve">
</a>

La guardamos con el nombre de `linux.xpm` o `icewm.xpm`. También puedes crear
más botones distintos al que va ir por defecto en tu tema y guardarlos con otro
nombre dentro de la carpeta taskbar.

Por último, es el turno de los iconos de la barra de tareas. Si queremos, los
podemos crear nosotros mismos o bien podemos usar unos que ya estén hechos
(siempre y cuando su licencia nos lo permita). Deben de medir 20x20, y estos
corresponden a las funciones: Mostrar escritorio (`desktop.xpm`) y Lista de
ventanas (`windows.xpm`).

Al final nuestra barra de tareas lucirá así:

<a href="/wp-content/uploads/2017/06/barra-de-tareas-completa-IceWM.png">
<img src="/wp-content/uploads/2017/06/barra-de-tareas-completa-IceWM.png" alt="Barra de tareas completa en IceWM">
</a>

### Carpeta `mailbox` (opcional)

Dentro de esta, se deben de crear o añadir iconos para la bandeja de correo.
Estos representan los siguientes estados:

- Error (`errmail.xpm`)
- Correo (`mail.xpm`)
- Nuevo correo (`newmail.xpm`)
- Sin correo (`nomail.xpm`)
- Correo no leído (`unreadmail.xpm`)

Estas imágenes deben de medir 16x16 y ser de un color o forma que las haga
distinguirse de la barra de tareas. Por ejemplo:

<a href="/wp-content/uploads/2017/06/iconos-correo-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/iconos-correo-IceWM.jpg" alt="Iconos de correo para IceWM">
</a>

Esta carpeta es opcional, si no usamos un gestor de correos para la terminal,
podemos prescindir de esta. Nuestro tema funcionará bien con o sin ella.

### Carpeta `icons`

En esta carpeta van a ir los iconos para aplicación (app), terminal (xterm) y
carpeta del menú (folder). Para ahorrar espacio en el tema, vamos a poner
solamente tres iconos y vamos a crear enlaces simbólicos para los demás. Por
ejemplo, `ln -s app_11x11.xpm app_16x16.xpm`, etc.

Aquí no importa mucho el tamaño de nuestros iconos, los podemos dejar grandes
(256x256) o bien chicos (32x32), tendrán el mismo aspecto. Yo prefiero dejarlos
cómo están. Estos son:

- **Aplicación**: `app_11x11.xpm`, `app_16x16.xpm`, `app_32x32.xpm`
- **Terminal**: `xterm_11x11.xpm`, `xterm_16x16.xpm`, `xterm_32x32.xpm`
- **Carpeta**: `folder_11x11.xpm`, `folder_16x16.xpm`, `folder_32x32.xpm`

Aquí hay otro truco: en este tema, he creado una imagen transparente de 48x48
para el icono de la carpeta o directorio. Recordemos que los archivos <abbr title="X PixMap">XPM</abbr>
soportan transparencias, y como quiero que en mi tema no se vean las carpetas
del menú la he hecho así de modo que cuando lo abrimos vemos esto:

<a href="/wp-content/uploads/2017/06/menu-de-aplicaciones-IceWM.png">
<img src="/wp-content/uploads/2017/06/menu-de-aplicaciones-IceWM.png" alt="Menú de aplicaciones en IceWM">
</a>

Si queremos ver las carpetas en los menús, simplemente pon un icono que te
guste o haz uno.

### Carpeta ledclock

Si queremos que nuestro reloj de la barra de tareas use imágenes <abbr title="X PixMap">XPM</abbr>
en vez del tipo de letra que definimos anteriormente, lo que
vamos a hacer es crear en nuestra carpeta principal la carpeta «ledclock» y en
nuestro archivo default.theme añadir la siguiente línea o ponerla a 1 :

    :::bash
    TaskBarClockLeds=1

En <abbr title="GNU Image Manipulation Program">GIMP</abbr>, crearemos las siguientes imágenes de 10x20, aunque aquellas que
lleven símbolos como «:», «.» y «/» pueden ser más pequeñas. Estas son las
siguientes:

- `a.xpm`
- `colon.xpm`
- `dot.xpm`
- `m.xpm`
- `p.xpm`
- `percent.xpm`
- `slash.xpm`
- `space.xpm`
- `0.xpm`
- `n1.xpm`
- ... (hasta `n9.xpm`)

Para que nuestros archivos <abbr title="X PixMap">XPM</abbr> parezcan transparentes, al crearlos con <abbr title="GNU Image Manipulation Program">GIMP</abbr>
vamos a abrir como capas la imagen que usamos para el fondo de la barra de
tareas (`taskbuttonbg.xpm`) y duplicamos y fusionamos las capas hasta que cubran
el fondo de las mismas. Si lo queremos, podemos usar otro efecto, color o fondo
para estos y así lograremos un reloj más distintivo y original.

### El menú de IceWM

Ahora nos situamos en la carpeta principal de nuestro tema, vamos a crear dos
imágenes para el menú de IceWM. Una de ellas es `menubg.xpm`, y es el fondo del
menú. La creamos de 950x2, en este caso la anchura puede variar. La imagen en
sí debe de ser de dos colores en degradado, este debe de empezar a la izquierda
este debe de empezar con el color oscuro primero y después el claro (usando la
herramienta de mezcla).

Para la imagen del elemento seleccionado, creamos una imagen de 4x25,  aquí
podemos usar la misma imagen que usamos para la barra de tareas
(`taskbuttonactive.xpm`) o bien una imagen con otro color o colores. Por
ejemplo, tengo un tema llamado «levo» al cual el menú lo he dejado de otro
color que el de la barra de tareas:

<a href="/wp-content/uploads/2017/06/menu-de-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/menu-de-IceWM.jpg" alt="Menú de aplicaciones en IceWM">
</a>

Así el menú resalta más, es cuestión de ir probando con uno u otro color y ver
cuál de ellos es el mejor para nuestro tema.

### La decoración de la ventana

Esta es la parte un tanto más laboriosa de este tutorial, y es que no es por la
cantidad de imágenes que vamos a crear, sino más bien por la cantidad de
enlaces simbólicos que haremos.

En primer lugar, vamos a empezar por la barra de título, en este tema de
ejemplo sólo vamos a crear una sola imagen que se usará tanto en el la ventana
activa como en la inactiva o normal. Y como se puede notar ambas tendrán el
mismo aspecto salvo por el color del tipo de letra de la ventana y los botones
inactivos.

Creamos en <abbr title="GNU Image Manipulation Program">GIMP</abbr> una imagen de 4x18, y en la parte inferior le ponemos una
pequeña línea del color oscuro o claro de nuestra preferencia. Aquí podemos
hacer dos cosas: O usar un color sólido o un degradado.

El resultado es distinto, y si por ejemplo usamos un color sólido nos
ahorraremos trabajo haciendo los marcos.

<a href="/wp-content/uploads/2017/06/tileAB-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/tileAB-IceWM.jpg" alt="TileAB en IceWM">
</a>

Una vez que hayamos hecho la imagen, la guardaremos con el nombre
`titleAB.xpm`. Y si pensamos en usar otro color para la ventana inactiva,
hacemos otra imagen con las mismas medidas y la guardamos con el nombre de
`titleIB.xpm`, si este es el caso crearemos los enlaces simbólicos de
`titleAB.xpm` (activa) y `titleIB.xpm` (inactiva o normal) por separado y para
el resto de las imágenes. Es decir

    :::bash
    ln -s titleAB.xpm titleAL.xpm

etc.

    :::bash
    ln -s titleIB.xpm titleIL.xpm

En este ejemplo, como sólo usaremos una imagen para ambas, enlazaremos a
`titleAB.xpm` para todas las demás. Los nombres son los siguientes:

Ventana activa: `titleAB.xpm`, `titleAL.xpm`, `titleAM.xpm`, `titleAP.xpm`,
`titleAR.xpm`, `titleAS.xpm`, `titleAT.xpm`

Ventana inactiva: `titleIB.xpm`, `titleIL.xpm`, `titleIM.xpm`, `titleIP.xpm`,
`titleIR.xpm`, `titleIS.xpm`, `titleIT.xpm`

Una vez que hemos terminado de hacer los enlaces simbólicos para la barra de
título pasamos a las esquinas de las ventanas.  Las esquinas superiores son
para la ventana activa: `frameATL.xpm` (esquina izquierda), `frameATR.xpm`
(esquina derecha).

Y para la ventana inactiva: `frameITL.xpm` y `frameITR.xpm`.

En <abbr title="GNU Image Manipulation Program">GIMP</abbr> creamos una imagen de 22x21 y con los colores que hemos usado para la
barra de título, esta vez creamos la esquina con dos rectángulos de un mismo
color y borramos unos cuantos píxeles justo en la esquina si queremos que estas
sean redondeadas. También debemos de dejar un cuadrado de 18x18 que vamos a
dejar transparente, y al final nuestra imagen se verá así:

<a href="/wp-content/uploads/2017/06/esquina-rectangulo-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/esquina-rectangulo-IceWM.jpg" alt="Esquina rectángulo IceWM">
</a>

Para la esquina superior derecha simplemente utilizamos la herramienta de volteo de <abbr title="GNU Image Manipulation Program">GIMP</abbr> (Shitf + H) y reflejamos la imagen de forma horizontal. Una vez que tenemos las esquinas superiores creamos sus respectivos enlaces simbólicos:

    :::bash
    ln -s frameATL.xpm frameITL.xpm
    ln -s frameATR.xpm frameITR.xpm

Y también se deben de crear enlaces con el prefijo «d», estos son para los
marcos de los cuadros de diálogo. Para este caso son: `dframeATL.xpm`,
`dframeATR.xpm`, `dframeITL.xpm` y `dframeITR.xpm`

Para las esquinas inferiores tomamos la imagen `frameATL.xpm` y usando la
herramienta de volteo la volteamos (sic) de forma vertical, y a partir de allí
podemos usarla de base para la esquina inferior izquierda (`frameABL.xpm`), de
modo que nos quede algo así:

<a href="/wp-content/uploads/2017/06/esquina-rectangulo-volteada-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/esquina-rectangulo-volteada-IceWM.jpg" alt="Esquina rectángulo IceWM">
</a>

También nos debe de quedar un cuadrado transparente de 18x18. Para hacer la
esquina inferior derecha repetimos el proceso antes descrito, volteamos la
imagen originalmente y la llamamos `frameABR.xpm`. Creamos los enlaces
simbólicos:

    :::bash
    ln -s frameABL.xpm frameIBL.xpm
    ln -s frameABL.xpm dframeABL.xpm
    ln -s frameABL.xpm dframeIBL.xpm

Y haz lo mismo con `frameABR.xpm` y sus enlaces: `frameIBR.xpm`, `dframeABR.xpm`,
`dframeIBR.xpm`.

Ahora vamos a hacer los bordes de la ventana, los cuales son borde superior
(`frameAT.xpm`), borde izquierdo (`frameAL.xpm`), borde derecho (`frameAR.xpm`)
y borde inferior (`frameAB.xpm`).

Primero haremos el borde superior, para esto creamos una imagen de 4x4 y en
esta hacemos una línea de 4x1 de un color oscuro en la parte superior, y
después otra línea igual debajo de esta con un color más claro. Recuerda usar
los mismos colores de la imagen `frameABL.xpm` o `frameABR.xpm`, o si lo deseas
puedes cortar alguna de estas imágenes con la herramienta de recorte al tamaño
indicado. Veamos el ejemplo:

<a href="/wp-content/uploads/2017/06/borde-IceWM.jpg">
<img src="/wp-content/uploads/2017/06/borde-IceWM.jpg" alt="Esquina rectángulo IceWM">
</a>

Para el borde inferior (`frameAB.xpm`), simplemente volteamos la imagen
verticalmente y la guardamos.

Entonces, creamos una imagen de 4x2 para el borde izquierdo, y en la lado
izquierdo hacemos una línea de 1x2 y la rellenamos de un color oscuro (o claro
según sea el caso), el resto del imagen va llevar el color principal de los
demás bordes. La guardamos con el nombre `frameAL.xpm`.

<a href="/wp-content/uploads/2017/06/borde-IceWM-volteado.jpg">
<img src="/wp-content/uploads/2017/06/borde-IceWM-volteado.jpg" alt="Esquina rectángulo IceWM volteada">
</a>

Para el borde derecho, con la herramienta de volteo la reflejamos verticalmente
y guardamos el resultado como `frameAR.xpm`.

Finalmente, creamos los enlaces simbólicos que nos hacen falta.

    :::bash
    ln -s frameAT.xpm frameIT.xpm
    ln -s frameAT.xpm dframeAT.xpm
    ln -s frameAT.xpm dframeIT.xpm

### Los botones de las ventanas

Para los botones de las ventanas tenemos varias opciones, entre estas podemos
crear botones clásicos, que lleven los símbolos de siempre, cuadrados,
redondos, con mucha o poca separación entre ellos, con efectos, sin efectos,
etc. Aquí la única limitación es nuestra creatividad.

Por lo regular se usan los 5 botones comunes que son: Cerrar (x), minimizar
(i), maximizar y restaurar (m), y botón del menú de ventana (s). Recuerda que
definimos los botones que vamos a usar en el tema en el archivo
`default.theme`, y que podemos agregar, quitar o cambiarlos del lugar según nos
parezca conveniente.

En este tema que estamos usando de ejemplo, he elegido botones redondos y
brillantes.

Pero primero vamos a crear en <abbr title="GNU Image Manipulation Program">GIMP</abbr> una nueva imagen de 26x36 que será para el
botón del menú. Como en `default.theme` pusimos que queríamos ver el icono de la
aplicación abierta aquí, vamos a hacer una imagen que no lleve botones. Para
esto abrimos como capas la imagen `titleAB.xpm` y la duplicamos varias veces,
después las acomodamos de tal manera que nos queden dos rectángulos del mismo
tamaño uno debajo del otro. Combinamos las capas de tal modo que se vea así:

<a href="/wp-content/uploads/2017/06/rectangulo-duplicado.jpg">
<img src="/wp-content/uploads/2017/06/rectangulo-duplicado.jpg" alt="Rectángulo duplicado">
</a>

Hecho esto la guardamos con el nombre `menuButtonA.xpm`, y creamos más enlaces
simbólicos:

    :::bash
    ln -s menuButtonA.xpm menuButtonI.xpm
    ln -s menuButtonA.xpm menuButtonO.xpm

Podemos usar la imagen `menuButtonA.xpm` como base para los demás botones. Sólo
la abrimos en <abbr title="GNU Image Manipulation Program">GIMP</abbr>, y
agregamos más o menos espacio en el lienzo, yo lo he dejado de 29x36,  y
duplicamos la capa si es necesario llenar espacios en blanco. Además creamos un
círculo (o rectángulo o símbolo que será nuestro nuevo botón) en el centro de
la imagen superior. Yo he hecho un círculo con la herramienta de selección
elíptica, después lo he rellenado de un tono de rojo (`#b9372c`), después en
una nueva capa con el pincel (has de hacerlo muy pequeño) he pintado de blanco
la parte inferior y sólo un poco la parte superior.  Y finalmente he cambiado
el modo de la capa a Solapar. De este modo los hacemos «brillantes». Después
con la herramienta de selección de rectángulos seleccionamos la imagen inferior
(debe de ser la mitad de la imagen) y la cubrimos de color negro. He aquí el
ejemplo:

<a href="/wp-content/uploads/2017/06/boton-cerrado-IceWM.png">
<img src="/wp-content/uploads/2017/06/boton-cerrado-IceWM.png" alt="Botón de cerrado">
</a>

Para los demás botones (`minimizeA.xpm`, `restoreA.xpm` y `maximizeA.xpm`) sólo
cambia el símbolo o en este caso el color, el procedimiento es el mismo. En
este caso, llamaremos a esta imagen `closeA.xpm`.

Para los botones inactivos (que terminan en «I») creamos una imagen exactamente
igual a la anterior, sólo que en vez dejar la mitad de color negro esta se va a
componer de dos imágenes exactamente iguales. Aquí he hecho un circulo gris
brillante y he duplicado la primera capa y la he puesto justo debajo de la
primera imagen de modo que se vean las dos...

<a href="/wp-content/uploads/2017/06/botones-inactivos-IceWM.png">
<img src="/wp-content/uploads/2017/06/botones-inactivos-IceWM.png" alt="Botones inactivos IceWM">
</a>

Cómo todos los botones que voy a usar son precisamente círculos, solamente
queda crear enlaces simbólicos para todos los demás botones.

    :::bash
    ln -s closeI.xpm minimizeI.xpm
    ln -s closeI.xpm maximizeI.xpm
    ln -s closeI.xpm restoreI.xpm

Y finalmente, para el botón de efecto (O) hacemos como el botón inactivo, sólo
que este va a tener dos imágenes distintas una debajo de otra. Es decir, arriba
va a estar el botón con efecto (al pasarle el puntero por encima), y abajo como
se va a ver una vez presionado. Yo suelo usar brillos o colores distintos en
los botones de efecto y un color más opaco en el botón presionado. Aquí el
ejemplo:

<a href="/wp-content/uploads/2017/06/boton-cerrado-efecto-IceWM.png">
<img src="/wp-content/uploads/2017/06/boton-cerrado-efecto-IceWM.png" alt="Botón de cerrado con efecto IceWM">
</a>

Como vemos, el botón de efecto es más brillante que el presionado. Aunque
podemos cambiar ese efecto por otro color, brillo, etc.

En este caso, he finalizado este tema creando los enlaces correspondientes.

    :::bash
    ln -s closeO.xpm minimizeO.xpm
    ln -s closeO.xpm maximizeO.xpm
    ln -s closeO.xpm restoreO.xpm

¡Listo!

## Compartiendo tu tema

Una vez finalizado nuestro tema es mejor que lo compartamos, ¿por qué? Por si
deseamos que más personas lo usen; además, sería bueno leer sus sugerencias o
comentarios. O hasta quizá recibamos apoyo para mejorar el tema.

Actualmente existe [Box-look.org](https://www.box-look.org/), una página ideal
para compartir nuestros temas de IceWM. Podemos puntuar temas, descargarlos y
compartirlos con otros usuarios de IceWM.

Solo queda empaquetar nuestro tema, subirlo a Box-look, agregar capturas de
pantalla y una descripción. Eso es todo.
