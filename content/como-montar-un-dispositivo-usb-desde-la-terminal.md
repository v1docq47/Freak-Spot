Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2016-07-17 13:48
Lang: es
Slug: como-montar-un-dispositivo-usb-desde-la-terminal
Status: published
Tags: /media, /mnt, GNU/Linux, interfaz de línea de órdenes, mount, umount, USB, dispositivo USB, lápiz de memoria, lápiz USB, memoria externa, pen drive, pendrive, Trisquel Belenos, Trisquel 7
Title: ¿Cómo montar un dispositivo USB desde la Terminal?

Como todo en GNU/Linux es un fichero, necesitamos una ubicación en el
sistema de ficheros para nuestro dispositivo USB. Normalmente se suelen
crear en `/media`, `/mnt` o en un directorio similiar diseñado para tal
propósito. En el sistema operativo que yo utilizo (Trisquel), se utiliza
la carpeta `/media`. La ubicación para mi USB la creo con esta
instrucción: `sudo mkdir /media/usb`.

Una vez creado el punto de montaje, necesitamos decirle al sistema
operativo el dispositivo que queremos montar. Con la instrucción `lsblk`
podemos listar los dispositivos disponibles. Ahora solo nos queda montar
con la instrucción `mount` el que queramos en el directorio que hemos
creado: `sudo mount /dev/sdc1 /media/usb` (en mi caso). Cuando esté
desmontado el dispositivo USB, el directorio donde lo hemos montado
quedará vacío. Puedes borrar el directorio creado con la instrucción
`sudo rmdir /media/usb`.
