Author: Jorge Maldonado Ventura
Category: Edición de imágenes
Date: 2016-07-10 11:52
Image: <img src="/wp-content/uploads/2016/07/tux__friends-825x510.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" width="825" height="510" alt="¿Cómo poner una imagen sobre un fondo con GIMP?">
Lang: es
Modified: 2017-07-30 01:37
Slug: como-poner-una-imagen-sobre-un-fondo-con-gimp
Status: published
Tags: capas, GIMP, GIMP 2.8.10, GNU/Linux, pingüinos, selección, transparencia, Trisquel, Tux, video, Trisquel Belenos, Trisquel 7
Title: ¿Cómo poner una imagen sobre un fondo con GIMP?

En [este
vídeo](https://goblinrefuge.com/mediagoblin/u/freakspot/m/como-poner-una-imagen-sobre-un-fondo-con-gimp/)
explico cómo poner una imagen sobre un fondo, teniendo en cuenta el
tamaño y la transparencia.

<!-- more -->

<video controls poster="/wp-content/uploads/2016/07/tux__friends-768x503.png">
  <source src="/video/Como-poner-una-imagen-sobre-un-fondo-con-GIMP.mp4" type="video/mp4">
  <track kind="captions" src="/wp-content/uploads/2016/12/imagen-sobre-un-fondo-gimp.vtt" srclang="es" label="Español">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

A continuación, os dejo las imágenes utilizadas en el vídeo:

<a href="/wp-content/uploads/2016/07/tux.png"><img class="aligncenter size-full wp-image-94" src="/wp-content/uploads/2016/07/tux.png" alt="Tux" width="2400" height="2796" srcset="/wp-content/uploads/2016/07/tux.png 1758w, /wp-content/uploads/2016/07/tux-258x300.png 258w, /wp-content/uploads/2016/07/tux-768x895.png 768w, /wp-content/uploads/2016/07/tux-879x1024.png 879w" sizes="(max-width: 2400px) 100vw, 2400px" /></a><a href="/wp-content/uploads/2016/07/images.duckduckgo.com_.jpeg"><img class="aligncenter size-full wp-image-95" src="/wp-content/uploads/2016/07/images.duckduckgo.com_.jpeg" alt="images.duckduckgo.com" width="2956" height="1936" srcset="/wp-content/uploads/2016/07/images.duckduckgo.com_.jpeg 2048w, /wp-content/uploads/2016/07/images.duckduckgo.com_-300x196.jpeg 300w, /wp-content/uploads/2016/07/images.duckduckgo.com_-768x503.jpeg 768w, /wp-content/uploads/2016/07/images.duckduckgo.com_-1024x671.jpeg 1024w" sizes="(max-width: 2956px) 100vw, 2956px"></a>
