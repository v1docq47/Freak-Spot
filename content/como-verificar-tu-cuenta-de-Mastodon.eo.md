Author: Jorge Maldonado Ventura
Category: Ruzo
Date: 2023-05-20 19:20
Modified: 2023-11-19 11:30
Lang: eo
Slug: cómo-verificar-tu-cuenta-de-Mastodon
Save_as: kiel-aŭtentigi-Mastodon-konton/index.html
URL: kiel-aŭtentigi-Mastodon-konton/
Tags: Mastodon, retejo, aŭtentigo
Title: Kiel aŭtentigi Mastodon-konton
Image: <img src="/wp-content/uploads/2023/05/Mastodon-profilo-kun-retejo-aŭtentigita.png" alt="" width="1309" height="769" srcset="/wp-content/uploads/2023/05/Mastodon-profilo-kun-retejo-aŭtentigita.png 1309w, /wp-content/uploads/2023/05/Mastodon-profilo-kun-retejo-aŭtentigita-654x384.png 654w" sizes="(max-width: 1309px) 100vw, 1309px">

Se vi volas pruvi, ke Mastodon-konto estas via kaj ne de aliulo, kiu
ŝajnas esti vi, vi povas aŭtentigi vian retejon. Por fari tion vi devas
sekvi la jenajn paŝojn: <!-- more -->

1. Premu **Redakti profilon** en via profila paĝo aŭ iru al
   **Preferoj** kaj poste al **Profilo**.
<a href="/wp-content/uploads/2023/11/agordoj-por-aŭtentigi-retejon-en-Mastodon.png">
<img src="/wp-content/uploads/2023/11/agordoj-por-aŭtentigi-retejon-en-Mastodon.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/11/agordoj-por-aŭtentigi-retejon-en-Mastodon.png 1920w, /wp-content/uploads/2023/11/agordoj-por-aŭtentigi-retejon-en-Mastodon-960x540.png 960w, /wp-content/uploads/2023/11/agordoj-por-aŭtentigi-retejon-en-Mastodon-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
2. En **Profilaj metadatumoj** aldonu retejon. Vi povas skribi «Retejo»
   en la *Etikedo*-kampo kaj la URL-n de via retejo en la
   *Enhavo*-kampo.
3. Alklaku la *Kontrolo*-langeton kaj poste kopiu la kodon, kiu aperas sub *JEN GVIDILO* (vi povas premi la
   **Kopii**-butonon), kaj algluu ĝin al la dosiero `index.html`, `index.php`
   aŭ al ekvivalenta dosiero de via retejo[^1].
<a href="/wp-content/uploads/2023/11/aŭtentigo-en-Mastodon.png">
<img src="/wp-content/uploads/2023/11/aŭtentigo-en-Mastodon.png" alt="" width="1376" height="739" srcset="/wp-content/uploads/2023/11/aŭtentigo-en-Mastodon.png 1376w, /wp-content/uploads/2023/11/aŭtentigo-en-Mastodon-688x369.png 688w" sizes="(max-width: 1376px) 100vw, 1376px">
</a>
4. Fine, premu **KONSERVI ŜANĜOJN**.

Kiam vi reŝarĝos vian Mastodon-profilon, via retejo aperos aŭtentigita.

<a href="/wp-content/uploads/2023/05/Mastodon-profilo-kun-retejo-aŭtentigita.png">
<img src="/wp-content/uploads/2023/05/Mastodon-profilo-kun-retejo-aŭtentigita.png" alt="" width="1309" height="769" srcset="/wp-content/uploads/2023/05/Mastodon-profilo-kun-retejo-aŭtentigita.png 1309w, /wp-content/uploads/2023/05/Mastodon-profilo-kun-retejo-aŭtentigita-654x384.png 654w" sizes="(max-width: 1309px) 100vw, 1309px">
</a>

[^1]: Kiam la aŭtentigo finiĝos, mi rekomendas al vi forigi la
    aŭtentigan kodon.
