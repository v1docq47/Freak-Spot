Author: Jorge Maldonado Ventura
Category: Truques
Date: 2023-11-17 11:59
Lang: pt
Slug: cómo-verificar-tu-cuenta-de-Mastodon
Save_as: como-verificar-conta-do-Mastodon/index.html
URL: como-verificar-conta-do-Mastodon/
Tags: Mastodon, sítio eletrónico, verificação
Title: Como verificar conta do Mastodon
Image: <img src="/wp-content/uploads/2023/11/perfil-do-Mastodon-verficado.png" alt="" width="1191" height="838" srcset="/wp-content/uploads/2023/11/perfil-do-Mastodon-verficado.png 1191w, /wp-content/uploads/2023/11/perfil-do-Mastodon-verficado-595x419.png 595w" sizes="(max-width: 1191px) 100vw, 1191px">

Se quiseres provar que uma conta Mastodon é tua e não de outra pessoa
que se faz passar por ti, podes verificar o teu sítio eletrónico. Para
fazer isso, é necessário seguir os passos a seguir:<!-- more -->

1. Carrega em **Editar perfil** na página do teu perfil ou vai a
   **Preferências** e depois a **O teu perfil**.
<a href="/wp-content/uploads/2023/11/metadados-do-perfil-no-Mastodon.png">
<img src="/wp-content/uploads/2023/11/metadados-do-perfil-no-Mastodon.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/11/metadados-do-perfil-no-Mastodon.png 1920w, /wp-content/uploads/2023/11/metadados-do-perfil-no-Mastodon-960x540.png 960w, /wp-content/uploads/2023/11/metadados-do-perfil-no-Mastodon-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
2. Em **Metadados de perfil** adiciona um sítio eletrónico. Podes colocar «sítio eletrónico» no campo *Rótulo* e o URL do teu sítio eletrónico no campo *Conteúdo*.
3. Carrega na aba *Verficação* e depois copia o código que aparece sob *VÊ COMO*. Cola o código no ficheiro `index.html`, `index.php` ou equivalente do teu sítio eletrónico[^1].
<a href="/wp-content/uploads/2023/11/verficação-no-Mastodon.png">
<img src="/wp-content/uploads/2023/11/verficação-no-Mastodon.png" alt="" width="1194" height="746" srcset="/wp-content/uploads/2023/11/verficação-no-Mastodon.png 1194w, /wp-content/uploads/2023/11/verficação-no-Mastodon-597x373.png 597w" sizes="(max-width: 1194px) 100vw, 1194px">
</a>
4. Finalmente, preme na aba *Editar perfil* e depois no botão **Gravar as alterações**.

Quando recarregares o teu perfil do Mastodon, o teu sítio web aparecerá
como verificado.

<a href="/wp-content/uploads/2023/11/perfil-do-Mastodon-verficado.png">
<img src="/wp-content/uploads/2023/11/perfil-do-Mastodon-verficado.png" alt="" width="1191" height="838" srcset="/wp-content/uploads/2023/11/perfil-do-Mastodon-verficado.png 1191w, /wp-content/uploads/2023/11/perfil-do-Mastodon-verficado-595x419.png 595w" sizes="(max-width: 1191px) 100vw, 1191px">
</a>

[^1]: Quando a verificação estiver concluída, recomendo que apagues o
    código de verificação.
