Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-09-21 19:05
Lang: eo
Slug: no-pierdas-tu-tiempo-y-dinero-viendo-anuncios
Save_as: ne-malŝparu-vian-vivon-vidante-reklamojn/index.html
URL: ne-malŝparu-vian-vivon-vidante-reklamojn/
Tags: reklamoj, konsilo
Title: Ne malŝparu vian vivon vidante reklamojn
Image: <img src="/wp-content/uploads/2023/09/distopia-anuncios-editado.png" alt="" width="1387" height="898" srcset="/wp-content/uploads/2023/09/distopia-anuncios-editado.png 1387w, /wp-content/uploads/2023/09/distopia-anuncios-editado-693x449.png 693w" sizes="(max-width: 1387px) 100vw, 1387px">

Vi spektas videon, kiun vi ŝatas. La video estas interrompita.
<span style="color: #E8C244">Ĝena reklamo</span>. Denove en la video.
Sponsorado. Abonu. Komentu...

## Malrapideco, poluado, konsumismo

Ciferecaj reklamoj igas vin aĉeti senutilajn aferojn, kiujn vi ne
bezonas, kaj malŝpari valoran tempon. Aldone reklamoj igas, ke retpâgoj
ŝarĝiĝu malpli rapide, kaj estigas multege da CO<sub>2</sub>-emisioj
(kaj rekte kaj nerekte).

## Reklamoj, kiuj mortigas

Reklamoj ĉiam estis uzataj por trompi niajn perceptojn pere de la
psikologio. Reklamantoj vendis tabakon kvazaŭ liberigan simbolon,
malsanajn manĝaĵojn kvazaŭ sanajn... Poste okazis problemoj de sano kaj
morto al homoj, kiuj estis konvinkitaj per tiuj reklamoj.

## Vi pagas ilin per viaj mono kaj tempo

Se vi aĉetas Interrete reklamitan produkton, la parto, kiu kostis al
firmaoj produkti kaj vidigi la reklamon, estas pagata de vi: per viaj
tempo kaj mono (memoru, ke estas riĉega industrio).

## Cenzuro

Vi ne povas esprimi nekonvenciajn politikajn opiniojn, nudeco ne
eblas... Reklamantoj ne ŝatas riskojn, ĉio devas esti kontrolita. Se ili
ne ŝatas vin, via video ne gajnos monon, ne aperos en la serĉrezultoj.
Ĉio, kio ne kontribuas al la konsumismo estos ignorita, ĉar tio ne donas
al ili monon.

## Sen privateco

Multaj firmaoj gvatas ĉion, kion vi faras en la Interreto, kreante
psikologiajn profilojn, kiuj ebligas al reklamantoj plibonigi manierojn
kontroli vin.

<h2><span style="color: #b5ffb5">Vivo sen reklamoj</span></h2>

<a href="/wp-content/uploads/2023/09/bosque-falso.png">
<img src="/wp-content/uploads/2023/09/bosque-falso.png" alt="" width="1024" height="989" srcset="/wp-content/uploads/2023/09/bosque-falso.png 1024w, /wp-content/uploads/2023/09/bosque-falso-512x494.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>

Pro tio mi blokas la reklamojn de retejoj, videoj (inkludante
sponsoradojn), ktp. Per la ŝparita tempo mi povas pli rapide trovi la
informon, kiun mi serĉas, kaj legi artikolojn aŭ senatende spekti
videojn. Tio ebligis al mi, lerni pli en malpli da tempo kaj pasigi pli
da tempo farante tion, kion mi ŝatas fari.

Se mi devas aĉeti ion, mi klopodas inteligente aĉeti ĝin:

- Kiuj estas la ingrediencoj?
- Ĉu estas opcioj pli altkvalitaj?
- Kie ĝi estis produktita?
- Kiom ĝi kostas?
- ...

Se mi ŝatas ion kaj volas subteni ĝin, mi faras tion rekte (sen perantoj),
por ke ĝi ricevu pli da mono ol tiom, kiom ĝi ricevus, se mi vendus mian
tempon al trompaj reklamantoj.

Kompreneble, Interreta reklamado daŭre ekzistos, ĉar ĉiam estos iu
sponsorita artikolo, iu video financita de firmao, ktp. Bonŝance eblas
eviti multajn reklamojn per programoj kiel
uBlock Origin kaj [Piped](/eo/YouTube-privatece-per-Piped/).
