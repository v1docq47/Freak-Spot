Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2023-09-12 18:00
Lang: es
Slug: no-pierdas-tu-tiempo-y-dinero-viendo-anuncios
Tags: anuncios, consejo
Title: No pierdas tu vida viendo anuncios
Image: <img src="/wp-content/uploads/2023/09/distopia-anuncios-editado.png" alt="" width="1387" height="898" srcset="/wp-content/uploads/2023/09/distopia-anuncios-editado.png 1387w, /wp-content/uploads/2023/09/distopia-anuncios-editado-693x449.png 693w" sizes="(max-width: 1387px) 100vw, 1387px">

Estás viendo un vídeo que te gusta. El vídeo se interrumpe otra vez.
<span style="color: #E8C244">Un anuncio molesto</span>. De vuelta en el
vídeo. Un patrocinio. Que te suscribas. Que dejes un comentario...

## Lentitud, contaminación, consumismo

Los anuncios digitales hacen que compres cosas inútiles que no necesitas
y que pierdas un tiempo muy valioso. Además, los anuncios hacen que las
páginas carguen más despacio y producen enormes emisiones de
CO<sub>2</sub> (tanto directa como indirectamente).

## Anuncios que matan

Los anuncios siempre han sido usados para manipular nuestras
percepciones usando la psicología. Los anunciantes vendían el tabaco
como símbolo de liberación, comidas insanas como saludables...
Después llegaron los problemas de salud y la muerte para las personas
que fueron convencidas por esos anuncios.

## Los pagas con tu dinero y tiempo

Si compras un producto anunciado en Internet, la parte que les ha
costado a las empresas producir y difundir el anuncio la pagas tú: con
tu tiempo y tu dinero (hablamos de una industria de miles de millones).

## Censura

No puede haber opiniones políticas poco convencionales, no puede haber
desnudos, críticas sociales radicales... A los anunciantes no les gusta
correr riesgos, todo tiene que estar controlado. Si no les gustas, tu
vídeo no ganará dinero, no saldrá en los resultados de búsqueda.

Todo lo que no contribuya al consumismo debe ser ignorado, porque no les
da dinero.

## Sin privacidad

Con el surgimiento de los anuncios basados en tus intereses muchas
empresas vigilan todo lo que haces en Internet, creando perfiles
psicológicos que permiten a los anunciantes optimizar las formas de
manipularte.

<h2><span style="color: #b5ffb5">Una vida sin anuncios</span></h2>

<a href="/wp-content/uploads/2023/09/bosque-falso.png">
<img src="/wp-content/uploads/2023/09/bosque-falso.png" alt="" width="1024" height="989" srcset="/wp-content/uploads/2023/09/bosque-falso.png 1024w, /wp-content/uploads/2023/09/bosque-falso-512x494.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>

Por todos estos motivos, bloqueo los anuncios de páginas web, de
vídeos (incluyendo patrocinios), etc. Con el tiempo que ahorro puedo
encontrar la información que busco de forma más eficiente y ver vídeos o
leer artículos sin esperas. Así, he logrado aprender más en menos tiempo
y puedo pasar más tiempo haciendo lo que realmente me gusta.

Si compro algo, trato de hacerlo con cabeza:

- ¿Cuáles son los ingredientes?
- ¿Hay opciones de mejor calidad?
- ¿Dónde se ha producido?
- ¿Cuánto cuesta?
- ...

Si me gusta algo y quiero apoyarlo, lo hago directamente (sin
intermediarios) para que reciba más dinero del que recibiría si vendiera
mi tiempo a anunciantes abusivos.

Claro, la publicidad en Internet seguirá existiendo, pues siempre habrá
algún artículo patrocinado, algún vídeo financiado por una empresa o lo
que sea que se nos escape. Por suerte, es posible evitar muchos anuncios
usando programas como [uBlock Origin](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/) y [Piped](/youtube-con-privacidad-con-piped/).
