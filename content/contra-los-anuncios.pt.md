Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-09-13 11:00
Lang: pt
Slug: no-pierdas-tu-tiempo-y-dinero-viendo-anuncios
Tags: anúncios, conselho
Save_as: não-percas-a-tua-vida-vendo-anúncios/index.html
URL: não-percas-a-tua-vida-vendo-anúncios/
Title: No percas a tua vida vendo anúncios
Image: <img src="/wp-content/uploads/2023/09/distopia-anuncios-editado.png" alt="" width="1387" height="898" srcset="/wp-content/uploads/2023/09/distopia-anuncios-editado.png 1387w, /wp-content/uploads/2023/09/distopia-anuncios-editado-693x449.png 693w" sizes="(max-width: 1387px) 100vw, 1387px">

Estás a ver um vídeo de que gostas. O vídeo é novamente interrompido.
<span style="color: #E8C244">Um anúncio irritante</span>. De volta ao
vídeo. Um patrocínio. Que te inscrevas. Que deixes um comentário...

## Lentidão, poluição, consumismo

Os anúncios digitais fazem-te comprar coisas inúteis de que não
precisas e desperdiçar tempo valioso. Além disso, os anúncios tornam
o carregamento das páginas mais lento e produzem enormes emissões de
CO<sub>2</sub> (tanto direta quanto indiretamente).

## Anúncios que matam

A publicidade sempre foi utilizada para manipular as nossas percepções
através da psicologia. Os publicitários vendiam o tabaco como um símbolo
de libertação, alimentos não saudáveis como saudáveis... Depois vieram
os problemas de saúde e a morte das pessoas que foram convencidas por
estes anúncios.

## Pagas-os com o teu dinheiro e tempo

Se comprar um produto anunciado na Internet, a parte que custou às
empresas a produção e divulgação do anúncio é paga por ti: com
o teu tempo e o teu dinheiro (estamos a falar de uma indústria de
milhares de milhões).

## Censura

Não pode haver opiniões políticas não convencionais, não pode haver
nudez, criticas sociais radicais... Os publicitários não gostam de
correr riscos, tudo tem de ser controlado. Se eles não gostarem de ti, o
teu vídeo não vai ganhar dinheiro, não vai aparecer nos resultados de
pesquisa.

Tudo o que não contribui para o consumismo deve ser ignorado, porque não
lhes dá lucro.

## Sem privacidade

Com o aumento da publicidade direcionada, muitas empresas
monitorizam tudo o que fazes na Internet, criando perfis psicológicos
que permitem aos anunciantes otimizar as formas de te manipular.

<h2><span style="color: #b5ffb5">Uma vida sem anúncios</span></h2>

<a href="/wp-content/uploads/2023/09/bosque-falso.png">
<img src="/wp-content/uploads/2023/09/bosque-falso.png" alt="" width="1024" height="989" srcset="/wp-content/uploads/2023/09/bosque-falso.png 1024w, /wp-content/uploads/2023/09/bosque-falso-512x494.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>

Por todas estas razões, bloqueio a publicidade em sítios web, vídeos
(incluindo patrocínios), etc. Com o tempo que poupo, posso encontrar a
informação que procuro de forma mais eficiente e ver vídeos ou ler
artigos sem ter de esperar. Assim, consegui aprender mais em menos tempo
e posso passar mais tempo a fazer o que realmente gosto.

Se compro alguma coisa, tento comprá-la de forma sensata:

- Quais são os ingredientes?
- Existem opções de melhor qualidade?
- Onde foi produzido?
- Quanto custa?
- ...

Se gosto de algo e quero apoiá-lo, faço-o diretamente (sem
intermediários) para que receba mais dinheiro do que receberia se
vendesse o meu tempo a anunciantes abusivos.

É claro que a publicidade na Internet vai continuar a existir, porque
haverá sempre algum artigo patrocinado, algum vídeo financiado por uma
empresa, eu sei lá. Felizmente, é possível evitar muitos anúncios usando
programas como o [uBlock Origin](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/) e o [Piped](/pt/YouTube-com-privacidade-com-Piped/).
