Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Edición de textos
Date: 2017-02-05 03:31
Image: <img src="/wp-content/uploads/2017/02/Captura-de-pantalla-de-2017-02-05-041046.png" width="743" height="107" class="attachment-post-thumbnail wp-post-image" alt="Corrector ortográfico en Vim" srcset="/wp-content/uploads/2017/02/Captura-de-pantalla-de-2017-02-05-041046.png 743w, /wp-content/uploads/2017/02/Captura-de-pantalla-de-2017-02-05-041046-300x43.png 300w" sizes="(max-width: 743px) 100vw, 743px">
JS: asciinema-player.js (top)
Lang: es
Modified: 2017-02-25 17:31
Slug: corrector-ortografico-en-vim
Status: published
Tags: edición de texto sin formato, errores ortográficos, español, GNU/Linux, Neovim, ortografía, Vim
Title: Corrector ortográfico en Vim

Muchos editores, como LibreOffice Writer, permiten detectar errores
ortográficos. Pero trabajar con editores
[WYSIWYG](https://es.wikipedia.org/wiki/WYSIWYG) no es ideal en todos
los casos. A veces puede que necesitemos corregir errores ortográficos
en un archivo de texto. Vim permite hacer esto.

Para activar la corrección automática ejecuta `:set spell` (el inglés es
el idioma por defecto). Podemos elegir un idioma concreto ejecutando
`:set spell spelllang=es`, siendo el valor de `spelllang` el idioma
utilizado (`es` equivale al español).

Normalmente, Vim no viene con soporte para otros idiomas aparte del
inglés. Pero no hay problema alguno, pues podemos crear nuestro propio
diccionario añadiendo palabras manualmente (se hace ejecutando `zg` en
el modo normal dentro de Vim). Pero es más sencillo utilizar un
diccionario que ya exista. Una buena opción es utilizar los diccionarios
de LibreOffice.<!-- more -->

Vamos a añadir ahora un diccionario de español para Vim. He grabado un
pequeño vídeo con [asciinema](https://directory.fsf.org/wiki/Asciinema)
que muestra cómo lo he instalado en mi ordenador. Puede que queráis
verlo después de leer los pasos de instalación, o directamente realizar
la instalación siguiendo el ejemplo del vídeo.

<asciinema-player poster="data:application/json;base64,W1tbIiAgNSAiLHsiZmciOjMsImJvbGQiOnRydWV9XSxbIkxhICIse31dLFsi
ZGV0ZWNjaVx1MDBmM24iLHsiYmciOjF9XSxbIiAiLHt9XSxbImF1dG9tXHUw
MGUxdGljYSIseyJiZyI6MX1dLFsiIGRlICIse31dLFsiZXJyb3JlcyIseyJi
ZyI6MX1dLFsiIGRlICIse31dLFsic2ludGF4aXMiLHsiYmciOjF9XSxbIiBl
cyAiLHt9XSxbIm11eSIseyJiZyI6MX1dLFsiICIse31dLFsiXHUwMGZhdGls
Iix7ImJnIjoxfV0sWyIuICAgICAgICAgICAgICAgICAiLHt9XV0sW1siICA0
ICIseyJmZyI6MywiYm9sZCI6dHJ1ZX1dLFsiXHUwMGJmIix7fV0sWyJDXHUw
MGYzbW8iLHsiYmciOjF9XSxbIiBsYSAiLHt9XSxbInV0aWxpem8iLHsiYmci
OjF9XSxbIiAiLHt9XSxbImRlbnRybyIseyJiZyI6MX1dLFsiIGRlIFZpbT8g
Iix7fV0sWyJFamVjdXRhbmRvIix7ImJnIjoxfV0sWyIgOnNldCBzcGVsbCAg
ICAgICAgICAgICAgICAgICAgICAgIix7fV1dLFtbIiAgMyAiLHsiZmciOjMs
ImJvbGQiOnRydWV9XSxbIiAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAiLHt9XV0sW1siICAyICIseyJmZyI6MywiYm9sZCI6dHJ1ZX1dLFsiVXBz
Li4uIFRoYXQgb25seSB3b3JrcyBmb3IgRW5nbGlzaCAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICIse31dXSxbWyIgIDEgIix7
ImZnIjozLCJib2xkIjp0cnVlfV0sWyIgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgIix7fV1dLFtbIjYgICAiLHsiZmciOjMsImJvbGQiOnRydWV9
XSxbIiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAiLHt9XV0sW1si
fiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAiLHsiZmciOjQs
ImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRy
dWV9XV0sW1sifiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAi
LHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1si
fiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAiLHsiZmciOjQs
ImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRy
dWV9XV0sW1sifiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAi
LHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1si
fiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAiLHsiZmciOjQs
ImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRy
dWV9XV0sW1sifiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAi
LHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1si
fiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAiLHsiZmciOjQs
ImJvbGQiOnRydWV9XV0sW1sifiAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAiLHsiZmciOjQsImJvbGQiOnRydWV9XV0sW1siLS0gSU5TRVJU
QVIgLS0iLHsiYm9sZCI6dHJ1ZX1dLFsiICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgNiwxICAgICAgICAgIFRvZG8g
Iix7fV1dXQ==
" src="../asciicasts/101968.json">Lo siento, asciinema-player no
funciona sin JavaScript.
</asciinema-player>

Debemos seguir los siguientes pasos:

1.  Hemos de buscar el diccionario para el idioma que deseemos. En este
    caso, nos sirve [el diccionario de español para
    LibreOffice](https://extensions.libreoffice.org/extensions/spanish-dictionaries),
    realizado por el proyecto [Recursos lingüísticos abiertos del
    español](https://github.com/sbosio/rla-es). Yo voy a descargar un
    diccionario de español que me sirva para cualquier región,
    disponible en la siguiente URL:
    <https://extensions.libreoffice.org/extensions/spanish-dictionaries/2.1/@@download/file/es_any.oxt>.
2.  Descomprime el archivo `es_any.oxt` (puedes usar el programa
    `unzip`) y crea el archivo de diccionario para Vim ejecutando
    `:mkspell es es_ANY` dentro de Vim, debes estar en el mismo
    directorio donde se extrajo el archivo `es_any.oxt`.
3.  Mueve el archivo `es.utf-8.spl` generado al directorio
    `$HOME/.vim/spell` o al directorio `spell` de Vim. En mi ordenador
    está en `/usr/share/vim/vim74/spell/`, pero la ruta puede variar
    dependiendo del sistema operativo.
4.  Vamos a probar si funciona. Abre Vim de nuevo y
    `ejecuta :set spell spelllang=es`. A continuación, escribe algún
    texto en español con alguna palabra mal escrita. Las palabras mal
    escritas deben aparecer resaltadas.

Para encontrar más información sobre este tema ejecuta `:h spell` dentro
de Vim.
