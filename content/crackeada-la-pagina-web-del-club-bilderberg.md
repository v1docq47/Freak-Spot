Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2016-12-31 18:59
Lang: es
Slug: crackeada-la-pagina-web-del-club-bilderberg
Status: published
Tags: capitalistas, club Bilderberg, crack, cracking, elitistas, grupo Bilderberg, hacktivismo, página web
Title: Crackeada la página web del Club Bilderberg

Los [*crackers*](https://es.wikipedia.org/wiki/Cracker) que han tomado
el control de la página web del [Grupo
Bilderberg](https://es.wikipedia.org/wiki/Grupo_Bilderberg) han dejado
un mensaje. Podéis ver una versión de la página web el día 31 de
diciembre de 2016 en la [Wayback
Machine](https://es.wikipedia.org/wiki/Wayback_Machine):
<http://web.archive.org/web/20161231171649/http://bilderbergmeetings.org/>.

El mensaje se compone de una llamada al público en general para que tome
conciencia y de una advertencia a este grupo de capitalistas elitistas.
