Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2023-03-03 21:04
Lang: de
Slug: crear-servicio-oculto-para-sitio-web-en-nginx
Tags: Debian, GNU/Linux, Nginx, versteckter Dienst, Tor
Save_as: Erstellen-eines-versteckten-Dienstes-für-eine-Website-in-Nginx/index.html
URL: Erstellen-eines-versteckten-Dienstes-für-eine-Website-in-Nginx/
Title: Erstellen eines versteckten Dienstes für eine Website in Nginx (<i lang="en">clearnet</i> und <i lang="en">darknet</i>)
Image: <img src="/wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite.png" alt="" width="1618" height="953" srcset="/wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite.png 1618w, /wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite-809x476.png 809w, /wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite-404x238.png 404w" sizes="(max-width: 1618px) 100vw, 1618px">

Du willst eine Webseite mit einem Nginx-Server erstellen und diese
Webseite auch als versteckten Dienst für Tor-Benutzer anbieten? Hier ist
eine Anleitung, wie man das unter Debian GNU/Linux macht.

Zuerst installieren wir die folgenden Pakete:

    :::bash
    sudo apt install nginx tor

Entkommentier dann die folgenden Zeilen in der Datei `/etc/tor/torrc`:

    :::bash
    #HiddenServiceDir /var/lib/tor/hidden_service/
    #HiddenServicePort 80 127.0.0.1:80

Es sollt so aussehen:

    :::text
    HiddenServiceDir /var/lib/tor/hidden_service/
    HiddenServicePort 80 127.0.0.1:80

Dann starten wir den Tor-Dienst neu:

    :::bash
    sudo systemctl restart tor

Wenn Tor neu startet, erstellt es das Verzeichnis `hidden_service/` und
füllt es mit der URL des versteckten Dienstes (Datei `hostname`) und den
öffentlichen und privaten Schlüsseln.

Wenn wir sowohl Nginx als auch Tor als Dienste laufen lassen und die
Adresse in `/var/lib/tor/hidden_service/hostname` aufrufen, können wir
die Nginx-Willkommensseite sehen.

<a href="/wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome.png">
<img src="/wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome.png" alt="" width="883" height="484" srcset="/wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome.png 883w, /wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome-441x242.png 441w" sizes="(max-width: 883px) 100vw, 883px">
</a>

Standardmäßig sollte sich die Website für Nginx im Pfad `/var/www/html/`
befinden. Wir müssen also nur die Website an diesem Ort entwickeln. Es
spielt keine Rolle, ob du die Tor-URL oder eine herkömmliche URL
verwendest, die Website ist dieselbe. Beachte, dass du relative URLs
verwenden musst, damit Links zu anderen Seiten auf deiner Website mit
`.onion`-URLs funktionieren.

## Extra: Kopfzeile <em>.onion verfügbar</em> hinzufügen

<a href="/wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite.png">
<img src="/wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite.png" alt="" width="1618" height="953" srcset="/wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite.png 1618w, /wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite-809x476.png 809w, /wp-content/uploads/2023/03/versteckter-Dienst-mit-nGinx-für-clearnet-Webseite-404x238.png 404w" sizes="(max-width: 1618px) 100vw, 1618px">
</a>

<!-- more -->

Du musst einfach die folgende `add_header`-Zeile zum Teil der Datei, der
`location /` sagt, in der Nginx-Konfigurationsdatei[^1] hinzufügen.

    :::nginx
    location / {
        # Aliaj aferoj, kiuj troviĝas en location /
        add_header Onion-Location http://[der_Name_deines_verstecktes_Dienst]/$request_uri;
    }

Du musst `[der_Name_deines_verstecktes_Dienst]` durch den Namen ersetzen,
der in der Datei `/var/lib/tor/hidden_service/hostname` zu finden ist
und etwa so aussieht[^2].

    :::nginx
    location / {
        add_header Onion-Location http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/$request_uri;
    }

[^1]: In diesem Fall `/etc/nginx/sites-available/default`.
[^2]: Der Name des versteckten Dienstes ist ein Beispiel. Kopiere
    ihn nicht und füg nicht ihn so ein, sondern gib den Namen des
    von dir erstellten versteckten Dienstes ein.
