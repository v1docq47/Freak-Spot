Author: Jorge Maldonado Ventura
Category: Kino
Date: 2022-12-30 21:30
Lang: eo
Slug: crear-vídeos-de-ruido-con-ffmpeg
Save_as: krei-hazardajn-videojn-per-FFmpeg/index.html
URL: krei-hazardajn-videojn-per-FFmpeg/
Tags: FFmpeg, bruo, videoj
Title: Krei hazardajn videojn per FFmpeg

[FFmpeg](https://es.wikipedia.org/wiki/FFmpeg) havas filtrilojn, kiuj
ebligas hazarde krei videojn. La
[`geq`](https://ffmpeg.org/ffmpeg-filters.html#geq)-filtrilo povas krei videan
bruon (uzante `nullsrc`-n kiel blankan fonon), dum la
[`aevalsrc`](https://ffmpeg.org/ffmpeg-filters.html#aevalsrc)-filtrilo povas krei
sonan bruon.

Do ni povas krei bruan nigra-blankan videon de `1280x720` pikseloj per
la jena komando:
```

ffmpeg -f lavfi -i nullsrc=s=1280x720 -filter_complex \
"geq=random(1)*255:128:128;aevalsrc=-2+random(0)" \
-t 10 bruo.mkv
```

<!-- more -->

<video controls="">
  <source src="/video/ruido.webm" type="video/webm">
  <p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>

Per la jena komando ni atingas la samon, sed kun koloroj:
```
ffmpeg -f rawvideo -video_size 1280x720 -pixel_format yuv420p -framerate 30 \
-i /dev/urandom -ar 48000 -ac 2 -f s16le -i /dev/urandom -codec:a copy \
-t 5 bruo-kun-koloroj.mkv
```

Ĉi tie ni uzas `/dev/urandom`, sed ni ankaŭ povas uzi la antaŭe uzitan
`geq`-filtrilon.


<video controls="">
  <source src="/video/ruido-a-color.webm" type="video/webm">
  <p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>

Oni povas uzi tiujn komandojn por krei ekzemplajn videojn. Ankaŭ oni
povas uzi ilin por rapide plenigi per rubaj videoj servilojn de retejoj,
kiuj akceptu videojn, kaj tiel ili perdus monon longatempe, se ili ne
forigus ilin (legu [«Kiel detrui Google»](/eo/kiel-detrui-Google/)), ĉar
tiaj videoj okupas multe da spaco. La lasta (5-sekunda) video okupas 106
<abbr title="megabajtojn">MB</abbr>.
