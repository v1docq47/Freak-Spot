Author: Jorge Maldonado Ventura
Category: Cine
Date: 2022-12-30 19:00
Lang: es
Slug: crear-vídeos-de-ruido-con-ffmpeg
Tags: FFmpeg, ruido, vídeos
Title: Crear vídeos de ruido con FFmpeg

[FFmpeg](https://es.wikipedia.org/wiki/FFmpeg) tiene filtros que
permiten crear aleatoriamente vídeos de ruido. El filtro
[`geq`](https://ffmpeg.org/ffmpeg-filters.html#geq) puede crear ruido de
vídeo (con `nullsrc` como fondo blanco), mientras que el filtro
[`aevalsrc`](https://ffmpeg.org/ffmpeg-filters.html#aevalsrc) puede crear
ruido de audio.

Así pues, podemos crear un vídeo de ruido en blanco y negro de
`1280x720` píxeles con el siguiente comando:

```
ffmpeg -f lavfi -i nullsrc=s=1280x720 -filter_complex \
"geq=random(1)*255:128:128;aevalsrc=-2+random(0)" \
-t 10 ruido.mkv
```

<!-- more -->

<video controls="">
  <source src="/video/ruido.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Con la siguiente orden logramos lo mismo, pero con color:
```
ffmpeg -f rawvideo -video_size 1280x720 -pixel_format yuv420p -framerate 30 \
-i /dev/urandom -ar 48000 -ac 2 -f s16le -i /dev/urandom -codec:a copy \
-t 5 ruido-a-color.mkv
```

Aquí estamos usando `/dev/urandom`, pero podemos aplicar también el filtro `geq`
usado anteriormente.


<video controls="">
  <source src="/video/ruido-a-color.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Estos comandos se pueden usar para generar vídeos de ejemplo. También se
pueden usar para llenar rápidamente de vídeos basura servidores de sitios web
que acepten vídeos, con lo que perderían dinero a largo plazo si no los
borran (léase [«Cómo destruir Google»](/C%C3%B3mo-destruir-Google/)),
pues estos vídeos ocupan mucho espacio. El último vídeo (de 5 segundos) ocupa 106 <abbr title="Megabytes">MB</abbr>.
