Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-09-24
Lang: es
Slug: ¡cuidado-con-los-directos-no-descuides-tu-privacidad
Tags: cuenta de usuario, directo, navegador, privacidad, seguridad, teletrabajo
Title: ¡Cuidado con los directos! No descuides tu privacidad

Con el aumento de las comunicaciones en directo probablemente tu
escritorio ya no es tan privado como antes —tanto el físico como el
digital—. Un riesgo que demasiada gente subestima.

Si nos ponemos en la piel de un superior, a este quizá no le haga mucha
gracia ver que el ordenador del trabajo se usa para videojuegos,
pornografía o cualquier otra cosa que considere inapropiada; o ver en
las sugerencias de la barra de direcciones del navegador sitios web que
contienen ideas políticas contrarias a las suyas, por ejemplo.

Lo ideal sería utilizar el ordenador de la empresa únicamente para cosas
de la empresa. Si este ordenador es el mismo que el de uso personal o se
comparte con alguien, se debe utilizar una cuenta de usuario específica
para la empresa, de forma que al compartir el escritorio no se vean
nuestros archivos, los programas que usamos en nuestro tiempo libre,
nuestro historial de navegación, etc.

Porque aunque solo pensábamos mostrar la presentación de diapositivas,
acabamos exponiendo información sin darnos cuenta cuando algo falla y
salimos de la presentación. Hombre precavido vale por dos: aunque algo
falle, no comprometo mi privacidad cuando uso una cuenta de usuario
específica para el trabajo.

Ni que decir tiene que el escritorio físico y lo que nos rodea mientras
nos encontramos en una videoconferencia es otro factor de riesgo.
Además, las personas con las que convivimos pueden comprometer nuestra
privacidad. Habrá que encontrar, pues, los horarios con menos gente en
casa, elegir con cuidado el lugar teniendo en cuenta lo que la cámara
puede ver y mantener la comunicación digital en el lugar menos ruidoso.

En resumen, las comunicaciones digitales por videoconferencia suponen
numerosos riesgos para la privacidad que deben ser tenidos en cuenta. Es
primordial conocer los peligros que entrañan para poder evitarlos,
incluso cuando surjan imprevistos.
