Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2019-11-08
Modified: 2019-11-09 01:40
Lang: es
Slug: cómo-curar-la-miopía-de-forma-natural
Tags: ojos, miopía, salud
Title: Cómo curar la miopía de forma natural
Image: <img src="/wp-content/uploads/2019/11/ojo-borroso.jpg" alt="ojo que se ve borroso" width="1024" height="681" srcset="/wp-content/uploads/2019/11/ojo-borroso.jpg 1024w, /wp-content/uploads/2019/11/ojo-borroso-512x340.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

Montañas de dinero mueve el negocio alrededor de la miopía en todo el
mundo. Las soluciones propuestas por las empresas y la mayoría de
oftalmólogos son temporales y requieren el desembolso continuo (comprar
cristales, monturas, lentillas), pero nunca solucionan el problema de
raíz ni atacan sus causas.

La miopía en humanos ocurre por un desequilibrio entre la potencia
de refracción de los medios transparentes del ojo y su longitud. Para
que la luz converja de forma adecuada en la retina se suelen utilizar
lentes con el poder refractivo necesario. Es más sencillo de entender
con esta imagen:

![Diagrama de un miope, con y sin corrección](/wp-content/uploads/2019/11/miopía-diagrama.svg)

Quienes dicen que la miopía no tiene solución se basan en la noción de
que no se puede disminuir la longitud del ojo. Sin embargo, conozco un
estudio con humanos que demuestra que sí se puede reducir
significativamente la longitud del ojo en tan solo 60 minutos[^1],
existen otros estudios con animales. Asimismo, el cristalino puede
relajarse para obtener una mejor visión a corto plazo.

<!-- more -->

Lo primero que deberías hacer si eres miope es evitar lo que te ha
producido miopía para que no vaya a más. ¿Estás siempre mirando cosas
cercanas (pantallas, libros...) sin tomar descansos? La visión de cerca
prolongada provoca miopía[^2]; evita o reduce actividades que requieran
mirar continuamente de cerca sin tomar descansos, especialmente si eres
niño o adolescente, pues durante la juventud es cuando más se desarrolla
la miopía.

Una vez atacadas las causas es necesario que se reduzca el uso de gafas
con cuidado (no es seguro prescindir de gafas al conducir si la miopía
es considerable, por ejemplo), y usar una corrección ligeramente menor a
la necesaria para una visión perfecta si es imposible ver sin gafas. Ese
pequeño margen lo utilizaremos para desafiar la vista con desenfoques
controlados a los que nuestra corteza cerebral prestará atención, y
serán compensados a corto plazo (relajación del
[músculo ciliar](https://es.wikipedia.org/wiki/M%C3%BAsculo_ciliar)) y,
con suerte, a largo plazo (disminución de la longitud axial).

Hay que buscar a menudo pequeños desafíos para los ojos. La forma más
sencilla es leer solo un poco más lejos de nuestro límite de visión, es
decir, cuando empieza a verse un poco borroso. Desde ese límite
buscaremos ganar el control del musculo ciliar y hacer que este aclare
la imagen. Esta técnica la podemos llamar «enfoque activo» (en Internet
otros la llaman *active focus* o *print pushing*, en inglés).

El vídeo más abajo[^3] (en inglés) explica, además de datos interesantes
sobre la miopía, una descripción de esta técnica y algunas
recomendaciones.

<video controls poster="/wp-content/uploads/2019/11/imagen-video-miopía.jpg" src="/video/Myopia%20-%20A%20Modern%20Yet%20Reversible%20Disease%20%E2%80%94%20Todd%20Becker,%20M.S.%20(AHS14).webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Con la técnica del vídeo anterior he reducido la miopía de 2 (izquierdo,
*i*) y 1,25 (derecho, *d*) dioptrías a 0,75 (i) y 0,25 (d) en un año.
Debo añadir he cambiado mucho mis hábitos, de forma que paso menos
tiempo frente a pantallas y más al aire libre.

Si inicias la aventura de reducir la miopía, es recomendable que midas
tu progreso. Solo es necesaria una cinta métrica e imágenes para medir
la miopía de forma precisa. Las letras (por ejemplo, de un libro) son
recomendables como imágenes, ya que notamos claramente que lo que vemos
es incorrecto o está borroso al no poder identificarlas. Los
[gráficos de Schnellen](https://es.wikipedia.org/wiki/Test_de_Snellen)
sirven también para medir la miopía.

[^1]: Read, Scott A., Michael J. Collins y Beata P. Sander. "Human optical axial length and defocus." *Investigative ophthalmology & visual science* 51.12 (2010): 6262-6269.
[^2]: Lougheed, Tim. "Los factores ambientales de la miopía." *Salud Pública de México* 56 (2014): 302-310.
[^3]: Becker, Todd [AncestryFoundation] (11 de agosto del 2014). *Myopia: A Modern Yet Reversible Disease — Todd Becker, M.S. (AHS14)* [vídeo]. Obtenido de https://youtube.com/watch?v=x5Efg42-Qn0
