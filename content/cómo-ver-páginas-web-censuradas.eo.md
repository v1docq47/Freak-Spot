Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2022-03-13 16:14
Lang: eo
Slug: cómo-ver-páginas-web-censuradas
Save_as: kiel-vidi-cenzuritajn-retejojn/index.html
URL: kiel-vidi-cenzuritajn-retejojn/
Tags: cenzuro, informa libereco
Title: Kiel vidi cenzuritajn retejojn?

Estas manieroj eviti la cenzuron pro politikaj celoj de iuj retejoj, kiu
kaj en Eŭropo kaj en Rusio nun okazas. Eblas uzi
[Tor](https://www.torproject.org/),
[VPN](https://eo.wikipedia.org/wiki/VPN), aliri al kopioj de retejoj
(per la [Wayback Machine](https://archive.org/) ekzemple), sendi kaj
ricevi cenzuritan enhavon per retpoŝto aŭ mesaĝiloj, ktp.
