Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2019-09-12
Image: <img src="/wp-content/uploads/2019/09/ave-Fénix.png" alt="">
Lang: es
Slug: de-la-ceniza-a-renacer
Tags: poesía, verso
Title: De la ceniza a renacer

Ardemos ya en la pira santa;  
vayamos al infierno en pie.  
Mano en mano entre ascuas  
que engullen nuestro ser.  

Entre la recia llamarada  
sentimos ya la piel perder.  
Volveremos como el fénix  
de la ceniza a renacer.  
