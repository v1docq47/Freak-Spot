Author: Jorge Maldonado Ventura
Category: Videludoj
Date: 2023-12-08 11:21
Lang: eo
Slug: compras-videojuegos-yo-tengo-mas-de-6000-gratis
Tags: aĉeti, emulado, GNU/Linukso, liberaj programoj
Save_as: ĉu-vi-aĉetas-videludojn-mi-havas-pli-ol-6000-senkoste/index.html
URL: ĉu-vi-aĉetas-videludojn-mi-havas-pli-ol-6000-senkoste/
Title: Ĉu vi aĉetas videludojn? Mi senkoste havas pli ol 6000
Image: <img src="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png" alt="" width="1441" height="835" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png 1441w, /wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando-720x417.png 720w" sizes="(max-width: 1441px) 100vw, 1441px">

La videluda industrio enspezas multege da mono. Tamen ekzistas miloj kaj
miloj da ludoj, kiuj povas esti senkoste ludataj. Mi ne nur parolas pri
[liberaj](https://freakspot.net/escribe-programas-libres/eo/lerni/) videludoj,
sed ankaŭ pri malnovaj ludoj de ludmaŝinoj, videludiloj, ktp.

Se vi uzas GNU/Linukson, vi povas instali multajn ludojn per la
pako-administrilo de via distribuaĵo. Aliaj ludoj estas distribuitaj per
la formatoj Flatpak, Snap, AppImage, aŭ oni devas kompili ilin. Por trovi
liberajn videludojn mi rekomendas [LibreGameWiki](https://libregamewiki.org/Main_Page).

Tamen, ni ne nur havas liberajn ludojn, sed miloj da malnovaj ludmaŝinaj
ludoj, kiuj povas esti ludataj per<!-- more --> la MAME-imitilo[^1].
Ankaŭ estas imitiloj por videludiloj, por ludoj nur en Windows
disponeblaj, ktp. Multaj malnovaj ludoj estas tre originalaj kaj
amuzaj, tiom ke la plejparto de la modernaj ludoj estas nuraj imitaĵoj
kaj inspiroj kun pli kvalitaj grafikoj.

Estas ja homoj, kiuj preferas nuntempajn videludojn, kiuj kunportas
novajn ideojn kaj havas bonan bildan distingokapablon. La problemo
estas, ke multaj el tiuj ludoj estas proprietaj, kostas monon kaj kutime
nur disponeblas en majoritataj lingvoj. Aliflanke liberaj ludoj ĉiam
povas esti plibonigitaj kaj kutime disponeblas en multaj lingvoj. Mi
preferas doni monon por antaŭenigi la disvolvon de liberaj ludoj ol
elspezi monon por io, kiun mi ne povas modifi.

Aliflanke, ne aĉeti videludojn ĉiam estos la plej sana kaj ekologia
decido. La plej grandaj firmaoj de la industrio kreas videludilojn ĉiujn
kelkajn jarojn. Tiumaniere ili instigas la konsumismon kaj generas
elektronikajn rubaĵojn. Ludoj, kiujn vi aĉetis, ĉesos funkcii en la
novaj videludiloj. Ankaŭ ekzistas ludoj, kiuj vendas virtualaĵojn... Nu,
ili estas ludoj kreitaj por malriĉigi vin kaj dependigi vin; evitindas.

Ideale estas havi maŝinon, kiu ebligas al vi ludi ĉiun ajn ludon, de ĉiu
ajn epoko, per ĉiu ajn bilda distingokapablo... Tiu maŝino estas, laŭ
mi, komputilo kun GNU/Linukso.

<a href="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png">
<img src="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png" alt="" width="804" height="1080" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png 804w, /wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball-402x540.png 402w" sizes="(max-width: 804px) 100vw, 804px">
</a>

Ĉiuokaze, preskaŭ ĉiam plej bonas iri al la strato kaj fari sporton. Ne
bonas resti horojn post horoj antaŭ ekrano, kvankam la ludoj estas
liberaj.

[^1]: Eblas instali ĝin per `sudo apt install mame` en distribuaĵoj
  bazitaj sur Debiano. Ludoj (ROMs) povas esti elŝutitaj en
  <https://archive.org/download/mame-merged/mame-merged/>,
  <https://www.mamedev.org/roms/> kaj en multaj aliaj paĝoj. En Debiano
  oni devas konservi la ludojn en la dosierujo `~/mame/roms`.
