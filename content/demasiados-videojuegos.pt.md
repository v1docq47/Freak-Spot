Author: Jorge Maldonado Ventura
Category: Videojogos
Date: 2023-12-15 12:00
Lang: pt
Slug: compras-videojuegos-yo-tengo-mas-de-6000-gratis
Tags: comprar, emulação, GNU/Linux, software livre
Save_as: compras-videojogos-eu-consegui-mais-de-6000-gratis/index.html
URL: compras-videojogos-eu-consegui-mais-de-6000-gratis/
Title: Compras videojogos? Eu tenho mais de 6000, grátis
Image: <img src="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png" alt="" width="1441" height="835" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png 1441w, /wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando-720x417.png 720w" sizes="(max-width: 1441px) 100vw, 1441px">

A indústria dos videojogos é multimilionária. No entanto,
existem milhares e milhares de jogos que podem ser jogados
gratuitamente. Não estou a falar apenas de jogos [livres](https://www.gnu.org/philosophy/free-sw.pt-br.html), mas também
de jogos de arcada antigos, de consola, etc.

Se usas GNU/Linux, podes instalar muitos jogos eletrónicos usando o
gestor de pacotes da sua distribuição. Outros jogos são distribuídos em
formato Flatpak, Snap, AppImage, ou devem ser compilados. Para encontrar
jogos livres, recomendo o
[LibreGameWiki](https://libregamewiki.org/Main_Page).

No entanto, não temos apenas jogos livres, mas milhares de jogos de
arcada antigos, que podem ser jogados com<!-- more --> o emulador MAME[^1]. Existem
também emuladores para consolas, para jogos apenas disponíveis no
Windows, etc. Muitos jogos antigos são muito originais e divertidos; a
maioria dos jogos modernos são meras imitações e inspirações com
melhores gráficos.

É claro que algumas pessoas preferem os videojogos actuais que
trazem ideias novas e podem ser vistos em alta definição. O problema é
que muitos destes jogos são proprietários, custam dinheiro e, muitas
vezes, só estão disponíveis nas línguas mais faladas. Os jogos livres,
por outro lado, podem sempre ser melhorados e estão frequentemente
disponíveis em muitas línguas. Prefiro dar dinheiro para promover o
desenvolvimento de jogos livres do que gastar dinheiro em algo que
não posso mudar.

Por outro lado, não comprar videojogos será sempre mais ecológico e
saudável. As maiores empresas do sector criam consolas de poucos em
poucos anos. Desta forma, incentivam o consumismo e geram resíduos
electrónicos. Os jogos comprados deixam de funcionar nas novas consolas.
Há também jogos que vendem coisas virtuais... Ora, são jogos concebidos
para te tornar mais pobre e viciado; devem ser evitados.

O ideal é ter uma máquina que permita jogar qualquer jogo, de qualquer
época, em qualquer resolução... Essa máquina é, no meu caso, um
computador com o GNU/Linux.

<a href="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png">
<img src="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png" alt="" width="804" height="1080" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png 804w, /wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball-402x540.png 402w" sizes="(max-width: 804px) 100vw, 804px">
</a>

Contudo, quase sempre a melhor coisa a fazer fazer é sair à rua e
praticar desporto. Não é bom passar horas e horas em frente a um ecrã,
mesmo que os jogos sejam livres.

[^1]: Pode ser instalado com `sudo apt install mame` em distribuições
baseadas no Debian. Os jogos (ROMs) podem ser descarregados em
  <https://archive.org/download/mame-merged/mame-merged/>,
  <https://www.mamedev.org/roms/> e em muitas outras páginas. No Debian,
  os jogos devem ser guardados no diretório `~/mame/roms`.
