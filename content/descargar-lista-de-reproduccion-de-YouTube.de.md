Author: Jorge Maldonado Ventura
Category: Ohne Kategorie
Date: 2023-04-15 22:00
Lang: de
Slug: descargar-lista-de-reproduccion-de-youtube
Save_as: YouTube-Wiedergabeliste-herunterladen/index.html
URL: YouTube-Wiedergabeliste-herunterladen/
Tags: playlist, PeerTube, YouTube
Title: YouTube-Wiedergabeliste herunterladen
Image: <img src="/wp-content/uploads/2023/04/logo-yt-dlp.png" alt="">

Wir können das Programm [`yt-dlp`](https://github.com/yt-dlp/yt-dlp/)
verwenden. [Es gibt mehrere Möglichkeiten, es zu
installieren](https://github.com/yt-dlp/yt-dlp/#installation), eine
davon ist die Verwendung des Paketmanagers unserer Distribution:

    :::text
    sudo apt install yt-dlp

Um eine Wiedergabeliste herunterzuladen, tippe einfach `yt-dlp`
gefolgt von der URL der Wiedergabeliste in einem Terminal und
drücke <kbd>Enter</kbd>. Zum Beispiel:

    :::text
    yt-dlp https://youtube.com/playlist?list=PLvvDxND6LkgB_5dssZod-3BET4_DFRmtU

Dieses Programm lädt nicht nur Videos von YouTube herunter, sondern kann
auch Videos von [vielen Websites](https://ytdl-org.github.io/youtube-dl/supportedsites.html) herunterladen.
