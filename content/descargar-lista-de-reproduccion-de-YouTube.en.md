Author: Jorge Maldonado Ventura
Category: Tricks
Date: 2023-04-13 11:00
Lang: en
Slug: descargar-lista-de-reproduccion-de-youtube
Save_as: how-to-download-YouTube-playlist/index.html
URL: how-to-download-YouTube-playlist/
Tags: playlist, PeerTube, YouTube
Title: How to download YouTube playlist
Image: <img src="/wp-content/uploads/2023/04/logo-yt-dlp.png" alt="">

You can use [`yt-dlp`](https://github.com/yt-dlp/yt-dlp/).
[There are many ways to install it](https://github.com/yt-dlp/yt-dlp/#installation), one of them is
with the package manager of your distro:

    :::text
    sudo apt install yt-dlp

To download a playlist you only have to type in a terminal `yt-dlp` and
the URL of the playlist you want to download, and press the
<kbd>Enter</kbd> key. For instance:

    :::text
    yt-dlp https://youtube.com/playlist?list=PLvvDxND6LkgB_5dssZod-3BET4_DFRmtU

This program not only downloads videos from YouTube, it can also
download videos from [many websites](https://ytdl-org.github.io/youtube-dl/supportedsites.html).
