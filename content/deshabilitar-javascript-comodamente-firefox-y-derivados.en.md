Author: Alyssa Rosenzweig
Category: Privacy
Date: 2017-12-05 18:08
Lang: en
Slug: desahabilitar-javascript-comodamente-firefox-y-derivados
Save_as: disable-JavaScript-in-Firefox/index.html
URL: disable-JavaScript-in-Firefox/
Tags: Abrowser, Firefox, Iceweasel, JavaScript, browser, privacy, programming
Title: Disable JavaScript easily in Firefox and derivatives

Given the ["trap" that supposes the presense of JavaScript on the
web](https://www.gnu.org/philosophy/javascript-trap.en.html), we could
be executing proprietary software without realising. This software can
compromise our privacy or do tasks that we don't want. Maybe we don't
want to run JavaScript because we're testing how a page functions
without JavaScript during the creation of a website.

<!-- more -->

To disable JavaScript in Firefox and derivative browsers, we can type
`about:config` in the URL bar and change the value of the preference
`javascript:enabled` from `true` to `false`. But doing this every time
we want to enable or disable JavaScript is rather inconvenient.

<a href="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png">
<img src="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png" alt="Changing the value of the preference javascript.enabled in Tor Browser" width="1000" height="279" srcset="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png 1000w, /wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox-500x139.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

To enable and disable JavaScript more comfortably we can use the
extension JavaScript Toggle On and Off, which is available in two
versions:

- [JavaScript Toggle On and Off (WebExtension)](https://addons.mozilla.org/en/firefox/addon/javascript-toggler/). For versions of Firefox greater than or equal to 57.
- [JavaScript Toggle On and Off](https://addons.mozilla.org/es/firefox/addon/javascript-toggle-on-and-off/). For versions of Firefox older than 57.

Once installed, the extension will create a button which we can click to
enable or disable JavaScript. There are many other extensions that allow
us to have control over the JavaScript code we want to execute, but this
extension is ideal if we simply want to enable or disable JavaScript.

If we want to have JavaScript disabled with the extension but
nevertheless execute JavaScript on certain websites, we can add the
<abbr title="Uniform Resource Locator">URL</abbr>s of these websites to
a blank list in the extension configuration menu.

<a href="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png">
<img src="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png" alt="Adding websites to the blank list of JavaScript Toggle On and Off" width="1276" height="706" srcset="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png 1276w, /wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off-638x353.png 638w" sizes="(max-width: 1276px) 100vw, 1276px">
</a>
