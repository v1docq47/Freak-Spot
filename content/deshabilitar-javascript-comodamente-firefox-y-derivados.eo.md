Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2020-10-08 20:05
Lang: eo
Slug: desahabilitar-javascript-comodamente-firefox-y-derivados
Save_as: malebligi-Ĝavoskripton-en-Firefox-kaj-ĝiaj-derivitaj-retumiloj/index.html
URL: malebligi-Ĝavoskripton-en-Firefox-kaj-ĝiaj-derivitaj-retumiloj/
Tags: Abrowser, Firefox, Ĝavoskripto, retumilo, privateco, programado
Title: Malebligi Ĝavoskripton facile en Firefox kaj ĝiaj derivitaj retumiloj

Pro la ["kaptilo", kiu signifas la esto de Ĝavoskripto en la
Reto](/eo/ĉu-ripari-aŭ-forigi-la-aŭtomate-instalitan-Ĝavoskripton/),
ni povus esti plenumante proprietan Ĝavoskripton senscie. Ĉi tiu
programaro povus kompromiti nian privatecon aŭ fari taskojn, kiujn ni ne
deziras.  Povas esti, ke ankaŭ ni ne volas plenumi Ĝavoskripton, ĉar ni
testas, kiel retejo funkcias sen Ĝavoskripto dum ĝia kreado.

<!-- more -->

Por malebligi Ĝavoskripton en Firefox kaj ĝiaj derivitaj retumiloj, ni povas
skribi `about:config` en la adresbreto kaj modifi la valoron de la
agordo `javascript:enabled` el `true` al `false`. Sed fari tion ĉiufoje,
kiam ni volas ebligi aŭ malebligi Ĝavoskripton, estas tre malkonforte.

<a href="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png">
<img src="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png" alt="Modifante la valoron de la agordo javascript.enabled en Tor Browser" width="1000" height="279" srcset="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png 1000w, /wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox-500x139.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

Por ebligi aŭ malebligi Ĝavoskripton pli komforte ni povas uzi la
kromaĵon [JavaScript Toggle On and Off](https://addons.mozilla.org/es/firefox/addon/javascript-toggler/).

La kromaĵo kreos post la instalado butonon, kiun ni povas klaki por
ebligi aŭ malebligi Ĝavoskripton. Estas multaj aliaj kromaĵoj, kiuj
ankaŭ lasas al ni kontroli la Ĝavoskriptan kodon, kiun ni volas plenumi
kaj la kodon, kiun ni ne volas plenumi; sed ĉi tiu kromaĵo estas ideala,
se ni nur deziras ebligi aŭ malebligi Ĝavoskripton.

Se ni volas plenumi Ĝavoskripton en kelkaj retejoj, kiam kun la kromaĵo
Ĝavoskripto estas malebligita, ni povas aldoni la URL-n de tiuj retejoj
al blanko-listo en la menuo de agordoj de la kromaĵo.

<a href="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png">
<img src="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png" alt="Aldonante retejoj al la blankolisto de JavaScript Toggle On and Off" width="1276" height="706" srcset="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png 1276w, /wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off-638x353.png 638w" sizes="(max-width: 1276px) 100vw, 1276px">
</a>
