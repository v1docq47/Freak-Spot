Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2023-01-28 11:00
Lang: eo
Slug: cómo-recoger-información-de-uso-de-usuarios-de-redes-sociales-ingeniería-social
Save_as: Kiel-esplori-homojn-per-sociaj-retejoj/index.html
URL: Kiel-esplori-homojn-per-sociaj-retejoj/
Tags: socia manipulado, Facebook, Instagram, sociaj retejoj, TikTok, Twitter
Title: Kiel esplori homojn per sociaj retejoj
Image: <img src="/wp-content/uploads/2023/01/secreto-mujer.png" alt="" width="1200" height="844" srcset="/wp-content/uploads/2023/01/secreto-mujer.png 1200w, /wp-content/uploads/2023/01/secreto-mujer-600x422.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Malmultaj ciferecaj detektivoj atentas la uzokutimojn de la sociaj
retejoj, kiuj povas riveli profundajn dezirojn, animstatojn, ktp. La
algoritmo de la sociaj <!-- more -->retejoj[^1] faras tion. Kiel profiti
ĝin esplorante homon?

Esplorante homon pere de sociaj retejoj necesas, ke ni ankaŭ kolektu
informon de uzo, kaj tion oni povas fari sekvante rin per izolita konto.
Per ĉi tiu metodo la algoritmo rekomendos al ni aferojn rilatajn al riaj
interesoj, ria animstato, ktp., aferoj kiuj ri eĉ ne afiŝis.

<img src="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg" alt="" width="6016" height="4000" srcset="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg 6016w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-3008x2000.jpg 3008w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-1504x1000.jpg 1504w" sizes="(max-width: 6016px) 100vw, 6016px">

Akiri la IP-adreson kaj la teknikajn datumojn[^2] de la homo estas
sufiĉe facilas: nur necesas gvidi rin al URL en servilo, kiun ni havu,
kaj por fari tion ni devas igi, ke ri alklaku ligilon kaj ŝarĝu ian
bildon aŭ videon, kiun ni havu por tio, kio ne malfacilas, se ni konas
riajn interesojn. Se ni havas rian retadreson, ni povas uzi
<i lang="en">web bug</i> (tio estas, nevidebla bildo).

[^1]: Aliflanke estas kelkaj sociaj retejoj kiel
    [Mastodon](https://joinmastodon.org/), kiu ne havas algoritmon de
    rekomendoj nek kolektas grandegajn kvantojn de uzantaj datumoj.
[^2]: Kie vi aliras la Interreton, kiu aparato ri uzas, kiu lingvoj estas
  agorditaj por ri, kiuj versioj de retumilo kaj de operaciumo ri uzas, ktp.

Se ni volas akiri pli da informo, ni devas akiri la HTTP-petojn de la
esplorita homo. Foje oni povas aĉeti datumojn al provizanto de
retkonekto aŭ en la [profunda
reto](https://es.wikipedia.org/wiki/Internet_profunda), foje la
registraraj sekurecaj fortoj aŭ spionaj agentejoj havas aliron al tiuj
datumoj. Krome oni povis uzi [socian manipuladon](https://es.wikipedia.org/wiki/Ingenier%C3%ADa_social_(seguridad_inform%C3%A1tica))
en la fizika mondo, aĉeti datumojn de paĝoj, kiujn la homo uzas, por
akiri rian telefonnumeron, ktp.

La sola maniero protekti vin de fiaj sociaj retejoj kaj la aliro al via
informo fare de kodumuloj, registaroj, ktp., estas ne uzante ilin. Oni
povus ankaŭ malpliigi la riskojn uzante ilin limigite: sen Ĝavoskripto
(kio multaj fiaj sociaj retejoj ne permesas); per [Tor](https://eo.wikipedia.org/wiki/Tor_(programaro));
nur afiŝi aŭ nur legi; ne alklaki en afiŝoj, kiuj atentigas vin; ne diri
tio, kion vi ŝatas; pasiĝi ĉiam la saman tempon en ĉiu afiŝo por ne
montri prefero por iu (io malfacila), ktp.

Finfine, facilas esplori homojn, kiuj ne estas uzantoj, sed uzitaj, ĉar
ili estas la produkto de firmaoj, kiuj komercas kun riaj vivoj, riaj
deziroj, riaj animstatoj, ktp. Oni povus pliigi la teknologia suvereneco
havante propran retejon, anstataŭ dependi de socia retejo, aŭ uzante
liberan socian retejon, kiu ankaŭ respektu la privateco de la uzantoj.
