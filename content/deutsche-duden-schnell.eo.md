Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2022-12-02 12:25
Lang: eo
Slug: Duden-Wörterbuch-schnell-Terminal
Save_as: Duden-vortaro-rapide-per-la-terminalo/index.html
URL: Duden-vortaro-rapide-per-la-terminalo/
Tags: duden, germana, vortaro
Title: Duden-vortaro rapide per terminalo
Image: <img src="/wp-content/uploads/2022/12/duden-Hallo-vorto-terminalo.png" alt="" width="736" height="288" srcset="/wp-content/uploads/2022/12/duden-Hallo-vorto-terminalo.png 736w, /wp-content/uploads/2022/12/duden-Hallo-vorto-terminalo-368x144.png 368w" sizes="(max-width: 736px) 100vw, 736px">

La Duden-vortaro estas unu el la plej bonaj germanaj vortaroj.
Bedaŭrinde la retejo havas multege da reklamoj kaj la fasado igas, ke
estu tre malrapide konsulti vortojn. Fortune estas [Python-programo
nomita Duden](https://github.com/radomirbosak/duden), kiu ebligas konsulti vortojn en la terminalo.

Por instali la programo simple plenumu la jenajn komandojn:

    :::bash
    sudo apt install python3-pip
    sudo pip3 install duden

Poste vi povas plenumi `duden Wort` por konsulti vortojn.
