Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2022-12-09 16:22
Lang: es
Slug: Duden-Wörterbuch-schnell-Terminal
Save_as: diccionario-Duden-rápido-con-la-terminal/index.html
URL: diccionario-Duden-rápido-con-la-terminal/
Tags: duden, diccionario, alemán
Title: Diccionario Duden rápido con la terminal
Image: <img src="/wp-content/uploads/2022/12/duden-Hallo-palabra-terminal.png" alt="" width="736" height="302" srcset="/wp-content/uploads/2022/12/duden-Hallo-palabra-terminal.png 736w, /wp-content/uploads/2022/12/duden-Hallo-palabra-terminal-368x151.png 368w" sizes="(max-width: 736px) 100vw, 736px">

El diccionario Duden es uno de los mejores diccionarios en alemán.
Desgraciadamente, la página web está llena de anuncios y la interfaz
vuelve muy lento el proceso de consultar palabras. Afortunadamente, existe un [programa de Python llamado Duden](https://github.com/radomirbosak/duden) que permite consultar palabras en la terminal.

Para instalar el programa en distribuciones de GNU/Linux basadas en
Debian basta con ejecutar los siguientes comandos:

    :::bash
    sudo apt install python3-pip
    sudo pip3 install duden

Después puedes ejecutar `duden Wort` (después de `duden` va la palabra)
para consultar palabras.
