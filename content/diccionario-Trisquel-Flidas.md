Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2018-05-01 01:15
Image: <img src="/wp-content/uploads/2018/05/diccionario-Trisquel-Flidas.png" alt="Vista del diccionario de Trisquel Flidas lanzado desde el menú de aplicaciones" width="1024" height="765" srcset="/wp-content/uploads/2018/05/diccionario-Trisquel-Flidas.png 1024w, /wp-content/uploads/2018/05/diccionario-Trisquel-Flidas-512x382.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
Lang: es
Slug: diccionario-trisquel-flidas
Tags: crítica, diccionario, guía, idiomas, MATE, programa, Trisquel, Trisquel 8, Trisquel Flidas
Title: Diccionario de MATE en Trisquel 8

La versión estándar de Trisquel 8 viene con un diccionario bastante
práctico. La versión Mini de Trisquel 8 no cuenta con este programa; hay
que instalarlo con la instrucción `sudo apt install mate-utils-common`.

Nada más abrirlo solo devolverá resultados para definiciones del inglés,
aunque podemos usar otro diccionario. Para ello pulsamos
<kbd>Ctrl</kbd>+<kbd>B</kbd> o pulsamos en **Ver>Fuentes de
diccionario**. A mí no me aparecían fuentes, y tuve que darle al botón
de recargar abajo a la derecha. Una vez cargada la lista de fuentes,
podemos pulsar el diccionario que queramos usar, en este caso yo elegí
**German-English FreeDict Dictionary** y se marcó la opción en negrita.

<!-- more -->

<a href="/wp-content/uploads/2018/05/selección-de-diccionario-mate.png">
<img src="/wp-content/uploads/2018/05/selección-de-diccionario-mate.png" alt="Seleccionando diccionario en Diccionario, el diccionario seleccionado aparece en negrita.">
</a>

Por defecto, los diccionarios se obtienen del servidor dict.org, aunque
podemos añadir más servidores de diccionarios. Podemos seleccionar el
servidor de diccionario en la barra lateral tras pulsar
<kbd>Ctrl</kbd>+<kbd>D</kbd> o en **Ver>Fuentes de diccionario**. Desde
el menú de preferencias podemos añadir uno nuevo.

Este sencillo programa de diccionario tiene también funcionalidades
muy útiles como **palabras similares** (palabras que se escriben igual)
y **estrategias disponibles**, que permite buscar utilizando un método
de búsqueda (por ejemplo, coincidiendo sufijos). La
[lista de estrategias no está traducida](https://bugzilla.gnome.org/show_bug.cgi?id=687747)
al castellano.

El programa cuenta con una sencilla y explicativa documentación que nos
enseña cosas como buscar texto, guardar definiciones e imprimir.

<a href="/wp-content/uploads/2018/05/miniaplicación-diccionario-mate.png">
<img src="/wp-content/uploads/2018/05/miniaplicación-diccionario-mate.png" alt="También existe una miniaplicación que incluye el programa de diccionario" width="812" height="633" srcset="/wp-content/uploads/2018/05/miniaplicación-diccionario-mate.png 812w, /wp-content/uploads/2018/05/miniaplicación-diccionario-mate-406x316.png 406w" sizes="(max-width: 812px) 100vw, 812px">
</a>

Si hacemos mucho uso del diccionario o si lo queremos tener más
accesible, podemos añadirlo como miniaplicación a la barra de inicio.
Para esto hacemos clic derecho en la barra de aplicaciones, le damos a
**Añadir al panel...** y seleccionamos **Dictionary Look up** (esto no
está traducido al español). A partir de entonces podremos buscar
definiciones escribiendo la definición y pulsando <kbd>Intro</kbd>.

Encuentro este programa muy útil, y me parece sencillo de usar. Sin
embargo, debe mejorar en el aspecto de idiomas, es confuso que haya
partes en español y otras sin traducir en inglés.
