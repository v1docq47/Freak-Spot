Author: Jorge Maldonado Ventura
Category: Image editing
Date: 2023-04-06 09:00
Lang: en
Slug: diff-para-imagenes-en-git
Save_as: git-diff-for-images/index.html
URL: git-diff-for-images/
Tags: Debian, diff, git, command line
Save_as: git-diff-for-images/index.html
URL: git-diff-for-images/
Title: <code>git diff</code> for images
Image: <img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="Komparo de du bildoj flanko ĉe flanko, bildo inter ili montrante la ŝanĝoj, por ke ili estu pli facile rimarkebla" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">

The default Git `diff` does not show differences between images. That's
understandable: it's not intended for that. However, it would be great
if Git would show image changes like code changes, wouldn't it? At least
something nicer than...

    :::text
    $ git diff
    diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    Binary files a/es-ES/images/autobuilder.png and b/es-ES/images/autobuilder.png differ

Something like this:

<a href="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png">
<img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">
</a>

I've done that using a script that uses the ImageMagick library to
compare images. Here's how to do the same thing.

<!-- more -->

On Debian-based distributions you have to install that package by
running...

    :::text
    sudo apt install imagemagick

Next, in a file with execute permission in any path accessible by the
`$PATH` environment variable, write the following code, which I saved in
`~/bin/git-imgdiff` in this tutorial:

    :::bash
    #!/bin/sh
    compare $2 $1 png:- | montage -geometry +4+4 $2 - $1 png:- | display -title "$1" -

Now tell Git the extensions you want to consider as images using the
`.gitattributes` file. If it doesn't exist, create it in the root
directory of a Git project or in your `$HOME` directory with the
following lines for GIF, PNG and JPG formats, or if the file already
exists, add them:

    :::text
    *.gif diff=image
    *.jpg diff=image
    *.jpeg diff=image
    *.png diff=image

To have the `.gitattributes` configuration you saved in the `$HOME`
directory loaded for all Git projects, you need to run the following
command:

    :::bash
    git config --global core.attributesFile ~/.gitattributes

Now configure Git to run the script you created before when comparing
images:

    :::text
    git config --global diff.image.command '~/bin/git-imgdiff'

It's as simple as that. You can customise the script to suit your needs.

If you just need to know what metadata has changed, you can install
`exiftool` to display something like this:

    :::diff diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    --- a/es-ES/images/autobuilder.png
    +++ b/es-ES/images/autobuilder.png
    @@ -1,21 +1,21 @@
     ExifTool Version Number         : 10.10
    -File Name                       : vHB91h_autobuilder.png
    -Directory                       : /tmp
    -File Size                       : 44 kB
    -File Modification Date/Time     : 2020:03:09 02:12:08+01:00
    -File Access Date/Time           : 2020:03:09 02:12:08+01:00
    -File Inode Change Date/Time     : 2020:03:09 02:12:08+01:00
    -File Permissions                : rw-------
    +File Name                       : autobuilder.png
    +Directory                       : es-ES/images
    +File Size                       : 63 kB
    +File Modification Date/Time     : 2020:03:09 01:35:22+01:00
    +File Access Date/Time           : 2020:03:09 01:35:22+01:00
    +File Inode Change Date/Time     : 2020:03:09 01:35:22+01:00
    +File Permissions                : rw-rw-r--
     File Type                       : PNG
     File Type Extension             : png
     MIME Type                       : image/png
    -Image Width                     : 796
    -Image Height                    : 691
    +Image Width                     : 794
    +Image Height                    : 689
     Bit Depth                       : 8
     Color Type                      : RGB
     Compression                     : Deflate/Inflate
     Filter                          : Adaptive
     Interlace                       : Noninterlaced
    -Significant Bits                : 8 8 8
    -Image Size                      : 796x691
    -Megapixels                      : 0.550
    +Background Color                : 255 255 255
    +Image Size                      : 794x689
    +Megapixels                      : 0.547

If that's the case, read on.

Install `exiftool`. On Debian-derived distributions you have to run this:

    :::text
    sudo apt install libimage-exiftool-perl

Then add this to the `.gitattributes` file:

    :::text
    *.png diff=exif
    *.jpg diff=exif
    *.gif diff=exif

Finally execute...

    :::text
    git config --global diff.exif.textconv exiftool`

Optionally, in the two methods I've shown, you can chose not to use
`--global` so that the chosen tool only applies to the Git project you
are working on.

I hope it's now easier for you to review the changes you've made to
images within a project.
