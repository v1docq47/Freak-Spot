Author: Jorge Maldonado Ventura
Category: Bildredaktado
Date: 2020-10-15
Modified: 2023-04-06 17:00
Lang: eo
Slug: diff-para-imagenes-en-git
Tags: Debiano, diff, git, komandlinio, Trisquel
Save_as: diff-por-bildoj-en-Git/index.html
URL: diff-por-bildoj-en-Git/
Title: <code>diff</code> por bildoj en Git
Image: <img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="Komparo de du bildoj flanko ĉe flanko, bildo inter ili montrante la ŝanĝoj, por ke ili estu pli facile rimarkebla" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">

La implicita `diff` de Git ne montras la ŝanĝojn inter bildoj. Tio estas
normala: ĝi ne estas pensita por tio. Tamen estus bonega, se Git montrus
la bildajn ŝanĝojn kiel la kodajn, ĉu ne? Almenaŭ io pli bela ol...

    :::text
    $ git diff
    diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    Binary files a/es-ES/images/autobuilder.png and b/es-ES/images/autobuilder.png differ

Io tia...

<a href="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png">
<img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">
</a>

Tion faris mi per skripto, kiu uzas ImageMagick, por kompari bildojn.
Ĉi tie montras mi, kiel vi povas fari la samon.

<!-- more -->

En distribuoj bazitaj sur Debiano bezonas instali la pakon plenumante...

    :::text
    sudo apt install imagemagick

Poste en dosiero kun permesoj de plenumado en vojo alirebla de la media
variablo `$PATH` skribu la jenan kodon, kiun mi konservas en
`~/bin/git-imgdiff` en ĉi tiu instruilo:

    :::bash
    #!/bin/sh
    compare $2 $1 png:- | montage -geometry +4+4 $2 - $1 png:- | display -title "$1" -

Poste diru al Git per la dosiero `.gitattributes` la dosierfinojn, kiujn
vi konsideras kiel bildoj. Se ĝi ne estas, kreu ĝin en la radika
dosierujo de Git-projekto aŭ en via `$HOME` dosierujo kun la jenajn
liniojn por la formatoj GIF, PNG kaj JPG, aŭ se la dosiero jam ekzistas,
aldonu ilin:

    :::text
    *.gif diff=image
    *.jpg diff=image
    *.jpeg diff=image
    *.png diff=image

 Por ke la agordaro de `.gitattributes`, kiun vi konservis en la
 `$HOME`-dosiero, estu uzita por ĉiuj Git-projektoj, vi devas plenumi la
 jenan komandon:

    :::bash
    git config --global core.attributesFile ~/.gitattributes

Nun agordu je Git tiel, por ke ĝi plenumu la antaŭe kreitan skripton,
kiam ĝi komparu bildojn:

    :::text
    git config --global diff.image.command '~/bin/git-imgdiff'

Tiel simpla. Vi povas adapti la skripton al viaj bezonoj.

Se al vi sufiĉas scii, kiuj metadatumoj estis ŝanĝitaj, vi povas instali
`exiftool`, por montri ion tian:

    :::diff diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    --- a/es-ES/images/autobuilder.png
    +++ b/es-ES/images/autobuilder.png
    @@ -1,21 +1,21 @@
     ExifTool Version Number         : 10.10
    -File Name                       : vHB91h_autobuilder.png
    -Directory                       : /tmp
    -File Size                       : 44 kB
    -File Modification Date/Time     : 2020:03:09 02:12:08+01:00
    -File Access Date/Time           : 2020:03:09 02:12:08+01:00
    -File Inode Change Date/Time     : 2020:03:09 02:12:08+01:00
    -File Permissions                : rw-------
    +File Name                       : autobuilder.png
    +Directory                       : es-ES/images
    +File Size                       : 63 kB
    +File Modification Date/Time     : 2020:03:09 01:35:22+01:00
    +File Access Date/Time           : 2020:03:09 01:35:22+01:00
    +File Inode Change Date/Time     : 2020:03:09 01:35:22+01:00
    +File Permissions                : rw-rw-r--
     File Type                       : PNG
     File Type Extension             : png
     MIME Type                       : image/png
    -Image Width                     : 796
    -Image Height                    : 691
    +Image Width                     : 794
    +Image Height                    : 689
     Bit Depth                       : 8
     Color Type                      : RGB
     Compression                     : Deflate/Inflate
     Filter                          : Adaptive
     Interlace                       : Noninterlaced
    -Significant Bits                : 8 8 8
    -Image Size                      : 796x691
    -Megapixels                      : 0.550
    +Background Color                : 255 255 255
    +Image Size                      : 794x689
    +Megapixels                      : 0.547

Tiam legu plu.

Instalu `exiftool`. En distribuoj bazitaj sur Debiano oni devas plenumi
tion:

    :::text
    sudo apt install libimage-exiftool-perl

Poste aldoni jenon al `.gitattributes`-dosiero.

    :::text
    *.png diff=exif
    *.jpg diff=exif
    *.gif diff=exif

Fine plenumu...

    :::text
    git config --global diff.exif.textconv exiftool`

En ambaŭ montritajn metodojn vi laŭvole povas ne aldoni `--global`,
por ke la elektita ilo nur apliku al la Git-projekto, kie vi estas
laborante.

Mi esperas, ke estas nun pli simpla por vi kontroli la ŝanĝojn de bildoj
en projekto.
