Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2023-02-21 21:00
Modified: 2023-05-28 09:00
Lang: eo
Slug: dinero-anónimo-digital-monero
Save_as: ĉifrovalutoj-anonimeco-sencentra-ekonomio/index.html
URL: ĉifrovalutoj-anonimeco-sencentra-ekonomio/
Tags: mono, Monero, privateco, ekonomio
Title: Ĉifrovalutoj, anonimeco, sencentra ekonomio
Image: <img src="/wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko.png" alt="" width="1667" height="2378" srcset="/wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko.png 1667w, /wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko-833x1189.png 833w, /wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko-416x594.png 416w" sizes="(max-width: 1667px) 100vw, 1667px">

Multaj homoj pensas, ke la ĉifrovalutoj estas anonimaj, sed ne
estas. La plejparto de ili estas pseŭdoanonimaj, ĉar oni povas spuri la
transakciojn kaj tiel malkovri kiu posedas la <!-- more -->monujon[^1].

Ideale estus, ke implicite la ĉifrovalutoj funkciu anonime por
eviti la kontrolon de la loĝantaro fare de la registaro kaj firmaoj; tio
estas, ke ili estu io simila al la kontanta mono? Kial tiom gravas la
privateco de la transakcioj?

Se la transakcioj estas publikaj, ĉiu ajn firmao aŭ homo povas scii
tion, kion vi elspezis kaj kiel vi tion elspezis; ili povas malkovri, kie
vi loĝas, kion vi ŝatas, ktp. Krome, la mono, kiun vi ricevas aŭ donas
povas esti makulita (se ĝi estis antaŭe uzita de krimuloj). Ĉiu ajn kun
teknikaj scioj povas spurigi la transakciojn[^2]. Estas firmaoj, kiuj
blokis transakciojn pro tio[^3].

La mono devas esti anstataŭigebla, tio estas, unu monero de eŭro
(ekzemple) devas valori la samon kiel alia monero de unu eŭro. Bitcoin
ne estas tiel, ĉar ili lasas spuron kaj estas homoj, kiuj preferus ne
akcepti monon, kiu devenas de krimaj agoj, kiu malpli valorus.

La ĉifrovalutoj tamen signifis revolucion, kiu ebligis sencentre
plenumi transaciojn sen la bezono de centra banko, kaj ekzistas kelkaj,
kiuj oferas veran anonimecon.

## Monero: vere anonima ĉifrovaluto

<a href="/wp-content/uploads/2023/02/monero-symbol-480.png">
<img src="/wp-content/uploads/2023/02/monero-symbol-480.png" alt="">
</a>

[Monero](https://eo.wikipedia.org/wiki/Monero_(%C4%89ifromono)) estas la
plej populara ĉifrovaluto, kiu ebligas privatajn transakciojn,
sencentre kaj malmultekoste. Malsame ol la plejparto de la ĉifrovalutoj,
Monero ne lasas spuron, per kiu oni povas identigi la transakciojn, la
rivevantojn aŭ la pagantojn: ĉio estas anonime registrita en blokĉeno.

Ĉi tio, kaj ankaŭ ĝia granda komunumo de programistoj, kripografiistoj,
tradukistoj, ktp., igas, ke Monero estu unu el la plej uzataj
ĉifrovalutoj.

## Granda energia kosto

La ĉifrovalutoj kiel Monero kaj Bitcoin funkcias per algoritmo
nomita laborpruvo, kiu garantias la sekurecon de la reto, sed kiu
ankaŭ signifas grandan energian koston. Ekzistas alia algoritmo nomita
pruvo de partoprenado, kiu havas malpli grandan energian koston, sed kiu
havas aliajn problemojn.

Kiuj kontraŭas la ĉifrovalutojn kutime kritikas ilian grandan
energian koston, sed oni ne kutimas tiom paroli pri la energia kosto de
la nuntempa banka sistemo (komputiloj, bankaŭtomatoj, oficejoj, ktp.).

## La popola valuto

Estas homoj, kiuj asertas, ke la ĉifrovalutoj estas la popola mono,
ĉar oni ne dependas de centraj bankoj kontrolitaj de registaroj, kiuj
kutimas presi monon kaj tiel igi, ke ĝia valoro malpliiĝu. Kontraŭe la
ĉifrovalutoj estas kreitaj per komputila povo validigante
transakciojn, tio estas, ne estis kreitaj el nenio kaj havas praktikan
utilecon. Tamen mini ĉifrovalutojn estas for de la aliro de homoj sen
teknikaj scioj aŭ sen taŭgaj komputiloj.

La ĉifrovalutoj kutime kondutas simile al oro aŭ al arĝento, ĉar oni
estigas malmultajn novajn ĉifromonerojn, do praktike multaj estas
deflaciaj.

## Kial ili ne estas pli uzitaj?

La ĉifrovalutoj nuntempe estas tre uzitaj: oni faras tage milionojn da
transakcioj. Bone, ne ĉiuj uzas ilin, ĉar ili ne estas facile uzeblaj por
multaj homoj kaj ne estas multe da instigo por elspezi ilin. Se vi havas
ion, kio iĝos pli valora aŭ konservos sian valoron kaj alion, kio perdos
sian valoron, kion vi preferas elspezi? Kompreneble vi konservas tion,
kio konservos aŭ gajnos valoron (oro, kelkaj ĉifrovalutoj) kaj vi
interŝanĝas tion, kio perdos valoron ([fidata
mono](https://eo.wikipedia.org/wiki/Fidata_mono)), antaŭ ol ĝi perdu
ĝin. Ĉi tiu fenomeno estas konata kiel la
[leĝo de Gresham](https://es.wikipedia.org/wiki/Ley_de_Gresham).

## Kiel anonime aĉeti ĉifrovalutojn?

Oni devas elekti sencentran kaj liberan platformon kiel
[Bisq](https://bisq.network/) kaj persone fari interŝanĝojn per kontanta
mono, sen lasi spuron per banka transakcio.

Ekzistas platformoj, kiuj multe faciligas la aĉeton de ĉifrovalutoj.
Tamen kelkaj ne lasas al vi sendi ĉifrovalutoj al via propra monujo, kio
estus kiel havi monon en banko, kiu ne permesas al vi elpreni ĝin kaj
nur lasas al vi interŝanĝi ĝin kontraŭ fidata mono. Tie estas kien ofte
iras la nespertaj spekulantoj. Se oni uzas centran platformon &mdash; ne
rekomendinda por la privateco &mdash;, plej bone estus elekti
platformon, kiu ebligu al vi elpreni monon al monujo, kiu estas sub via
kontrolo kaj ne sub la kontrolo de firmao.

## Ne pagi impostojn

Kiam vi aĉetas per kontanta mono, vi povas eviti pagi impostojn, same
per anonimaj ĉifrovalutoj kiel Monero.

<a href="/wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko.png">
<img src="/wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko.png" alt="" width="1667" height="2378" srcset="/wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko.png 1667w, /wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko-833x1189.png 833w, /wp-content/uploads/2023/02/ne-aĉetu-Monerojn-glumarko-416x594.png 416w" sizes="(max-width: 1667px) 100vw, 1667px">
</a>

[^1]: Kvankam oni povas uzi miksilojn de transakcioj por senspurigi
    ĉifrovalutojn kaj kovri la monan spuron.
[^2]: Legu la artikolon
    [«¿Cómo los gobiernos rastrean las transacciones con cripto para combatir el crimen?»](https://es.beincrypto.com/como-gobiernos-rastrean-transacciones-cripto-para-combatir-crimen/).
[^3]: Legu la artikolon [«Bitcoin's Fungibility Graveyard»](https://sethforprivacy.com/posts/fungibility-graveyard/).
