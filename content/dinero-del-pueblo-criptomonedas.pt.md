Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2023-02-22 14:00
Modified: 2023-05-08 17:00
Lang: pt
Slug: dinero-anónimo-digital-monero
Save_as: Criptomoedas-anonimato-economia-descentralizada/index.html
URL: Criptomoedas-anonimato-economia-descentralizada/
Tags: dinheiro, Monero, privacidade, economia
Title: Criptomoedas, anonimato, economia descentralizada
Image: <img src="/wp-content/uploads/2023/02/não-compre-Monero-adesivo.png" alt="Não compre Monero. Criptomoedas são nocivas ao sistema bancário e podem debilitar o aparelho estatal" width="827" height="1181" srcset="/wp-content/uploads/2023/02/não-compre-Monero-adesivo.png 827w, /wp-content/uploads/2023/02/não-compre-Monero-adesivo-413x590.png 413w" sizes="(max-width: 827px) 100vw, 827px">

Muitas pessoas pensam que as criptomoedas são anónimas, mas não é este o
caso. A maioria das criptomoedas são pseudo-anónimas, uma vez que as
transações podem ser rastreadas e assim descobrir a quem pertence a
carteira<!-- more -->[^1].

Idealmente, por defeito, as criptomoedas deveriam funcionar anonimamente
para evitar o controlo governamental e empresarial da população; por
outras palavras, deveriam ser semelhantes ao dinheiro. Porque é que a
privacidade das transações é tão importante?

Se as transações forem públicas, qualquer empresa ou pessoa pode
descobrir o que gastaste e como o gastaste; podem descobrir onde vives,
o que gostas, etc. Além disso, o dinheiro que recebes ou dás
pode ser manchado (se já foi anteriormente utilizado por criminosos).
Qualquer pessoa com conhecimentos técnicos pode rastrear transações[^2].
Há empresas que bloquearam transações por este motivo[^3].

O dinheiro deve ser
[fungível](https://pt.wikipedia.org/wiki/Fungibilidade), ou seja, uma
moeda de euro deve valer o mesmo que outra moeda de euro. As
<i lang="en">bitcoins</i> não são assim, porque deixam um rasto e
algumas pessoas prefeririam não aceitar dinheiro proveniente de
actividades criminosas, que valeria menos.

As criptomoedas, porém, têm sido uma revolução que permitiu transações
descentralizadas sem a necessidade de um banco central, e há algumas que
oferecem um verdadeiro anonimato.

## Monero: uma criptomoeda verdadeiramente anónima

<a href="/wp-content/uploads/2023/02/monero-symbol-480.png">
<img src="/wp-content/uploads/2023/02/monero-symbol-480.png" alt="">
</a>

[Monero](https://www.getmonero.org/pt-br/index.html) é a criptomoeda
mais popular que permite transações privadas, descentralizadas e de
baixo custo. Ao contrário da maioria das criptomoedas, Monero não deixa
vestígios através dos quais as transações, destinatários ou pagadores
possam ser identificados: tudo é registado na cadeia de blocos de
forma anónima.

Isto, juntamente com a sua grande comunidade de criadores, criptógrafos,
tradutores, etc., faz de Monero uma das criptomoedas mais utilizadas.

## Alto custo energético

As criptomoedas como Monero e Bitcoin funcionam com um
algoritmo chamado prova de trabalho, que garante a segurança da rede,
mas também tem um elevado custo energético. Existe outro algoritmo
chamado [prova de participação](https://pt.wikipedia.org/wiki/Prova_de_participa%C3%A7%C3%A3o), que tem um custo energético mais baixo, mas que
tem outros problemas.

Os opositores das criptomoedas criticam frequentemente o seu elevado
custo energético, mas o custo energético do actual sistema bancário
(equipamento informático dos bancos, caixas automáticos, escritórios,
etc.) é discutido com menos frequência.

## A moeda do povo?

Alguns dizem que as criptomoedas são o dinheiro do povo, uma vez que não
dependem dos bancos centrais controlados pelo governo, que muitas vezes
imprimem dinheiro e assim causam a diminuição do seu valor. Pelo
contrário, as criptomoedas são criadas com poder computacional através
da validação de transações, ou seja, não são criadas a partir do nada e
têm uma utilidade prática. No entanto, minerar criptomoedas estão fora
do alcance de pessoas sem conhecimentos técnicos ou sem equipamento
informático apropriado.

As criptomoedas tendem a comportar-se de forma semelhante ao
ouro ou à prata, uma vez que muito poucas novas criptomoedas são
emitidas, o que na prática torna muitas delas deflacionárias.

## Porque não são mais utilizadas?

As criptomoedas são actualmente muito utilizadas: são feitas
milhões de transações todos os dias. Claro, não são utilizadas por
todos, porque não são fáceis de utilizar por muitas pessoas e não há
muito incentivo para as gastar. Se tens algo que vai aumentar ou reter
valor e algo que vai perder valor, o que preferes gastar? Obviamente,
manténs o que vai aumentar ou reter o seu valor (ouro, algumas
criptomoedas) e trocas o que vai perder valor ([moedas
fiduciárias](https://pt.wikipedia.org/wiki/Moeda_fiduci%C3%A1ria)) antes
que perca valor. Este fenómeno é conhecido como a
[lei de Gresham](https://pt.wikipedia.org/wiki/Lei_de_Gresham).

## Como comprar criptomoedas de forma anónima?

Deves escolher uma plataforma descentralizada e livre como
[a Bisq](https://bisq.network/pt-pt/) e fazer trocas pessoalmente com
dinheiro, sem deixar rasto com uma transferência bancária.

Existem plataformas que tornam muito fácil a compra de criptomoedas. No
entanto, algumas não o deixam enviar criptomoedas para a tua
própria carteira, o que seria como ter dinheiro num banco que não o
deixa tirar e só o deixa trocar por dinheiro fiduciário. É aí que os
especuladores novatos tendem a ir. Se utilizar uma plataforma
centralizada &mdash; não recomendado para privacidade &mdash;, é melhor
escolher uma plataforma que te permita tirar dinheiro para uma carteira,
que está sob o teu controlo e não o de uma empresa.

## Não pagar impostos

Quando comercias com dinheiro, podes evitar pagar impostos, com
criptmoedas anónimas como Monero também.

<a href="/wp-content/uploads/2023/02/não-compre-Monero-adesivo.png">
<img src="/wp-content/uploads/2023/02/não-compre-Monero-adesivo.png" alt="Não compre Monero. Criptomoedas são nocivas ao sistema bancário e podem debilitar o aparelho estatal" width="827" height="1181" srcset="/wp-content/uploads/2023/02/não-compre-Monero-adesivo.png 827w, /wp-content/uploads/2023/02/não-compre-Monero-adesivo-413x590.png 413w" sizes="(max-width: 827px) 100vw, 827px">
</a>

[^1]: Embora os misturadores de transações possam ser utilizados para
    lavar as criptomoedas e cobrir o rasto do dinheiro.
[^2]: Fonte: [«¿Cómo los gobiernos rastrean las transacciones con cripto para combatir el crimen?»](https://es.beincrypto.com/como-gobiernos-rastrean-transacciones-cripto-para-combatir-crimen/).
[^3]: Fonte: [«Bitcoin's Fungibility Graveyard»](https://sethforprivacy.com/posts/fungibility-graveyard/).
