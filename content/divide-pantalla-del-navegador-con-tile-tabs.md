Author: Jorge Maldonado Ventura
Category: Trucos
Date: 2017-03-11 17:46
Image: <img src="/wp-content/uploads/2017/03/poster-Tile-Tabs.png" alt="Poster del Video Tile Tabs" class="attachment-post-thumbnail wp-post-image" width="1920" height="1080" srcset="/wp-content/uploads/2017/03/poster-Tile-Tabs.png 1920w, /wp-content/uploads/2017/03/poster-Tile-Tabs-960x540.png 960w, /wp-content/uploads/2017/03/poster-Tile-Tabs-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
Lang: es
Modified: 2018-03-10 13:27
Slug: divide-la-pantalla-del-navegador-con-tile-tabs
Tags: Abrowser, complemento, Firefox, Icecat, Iceweasel, navegador, páginas web, video
Title: Divide la pantalla del navegador con Tile Tabs

Cuando necesitamos comparar dos páginas web o tenerlas a simple vista,
resulta incómodo cambiar de pestaña en el navegador. En este video os
presento [Tile
Tabs](https://addons.mozilla.org/es-ES/firefox/addon/tile-tabs/), un
complemento para Firefox y navegadores derivados de Firefox que nos permite ver
varias páginas web al mismo tiempo.

<ins cite="actualización" datetime="2018-07-11T17:49:04+02:00">
Esta extensión solo es compatible con versiones de
Firefox (y derivados de Firefox) inferiores a la 57. Para versiones
posteriores existe
[una extensión](https://addons.mozilla.org/en-US/firefox/addon/tile-tabs-we/)
realizada por la misma desarrolladora que no funciona igual, ya que la
<abbr title="Application Programming Interface">API</abbr> de
WebExtensions tiene más limitaciones.</ins>

<!-- more -->

<video controls poster="/wp-content/uploads/2017/03/poster-Tile-Tabs.png">
  <source data-res="1080p" src="/temporal/Tile_Tabs.webm" type="video/webm">
  <source data-res="720p" src="/temporal/Tile_Tabs1280x720.webm" type="video/webm">
  <source data-res="480p" src="/temporal/Tile_Tabs854x480.webm" type="video/webm">
  <source data-res="360p" src="/temporal/Tile_Tabs640x360.webm" type="video/webm">
  <track kind="captions" src="/wp-content/uploads/2017/04/Tile_Tabs.vtt" srclang="es" label="Español">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Puedes descargar el vídeo, hay varias resoluciones disponibles:
<a download href="/temporal/Tile_Tabs.webm">1920x1080</a>,
<a download href="/temporal/Tile_Tabs1280x720.webm">1280x720</a>,
<a download href="/temporal/Tile_Tabs854x480.webm">854x480</a>,
<a download href="/temporal/Tile_Tabs640x360.webm">640x360</a>.

También puedes <a download href="/wp-content/uploads/2017/04/Tile_Tabs.vtt">descargar los subtítulos (en español).</a>
