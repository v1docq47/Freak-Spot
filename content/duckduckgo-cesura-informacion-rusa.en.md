Author: Jorge Maldonado Ventura
Category: News
Date: 2022-03-17
Lang: en
Slug: duckduckgo-censura-desinformacion-rusa
Save_as: DuckDuckGo-censors-Russian-disinformation/index.html
URL: DuckDuckGo-censors-Russian-disinformation/
Tags: censorship, information freedom, politics, privacy, Russia, free software
Title: DuckDuckGo censors “Russian disinformation”
Image: <img src="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg" alt="" width="2083" height="1667" srcset="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg 2083w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-1041x833.jpg 1041w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-520x416.jpg 520w" sizes="(max-width: 2083px) 100vw, 2083px">

DuckDuckGo’s CEO [said on Twitter](https://nitter.snopyta.org/yegg/status/1501716484761997318):

> Like so many others I am sickened by Russia’s invasion of Ukraine and
> the gigantic humanitarian crisis it continues to create. #StandWithUkraine️

> At DuckDuckGo, we've been rolling out search updates that down-rank
> sites associated with Russian disinformation.

This decision is problematic for many users who want to decide by
themselves what is misinformation and what is not. That’s the reason why
many people unhappy with DuckDuckGo’s decision who are also privacy and
free software advocates now recommend using search engines like [Brave
Seach](https://search.brave.com/) and [Searx](https://searx.github.io/searx/).
