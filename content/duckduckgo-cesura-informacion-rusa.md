Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-03-13 16:00
Lang: es
Slug: duckduckgo-censura-desinformacion-rusa
Tags: censura, libertad de información, política, privacidad, Rusia, software libre
Title: DuckDuckGo censura «desinformacion rusa»
Image: <img src="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg" alt="" width="2083" height="1667" srcset="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg 2083w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-1041x833.jpg 1041w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-520x416.jpg 520w" sizes="(max-width: 2083px) 100vw, 2083px">

El jefe de DuckDuckGo [afirmó en Twitter](https://nitter.snopyta.org/yegg/status/1501716484761997318):

> Como tantos otros, estoy asqueado por la invasión rusa de Ucrania y la
> gigantesca crisis humanitaria que continúa creando. #StandWithUkraine️
>
> En DuckDuckGo hemos estado implementando actualizaciones de búsqueda
> que bajan el posicionamiento de los sitios web asociados con la desinformación rusa.

Esta decisión de DuckDuckGo es problemática para muchos usuarios que
quieren decidir por sí mismos qué es desinformación y qué no. Es por
ello que muchas personas descontentas
con la decisión de DuckDuckGo y defensoras de la privacidad y del <i
lang="en">software</i> libre están recomendando ahora el uso de
buscadores como [Brave Search](https://search.brave.com/) y
[Searx](https://searx.github.io/searx/).
