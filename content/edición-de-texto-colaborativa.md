Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2017-03-04 16:34
Image: <img src="/wp-content/uploads/2017/03/etherpad-intro.png" alt="Imagen introductoria Etherpad" class="attachment-post-thumbnail wp-post-image" width="1024" height="347" srcset="/wp-content/uploads/2017/03/etherpad-intro.png 1024w, /wp-content/uploads/2017/03/etherpad-intro-512x173.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
Lang: es
Modified: 2017-03-05 21:57
Slug: edicion-de-texto-colaborativa-en-tiempo-real
Tags: colaboración, edición de textos, editores de texto, en línea, Etherpad, Riseup, Tor
Title: Edición de texto colaborativa en tiempo real

Etherpad es un editor de texto en línea personalizable que permite la
edición de texto colaborativa en tiempo real.

<!-- more -->

Puedes [instalar
Etherpad](https://github.com/ether/etherpad-lite#installation) en un
servidor o usar algún servicio en línea que permita usarlo.
[Riseup](https://riseup.net/es/about-us) ofrece un servicio gratuito en
<https://pad.riseup.net/>. Voy a enseñaros lo fácil que es empezar a
usarlo.

Primero debéis crear un *pad* (así llama Etherpad a un documento de
texto colaborativo) o usar un pad que ya exista. Simplemente debemos
suministrar su nombre para crearlo o conectarnos.

<figure>
  <a href="/wp-content/uploads/2017/03/crea-pad-riseup-etherpad.png">
    <img src="/wp-content/uploads/2017/03/crea-pad-riseup-etherpad.png" alt="creando un pad en el servicio de Etherpad de Riseup" class="size-fill" width="1007" height="667" srcset="/wp-content/uploads/2017/03/crea-pad-riseup-etherpad.png 1007w, /wp-content/uploads/2017/03/crea-pad-riseup-etherpad-503x333.png 503w" sizes="(max-width: 1007px) 100vw, 1007px">
  </a>
  <figcaption class="wp-caption-text">Creando o conectándose a un <em>pad</em> en el servicio de Etherpad de Riseup</figcaption>
</figure>

Hay que tener en cuenta que en el servicio de Etherpad de Riseup **todos
los pads se destruyen automáticamente tras 30 días de inactividad**.
Pero no hay problema porque podemos guardar los textos que hayamos
producido en cualquier momento. Para contar con más privaciad, podéis
usar una [red privada
virtual](https://es.wikipedia.org/wiki/Red_privada_virtual) o acceder al
servicio usando <abbr title="The Onion Router">[Tor](https://es.wikipedia.org/wiki/Tor_(red_de_anonimato))</abbr>:
<http://5jp7xtmox6jyoqd5.onion/>.

<figure>
  <a href="/wp-content/uploads/2017/03/etherpad-riseup.png">
    <img src="/wp-content/uploads/2017/03/etherpad-riseup.png" alt="edición de texto colaborativa con Etherpad" class="size-fill" width="1023" height="630" srcset="/wp-content/uploads/2017/03/etherpad-riseup.png 1023w, /wp-content/uploads/2017/03/etherpad-riseup-511x315.png 511w" sizes="(max-width: 1023px) 100vw, 1023px">
  </a>
  <figcaption class="wp-caption-text">La interfaz de Riseup</figcaption>
</figure>

Como podéis ver en la imagen más arriba, la interfaz es muy sencilla. Al
lado de la zona de texto hay un *chat* para comunicarse con las personas
con las que se está realizando la edición colaborativa. Todo el mundo
puede editar y charlar al mismo tiempo. El texto que ha escrito cada
persona se ve con un color distinto para distinguir la autoría, aunque
esto se puede deshabilitar si queremos.

Para ver la historia de edición podemos hacer clic en el icono del
reloj.

<figure>
  <a
  href="/wp-content/uploads/2017/03/etherpad-past.png">
    <img src="/wp-content/uploads/2017/03/etherpad-past.png" alt="historia de edición Etherpad" class="size-fill" width="1022" height="228" srcset="/wp-content/uploads/2017/03/etherpad-past.png 1022w, /wp-content/uploads/2017/03/etherpad-past-511x114.png 511w" sizes="(max-width: 1022px) 100vw, 1022px">
  </a>
<figcaption class="wp-caption-text">Viendo la historia de edición</figcaption>
</figure>

Cuando queramos, podemos exportar el resultado de la edición a otros
formatos y guardarlo en nuestro ordenador. También podemos importar
archivos de texto o archivos en formato HTML.

<a href="/wp-content/uploads/2017/03/etherpad-riseup-export.png">
  <img src="/wp-content/uploads/2017/03/etherpad-riseup-export.png" alt="exportar el texto editado a otros formatos">
</a>
