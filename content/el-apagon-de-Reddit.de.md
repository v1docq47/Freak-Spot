Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2023-06-11 10:04
Lang: de
Slug: deja-de-usar-Reddit
Tags: Reddit, Lemmy, Postmill, Lobsters, /kbin, Privatsphäre, freie Software
Save_as: Schluss-mit-Reddit/index.html
URL: Schluss-mit-Reddit/
Title: Schluss mit Reddit
Image: <img src="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg" alt="" width="2048" height="1024" srcset="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg 2048w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-1024x512.jpg 1024w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-512x256.jpg 512w" sizes="(max-width: 2048px) 100vw, 2048px">

[Tausende von Reddit-Gemeinschaften](https://safereddit.com/r/ModCoord/comments/1401qw5/incomplete_and_growing_list_of_participating/) werden ab morgen nicht mehr
zugänglich sein. Sie protestieren gegen [die Entscheidung, von Apps, die
die API nutzen, Millionen von Dollar zu
verlangen](https://safereddit.com/r/apolloapp/comments/144f6xm/apollo_will_close_down_on_june_30th_reddits/).
Als Folge dieser Politik werden viele Apps nicht mehr funktionieren.

Obwohl einige Leute empfehlen, Reddit nicht mehr zu nutzen, ist der
Protest auf zwei Tage begrenzt. Das Problem bei einem zeitlich
begrenzten Boykott ist, dass er diese Botschaft an die Eigentümer sendet:
viele Leute sind wütend, aber nach zwei Tagen werden sie wiederkommen
und wir werden weiter Geld verdienen. Reddit hat schon vor Jahren
aufgehört, freie Software zu sein, und es wird auch nicht aufhören,
Informationen zu zensieren, die es für unerwünscht hält.

Ich unterstütze den Reddit-Boykott, aber ich denke, er sollte nicht
nur zwei Tage andauern, sondern dauerhaft sein. Es gibt mehrere ähnliche
Programme, die frei sind und die Privatsphäre ihrer
Nutzer respektieren: [Lemmy](https://join-lemmy.org/),
[/kbin](https://kbin.pub/), [Postmill](https://postmill.xyz/),
[Lobsters](https://lobste.rs/), [Tildes](https://tildes.net/)...

[Lemmy](https://join-lemmy.org/) und [/kbin](https://lobste.rs/) sind im
Gegensatz zu Reddit freie und föderierte Programme, d.h. die
Administratoren einer Instanz können Informationen von Instanzen, die
sie nicht kontrollieren, nicht zensieren oder ihnen etwas aufzwingen;
jede Instanz hat seine eigene Politik.
