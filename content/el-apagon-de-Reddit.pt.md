Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-06-11 10:04
Lang: pt
Slug: deja-de-usar-Reddit
Tags: Reddit, Lemmy, Postmill, Lobsters, /kbin, privacidade, software livre
Save_as: deixa-de-utilizar-o-Reddit/index.html
URL: deixa-de-utilizar-o-Reddit/
Title: Deixa de utilizar o Reddit
Image: <img src="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg" alt="" width="2048" height="1024" srcset="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg 2048w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-1024x512.jpg 1024w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-512x256.jpg 512w" sizes="(max-width: 2048px) 100vw, 2048px">

[Milhares de comunidades do
Reddit](https://safereddit.com/r/ModCoord/comments/1401qw5/incomplete_and_growing_list_of_participating/)
vão deixar de estar acessíveis amanhã em protesto [contra a decisão de
cobrar milhões de dólares às aplicações que utilizam a API](https://safereddit.com/r/apolloapp/comments/144f6xm/apollo_will_close_down_on_june_30th_reddits/). Como
resultado desta política, muitas aplicações vão deixar de funcionar.

No entanto, embora algumas pessoas estejam a recomendar que se deixe de
usar o Reddit, o protesto está limitado a dois dias. O problema de fazer
um boicote temporário é que envia esta mensagem aos proprietários:
muitas pessoas estão zangadas, mas passados dois dias vão voltar e nós
vamos continuar a ganhar dinheiro. O Reddit deixou de ser <i lang="en">software</i> livre
há anos e não vai deixar de censurar informação de que não gosta.

Apoio o boicote ao Reddit, mas acho que não deve durar apenas dois dias;
deve ser permanente. Existem vários programas como o Reddit que são
livres e respeitam a privacidade dos seus utilizadores: [Lemmy](https://join-lemmy.org/), [/kbin](https://kbin.pub/),
[Postmill](https://postmill.xyz/), [Lobsters](https://lobste.rs/),
[Tildes](https://tildes.net/)...

[O Lemmy](https://join-lemmy.org/) e [o /kbin](https://kbin.pub/), ao
contrário do Reddit, são livres e federados, pelo que os administradores
de uma instância não podem censurar informação de instâncias que não
controlam nem impor-lhes nada; cada instância tem a sua própria política.
