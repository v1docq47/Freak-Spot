Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-04-03 22:20
Lang: eo
Slug: el-fediverso-puede-ser-bastante-tóxico
Save_as: la-fediverso-povas-esti-tre-toksika/index.html
URL: la-fediverso-povas-esti-tre-toksika/
Tags: Fediverso, Mastodon, Pleroma
Title: La Fediverso povas esti tre toksika

Mastodon, inspirita de GNU social, kaj Pleroma estas la plej popularaj
konsistaĵoj de tio, kion ni hodiaŭ konas kiel «Fediverson». Ili ĉiuj
estas esence interligitaj liberaj klonoj de Twitter, kiuj rilate
funkcias unu kun la alia per ActivityPub-protokolo.

En multaj aspektoj la Fediverso estas liberiga forto por la bono. Ĝia
federacia dezajno distribuas la administradon kaj kostojn inter multaj
sendependaj estaĵoj, io kion mi rigardas kiel tre potencan dezajnan
elekton. Ĝiaj kontroladaj iloj ankaŭ faras bonan laboron por teni
novnaziojn for de viaj fluoj kaj provizi komfortan ejon por esprimi
viajn pensojn, speciale se via esprimmaniero estas kalumniita de la
socio. Grandaj aroj de fediversaj anoj trovis en ĝi hejmo por sin
esprimi, kio estas negita al ili alie pro ilia sekso, socia sekso aŭ
aliaj ecoj. Ĝi ankaŭ esence mankas komerca propagando.

Sed ĝi daŭre estas klono de Twitter, kaj multaj el la sociaj kaj
psikologiaj malbonoj, kiujn ĝi havas, estas en la Fediverso. Ĝi estas fluo
de hazardaj pensoj de aliuloj, ofte nefiltrita, kiuj estas montritaj al
vi sen juĝo de valoroj &mdash; eĉ kiam valorjuĝo povus esti saĝa.
Funkcioj kiel diskonigi aŭ «ŝati» afiŝojn, postiri nombrojn de sekvantoj
kaj etajn influantojn, ĉi tiuj aferoj altigas la nivelon de dopamino,
kiel iu ajn socia retejo faras. La pliigita signolimo ne vere helpas; la
plejmulto de afiŝoj estas tre mallongaj kaj neniu volas legi eseon
agreseme plenigitan per vortoj en mallarĝa kolumno.

La Fediverso estas medio optimigita por akraj diskutoj. La diskutoj en
ĉi tiu medio okazas sub ĉi tiuj limigoj, publike, kun la malgranda
nombro de sekvantoj de ambaŭ flankoj enirante kaj elirante por
plifortigi iliajn opiniojn kaj ataki la kontraŭulojn. Progreso estas
mezurata per gajnoj de ideologia teritorio kaj per kreskantaj kaj
malkreskantaj amasoj de partoprenantoj, kiuj komentas ĉie en la
grandegaj fadenoj. Vi ne nur argumentas vian opinion, sed vi ankaŭ
prezentas ĝin al via publiko kaj al la publiko de via kontraŭulo.

La sociaj retejoj ne estas bonaj por vi. La Fediverso eltiris la plej
malbonon el mi kaj ĝi povas ankaŭ eltiri la plej malbonon el vi. La
kondutoj, kiujn ĝi instigas, estas klare difinitaj kiel ĉikano, konduto
kiu ne estas unika de iu ideologia kondiĉo. Homoj vundiĝas en la
Fediverso. Memoru tion. Konsideru rigardi la spegulon kaj demandi vin,
ĉu via rilato kun la platformo estas sana por vi kaj por la homoj ĉirkaŭ
vi.

*Ĉi tiu artikolo estas traduko de la artikolo
«[The Fediverse can be pretty toxic](https://drewdevault.com/2022/07/09/Fediverse-toxicity.html)»
publikigita de Drew Devault sub la [CC BY-SA
2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.es)-permesilo.*
