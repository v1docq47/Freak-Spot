Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2020-03-01
Lang: es
Slug: el-impacto-de-Internet-en-la-tierra
Tags: anuncios, contaminación, criptomoneda, Internet, medio ambiente, vídeos
Title: Internet contamina, úsalo de forma consciente
Image: <img src="/wp-content/uploads/2020/03/server_farm.jpg" alt="" width="1024" height="678" srcset="/wp-content/uploads/2020/03/server_farm.jpg 1024w, /wp-content/uploads/2020/03/server_farm-512x339.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

Internet contamina, y mucho. El coste energético de toda la
infraestructura de Internet es muy grande. Es difícil contabilizar el
consumo energético y el impacto ambiental de Internet de forma precisa.
Según leí el año pasado, el 10% de la electricidad global se destina a
Internet[^1].

¿Cuáles son los usos más contaminantes de Internet? ¿Cómo usar
Internet de forma más responsable? Si bien mantener una reunión por
Internet es menos contaminante que coger un avión y cosas por el
estilo, hay usos de Internet muy ineficientes y contaminantes.

<!-- more -->

## La descarga continua o *streaming*

<a href="/wp-content/uploads/2020/03/reproducción-de-vídeo-en-Internet.png">
<img src="/wp-content/uploads/2020/03/reproducción-de-vídeo-en-Internet.png" alt="" width="789" height="443" srcset="/wp-content/uploads/2020/03/reproducción-de-vídeo-en-Internet.png 789w, /wp-content/uploads/2020/03/reproducción-de-vídeo-en-Internet-394x221.png 394w" sizes="(max-width: 789px) 100vw, 789px">
</a>

La mayor parte del tráfico de Internet viene de vídeos. Reproducir
vídeos requiere el envío continuo de mucha información; además, su
tamaño hace que se necesite mucho almacenamiento, lo que se traduce en
más servidores que hay que alimentar con electricidad.

Lo ideal sería descargar solo una vez algo que vas a consultar varias
veces. Sin embargo, las leyes anticopia y las restricciones técnicas
de muchas plataformas (como Netflix y Spotify) lo dificultan. Por ello
recomiendo boicotear a esas empresas y preferir el contenido con
licencias libres.

## Criptomonedas

<a href="/wp-content/uploads/2020/03/símbolo-de-Bitcoin-con-unos-y-ceros.jpg">
<img src="/wp-content/uploads/2020/03/símbolo-de-Bitcoin-con-unos-y-ceros.jpg" alt="Símbolo de Bitcoin con unos y ceros" width="1024" height="580" srcset="/wp-content/uploads/2020/03/símbolo-de-Bitcoin-con-unos-y-ceros.jpg 1024w, /wp-content/uploads/2020/03/símbolo-de-Bitcoin-con-unos-y-ceros-512x290.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>

Hay monedas virtuales como Bitcoin que usan algoritmos que consumen
muchísima electricidad[^2].

Por otro lado, existen criptomonedas con un bajo consumo energético como
[FairCoin](https://faircoin.world/). Es aun más sostenible, y resiliente
frente a cortes eléctricos, el uso de [monedas locales](https://es.wikipedia.org/wiki/Moneda_local).

## Anuncios

<a href="/wp-content/uploads/2020/03/el-consumo-te-consume.jpg">
<img src="/wp-content/uploads/2020/03/el-consumo-te-consume.jpg" alt="El consumo te consume" width="1024" height="768" srcset="/wp-content/uploads/2020/03/el-consumo-te-consume.jpg 1024w, /wp-content/uploads/2020/03/el-consumo-te-consume-512x384.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>

Ni que decir tiene que los anuncios fomentan un comportamiento
consumista. Encima la industria de la publicidad consume muchísima
electricidad en Internet. Por culpa de este modelo de financiación, las
páginas tardan más en cargar y la Red está repleta de anuncios y
elementos de rastreo que recopilan datos de potenciales consumidores. La
infraestructura de recopilación de datos que crea la industria de la
publicidad luego la usan los estados para controlar a la población y
para identificar y reprimir a disidentes.

Hay formas de
bloquear anuncios y elementos de rastreo mediante el uso de extensiones
del navegador como [Ublock Origin](https://es.wikipedia.org/wiki/UBlock_Origin) o [desactivando
JavaScript](/desahabilitar-javascript-comodamente-firefox-y-derivados/).

## Conclusión

Quienes dicen que Internet o una parte de este es una nube son
imbéciles; Internet son ordenadores (fabricados con algunos materiales
no renovables), cables, antenas, electricidad, obreros... Usa con
moderación esta infraestructura.

[^1]: [Internet drar 10% av världens elanvändning - och andelen stiger](https://cornucopia.cornubot.se/2019/02/internet-drar-10-av-varldens.html?m=1)
[^2]: [Minar bitcoins ya consume más electricidad que todos los habitantes de Suiza al año](https://www.elconfidencial.com/mercados/2019-07-05/bitcoin-consume-electricidad-ano-toda-suiza_2108067/)
