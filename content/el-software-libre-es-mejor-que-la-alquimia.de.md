Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2022-12-31 14:50
Lang: de
Slug: el-software-libre-es-mejor-que-la-alquimia
Save_as: Freie-Software-ist-besser-als-Alchemie/index.html
URL: Freie-Software-ist-besser-als-Alchemie/
Tags: freie Software, Vorteile
Title: Freie Software ist besser als Alchemie

Ist es schwierig, Menschen, die sich nicht mit Computern auskennen, die
Vorteile freier Software zu erklären? So wie man kein Journalist sein
muss, um die Vorteile der Pressefreiheit zu verstehen, muss man auch
kein Programmierer sein, um die Vorteile freier Software zu erkennen.

<!-- more -->

Freie Software garantiert den Nutzern vier wesentliche Freiheiten:

1. Die Freiheit, das Programm nach eigenen Wünschen und zu jedem Zweck auszuführen.
2. Die Freiheit, die Funktionsweise des Programms zu untersuchen und zu ändern, so dass es die Datenverarbeitung nach eigenen Wünschen durchführt. Voraussetzung dafür ist der Zugriff auf den Quellcode.
3. Die Freiheit, Kopien weiterzuverteilen, damit man anderen helfen kann.
4. Die Freiheit, das Programm zu verbessern und die Verbesserungen zu veröffentlichen, so dass die gesamte Gemeinschaft davon profitiert. Voraussetzung dafür ist der Zugriff auf den Quellcode.

Indem sie den Nutzern diese Freiheiten garantiert, reduziert freie
Software die Möglichkeit der Beherrschung des Nutzers durch die
Programmschöpfer, wenn sie diese nicht sogar ausschließt. Im
Wesentlichen geht es darum, zu verhindern, dass jemand die Unkenntnis
der Nutzer ausnutzt, um er zu kontrollieren oder anderweitig
auszunutzen.

Das Grundkonzept zum Verständnis freier Software ist die Freiheit. Mit
freier Software haben die Nutzer die Freiheit und die Kontrolle, mit
ihrem Computer zu tun, was sie wollen, es sei denn, sie berauben jemand
anderen einer der vier Grundfreiheiten.

Freie Software hat aufgrund ihrer Eigenschaften Vorteile, die sich auf
viele Bereiche übertragen lassen:

- **Ökologisch**. Freie Software ist vollständig wiederverwendbar, sie
  ist nicht mitschuldig an geplantem Obsoleszenz und
  unkontrolliertem Konsum.
- **Wirtschaftlich**. Freie Software ist sehr billig, meistens sogar
  kostenlos, weil sie immer wieder verwendet werden kann. Normalerweise
  wird sie für die Programmierung sehr spezifischer Funktionalitäten
  oder für technischen Service bezahlt.
- **Bildung**. Es ist unmöglich zu lernen, wie ein Programm wirklich
  funktioniert, ohne Zugang zum Quellcode zu haben.
- **Ethisch**. Sie ist eine Antwort auf die Forderung, dass alles Wissen
  frei sein sollte und keiner Form von Zensur oder Beschränkung
  unterworfen werden darf.
- **Politisch**. Die Kontrolle der Bevölkerung ist bei freier Software
  sehr schwierig, denn da der Quellcode zugänglich ist, kann jeder
  bösartige Funktionen entdecken.
- **Privat**. Der Datenschutz ist bei freier Software besser
  gewährleistet. Massenspionage durch versteckte Funktionalitäten in
  Programmen ist nicht möglich, da der Code überprüfbar ist.
- ...

Freie Software ist empirisch und folgt einer wissenschaftlichen
Arbeitsmethode. Proprietäre Software hingegen beruht auf Obskurantismus,
auf dem Glauben oder auf dem guten Willen oder Können ihrer Schöpfer. Im
Grunde ist es so, als würde man die Arbeitsmethoden und Überzeugungen
der Alchemie mit denen der Wissenschaft vergleichen.

