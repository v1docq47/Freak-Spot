Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2022-12-31 12:20
Lang: en
Slug: el-software-libre-es-mejor-que-la-alquimia
Save_as: free-software-is-better-than-alchemy/index.html
URL: free-software-is-better-than-alchemy/
Tags: free software, advantages
Title: Free software is better than alchemy

Is it difficult to explain the benefits of free software to people who
don't understand computers? Just as you don't have to be a journalist to
understand the benefits of the freedom of the press, you don't have to
be a programmer to understand the benefits of free software.

<!-- more -->

Free software guarantees four essential freedoms to users:

1. The freedom to run the program as you wish, for any purpose.
2. The freedom to study how the program works, and change it so it does  it to one's own needs. Access to the source code is a prerequisite for this.
3. The freedom to redistribute copies so you can help others.
4. The freedom to improve the program and to publish the improvements,
   so that the whole community benefits. Access to the source code is a
   prerequisite for this.

By guaranteeing these freedoms to users, free software reduces, if not
eliminates, any possibility of domination of the program's creators over
the user. In essence, it is about preventing anyone from taking
advantage of the users' lack of knowledge to try to control or otherwise
take advantage of them.

The basic concept for understanding free software is freedom. With free
software, users have the freedom and control to do whatever they please
with their computer, except to deprive someone else of one of the four
basic freedoms.

Free software, because of its characteristics, has advantages that can
be transferred to many areas:

- **Ecological**. Free software is completely reusable, it is not
  complicit in programmed obsolescence and uncontrolled consumption.
- **Economical**. Free software is very cheap, most of the time free,
  because it can always be reused. Normally it is usually paid for the
  programming of very specific functionalities or for technical service.
- **Educational**. It is impossible to learn how a program really works
  without access to the source code.
- **Ethical**. It responds to the claim that all knowledge should be
  free and not subject to any form of censorship or restriction.
- **Political**. Control of the population is very difficult with free
  software, since, as the source code is accessible, anyone can detect
  malicious functionalities.
- **Private**. Privacy is a greater guarantee with free software. Mass
  espionage through hidden functionalities in programs is not possible,
  as the code is auditable.
- ...

Free software is empirical and follows a scientific working method.
Proprietary software, on the other hand, is based on obscurantism, on
faith or on the goodwill or skill of its creators. Basically, it is like
comparing the working methods and beliefs of alchemy with those of
science.
