Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-01-02 10:30
Lang: eo
Slug: el-software-libre-es-mejor-que-la-alquimia
Save_as: liberaj-programoj-estas-pli-bonaj-ol-alkemio/index.html
URL: liberaj-programoj-estas-pli-bonaj-ol-alkemio/
Tags: libera programaro, avantaĝoj
Title: Liberaj programoj estas pli bonaj ol alkemio

Ĉu malfacilas klarigi la avantaĝojn de liberaj programoj al homoj, kiuj
ne komprenas pri komputiloj? Same kiel oni ne devas esti ĵurnalisto por
kompreni la profitojn de la gazetarlibereco, oni ne devas esti
programisto por kompreni la profitojn de la liberaj programoj.

<!-- more -->

La libera programaro garantias al la uzantoj kvar esencajn liberecojn:

1. La liberecon plenumi la programon kiel vi volas, por ĉia celo.
2. La liberecon studi kiel la programo funkcias kaj adapti ĝin al viaj propraj necesoj. La aliro al la fontkodo estas antaŭkondiĉo por ĉi tio.
3. La liberecon redistribui kopiojn por helpi aliulojn.
4. La liberecon plibonigi la programon kaj publikigi la plibonigojn, por
 ke la tuta komunumo profitu. La aliro al la fontkodo estas antaŭkondiĉo por ĉi tio.

Garantiante ĉi tiujn liberecojn al la uzantoj la libera programaro
malpliigas, aŭ eĉ forigas, ĉiun eblecon de regado de la programkreintoj
super la uzantoj. Finfine temas pri eviti, ke iu utiligu la nescion de
la uzanto por klopodi kontroli ilin aŭ profiti de ili iel ajn.

La ĉefa koncepto por kompreni la liberan programaron estas la libereco.
Per la libera programaro la uzantoj havas la liberecon kaj la kontrolon
fari kion ajn ili volas kun sia komputilo, krom senigi alian homon je iu
el la kvar bazaj liberecoj.

La libera programaro, pro ĝiaj ecoj, havas avantaĝojn, kiujn oni povas
profiti en multaj branĉoj:

- **Ekologia**. La liberaj programoj estas tute reutiligeblaj, ne estas
  komplico de la planita malnoviĝo kaj troa konsumado.
- **Ekonomia**. La libera programaro estas tre malmultekosta, preskaŭ
  ĉiam senkosta, ĉar oni ĉiam povas reutiligi ĝin. Kutime oni pagas por la
  programado de tre specifaj funkcioj aŭ por teknika servo.
- **Edukada**. Ne eblas lerni kiel programo vere funkcias, se vi ne
  aliras al la fontkodo.
- **Etika**. Ĝi estas la respondo al la postulo, ke ĉiu la scio devas
  esti libera kaj ne esti iel cenzurita aŭ limigita.
- **Politika**. La kontrolo de la loĝantaro tre malfacilas per liberaj
  programoj, ĉar ĉiu ajn povas malkovri la fifunkciojn, ĉar la fontkodo
  estas alirebla.
- **Privata**. Privateco estas pli granda garantio kun libera programaro. Amasa spionado per kaŝitaj funkcioj en programoj ne eblas, ĉar la kodo estas kontrolebla.
- ...

La libera programaro estas empiria kaj sekvas sciencan labormetodon.
Aliflanke la proprieta programaro baziĝas sur la obskurantismo, fido aŭ
la bonvolo aŭ kapablo de ĝiaj kreintoj. Esence ĝi estas kiel kompari la
labormetodojn kaj kredojn de alkemio kun tiuj de scienco.

