Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2017-06-07 23:41
Lang: es
Slug: el-software-libre-es-mejor-que-la-alquimia
Tags: software libre, ventajas
Title: El <i lang="en">software</i> libre es mejor que la alquimia

¿Resulta difícil explicar las ventajas del <i lang="en">software</i> libre a personas que
no entienden de ordenadores? Del mismo modo que no hay que ser
periodista para entender los beneficios de la libertad de prensa, no hay
que ser programador para entender los beneficios del <i lang="en">software</i> libre.

<!-- more -->

El <i lang="en">software</i> libre garantiza cuatro libertades esenciales a las usuarios:

1. La libertad de ejecutar el programa como uno desee, con cualquier
   propósito.
2. La libertad de estudiar el funcionamiento del programa y adaptarlo a
   las propias necesidades. El acceso al código fuente es un
   prerrequisito para esto.
3. La libertad de redistribuir copias para ayudar a los demás.
4. La libertad de mejorar el programa y de publicar las mejoras, de modo
   que toda la comunidad se beneficie. El acceso al código
   fuente es un prerrequisito para esto.

Al garantizar estas libertades a las usuarias, el <i lang="en">software</i> libre reduce,
si no elimina, toda posibilidad de dominación de los creadores del
programa sobre el usuario. En el fondo se trata de impedir que nadie se
aproveche del desconocimiento de las usuarios para tratar de
controlarlos o aprovecharse de ellos de cualquier otra forma.

El concepto básico para entender el <i lang="en">software</i> libre es la libertad. Con
el <i lang="en">software</i> libre los usuarios tienen la libertad y el control de hacer
lo que les plazca con su computación, excepto privar de alguna de las
cuatro libertades básicas a otra persona.

El <i lang="en">software</i> libre, por sus características, goza de ventajas que se
pueden trasladar a muchos ámbitos:

- **Ecológico**. El <i lang="en">software</i> libre es completamente reutilizable, no es
  cómplice de la obsolescencia programada y del consumo descontrolado.
- **Ecónomico**. El <i lang="en">software</i> libre es muy barato, la mayoría de las
  veces gratuito, pues siempre se puede reutilizar. Normalmente se suele
  pagar por la programación de funcionalidades muy concretas o por
  servicio técnico.
- **Educativo**. Es imposible aprender cómo funciona realmente un
  programa si no se accede al código fuente.
- **Ético**. Responde a la reclama de que todo el conocimiento debe ser
  libre y no estar sujeto a ninguna forma de censura o restricción.
- **Político**. El control de la población es muy difícil con el
  <i lang="en">software</i> libre, pues, al ser accesible el código fuente, cualquier
  persona puede detectar las funcionalidades maliciosas.
- **Privado**. La privacidad es una mayor garantía con el <i lang="en">software</i>
  libre. El espionaje masivo mediante funcionalidades ocultas en
  programas no es posible, al ser el código auditable.
- ...

El <i lang="en">software</i> libre es empírico y sigue un método de trabajo científico.
Por otro lado, el <i lang="en">software</i> privativo se basa en el oscurantismo, en la
fe o en la buena voluntad o habilidad de sus creadores. Básicamente, es
como comparar los métodos de trabajo y las creencias de la alquimia con
los de la ciencia.
