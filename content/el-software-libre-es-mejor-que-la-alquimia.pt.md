Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-01-02 16:00
Lang: pt
Slug: el-software-libre-es-mejor-que-la-alquimia
Save_as: o-software-livre-é-melhor-do-que-a-alquimia/index.html
URL: o-software-livre-é-melhor-do-que-a-alquimia/
Tags: software livre, vantagens
Title: O <i lang="en">software</i> livre é melhor do que a alquimia

É difícil explicar os benefícios do <i lang="en">software</i> livre a
pessoas que não compreendem computadores? Tal como não é preciso ser
jornalista para compreender os benefícios da liberdade de imprensa, não
é preciso ser programador para compreender os benefícios do
<i lang="en">software</i> livre.

<!-- more -->

O <i lang="en">software</i> livre garante quatro liberdades essenciais aos utilizadores:

1. A liberdade de executar o programa como quiser, para qualquer propósito.
2. A liberdade de estudar como funciona o programa e adaptá-lo às tuas
   próprias necessidades. O acesso ao código fonte é um pré-requisito
   para tal.
3. A liberdade de redistribuir cópias para ajudar os outros.
4. A liberdade de melhorar o programa e de publicar as melhorias, de
   modo a que toda a comunidade beneficie. O acesso ao código fonte é um
   pré-requisito para tal.

Ao garantir estas liberdades aos utilizadores, o <i lang="en">software</i> livre reduz,
ou elimina, qualquer possibilidade de domínio pelos criadores do
programa sobre o utilizador. Em essência, trata-se de impedir que alguém
se aproveite da falta de conhecimento dos utilizadores para tentar
controlá-los ou tirar partido deles de qualquer outra forma.

O conceito básico para compreender o <i lang="en">software</i> livre é a
liberdade. Com <i lang="en">software</i> livre, os utilizadores têm a liberdade e o
controlo para fazer o que quiserem com o seu computador, excepto para
privar outra pessoa de uma das quatro liberdades básicas.

O <i lang="en">software</i> livre, devido às suas características, tem
vantagens que podem ser transferidas para muitas áreas:

- **Ecologia**. O <i lang="en">software</i> livre é completamente reutilizável, não é
  cúmplice da obsolescência programada e do consumo descontrolado.
- **Economia**. O <i lang="en">software</i> livre é muito barato, quase
  sempre gratuito, porque pode sempre ser reutilizado.
  Normalmente é pago pela programação de funcionalidades muito
  específicas ou pelo serviço técnico.
- **Educação**. É impossível aprender como funciona realmente um
  programa sem acesso ao código fonte.
- **Ética**. Responde à reivindicação de que todo o conhecimento deve ser
  livre e não estar sujeito a qualquer forma de censura ou restrição.
- **Política**. O controlo da população é muito difícil com <i lang="en">software</i>
  livre, uma vez que, como o código fonte é acessível, qualquer pessoa
  pode detectar funcionalidades maliciosas.
- **Privacidade**. A privacidade é uma maior garantia com <i lang="en">software</i> livre. A
  espionagem em massa através de funcionalidades ocultas em programas
  não é possível, uma vez que o código é auditável.
- ...

O <i lang="en">software</i> livre é empírico e segue um método de
trabalho científico. O <i lang="en">software</i> proprietário, por outro lado, baseia-se
no obscurantismo, na fé ou na boa vontade ou habilidade dos seus
criadores. Basicamente é como comparar os métodos de trabalho e crenças
da alquimia com os da ciência.
