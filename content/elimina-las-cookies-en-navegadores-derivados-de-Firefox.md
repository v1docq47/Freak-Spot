Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2018-03-23 23:47
Lang: es
Modified: 2023-02-26 20:18
Slug: elimina-las-cookies-en-navegadores-derivados-de-firefox
Tags: cookies, galletas, navegador, páginas web, privacidad
Title: Elimina las cookies en navegadores derivados de Firefox

Las *cookies* o galletas informáticas son usadas en gran medida como
elemento de rastreo por empresas relacionadas con la publicidad en
Internet o la recogida y análisis masivo de datos.

Para proteger la privacidad es recomendable eliminar las galletas tras
abandonar los sitios web. A pesar de que esto supone algunos problemas
menores, pues es necesario volver a introducir credenciales si se trata
de una web en que se inicia sesión, es una buena forma de protegerse
contra el rastreo. Esto es lo que hace Tor Browser por defecto.

Sin embargo, si nosotros conocemos sitios web que son de confianza, no
tiene sentido eliminar esas galletas. Lo mejor sería eliminarlas por
defecto, excepto en los sitios web de confianza. O puede que para
algunas personas eliminar todas las galletas sea excesivo, ya que
para esas personas sería un mejor compromiso eliminar solo las galletas
de terceros.

En cualquier caso, podemos establecer las reglas para eliminar galletas
a nuestra medida con la extensión para Firefox (y navegadores derivados
de Firefox)
[Cookie AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/).

<!-- more -->

Por defecto, la extensión utiliza una configuración bastante permisiva.
Para acceder a las opciones basta pulsar el icono de la extensión.

<a href="/wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete.png">
<img src="/wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete.png" alt="" width="799" height="444" srcset="/wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete.png 799w, /wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete-399x222.png 399w" sizes="(max-width: 799px) 100vw, 799px">
</a>

Para mi gusto es importante establecer **Limpieza automática habilitada**,
así no hace falta eliminar todas las galletas manualmente, pulsando en
el botón **Limpiar**. En la imagen anterior se pueden observar dos
sugerencias de reglas y dos botones: **Lista gris** y **Lista blanca**.
Las reglas se crean usando sencillas expresiones regulares. La expresión
`freakspot.net` elimina las galletas de <https://freakspot.net>, y
`*.freakspot.net` elimina las galletas de <https://freakspot.net> y sus
subdominios. La *lista gris* indica que las galletas no se borran
hasta que se reinicie el navegador, esto es útil para sitios web en los
que se quiere estar con la sesión iniciada y limpiar las galletas al
reiniciar el navegador. La *lista blanca* impide que se eliminen las
galletas.

Para ajustar otras opciones de configuración, podemos pulsar el botón
**Configuración**. Al lado de cada opción hay un símbolo de
interrogación que nos lleva a una página de documentación con el
significado de dicha opción. En la fecha en que escribo este artículo,
dicha documentación solo está en inglés.

<a href="/wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete.png">
<img src="/wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete.png" alt="" width="1375" height="780" srcset="/wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete.png 1375w, /wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete-687x390.png 687w" sizes="(max-width: 1375px) 100vw, 1375px">
</a>

En el menú lateral que vemos desde la página de configuración, podemos
acceder a la **Lista de expresiones**, para editar todas las reglas que
hemos definido, así como exportar e importar las reglas para
reutilizarlas en otro navegador.

<a href="/wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete.png">
<img src="/wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete.png" alt="" width="1375" height="992" srcset="/wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete.png 1375w, /wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete-687x496.png 687w" sizes="(max-width: 1375px) 100vw, 1375px">
</a>
