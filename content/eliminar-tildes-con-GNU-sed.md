Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2022-12-31 16:00
Lang: es
Slug: eliminar-tildes-con-GNU-sed
Tags: iconv, GNU/Linux, tildes, sed
Title: Eliminar tildes con <code>sed</code> o <code>iconv</code> (GNU/Linux)

A veces resulta útil eliminar las tildes de un texto o palabras. Para
ello podemos utilizar `sed`, que suele estar ya instalado en GNU/Linux.

Basta crear un archivo como el siguiente...

    :::bash

    #!/bin/sed -f

    # Este programa elimina las tildes.
    #
    # Ejemplo de uso
    # $ echo 'Bajó del árbol.' | ./eliminar-tildes.sed

    # En mayúscula
    s/Á/A/g
    s/É/E/g
    s/Í/I/g
    s/Ó/O/g
    s/Ú/U/g

    #s/Ñ/N/g     # Descomenta para sustituir la eñe

    # En minúscula
    s/á/a/g
    s/é/e/g
    s/í/i/g
    s/ó/o/g
    s/ú/u/g

    #s/ñ/n/g     # Descomenta para sustituir la eñe

Darle permisos de ejecución (`sudo chmod u+x eliminar-tildes.sed`) y,
luego, ejecutar algo como `echo 'Bajó del árbol' |
./eliminar-tildes.sed`, que le quitará las tildes a la frase, quedando
así: «Bajo del arbol». Podemos pasarle cualquier texto al programa
mediante una tubería. Así pues, `cat texto.txt | ./eliminar-tildes.sed`
le quitaría las tildes al texto del archivo `texto.txt`.

Mi motivación para crear el programa era quitarles las tildes a los
textos para poner un ejercicio de aprendizaje de español que consiste en
acentuar palabras. Si lo haces por otro motivo, es posible que te
interese también quitarle la virgulilla a la eñe. En ese caso,
descomenta las líneas donde hago la sustitución de la eñe.

Otra opción es convertir el texto a ASCII haciendo una transliteración
con el programa `iconv`, que también suele estar instalado en GNU/Linux.
Así, para convertir un texto a ASCII bastaría con ejecutar algo como
`iconv -f utf-8 -t ascii//translit texto.txt` o `echo 'árbol y eñe' |
iconv -f utf-8 -t ascii//translit`.

