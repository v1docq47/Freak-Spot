Author: Jorge Maldonado Ventura
Category: Edição de textos
Date: 2022-12-31 17:00
Lang: pt
Modified: 2023-01-23 10:11
Slug: eliminar-tildes-con-GNU-sed
Save_as: eliminar-acentos-com-o-sed-ou-iconv/index.html
URL: eliminar-acentos-com-o-sed-ou-iconv/
Tags: iconv, GNU/Linux, acentos, sed
Title: Remover acentos com o <code>sed</code> ou o <code>iconv</code> (GNU/Linux)

Por vezes é útil remover acentos de um texto ou de palavras. Para tal podemos utilizar o `sed`, que normalmente já está instalado no GNU/Linux.

Basta criar um ficheiro como o seguinte...

    :::bash

    #!/bin/sed -f

    # Este programa remove os acentos, funciona tanto para espanhol como para
    # português.
    #
    # Exemplo de utilização
    # $ echo 'Eles também têm tempo.' | ./remover-acentos.sed

    # Em maiúsculas
    s/Á/A/g
    s/É/E/g
    s/Í/I/g
    s/Ó/O/g
    s/Ú/U/g

    s/À/A/g

    s/Ã/A/g
    s/Õ/O/g

    s/Â/A/g
    s/Ê/E/g
    s/Ô/O/g


    # Em minúsculas
    s/á/a/g
    s/é/e/g
    s/í/i/g
    s/ó/o/g
    s/ú/u/g

    s/à/a/g

    s/ã/a/g
    s/õ/o/g

    s/â/a/g
    s/ê/e/g
    s/ô/o/g

Dar-lhe permissão para executar (`sudo chmod u+x remover-acentos.sed`)
e, depois, executar algo como `echo 'Eles também têm tempo' |
./remover-acentos.sed`, que retirará os acentos da frase, deixando-a
assim: «Eles tambem tem tempo». Podemos passar qualquer texto para o
programa através de um tubo. Assim, `cat texto.txt |
./remover-acentos.sed` removeria os acentos do texto no ficheiro
`texto.txt`.

Outra opção é converter o texto para ASCII por transliteração com o
programa `iconv`, que também está normalmente instalado no GNU/Linux.
Assim, para converter um texto para ASCII, simplesmente tens de executar
algo como `iconv -f utf-8 -t ascii//translit texto.txt` ou
`echo 'Não é uma árvore' | iconv -f utf-8 -t ascii//translit`.

