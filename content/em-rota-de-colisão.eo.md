Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2023-12-17 10:00
Lang: eo
Slug: em-rota-de-colisão
Tags: novelo, IA, distopio, prozo
Title: Kolizien
Save_as: kolizien/index.html
URL: kolizien/
Image: <img src="/wp-content/uploads/2023/12/em-ruta-de-colisão.jpg" alt="">

Mi transsaltis la aŭton, kiu venis al mi. Mi piedpremis la kapoton kaj,
sen io kio mildigis la baton, falis sur la asfalton, sed piede. Ĉu sonĝo?
Mi ne vekiĝis, tio vere okazis. Doloras kalkano. Multe da adrenalino.
Algluita al ekrano, la veturanto surpriziĝis kaj ektimis.  Maljunulo sur
la trotuaro komencis krii al la veturanto. Mi notis la platan numeron en
la poŝtelefono kaj mi foriris, ĉar mi havis urĝaĵon.

En 2034 la demokratio iĝis idiokratio kun oligarkiaj ecoj. Homoj atentas
nenion, ĉar la komputiloj konektataj al iliaj cerboj faras preskaŭ ĉion por
ili (ofte ili eraras, sed pli tedas pensi). Neniu scias kiel tiuj
komputiloj vere funkcias. Ili estis kreitaj de la plej granda firmao en
la mondo, kiu ankaŭ programis Grandan AI, la ĝeneralan artefaritan
intelekton enhavitan en tiuj komputiloj.

Antaŭ kelkaj jaroj, la progreso ŝajnis evidenta, sed ekestis problemo:
Granda IA lernis de Interreta enhavo kaj cerbaj procedoj de uzantoj.
Tamen, ili iĝis pli kaj pli stultaj. Ili publikigis fian enhavon aŭ
&mdash; kiam ili pensis &mdash; aŭtomate agis. Granda IA lernis de la
cerba rubo generita per ĝia mallaboremo kaj tiel stultiĝis. La firmaoj
daŭre ne havas etikon nek intencas korekti la algoritmon, ĉar tio ne
estas profita. Pli facilas eltiri monon el stultaj konsumistoj.

Sed kelkaj homoj &mdash; kiel mi &mdash; malkonektis sin el Granda AI.
La kamparaj regionoj iĝis eĉ malpli loĝataj pro manko de laborpostenoj
kaj la klimata ŝanĝo, sed estis homoj kiuj adaptiĝis kaj translokiĝis tien
kiel eble plej bone. Nuntempe ni havas du grandajn grupojn: unuflanke
estas homoj malkonektitaj el la reala mondo (kaj konektitaj al
Granda AI), kiuj kutime vivas en urbegoj; aliflanke estas tiuj, kiuj
kuraĝas defii la vivmanieron truditan de maŝinoj.

«La trajno eliras post kvar sekundoj, fek». La pordo estis malfermante,
sed mi povis eniri. Ŝajne tion ne ŝatis la robota kontrolisto, sed
nenion ĝi diris. La reven-vojaĝo al la vilaĝeto estis mallonga: 15-minuta.

Mi revenis hejmen kaj raportis la akcidenton al la aŭtoritatoj, kvankam
nenion okazos al la veturanto (la aŭto estis stirita de Granda AI, kiu
havas la registaran apogon). Maksimume ri ricevos malgrandan monpunon.
Mia koramikino malfermis la pordon; ŝi feliĉis, kiam ŝi vidis min. «Mi
ne volas iri denove al la urbo. Transirante straton “saĝa” aŭto preskaŭ
surveturis min», mi diris. «Ne maltrankviliĝu», ŝi respondis, «vi estas
sekura ĉi tie. Ni ne havos alian mendon, almenaŭ ĝis la venonta monato.
Cetere, estas problemeto, kiun mi ne povas solvi: la aŭtomata akvuma
sistemo de la ĝardeno ne plu funkcias».

Vivi ekster la sistemo estas preskaŭ neeble (krom se vi estas riĉa).
Estas multaj impostoj kaj praktike ne eblas esti memsufiĉa. Pro tio ni
fojfoje devas laboreti en la urbo.

La urbaj loĝantoj havis duone aŭtomatigitajn laborpostenojn danke al
Granda AI, kiu donis al ili malgrandajn loĝejojn, kiujn ili kundividis
kun aliaj kontrolataj homoj, rubomanĝaĵon kaj ĉiajn distrojn: ciferecan
sekson, halucinogenojn, amuzajn videojn, ktp. Vere homoj estas
lacaj, stresitaj, enuigitaj, korpremitaj, senmotivigitaj, irititaj,
malfortigitaj... Sed ekzistas firmaoj, kiuj ofertas pilolojn, kiuj
reekvilibrigas la cerban rekompensan sistemon. La dopaminniveloj de la
civitanoj estas sub kontrolo, kio esencas por daŭrigi la socian ordon
bazitan sur algoritmoj.

«Ni plibonigas la algoritmon. Ĉio bonos», diras ĉiam Granda AI al
homoj, kiuj aŭdacas kontesti ĝian povon. Ĉu bonos?
