Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2023-12-15 17:25
Lang: es
Slug: em-rota-de-colisão
Tags: cuento, IA, distopía, prosa
Title: En curso de colisión
Save_as: en-curso-de-colisión/index.html
URL: en-curso-de-colisión/
Image: <img src="/wp-content/uploads/2023/12/em-ruta-de-colisão.jpg" alt="">

En curso de colisión salté el coche que venía hacia mí. Pisé el capó y,
sin nada que me amortiguara el golpe, caí en el asfalto, pero de pie.
¿Un sueño? No me desperté, sucedió realmente. Dolor en el tobillo. Mucha
adrenalina. Pegado a la pantalla, el conductor se sorprendió y se asustó.
Una anciana en la acera empezó a gritarle al conductor. Apunté la
matrícula en el móvil y me piré, porque tenía prisa.

Desde 2034 la democracia es más bien una idiocracia con características
oligárquicas. Las personas no prestan atención a nada, pues los
ordenadores conectados a sus cerebros hacen casi todo por ellas (muchas
veces se equivocan, pero es más molesto pensar). Nadie sabe cómo
funcionan realmente esos ordenadores. Fueron creados por la empresa más
poderosa del mundo, que también programó Gran IA, la inteligencia
artificial general incluida en esos ordenadores.

Hace algunos años, el progreso parecía evidente, pero surgió un
problema: Gran IA aprendía del contenido de Internet y de los procesos
cerebrales de los usuarios. Sin embargo, estos eran cada vez más
imbéciles, publicaban contenido basura o &mdash;cuando pensaban&mdash;
actuaban de forma automática. Gran IA aprendió de la basura cerebral
generada por su holgazanería y, de esta forma, se volvió más estúpida.
La empresa sigue sin ética y no tiene intención de corregir el
algoritmo, pues eso no le genera ingresos. Es más fácil vaciarles los
bolsillos a lelos consumistas.

Pero algunas personas &mdash;como yo&mdash; se desconectaron de Gran IA.
Las zonas rurales se quedaron aún más deshabitadas debido a la falta de
empleo y al cambio climático, pero hubo gente que se adaptó y se instaló
en ellas lo mejor que pudieron. Actualmente tenemos dos grandes grupos:
por un lado, están las personas desconectadas del mundo real &mdash;y
conectadas a Gran IA&mdash; que suelen vivir en grandes ciudades; por
otro lado, están los que se atreven a desafiar el estilo de vida
impuesto por las máquinas.

«El tren sale en cuatro segundos, mierda». La puerta se estaba cerrando,
pero conseguí entrar. Parece que eso no le gustó al revisor robot, pero
no dijo nada. El viaje de vuelta al pueblo sería breve: 15 minutos.

Volví a casa; informé a las autoridades sobre el accidente, aunque no le
pasaría nada al «conductor» (el coche lo conducía Gran AI, que tiene el
favor del gobierno). Como mucho le pondrán una pequeña multa. Mi
compañera me abrió la puerta, se alegró de verme. «No quiero ir
más a la ciudad. Al cruzar una calle, un coche “inteligente” casi me
atropella», dije. «No te preocupes», respondió ella, «estás seguro aquí.
No vamos a tener otro encargo, por lo menos hasta el próximo mes.
Cambiando de tema, hay un pequeño problema que no consigo solucionar: el
sistema automático de riego del huerto ya no me funciona».

Vivir fuera del sistema es casi imposible (a no ser que seas rico). Hay
muchos impuestos y es prácticamente imposible ser autosuficiente. Es por
esta razón que, de vez en cuando, tenemos que hacer algún trabajillo en
la ciudad.

Los habitantes de la ciudad tenían trabajos semiautomatizados gracias a
Gran IA, que les ofrecía pequeños alojamientos que compartían con otros
autómatas, comida basura y entretenimiento de todo tipo: sexo digital,
psicodélicos, vídeos graciosos, etc. Las personas en realidad están
cansadas, aburridas, desanimadas, estresadas, tristes, desmotivadas,
irritadas, debilitadas... Pero hay empresas que ofrecen comprimidos que
reequilibran el sistema de recompensa cerebral. Los niveles de dopamina
de los ciudadanos están bajo control, algo esencial para mantener el
orden social basado en algoritmos.

«Estamos mejorando el algoritmo. Todo va a salir bien», dice siempre
Gran IA a quien osa cuestionar su poder. ¿Saldrá todo bien?
