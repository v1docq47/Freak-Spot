Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2023-12-15 14:25
Lang: pt
Slug: em-rota-de-colisão
Tags: conto, IA, distopia
Title: Em rota de colisão
Image: <img src="/wp-content/uploads/2023/12/em-ruta-de-colisão.jpg" alt="">

Em rota de colisão saltei por cima do carro que vinha na minha direção.
Pisei o capot e, sem nada que me pudesse suavizar a pancada, caí no
asfalto, mas de pé. Será possível acordar? Não acordei, isso aconteceu
realmente. Dor no tornozelo. Muita adrenalina. Aferrado ao ecrã, o
motorista ficou surpreendido e assustado. Uma velhinha no passeio
começou a gritar ao motorista. Anotei a matrícula no telemóvel e fui
embora, pois tinha pressa.

Desde o ano 2034 a democracia tornou-se idiocracia com traços
oligárquicos. As pessoas não prestam atenção a nada, pois os computadores
ligados aos seus cérebros fazem quase tudo por elas (muitas vezes cometem
erros, mas é mais chato pensar). Ninguém sabe realmente como é que esses
computadores funcionam. Foram criados pela maior
empresa do mundo, que também programou a Grande IA, a inteligência
artificial geral incluída nesses computadores.

Há alguns anos, os  progressos pareciam evidentes, mas um problema
começou acontecer: a Grande IA aprendia do conteúdo da Internet e
dos processos cerebrais dos utilizadores. Porém, estes começaram a
ficar mais burros, postavam conteúdos lixo ou — quando pensavam —
agiam de forma quase automática. A Grande IA aprendeu do lixo
cerebral gerado pela vadiagem e ficou assim mais burra. A empresa
continua a não ter ética nem intenção de corrigir o algoritmo, pois
isso não dá lucro. É mais fácil tirar dinheiro de tolos consumistas.

Mas algumas pessoas — como eu — desligaram-se da Grande IA. As zonas
rurais ficaram ainda mais despovoadas devido à falta de emprego e às
alterações climáticas, mas houve pessoas que se adaptaram e se
instalaram nelas o melhor que puderam. Atualmente, temos dois grandes
grupos: de um lado, estão as pessoas desligadas do mundo real — e
ligadas à Grande IA — que tendem a viver em grandes cidades; de outro
lado, há pessoas que se atrevem a desafiar o estilo de vida imposto
pelas máquinas.

«O comboio parte em 4 segundos, merda». A porta estava a fechar-se, mas
consegui entrar. O revisor robot pareceu não gostar, mas não disse nada.
A viagem de volta à aldeia ia ser breve: 15 minutos.

Voltei para casa; notifique acerca do acidente, mas nada acontecerá ao
«motorista» — o carro era controlado pela Grande IA, que tem o apoio do
governo. No máximo, será aplicada uma pequena multa. A minha companheira
abriu-me a porta, ficou feliz por me ver de volta. «Não quero ir mais à
cidade. Quando estava a atravessar uma passadeira, um carro
“inteligente” quase me atropela», eu disse. «Tudo bem», respondeu ela,
«estás seguro aqui. Não vamos ter outra encomenda durante pelo menos um
mês. Por outro lado, há um pequeno problema que não consigo resolver: o
sistema automático de rega da horta deixou de funcionar».

Viver fora do sistema é quase impossível (a menos que sejas rico). Há
muitos impostos e é praticamente impossível ser autossuficiente. É por
isso que, de vez em quando, temos de fazer algum trabalho na cidade.

As pessoas na cidade têm empregos semi-automatizados no seio da Grande
IA, que lhes oferece pequenos alojamentos partilhados com outros
autómatos, <i lang="en">junk food</i> e entretenimento de todos os tipos: sexo
eletrónico, psicadélicos, vídeos engraçados, etc. As pessoas estão na
verdade cansadas, aborrecidas, desanimadas, stressadas, tristes,
desmotivadas, irritadas, debilitadas... Mas há empresas que oferecem
comprimidos que reequilibram o sistema de recompensa do cérebro. Os
níveis dopaminérgicos dos cidadãos estão sob controlo, o que é essencial
para manter a ordem social baseada em algoritmos.

«Estamos a melhorar o algoritmo. Tudo vai ficar bem», diz sempre a
Grande IA a quem ousa questionar o seu poder. Vai?
