Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2018-06-20 12:50
Image: <img src="/wp-content/uploads/2018/06/Uruko.svg" alt="">
Lang: en
Slug: entrevista-uruk
Tags: GNU/Linux, interview, project, Trisquel, Uruk
Title: Interview about Uruk project

The [Uruk Project](https://urukproject.org/en/index.html) includes many
useful applications and a GNU/Linux distribution based on Trisquel. I've
interviewed Hayder Majid, one of their creators, to find out more about
it.

<!-- more -->

**Can you please briefly introduce the Uruk Project and the Uruk
GNU/Linux distribution?**

The project was born in 2016 but the story began along days ago from
that year. My friend alimiracle and me were thinking about creating a
fully free distro to fulfill our needs and share it with others, so we
met a small team with the same idea, and we quickly joined it. The team
divided the work in a wrong way and the project failed. After that,
until 2016, we made some apps like Uruk cleaner,
[Masalla icon theme](https://www.gnome-look.org/p/1012467),
[UPMS](https://notabug.org/alimiracle/UPMS) and some other stuff, so we
decided to make these apps under one umbrella. We called it Uruk
Project, and made a fully free distro with the same name that respects
the users' freedom and privacy.

**What is the background of the project contributors and their
motivations?**

We have many people who help us in Uruk Project and who share
our goals and motivations about free software, but mainly we have two
types of contributors: First one, the project team members. They have
projects under Uruk Project, or developed in one or more of Uruk
subprojects, that made a substantial change in Uruk Project, the
active team members are the following:

- *Ali Abdul Ghani (Ali Miracle)*: programmer and founder of Uruk
  Project, developer of Uruk GNU/Linux, and many other projects.
- *Hayder Majid (Hayder Ctee)*: computer engineering, programmer and
  designer, founder of Uruk Project and Uruk GNU/Linux, and many other
  projects.
- *Rosa*: Programmer, packager, server manager and developer of Uruk
  Project and Uruk GNU/Linux.
- *Ahmed Nourllah*: Programmer and developer of Uruk project and main
  developer of Uruk Wiki.

Second one is the other type of contributors, who may support the
project translating or writing code, packaging, etc.

**As a user of Uruk GNU/Linux, I run it on an old Lenovo Thinkpad X60
laptop and still have a smooth user experience. Thank you for making the
Masalla icon theme
you mentioned before. Being Uruk GNU/Linux a distribution based on
Trisquel, I wonder why you didn't choose Debian to base upon or
contributed directly to Trisquel instead of creating a new distro.**

Like Rosa said, "you can’t make all people eat one type of cake".

We chose Trisquel because we believe in free software philosophy.
Trisquel is a fully free distro which meets our goals but not our needs,
so we made our distro. If you tried Uruk and Trisquel before, you
will find some differences between them because Uruk is more
customizable and has lots of apps created by the Uruk team (like upms,
ucc and other apps). We also take the community opinion as the basis of
our releases, and I think we influence Trisquel, but indirectly: after
all, you can see it in Trisquel 8, which uses the [MATE
desktop](https://en.wikipedia.org/wiki/MATE_(desktop_environment)) as
the default desktop environment and VLC as a media player [Uruk did that
before].

**Are you taking steps to gain recognition from the Free Software
Foundation and be added to the list of distributions that are entirely
free as in freedom?**

Yes, we are taking serious step to add Uruk GNU/Linux to the GNU free
distributions list, but it takes a long time, or like my friend Ali
said "one thousand years [smile]".

<figure>
    <a href="/wp-content/uploads/2018/06/Uruk-2.0.capture.png">
    <img src="/wp-content/uploads/2018/06/Uruk-2.0.capture.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2018/06/Uruk-2.0.capture.png 1024w, /wp-content/uploads/2018/06/Uruk-2.0.capture-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
    </a>
    <figcaption class="wp-caption-text">Screenshot of Uruk GNU/Linux 2.0</figcaption>
</figure>

**The last release was GNU/Linux Uruk 2.0, based on Trisquel 8. What are
your plans for the future?**

We have many plans in future, like adding some new flavors in our Uruk
GNU/Linux releases, improve Project infrastructure, new apps created by
the project team, and some other surprises [wink].
