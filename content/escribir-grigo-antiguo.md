Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2019-02-16
Lang: es
Slug: escribir-en-griego-antiguo-en-gnulinux
Tags: griego, griego antiguo, GNU/Linux, idiomas, MATE, teclado
Title: Escribir en griego antiguo en GNU/Linux

En este artículo enseño cómo escribir en griego antiguo en GNU/Linux
mediante una distribución de teclado para el griego antiguo y enseñando
con un vídeo práctico cómo escribir iotas suscritas, espíritus, etc.

<!-- more -->

Lo primero es configurar la distribución del teclado para usar el
teclado griego con la ortografía politónica, que es la que usa el griego
antiguo. En el artículo [Cambiar entre distribuciones de teclado
rápidamente en MATE](/multiples-mapas-de-teclas/) enseño
cómo añadir y cambiar una distribución de teclado en el escritorio MATE.
En el caso de que usemos MATE, lo único que tenemos que hacer es seguir
las instrucciones de dicho artículo, pero en vez de seleccionar la
distribución de teclado del esperanto seleccionamos la del griego
politónico. Para ello desde la pestaña **Por país** elegimos el país
**Grecia** y la variante **Griego Griego (politónico)** como en la
siguiente imagen. En otro entorno de escritorio, quizá sea algo
diferente la forma de cambiar la distribución de teclado.

<!-- more -->

<a href="/wp-content/uploads/2019/02/griego-politónico-teclado.png">
<img src="/wp-content/uploads/2019/02/griego-politónico-teclado.png" alt="Desde la pestaña «Por país», elegimos el país «Grecia» y desde «Variantes» elegimos «Griego Griego (politónico)»" width="930" height="608" srcset="/wp-content/uploads/2019/02/griego-politónico-teclado.png 930w, /wp-content/uploads/2019/02/griego-politónico-teclado-465x304.png 465w" sizes="(max-width: 930px) 100vw, 930px">
</a>

Ahora técnicamente ya podemos escribir en griego antiguo, aunque
resultará algo complicado si tenemos un teclado físico con otro
alfabéto. Si no nos sabemos de memoria la posición de las letras griegas
y vamos a usar mucho el teclado del griego antiguo, podemos usar
pegatinas sobre las letras del teclado físico o marcarlas de alguna
forma. Otra opción es usar un teclado virtual (como el programa Onboard)
si solo lo vamos a usar ocasionalmente y no queremos marcar nuestro
teclado físico.

<figure>
    <a href="/wp-content/uploads/2019/02/usando-Onboard-para-escribir-en-griego-antiguo.png">
    <img src="/wp-content/uploads/2019/02/usando-Onboard-para-escribir-en-griego-antiguo.png" alt="" width="1023" height="645" srcset="/wp-content/uploads/2019/02/usando-Onboard-para-escribir-en-griego-antiguo.png 1023w, /wp-content/uploads/2019/02/usando-Onboard-para-escribir-en-griego-antiguo-511x322.png 511w" sizes="(max-width: 1023px) 100vw, 1023px">
    </a>
    <figcaption class="wp-caption-text">Usando Onboard para escribir en
    griego antiguo</figcaption>
</figure>

Resulta fácil escribir letras, pero aún debemos aprender a escribir con
acentos, iotas suscritas, etc. Para ello podemos observar la imagen de
la distribución de teclado (la que aparece en la **Vista previa** cuando
seleccionamos la distribución de teclado en MATE) y pulsar las teclas
necesarias para producir las combinaciones deseadas. La mejor forma de
aprender es probar y practicar. A continuación, para que veáis un
ejemplo práctico, dejo un vídeo en el que transcribo usando el teclado
virtual Onboard el siguiente texto de Esopo, que contiene iotas
suscritas, acentos, espíritus y demás:

> Λέαινα ὀνειδιζομένη ὑπὸ ἀλώπεκος ἐπὶ τῷ διὰ παντὸς ἕνα τίκτειν· «Ἕνα,
ἔφη, ἀλλὰ λέοντα.» Ὅτι τὸ καλὸν οὐκ ἐν πλήθει δεῖ μετρεῖν, ἀλλὰ πρὸς
ἀρετὴν ἀφορᾶν.

<video controls>
  <source src="/video/escribiendo-en-griego-antiguo-con-onboard.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>
