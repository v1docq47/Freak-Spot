Author: Jorge Maldonado Ventura
Category: Edición de imágenes
Date: 2016-07-26 21:59
Lang: es
Modified: 2017-03-10 23:50
Slug: esteganografia-sencilla
Status: published
Tags: esteganografía, GNU/Linux, Linux Mint, Linux Mint 18 "Sarah", ocultar imagen en imagen, ocultar mensajes en imágenes, interfaz de línea de órdenes
Title: Esteganografía sencilla

¿Alguna vez has querido ocultar un mensaje o un archivo en una imagen?
Se puede hacer muy fácilmente en menos de treinta segundos.

<figure id="attachment_197" style="width: 1366px" class="wp-caption aligncenter"><a href="/wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-01-11.png"><img class="size-full wp-image-197" src="/wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-01-11.png" alt="Instrucciones para ocultar texto en una imagen (demostración práctica)" width="1366" height="768" srcset="/wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-01-11.png 1366w, /wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-01-11-300x169.png 300w, /wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-01-11-768x432.png 768w, /wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-01-11-1024x576.png 1024w" sizes="(max-width: 1366px) 100vw, 1366px" /></a><figcaption class="wp-caption-text">Ocultando texto en una imagen</figcaption></figure>

En la imagen de arriba lo veis. Realmente, solo hace falta usar la
instrucción `echo 'Tu mensaje' >> tu_imagen`, lo demás es solo para
demostraros que funciona y para enseñaros a después el mensaje. La
imagen se puede abrir posteriormente sin problema; tiene la misma
apariencia. La única diferencia es que ocupa más espacio. En el ejemplo
que os he mostrado, la imagen ocupa 15 bytes más que antes, después de
haber escrito el mensaje.

¿No entendéis todo el código que he escrito? Tranquilos, ahora lo
explico. `file favicon.png`, me dice el tipo del archivo. `ls -l`
muestra información detallada de todos los archivos del directorio en el
que me encuentro. Como solo hay uno, solo aparece la imagen. El número
2588 que aparece tras ejecutarlo es tamaño en bytes de la imagen. Con
`ls -lh` puedo ver el tamaño en un formato más legible, en kilobytes en
vez de en bytes. Como solo me interesa el tamaño, paso `ls -lh` a `cut
-d" " -f5` por una tubería para que solo me aparezca la columna 5, cada
columna está separada por un espacio (" "). Por último, leo el mensaje
oculto con `strings`, que extrae todos los caracteres alfanuméricos
encontrados en la imagen.

Si vuelvo a ejecutar la instrucción `file favicon.png`, encontraré el
mismo formato que antes: PNG.

Podemos aplicar este método no solo a texto, sino también a archivos de
distinto tipo.

<a href="/wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-49-26.png"><img class="aligncenter size-full wp-image-199" src="/wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-49-26.png" alt="Screenshot from 2016-07-26 23-49-26" width="1025" height="435" srcset="/wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-49-26.png 1025w, /wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-49-26-300x127.png 300w, /wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-49-26-768x326.png 768w, /wp-content/uploads/2016/07/Screenshot-from-2016-07-26-23-49-26-1024x435.png 1024w" sizes="(max-width: 1025px) 100vw, 1025px"></a>
