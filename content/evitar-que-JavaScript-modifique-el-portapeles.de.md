Author: Jorge Maldonado Ventura
Category: Tricks
Date: 2023-06-25 17:00
Lang: de
Slug: evitar-que-javascript-modifique-el-portapeles
Save_as: verhindern-dass-JavaScript-die-Zwischenablage-in-Firefox-verändert/index.html
URL: verhindern-dass-JavaScript-die-Zwischenablage-in-Firefox-verändert/
Tags: Firefox, JavaScript, Zwischenablage
Title: Verhindern, dass JavaScript die Zwischenablage in Firefox verändert
Image: <img src="/wp-content/uploads/2023/06/JavaScript-ändert-die-Zwischenablage.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/06/JavaScript-ändert-die-Zwischenablage.png 1920w, /wp-content/uploads/2023/06/JavaScript-ändert-die-Zwischenablage-960x540.png 960w, /wp-content/uploads/2023/06/JavaScript-ändert-die-Zwischenablage-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Ist es dir schon einmal passiert, dass du Text von einer Seite kopierst
und beim Einfügen erscheint ein geänderter Text oder gar nichts? Das
liegt daran, dass die Seite einen JavaScript-Code ausführt, der die
Zwischenablage verändert.

Um das zu vermeiden, kannst du [JavaScript
deaktivieren](/en/disable-JavaScript-in-Firefox/) oder die
Firefox-Einstellungen wie folgt verändern:

1. Tippe `about:config` in die Adressleiste.
2. Dücke <kbd>Enter</kbd> und klicke auf **Risiko akzeptieren und
   fortfahren**.
3. Tippe `dom.event.clipboardevents.enabled` in das Suchfeld.
4. Doppelklicke auf diese Einstellung oder dücke die entsprechende
   **Umschalten**-Taste, um ihren Wert in `false` zu ändern.

<a href="/wp-content/uploads/2023/06/Einstellung-dom-event-clipboardevents-enable-Firefox-umschalten.png">
<img src="/wp-content/uploads/2023/06/Einstellung-dom-event-clipboardevents-enable-Firefox-umschalten.png" alt="" width="959" height="302" srcset="/wp-content/uploads/2023/06/Einstellung-dom-event-clipboardevents-enable-Firefox-umschalten.png 959w, /wp-content/uploads/2023/06/Einstellung-dom-event-clipboardevents-enable-Firefox-umschalten-479x151.png 479w" sizes="(max-width: 959px) 100vw, 959px">
</a>

<figure>
<a href="/wp-content/uploads/2023/06/JavaScript-Zwischenablage-Ereignisse-deaktiviert.png">
<img src="/wp-content/uploads/2023/06/JavaScript-Zwischenablage-Ereignisse-deaktiviert.png" alt="" width="935" height="54" srcset="/wp-content/uploads/2023/06/JavaScript-Zwischenablage-Ereignisse-deaktiviert.png 935w, /wp-content/uploads/2023/06/JavaScript-Zwischenablage-Ereignisse-deaktiviert-467x27.png 467w" sizes="(max-width: 935px) 100vw, 935px">
</a>
    <figcaption class="wp-caption-text">So sollte es aussehen</figcaption>
</figure>

Auf diese Weise können die Webseiten, die du besuchst, deine
Zwischenablage nicht verändern. Beachte, dass einige
Webanwendungen, die die Zwischenablage mit JavaScript verändern
(wie z. B. [Collabora](https://de.wikipedia.org/wiki/Collabora_Online)),
werden nicht mehr in der Lage sein, dies zu tun, wodurch ihre
Einfügefunktion nicht mehr richtig funktioniert wird.
