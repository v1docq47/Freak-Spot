Author: Jorge Maldonado Ventura
Category: Trucos
Date: 2023-06-24 17:40
Lang: es
Slug: evitar-que-javascript-modifique-el-portapeles
Tags: Firefox, JavaScript, portapapeles
Title: Evitar que JavaScript modifique el portapeles en Firefox
Image: <img src="/wp-content/uploads/2023/06/JavaScript-modificando-el-portapapeles.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/06/JavaScript-modificando-el-portapapeles.png 1920w, /wp-content/uploads/2023/06/JavaScript-modificando-el-portapapeles-960x540.png 960w, /wp-content/uploads/2023/06/JavaScript-modificando-el-portapapeles-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

¿Te ha pasado alguna vez que copias texto de una página y al pegar
aparece un texto modificado, o no aparece nada? Eso ocurre porque la
página en cuestión ejecuta un código JavaScript que modifica el
portapapeles.

Para evitar esto puedes [desactivar
JavaScript](/desahabilitar-javascript-comodamente-firefox-y-derivados/)
o cambiar la configuración de Firefox de esta forma:

1. En la barra de direcciones escribe `about:config`.
2. Pulsa <kbd>Enter</kbd> y dale a **Aceptar el riesgo y continuar**.
3. En el cuadro de búsqueda escribe `dom.event.clipboardevents.enabled`.
4. Haz doble clic en esa opción o pulsa en su botón **Alternar** para
   cambiar su valor a `false`.

<a href="/wp-content/uploads/2023/06/alternar-ajuste-dom-event-clipboardevents-enable-Firefox.png">
<img src="/wp-content/uploads/2023/06/alternar-ajuste-dom-event-clipboardevents-enable-Firefox.png" alt="" width="990" height="294" srcset="/wp-content/uploads/2023/06/alternar-ajuste-dom-event-clipboardevents-enable-Firefox.png 990w, /wp-content/uploads/2023/06/alternar-ajuste-dom-event-clipboardevents-enable-Firefox-495x147.png 495w" sizes="(max-width: 990px) 100vw, 990px">
</a>

<figure>
<a href="/wp-content/uploads/2023/06/eventos-portapapeles-Firefox-desactivados.png">
<img src="/wp-content/uploads/2023/06/eventos-portapapeles-Firefox-desactivados.png" alt="" width="972" height="122" srcset="/wp-content/uploads/2023/06/eventos-portapapeles-Firefox-desactivados.png 972w, /wp-content/uploads/2023/06/eventos-portapapeles-Firefox-desactivados-486x61.png 486w" sizes="(max-width: 972px) 100vw, 972px">
</a>
    <figcaption class="wp-caption-text">Debe quedar así</figcaption>
</figure>

De esta forma, las páginas web que visites no podrán modificar tu
portapapeles. Ten en cuenta que algunas aplicaciones web que modifican
el portapapeles con JavaScript (como [Collabora](https://es.wikipedia.org/wiki/Collabora_Online))
ya no podrán hacerlo, por lo que su funcionalidad de pegar dejará de funcionar correctamente.
