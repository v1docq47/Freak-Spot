Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2018-07-13 21:30
Lang: eo
Modified: 2018-07-14 18:05
Save_as: urba-elirado/index.html
Slug: éxodo-urbano
Tags: elirado, kamparo, prozo, rakonto, urbo
Title: Urba elirado
Url: urba-elirado/

La lumo kaŝiĝis post la monto verda; ri havis sufiĉan tempon por iri al
sia eta kaj trankvila rifuĝejo. Ri loĝis pacege kaj harmoniege en
naturejo.

Iutage trovis ri alian personon en arbaro kie troviĝas proksima al ria
rifuĝejo dum ri kolektis berojn. Ri proksimiĝis al la nekonata, kiu
rigardis perdite al ĉiuj direktoj. Kiam ŝi ekperceptis rian apudeston,
ri salutis rin ĝoje, kaj ili ekparolis.

Ri devenis de urbo serĉanta trankvilan ejon kie ri povis loĝi pace kaj
libere. Ambaŭ komprenis sin tre bone jam de la komenco, do ili decidis
kunlabori kaj loĝi kune.

La historiojn kiujn ri rakontis pri la urbo estis strangaj. Tie ne estis
libereco. La bestoj ne kuris nek flugis libere, sed ili portis ligilojn
ĉirkaŭ siaj koloj kaj spiris malpuran aeron. La homoj kiuj direktis
tiujn kvarpiedo-bestojn ankaŭ havis mastroj al kiuj ili devis servi.

Kiam ri alvenis al la monto kaj ekkonis ria nova kunulo, ri trovis
bonegan vivmanieron. Ri konis la plezuron pri observi la stelojn, pri
spiri puran aeron, pri moviĝi libere...
