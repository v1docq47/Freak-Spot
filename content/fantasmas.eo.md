Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2021-03-04
Lang: eo
Slug: fantasmas
Tags: civilizacio, disfalo, politika kontrolo, dekadenco
Title: Fantomoj?
Image: <img src="/wp-content/uploads/2020/05/fantasma.png" alt="">

<a href="/wp-content/uploads/2020/05/chocar-con-una-farola-por-mirar-el-celular.gif">
<img src="/wp-content/uploads/2020/05/chocar-con-una-farola-por-mirar-el-celular.gif" alt="">
</a>

La homamaso nenion komprenas: ĝi estas pigra kaj ne volas mem pensi.
Amuzaĵoj senĉese, manĝaĵoj... Kion pli oni povas deziri? Ĝi havas eĉ
fiero aparteni en ĉi tiu edeno, kiun ĝi defendos agitante kolorajn
ŝtofojn kaj esprimante «ĝian» pripensitan opinion. Ĝisnune ĝi estas bona
konsumante ĝian vivon ĉirkaŭ *fantomoj* &mdash;kiel [Max
Stirner](https://eo.wikipedia.org/wiki/Max_Stirner) dirus&mdash;; sed ĉu
ĝi kredos en fantomoj kiam mankas manĝaĵoj?, kiam ĝi ne povas elturniĝi
plu?

Pro la amasa formorto de specoj, la eta perdo de tero, la kresko de la
kostoj de petrolo kaj aliaj kreitaj problemoj, kiu pli malbone povas
fini estas kiu vivas en alia realo, kvankam neniu eskapas.

<!-- more -->

Kiom da arboj, riveroj, montaroj, birdoj, insektoj... el via regio vi
povas nomi?; Kiom da markoj de poŝtelefonoj, aŭtoj, videludoj, de aliaj
aferoj? Ĉu vi povus supervivi, se tuje estus malplenaj la bretaro de la
superbazaro, sen la helpo de la ŝtato, de via familio?

De la Roma imperio kelkaj civitanoj travivis vidante spektaklojn, danke
al la grajno, kiun la ŝtato donis al ili dum ĝi povis:
[*panes et circenses*](https://eo.wikipedia.org/wiki/Pano_kaj_ludoj).
Iutage la manĝaĵo ne estus alirebla, ne protektus ilin plu la disfalante
Romo. Se scii kiel supervivi sola nek povi gvidi iliajn proprajn vivojn,
venis dungantoj por detranĉi iliajn kapojn.

«La celo de la ŝtato estas ĉiam la sama: limigi la individuon,
malsovaĝigi rin, subordigi rin, submeti ĝin». &mdash; Max Stirner
