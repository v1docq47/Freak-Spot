Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2018-10-06 21:34
Lang: eo
Save_as: produktema-estonto/index.html
Slug: futuro-productivo
Tags: kapitalismo, distopio, estonto, produktemo, prozo, virtuala realo, proprieta programaro, teĥnologio
Title: Produktema estonto
URL: produktema-estonto/

Hodiaŭ ri vekiĝas je la oka matene per la brua veksonorilo de sia
*virtuala asistanto*. Matenmanĝas la manĝon, kion ĝi preparas al ri kaj
ri veturas al la laborejo per sia *inteligenta veturilo*.

<!-- more -->

Tamen ri devas vekiĝi ĉiam pli frue ol kutime, ĉar la inteligentaj
veturiloj kutimas aliigi iom la vojon por plibonigi la uzulan sperton.
Tio havas la avantaĝon, ke ili ĉiam lasas al oni vidi la novajn
reklamojn, kiuj troviĝas en la strato, kaj aĉeti iun aĉetaĵon antaŭ
alveni al celoko.

Ĉi-foje ri alvenis al aĉetejo, en kiu ri aĉetas pastoringojn.
«Lastatempe mi malfacilas moviĝi», pensas dum ri maĉas la pastoringon,
«eble mi devus aliigi miajn agordojn kaj uzi iun programon por
sporti».

Ri havas la 6-an nivelon, kio ne estas malbona. Danke al ria rango,
havas ri estimindan laboron kiel psikologiisto, pli trankvila ol aliaj.
La laboro mem estas tre ŝarga, sed ĝi estas socie respektita. Sendube ĝi
estas pli bonega ol la pli etaj niveloj. La plej malbona el ĉiuj estas la
1a, ĉar la 1-aj uloj estas malliberitaj preskaŭ ĉiam ĝis la fino de siaj
vivoj en la *ejoj por socia adapto kaj reensocietigo*.

Dum ri maĉas, ri ne demovas la rigardon eĉ momenton de la poŝtelefono.
Antaŭ ekmanĝi ri fotas la pastoringon, kiel kutime, por diskonigi ĝin en
la sociaj retejoj. Se oni estas aga, estas pli probabla havigi
sekvantojn, kaj eble plialtiĝi la rango. Ri ankaŭ vizitas la sociaj
retejoj por scii, kio okazas en la mondo.

La sociaj retejoj, kvankam malsamaj, estis gviditaj de la sama
korporacio, kiu kontrolas iliajn algoritmojn. La homoj deziras scii la
algoritmojn por gajni influon kaj ascendi nivelojn. Ju pli alta la
nivelo, des pli granda estas la influo kaj la aĉeta povo.

Ri finas la manĝaĵon, diskonigas kaj etikedas lastan novaĵon, ri veturas
per aŭto al laborejo. Dum la veturado, ri kutimas spekti ajn filmon aŭ
eniri al iu mondo de virtuala realo, danke al la abono, kiu estas
permesitaj al la 6-e nivelitaj. Tamen estas iom malagrablaj kelkaj da
la kvar aŭ kvin reklamoj, kiuj ĉiam interrompas rin dum la veturado, ĉar
ria abono estas normala. Ri bezonus almenaŭ la nivelon 8 por havi unu
sen reklamoj.

Finfine la veturilo parolas al ri, ke ri ĵus alvenis al ria celo. Estas
la 09:00, kaj ri ekeniras en la klinikon. Ri salutas kiel ĉiam tuj post
eniri al sia sekretario de nivelo 5a.

Ria laboro estas iom unuforma kaj teda, sed estas facile farebla. Krom,
ekde ri ascendis nivelon, ri havas helpanta, kiu helpas al ri en ria
laboro. La tempoj de konsulto estas streĉaj kaj bezonas rapidecon. La
grandparto da pacientoj estas kontraŭaŭtoritatismaj junaj, malmotivitaj,
perfortaj aŭ havantaj ajn alian problemon por akomodiĝi en la socio.
Preskaŭ neniam ri sukcesas je siaj kuracatoj kaj estas devigita je sendi
ilin al *edukanta aŭtoritato*, kiu havas pli fortaj metodoj ol la
kutimaj diagnozoj, planoj kaj rekomendoj de riaj protokolaj sesioj.

Ri ne scias precize, kiuj estas la metodoj uzitaj de la edukanta
aŭtoritato. Ri scias, ke ili ricevas iun medikamenton aŭ iun «periodo de
isolado kaj pripenso» en ejo de adaptado kaj reensocietigo, kiu antaŭ
estis nomata «restado en puna institucio». Multaj vortoj aliiĝis por
adapti al la nova epoko.

Ri finas la sesiojn. Ili okazis kiel kutime, krom kiam paciento petis al
ri, ke ili liberigi rin kaj poste insultis al ri. Tiu homo estas savaĝa
de Rivero Verda, kie oni purigis la teron per forigo de arbaroj kaj
konstruis novan urban areon. Ri malobeis la leĝon kontraŭante la Ŝtaton,
kaj oni alkondukis ŝi ĉenigita al ria kliniko, kie por la manko de tempo
kaj progreso ri sendis rin al edukanta aŭtoritato. Tute ne ŝatas al ri
trakti kun savaĝaj kaj perfortaj homoj; preskaŭ neniam havas ili solvon.
«Mi esperas, ke mi ascendas al la 7a nivelo por ke ili lasus mi labori
mian tutan labortagon telematike», ri pensis.

Ri eliras el la laborejo kaj eniras la aŭton. Ĉi-foje ĝi rekomendas al
ri iri al nova restauranto de rapida manĝo, kie la homoj manĝas dum ili
legas tujmesaĝojn kaj uzas la sociajn retejojn. La Interreto ĉi tie
estas rapidega, do estas multaj homoj, kiuj iris, por ke oni manĝigas
ilin intravejne dum ili ludas Interrete diversajn ludojn de virtuala
realo, kiuj bezonas ĉiam multe da kapacito.

Subite, dum ri manĝas en tiu restauranto, ri fokusis pri reklamo
rekomendita de sia favorata socia retejo pri robota kuro en la
Labirito-stadiono. Bedaŭrinde ri ne havas sufiĉon per sia enspezo por
vidi ĝin. Sed, bonŝance ri povas akiri la mankanta monon per ekstra
laboro.

Reiras al hejmo. Estas la 14:00. Ri petas la novajn aĉetajojn, kiujn ri
bezonas, al sia virtuala asistanto. La virtuala asistanto funkcias
rapidege kaj havas multajn trajtojn, sed ĉiun semajnon kutimas bezoni
ajn ĝisdatigon aŭ plibonaĵon, kiuj plejofte ne estas ĉipaj.

Pli posttempe ri faros tri ekstrajn laborhorojn por akiri la monon
bezonata por aĉeti la bileton de la robota kuro.

Post ludi ludon de virtuala realon momenton, relaboras. Ĉi-okaze ri
povas fari tion el hejmo, ĉar vespere sia firmo nur ofertas sesiojn tra
telekunsidado per programeto de *inteligentaj aparatoj*.

Finitaj la ekstrajn laborhorojn, ri havas sufiĉon da mono por aĉeti la
deziratan bileton. Sed ri estas laca kaj apenaŭ havas tempon por skribi
ion en la sociaj retejoj en la fino de la tago. Doe ri malgajnas tri
sekvantojn. Ĝi ne estas multaj, sed gajni sekvantojn denove bezonas
tempon.

Venas la horon por dormi. Ri zorgas iom pensante kion afiŝi aŭ konigi
por regajni sekvantojn; se ri malgajnas iujn pli, ri descendos rangon
kaj havos pli malbonan laboron. «Mi ne volas memorigi miajn lastan
laboron kiel sekretario», pensas ri.

Finfine, ri restas. Ri havas malagrablan sonĝon, en kiu ri estas
descendita al 5a nivelo. Ri vekiĝas en la noktmezo pro la kunfrapo de la
terurŝongo. Ri manĝas trankvilpilolon, reiras sub liton. Ri konsoliĝas
pensante, ke ri amuziĝos en domanĉo spektante la kuron de robotoj, kaj
fermas la okulojn.
