Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2018-07-16 03:13
Modified: 2021-11-02
Lang: es
Slug: gitlab-recaptcha-privativo
Tags: alojamiento de código, CAPTCHA, Git, GitLab, Google, privacidad, reCAPTCHA, repositorio, software libre, software privativo
Title: Gitlab usa CAPTCHAs privativos

GitLab contiene <i lang="en">software</i> privativo de Google y no parece que vayan a
eliminarlo. En concreto, usa el programa reCAPTCHA de Google,
[documentando incluso su configuración](https://docs.gitlab.com/ee/integration/recaptcha.html).

Esto es problemático no solo por la
[cuestión de la libertad de <i lang="en">software</i>](https://www.gnu.org/philosophy/shouldbefree.es.html),
sino también por otros efectos secundarios de reCAPTCHA (como la
[explotación laboral](/como-explota-Google-con-CAPTCHAs/) y el [reconocimiento facial para fines
bélicos](/al-completar-un-recaptcha-de-google-ayudas-a-matar/)).

El código se carga directamente desde los servidores de Google, lo cual
podría impedir la apertura de incidencias y el registro de nuevas
cuentas cuando el servidor de Google estuviera caído.

Hay desde hace tiempo
[varias](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/45684)
[incidencias](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/46548)
abiertas en el gestor de incidencias de GitLab.
