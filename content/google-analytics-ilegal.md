Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-01-20 13:00
Lang: es
Slug: google-analytics-ilegal
Tags: Austria, Google, Google Analytics
Title: Google Analytics es ilegal, según la Autoridad de Protección de Datos de Austria
Image: <img src="/wp-content/uploads/2022/01/google-analytics-ilegal.png" alt="Google Analytics, ilegal">

La Autoridad de Protección de Datos (Datenschutzbehörde) de Austria ha
dictaminado que los proveedores de sitios web austriacos que utilizan
Google Analytics infringen el
[Reglamento General de Protección de
Datos](https://es.wikipedia.org/wiki/Reglamento_General_de_Protecci%C3%B3n_de_Datos) (RGPD).
Considera que se infringe el artículo 44, pues el programa
estadístico envía información personal del usuario a los centros de
datos de Google en Estados Unidos.

La [sentencia de la
Datenschutzbehörde](https://noyb.eu/sites/default/files/2022-01/E-DSB%20-%20Google%20Analytics_DE_bk_0.pdf)
responde a una denuncia contra una editorial austriaca que usaba Google
Analytics. Fue interpuesta por la asociación de
protección de datos NOYB, fundada por el abogado y activista Max
Schrems, en agosto de 2020.

La conclusión de la sentencia es que Google Analytics no ofrece un
nivel de protección adecuado: envía datos que identifican al usuario
como la dirección IP y los ajustes del navegador. Las cláusulas
contractuales estándares de Google no proporcionan un nivel de
protección adecuado para eliminar la vigilancia y la posibilidad de
acceso de las agencias de inteligencia estadounidenses en virtud de la
Foreign Intelligence Surveillance Act (FISA).

Muchas empresas de Estados Unidos y de la Unión Europea decidieron
ignorar este problema. Es por ello que una empresa austriaca ha acabado
en el punto de mira de la Datenschutzbehörde.

> En vez de adaptar los servicios para que cumplan el RGPD, las empresas
> de EE. UU. han intentado simplemente añadir texto a sus políticas de
> privacidad e ignorar al Tribunal de Justicia. Muchas empresas de la
> Unión Europea han seguido su ejemplo en vez de cambiarse a opciones
> legales.
>
> Max Schrems

La decisión no plantea una posible sanción, ya que se considera un
procedimiento de ejecución «público», en el que no se escucha al
denunciante. No hay información sobre si ha habido una multa o si el
Datenschutzbehörde tiene previsto también poner una multa. El RGPD prevé
sanciones de hasta 20 millones de euros o el 4% del volumen de negocios
global en estos casos.

## ¿Hay opciones legales para recoger estadísticas de sitios web?

Existen muchas alternativas que respetan las leyes de protección de datos.
Este sitio web, por ejemplo, usa
[AWStats](https://es.wikipedia.org/wiki/Awstats). Otra opción,
probablemente la más parecida a Google Analytics por las
funcionalidades que ofrece, es [Mantomo](https://matomo.org/).
