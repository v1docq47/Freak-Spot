Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2018-01-21 00:11
Image: <img src="/wp-content/uploads/2018/01/cambiando-un-archivo-de-solo-lectura-con-Vim.png" alt="">
Lang: es
Slug: guardar-archivo-como-superusuario-vim
Tags: consejos, dd, Neovim, sudo, superusuario, truco, Vim
Title: Guardar archivo como superusuario desde Vim

Si usas Vim o Neovim, es probable que alguna vez te hayas encontrado con
que no puedes guardar las modificaciones que has hecho en un archivo,
porque no tienes permiso de escritura o el archivo no te pertenece.

<!-- more -->

La solución más sencilla es acordarse de usar `sudo` antes de editar un
archivo sobre el que no tenemos permiso (suelen ser archivos de
configuración del sistema). Pero cuando el olvido de `sudo` ya ha
ocurrido y hemos realizado muchas modificaciones, el siguiente truco nos
ahorrará disgustos y tiempo.

Lo primero que debemos hacer es analizar la situación. Si el editor no
nos deja guardar el archivo es porque no tenemos permiso para
modificarlo. Podemos intentarlo, ejecutando `:w!`, pero si el archivo
pertenece a otro usuario y no tenemos permiso de escritura, no
funcionará.

Si nos ha dejado leer el contenido del archivo, lo que sí
podemos hacer es guardarlo con otro nombre de archivo. En Vim podemos
hacerlo ejecutando `:w otro_nombre`. Pero esta solución no es del todo
satisfactoria, ya que tendríamos que ejecutar una nueva instrucción para
reemplazar el archivo que estamos editando: `!sudo mv otro_nombre
%`[^1].

Hay una forma de lograr nuestro objetivo de forma más sencilla: usando
el programa dd. Simplemente debemos ejecutar `:w !sudo dd of=%`. Esta
instrucción funciona de la siguiente manera:

- `:w !`. Como explica la documentación de Vim (`:h w_c`), las
  instrucciones que siguen al símbolo de exclamación se ejecutan tomando
  el contenido del búfer (es decir, el archivo) como entrada estándar.
- `sudo` nos permite ejecutar la instrucción con permisos de
  superusuario.
- `dd of=%` toma como entrada la entrada estándar y escribe su contenido
  en el archivo que estamos editando.

![Demostración](/wp-content/uploads/2018/01/demostración-sudo-vim.gif)

Al ejecutar la anterior instrucción, debemos introducir nuestra
contraseña. Luego Vim nos avisará de que el archivo ha cambiado y de que
el búfer se ha modificado también. Simplemente, pulsamos <kbd>O</kbd> y,
despúes, <kbd>Intro</kbd> para continuar.

Como el truco puede resultar difícil de recordar, es buena idea
añadir la siguiente línea al archivo de configuración de Vim:

    ::vim
    cmap w!! w !sudo dd of=%<Enter>

Con ella, solo hace falta ejecutar `:w!!` la próxima vez que tengamos
este problema.


[^1]: `%` equivale a poner la ruta del archivo que estamos editando,
  ejecuta `:h %` para más información. El `!` al principio de la
  instrucción le indica a Vim que ejecute una instrucción usando el
  intérprete del sistema, ejecuta `:h :!` para más información.
