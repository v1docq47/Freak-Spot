
/*
Tipue Search 6.0
Copyright (c) 2017 Tipue
Copyright (c) 2020 Jorge Maldonado Ventura
Tipue Search is released under the MIT License
http://www.tipue.com/search
*/


/*
Stop words
Stop words list from http://www.ranks.nl/stopwords
*/

var tipuesearch_stop_words = ['ĉi', 'la', 'tiu'];

// Word replace

var tipuesearch_replace = {'words': [
     {'word': 'javscript', 'replace_with': 'javascript'},
     {'word': 'jqeury', 'replace_with': 'jquery'}
]};


// Weighting

var tipuesearch_weight = {'weight': [
     {'url': 'http://www.tipue.com', 'score': 20},
     {'url': 'http://www.tipue.com/search', 'score': 30},
     {'url': 'http://www.tipue.com/is', 'score': 10}
]};


// Illogical stemming

var tipuesearch_stem = {'words': [
     {'word': 'e-mail', 'stem': 'email'},
     {'word': 'javascript', 'stem': 'jquery'},
     {'word': 'javascript', 'stem': 'js'}
]};


// Related searches

var tipuesearch_related = {'searches': [
     {'search': 'LAMP', 'related': 'GLAMP'},
]};


// Internal strings

var tipuesearch_string_1 = 'Sen titolo';
var tipuesearch_string_2 = 'Montrante resultojn por';
var tipuesearch_string_3 = 'Serĉi anstataŭ';
var tipuesearch_string_4 = '1 resultoj';
var tipuesearch_string_5 = 'resultojn';
var tipuesearch_string_6 = 'Antaŭe';
var tipuesearch_string_7 = 'Pli';
var tipuesearch_string_8 = 'Nenio trovita.';
var tipuesearch_string_9 = 'La komunaj vortoj estas kutime ignoritaj.';
var tipuesearch_string_10 = 'La serĉo estas tro mallonga';
var tipuesearch_string_11 = 'Devas havi almenaŭ 1 simbolon.';
var tipuesearch_string_12 = 'Devas havi';
var tipuesearch_string_13 = 'simboljn aŭ pli.';
var tipuesearch_string_14 = 'sekundoj';
var tipuesearch_string_15 = 'Serĉoj rilataj al';


// Internals


// Timer for showTime

var startTimer = new Date().getTime();

