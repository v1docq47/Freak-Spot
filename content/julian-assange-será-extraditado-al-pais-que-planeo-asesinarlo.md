Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-06-17 15:36
Lang: es
Slug: Julian-Assange-será-extraditado-al-pais-que-planeo-asesinarlo
Tags: Estados Unidos, Julian Assange, Reino Unido
Title: Julian Assange será extraditado al país que planeó asesinarlo
Image: <img src="/wp-content/uploads/2022/06/free-assange-2.webp" alt="" width="824" height="464" srcset="/wp-content/uploads/2022/06/free-assange-2.webp 824w, /wp-content/uploads/2022/06/free-assange-2.-412x232. 412w" sizes="(max-width: 824px) 100vw, 824px">

El [gobierno británico decide extraditar](https://nitter.42l.fr/wikileaks/status/1537726323858219009) a [Julian
Assange](https://es.wikipedia.org/wiki/Julian_Assange) a Estados Unidos,
[el país que planeó
asesinarlo](https://www.jornada.com.mx/notas/2021/09/26/mundo/revelan-que-la-cia-planeo-secuestrar-y-asesinar-a-assange-en-londres/); sus abogados recurrirán al Tribunal Supremo.

El trato recibido por Assange en prisión en Reino Unido es
inhumano: torturas psicológicas que le han llevado a sufrir un estrés
extremo y a tener ganas de suicidarse, [según las observaciones de
más de sesenta médicos y del relator especial de las Naciones
Unidas](https://www.lavanguardia.com/internacional/20191125/471840750638/sesenta-medicos-temen-assange-pueda-morir-prision.html).
En Estados Unidos puede llegar a cumplir penas que sumen 175 años bajo
la Ley de Espionaje.  Assange actuó como periodista publicando abusos de
poder y crímenes de guerra.
