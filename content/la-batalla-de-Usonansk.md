Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2023-11-26 12:55
Lang: es
Slug: la-batalla-de-usonansk
Tags: privacidad, prosa, seguridad, guerra
Title: La batalla de Usonansk
Image: <img src="/wp-content/uploads/2023/11/soldiers-fighting-a-cyberwar.png" alt="" width="1280" height="853" srcset="/wp-content/uploads/2023/11/soldiers-fighting-a-cyberwar.png 1280w, /wp-content/uploads/2023/11/soldiers-fighting-a-cyberwar-640x426.png 640w" sizes="(max-width: 1280px) 100vw, 1280px">

*Este relato está basado en hechos reales. Los datos identificativos
han sido distorsionados.*

«Según las fuentes que tenemos, planean centrar sus fuerzas en esta
región». Poco sabían los generales de que sus fuentes habían sido
intencionadamente envenenadas con información falsa. Sucedió algo bien
diferente.

Resulta que ciertos militares y personas con altos cargos investigaban
sobre la región que iría ser atacada, dejando un rastro digital que
permitía al enemigo confirmar que se habían tragado su información
falsa. Habían encontrado puntos débiles en la cadena de mando y no
dejarían de explotarlos. Algunas de estas personas eran testarudas y
tenían una larga trayectoria profesional, por lo que no tenían ningún
interés en dejar sus altos cargos, a pesar de sus sucesivos errores.
Así pues, sufrieron ataques de ingeniería social.

El círculo social de personas clave fue altamente monitorizado para
obtener todo tipo de información, identificar puntos débiles, etc. El
peligro de una confrontación directa y el miedo a escalar el conflicto
fueron algunas de las debilidades identificadas en el enemigo.

El día 14 de agosto recibieron un ataque sorpresa que recorrió el mundo.
Su precipitada respuesta fue comentada, muy minuciosamente en despachos
importantes. Estaban perdiendo credibilidad y poder a pasos agigantados.
Nadie creía en la victoria. Sacaban su dinero de acciones y bancos, de
esos lugares que creían tan seguros.

Estaban gravemente infectados por parásitos que vivían a su costa y
se iban de la lengua, libraban luchas internas... El enorme gigante
tenía pies de barro.
