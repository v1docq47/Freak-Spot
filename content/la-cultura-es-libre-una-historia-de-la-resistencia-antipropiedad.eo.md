Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2022-08-05 11:47
Modified: 2024-01-02 19:52
Lang: eo
Slug: libro-sobre-la-historia-de-la-cultura-libre
Save_as: libro-pri-la-historio-de-la-libera-kulturo/index.html
URL: libro-pri-la-historio-de-la-libera-kulturo/
Tags: libera kulturo, EPUB, HTML, libro, PDF, traduko
Title: Libro pri la historio de la libera kulturo
Image: <img src="/wp-content/uploads/2022/01/imagen-portada-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad.png" alt="" width="980" height="501" srcset="/wp-content/uploads/2022/01/imagen-portada-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad.png 980w, /wp-content/uploads/2022/01/imagen-portada-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad-490x250.png 490w" sizes="(max-width: 980px) 100vw, 980px">

Mi tradukis al Esperanto la libron verkitan en la portugala <a href="https://baixacultura.org/download/14753/"><cite>A
Cultura é Livre: Uma história da resistência
antipropriedade</cite></a> (<cite>La kulturo estas libera: historio de
la kontraŭproprieta rezisto</cite>). Mi disponebligas la tradukon: [vi povas
elŝuti aŭ konsulti
ĝin](https://freakspot.net/eo/libro/la-kulturo-estas-libera/) en kelkaj
dosierformoj.

Mi ŝatus, ke la Esperanta traduko ankaŭ estu disponebla kiel presita
libro, sed antaŭe mi devas plibonigi la PDF-aranĝon.
Mi kreas la libron konvertante Markdown-dosierojn per
[Pandoc](https://pandoc.org/), vi povas tute vidi kiel en la [fontkodo de
la projekto](https://notabug.org/jorgesumle/la-kulturo-estas-libera-historio-de-kontrauproprieta-rezisto).
