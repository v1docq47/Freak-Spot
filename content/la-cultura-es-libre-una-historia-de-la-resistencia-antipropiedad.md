Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2022-01-07 16:20
Lang: es
Slug: libro-sobre-la-historia-de-la-cultura-libre
Tags: cultura libre, EPUB, HTML, libro, PDF, traducción
Title: Libro sobre la historia de la cultura libre
Image: <img src="/wp-content/uploads/2022/01/imagen-portada-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad.png" alt="" width="980" height="501" srcset="/wp-content/uploads/2022/01/imagen-portada-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad.png 980w, /wp-content/uploads/2022/01/imagen-portada-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad-490x250.png 490w" sizes="(max-width: 980px) 100vw, 980px">

He traducido al español el libro sobre cultura libre escrito en
portugués <a href="https://baixacultura.org/download/14753/"><cite>A
Cultura é Livre: Uma história da resistência
antipropriedade</cite></a> (<cite>La cultura es libre: una historia de
la resistencia antipropiedad</cite>). Pongo a vuestra disposición el
libro traducido: [podéis descargarlo o
consultarlo](https://freakspot.net/libro/la-cultura-es-libre/) en varios
formatos.

Me gustaría que la traducción española también estuviera disponible como
libro impreso, pero antes tengo que mejorar el formato del PDF. Genero
el libro a partir de archivos Markdown con
[pandoc](https://pandoc.org/), podéis ver exactamente cómo en el [código
fuente del proyecto](https://notabug.org/jorgesumle/la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad).
