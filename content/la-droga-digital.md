Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2019-09-26
Image: <img src="/wp-content/uploads/2019/09/foto-para-las-redes-sociales.jpg" alt="" width="1024" height="687" srcset="/wp-content/uploads/2019/09/foto-para-las-redes-sociales.jpg 1024w, /wp-content/uploads/2019/09/foto-para-las-redes-sociales-512x343.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">
Lang: es
Slug: la-droga-digital-redes-sociales
Tags: adicción, redes sociales
Title: La droga digital: redes sociales

Chutes de dopamina instantáneos con redes sociales: esto no puede ser
malo, ¿no?

<!-- more -->

Cuando envías mensajes a «amigos» en redes sociales, cuando recibes un
«me gusta»...
[te sientes bien](https://bioelementa.com/redes-sociales-dopamina).
Estas plataformas están diseñadas para que permanezcas el mayor tiempo
posible en ellas, y así poder influirte políticamente, socialmente y que
compres lo que sus anunciantes quieren.

Están tan bien diseñadas que crean adicción con un bucle de
retroalimentación de dopamina. El tabaco, el alcohol, la cocaína y el
juego también generan adicción, pero a diferencia de estas actividades,
los jóvenes tienen ahora un facilísimo acceso a esta droga socialmente
aceptada, en una etapa de su desarrollo en la que van a adquirir hábitos
que les acompañarán a lo largo de su vida.

Consecuencias: pierden habilidades sociales, no saben enfrentarse a
situaciones duras o estresantes, problemas oculares, dificultades en los
estudios, etc. Es muy tentador refugiarse en el placer instantáneo de
las redes sociales acercándose el móvil a la cara.
