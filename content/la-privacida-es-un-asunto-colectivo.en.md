Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2023-01-30 15:00
Image: <img src="/wp-content/uploads/2019/12/graffiti-camara-vigilancia.jpg" alt="Graffiti of a security camera">
Lang: en
Slug: la-privacidad-es-un-asunto-colectivo
Save_as: Privacy-is-a-collective-issue/index.html
URL: Privacy-is-a-collective-issue/
Tags: advice, privacy
Title: Privacy is a collective issue

Many people give a personal explanation as to why they do or do not
protect their privacy. Those who don't care much are heard to say that
they have nothing to hide. Those who do care do so to protect themselves
from unscrupulous companies, repressive states, etc. In both positions
it is often wrongly assumed that privacy is a personal matter, and it is
not.

Privacy is both an individual and a public matter. Data collected by
large companies and governments is rarely used on an individual basis.
We can understand privacy as a right of the individual in relation to
the community, as [Edward Snowden](https://en.wikipedia.org/wiki/Edward_Snowden) says:

> Arguing that you don't care about the right to privacy because you have
> nothing to hide is no different than saying you don't care about free
> speech because you have nothing to say.

Your data can be used for good or bad. Data collected unnecessarily and
without permission is often used for bad.

States and big tech companies blatantly violate our privacy. Many people
tacitly acquiesce by arguing that nothing can be done to change it:
companies have too much power and governments won't do anything to
change things. And, certainly, those people are used to giving power to
companies that make money from their data and are thus telling states
that they are not going to be a thorn in their side when they want to
implement mass surveillance policies. In the end, it harms the privacy
of those who care.

Collective action starts with the individual. Each person should reflect
on whether they are giving out data about themselves that they should
not, whether they are encouraging the growth of anti-privacy companies
and, most importantly, whether they are compromising the privacy of
those close to them. **The best way to protect private information is not
to give it out**. With an awareness of the problem, privacy projects can
be supported.

Personal data is very valuable &mdash; so much so that some call it the “new
oil” &mdash; not only because it can be sold to third parties, but also
because it gives power to those who hold it. When we give it to
governments, we give them the power to control us. When we give them to
companies, we are giving them power to influence our behaviour.
Ultimately, privacy matters because it helps us preserve the power we
have over our lives that they are so intent on taking away. I'm not
going to give away or sell my data, are you?
