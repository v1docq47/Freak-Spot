Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2024-01-12 00:19
Lang: eo
Slug: jamás-pagues-con-tarjeta
Save_as: neniam-pagu-per-karto/index.html
URL: neniam-pagu-per-karto/
Tags: financoj
Title: Neniam pagu per karto
Image: <img src="/wp-content/uploads/2024/01/privacidad-financiera.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2024/01/privacidad-financiera.png 1920w, /wp-content/uploads/2024/01/privacidad-financiera-960x540.png 960w, /wp-content/uploads/2024/01/privacidad-financiera-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Bankoj ŝategas vendi al vi kartojn. Ne falu en kaptilon: pagi per kartoj
entenas multajn danĝerojn kaj kromajn kostojn.

<!-- more -->

## Vi elspezos pli

Multaj esploroj[^1] pruvis, ke homoj, kiuj pagas per karto, pli
elspezas.
Kiam vi pagas per kontanta mono, vi vidas la monon, kion vi elspezos per
viaj propraj okuloj kaj konsumas malpli impulse. Per karto, aliflanke,
la kvanto, kiun vi elspezas, estas neĉefa; vi ne devas kalkuli biletojn
nek pripensi, kiom da mono restas en via monujo, vi simple uzas la
karton sen pensi.

Al tio oni devas aldoni tion, kion bankoj postulas por uzi kartojn, kaj
la kostojn de kreditaj pagoj.

<h2 id="sen-privateco">Forgesu pri privateco</h2>

Bankoj vendas viajn financajn informojn al aliaj firmaoj. Eĉ se ili ne
farus tion, multaj bankistoj kaj teknologiaj firmaoj povas vidi vian
financajn aktivaĵojn. Kiam vi aĉetas per karto, la datenoj de la aĉeto,
kiel la paga horo, la produkto aŭ la kvanto, ĉesas esti privataj.

Tiuj datenoj povas esti uzataj por scii, kiom da viaj enspezoj vi
elspezas por manĝaĵoj, ŝatokupoj, drogoj, ktp. Se vi havas firmaon, tiuj
datenoj povus esti aĉetitaj aŭ ŝtelitaj per kodrompistoj, kaj viaj
konkurantoj povus akiri kaj uzi ilin por venki vin en prezmilito,
ekzemple.

## Baroj kaj dependeco

Malsame ol kontanta mono, kiun ne eblas distance kontroli, kartoj povas
esti malŝaltitaj aŭ blokitaj fare de bankoj iam ajn. Krome, se ne estas
elektro nek Interreto, ne eblas pagi.

## Ĉiuj pagos pli da impostoj

Firmaoj, kiuj akceptas bankkartajn pagojn, ne povas kaŝi ilin al la
registaro. Sekve, ili pagas pli da impostoj, kiuj estos uzataj por
financi militojn aŭ aliajn malpopularajn politikojn. Kiam vi pagas per
karto, vi perdas povon, donante ĝin al la banko kaj al la registaro.

## Konkludo

Kartoj igas vin pli malriĉa, pli dependa, malpli pova, kaj vi havos
malpli da privateco. Tial mi rekomendas pagi per kontanta mono aŭ
ĉifrovalutoj, kiuj ebligas privatajn transakciojn, kiel Monero. Kartoj
ne estus problemaj, se pagoj estus anonimaj kaj ne povus esti blokitaj
aŭ limigitaj fare de banko.

[^1]: Banker, S., Dunfield, D., Huang, A. <i lang="lt">et al.</i> Neural mechanisms of credit card spending. <cite>Sci Rep, 11</cite>, 4070 (2021). <https://doi.org/10.1038/s41598-021-83488-3>
