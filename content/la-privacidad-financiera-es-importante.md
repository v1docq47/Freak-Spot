Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2024-01-11 10:00
Lang: es
Slug: jamás-pagues-con-tarjeta
Tags: finanzas
Title: Por qué jamás debes pagar con tarjeta
Image: <img src="/wp-content/uploads/2024/01/privacidad-financiera.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2024/01/privacidad-financiera.png 1920w, /wp-content/uploads/2024/01/privacidad-financiera-960x540.png 960w, /wp-content/uploads/2024/01/privacidad-financiera-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

A los bancos les encanta venderte tarjetas. No caigas en la trampa:
pagar con tarjeta entraña un montón de peligros y costes extra.

<!-- more -->

## Siempre gastarás más

Numerosos estudios[^1] muestran que la gente que paga con tarjeta gasta
más. Cuando pagas en efectivo, ves el dinero que vas a gastar con tus
propios ojos y consumes de forma menos impulsiva. Con tarjeta, por otro
lado, la cantidad que gastas queda en segundo plano; no tienes que
contar billetes ni pensar en cuánto dinero te queda en la cartera,
simplemente pasas la tarjeta sin pensar.

A eso hay que sumarle lo que cobran los bancos por usar tarjetas y las
comisiones cuando pagas a crédito.

<h2 id="falta-de-privacidad">Falta de privacidad</h2>

Los bancos venden tu información financiera a otras empresas. Aunque no
lo hicieran, muchos banqueros y empresas de informática pueden ver tu
actividad financiera. Cuando compras con tarjeta, los datos de la compra
como la hora del pago, el producto o la cantidad dejan de ser privados.

Esos datos pueden ser usados para saber qué porcentaje de tus ingresos
gastas en comida, en ocio, en drogas, etc. Si tienes una empresa, esos
datos pueden ser comprados o jaqueados, llegando a manos de la competencia,
que los podrá usar para vencerte en una [guerra de
precios](https://es.wikipedia.org/wiki/Guerra_de_precios), por ejemplo.

## Te pueden dejar con el culo al aire

A diferencia del dinero en efectivo, que no puede ser controlado
remotamente, las tarjetas pueden ser desactivadas o bloqueadas por los
bancos en cualquier momento. Además, si no hay electricidad ni Internet,
no puedes pagar.

## Todo el mundo paga más impuestos

Los negocios que aceptan pagos con tarjeta no pueden ocultárselos al
gobierno. Así pues, acaban pagando más impuestos, que serán usados
para financiar guerras u otro tipo de políticas impopulares. Cuando
pagas con tarjeta, haces que el banco y el gobierno ganen poder y tú lo
pierdas.

## Conclusión

Las tarjetas te dejan más pobre, más dependiente, menos poderoso y con
menos privacidad. Por ello recomiendo pagar en efectivo o con
criptomonedas que permiten transacciones privadas, como
[Monero](https://www.getmonero.org/es/index.html). Las tarjetas no
serían tan problemáticas si los pagos fueran anónimos y no pudieran ser
bloqueados o restringidos por el banco.

[^1]: Banker, S., Dunfield, D., Huang, A. <i lang="lt">et al.</i> Neural mechanisms of credit card spending. <cite>Sci Rep, 11</cite>, 4070 (2021). <https://doi.org/10.1038/s41598-021-83488-3>
