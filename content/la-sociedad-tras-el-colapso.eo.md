Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2018-09-15 23:48
Lang: eo
Modified: 2022-10-19 19:30
Save_as: socio-post-disfalo/index.html
Slug: la-sociedad-tras-el-colapso
Tags: civilizo, estonteco, disfalo, nuclea milito, prozo
Title: La socio post la disfalo
URL: socio-post-disfalo/

Disfalita la civilizo, la manieroj de kompreni la vivon aliiĝis
radikale, kaj bone kaj malbone.

La landoj militis en ferocaj militoj por ŝteli la risurcojn al aliaj
regionoj, per senespera klopodo por konservi siajn riĉajn vivmanierojn.
Malgraŭe la regionoj kaj landoj plej senavantaĝaj rezistis, kaj ili
malfaciliĝis iliajn celojn, kiujn la socio malaprobis, do iamaniere la
grandparton da risurcoj partigis la reginta klaso. La sklaveco kaj la
genocido okazis en multaj landoj kaj regionoj. Sed la tensio neniam
ĉesis.

Unu monaton, en la jaro 2050, oni faligis 18 atombombojn en kelkajn
partojn de la mondo, kiuj formortigis multajn speciojn kaj malpliigis la
homan populacion. La atombombojn estis senkompate faligitaj de potencaj
nacioj unu al alia. Eble multaj faris tion pri rabio kaj kiel lasta
montro de sia potenco, kvankam ili malaperus iujn minutojn poste; ĉiam
fidelaj al sia nacio, ili efektivigis la ordojn.

<!-- more -->

La detruo estis preskaŭ tuta. Neniu granda urbo staris, kaj kiuj staris
iĝis inferno por siaj loĝantoj. Ne estis ebla daŭri la urbana
vivmaniero, kiam kio ĝin ebligas malaperis; ne restis akvon, manĝojn,
elektron...

Batalis du idealoj: la anarkista kaj la aŭtoritatisma. Kiam malplenigis
la akva rezervoj kaj la homoj ne povis sati iliajn bazajn necesaĵojn, la
tensio ne povis esti pli granda.

Kompreneble en multaj urboj ekis la pugno por la lastaj risurcoj, kie la
homoj, malesperaj, izolitaj kaj antaŭ sklavigitaj nur lernis sekvi siajn
bazajn instinktojn, farante ion por supervivi.

Jam ne estis ŝtatoj nek armeoj de robotoj nek monopolaj korporacioj. La
grandparto da homoj kiu supervivis, kiam la bomboj faligis, mortis iom
poste per radiadaj malsanego aŭ vundo. Nenio volis scii pri la antikvan
sistemon, sed tamen aperis kelkajn aŭtoritatismaj grupoj, kiuj fondis
etaj diktatorecoj kaj destruis la nemultaj vivaj ĉirkaŭaĵoj, ili fosis
sian tombon.

En ejoj malpli loĝataj kaj kie malpli influo havis la ideologio, kiu
alportis al la mondo al la bordo de ĝia tuta detruo, grupetoj trovis la
okazon por disvolvi tute sen la premo kaj la daŭraj atakoj de la
sistemo, kiu jam ne ekzistis. Ili mem konis la diktaturoj; ili klare
scias, ke la nura formo de supervivi, feliĉi kaj vivi en paco estis
laborante komune, sen hierarĥioj nek absoluta povo.

La malgrandaj komunumoj frue rekonstruiĝis kaj plenumigis la valorojn de
la solidareco kaj interhelpo. La decidoj estis farataj asemblee, penante
ĉiam apliki samopiniiĝon. Kontraŭe al la etaj diktaturoj kaj aliaj
aŭtoritatismaj grupoj, cirkaŭ la anarkistaj komunumetoj, la naturo
rekreskis el la cindroj.

Oni bezonis kelkajn jarojn pli por establi la komunan modelon. La
komunumetoj tiam estis asociaj federacie, etendinte sin je grando de
landoj. Ili Kunlaboris por la defendo kontraŭ eventualaj agresoj de
rabeguloj de la malmultaj aŭtoritatismaj grupoj, kiuj maltimis ataki iun
vilaĝon. Ili estas establinte novan modelon de socio demonstrinte ĝian
validecon en la praktiko.

Ili estas rekonstruinte la teĥnologion, la kulturon kaj la proprajn
valuojn de sia socio. Antaŭ la terura nuklea milito ekzistis uloj, kiuj
ankaŭ havis iliajn valuojn, sed estis malplimulto aŭ estis premitaj.
Ili Rehavigis tiun scion enhavigita en formo de libroj, iloj, liberaj
programaroj, apertaj desegnoj... Malmulto restis de la alio. Eble kelkaj
memoraparatoj de ajn hostilo enhavis ion, sed multfoje estis ciferigitaj
kaj ne eblis legi ĝin aŭ uzi ĝin sen pasvorto, tio okazis plejfoje je la
enhavo limigita per DRM[^*] kaj proprieta programaro.

La socio multe ŝanĝiĝis, kaj la individuo kaj la komunumo plene
disvolviĝis sen limigo. Ĉi-okaze respektinte la biomedion: disvolvinte la
veganismon, la ekologian terkulturon kaj ebligante la bestan liberigon de
la homa premo.

Malmultaj homoj de mia generacio povas kompreni ĉi tiun novan projekto,
kiun hodiaŭ mi skribas. Kaj pravas, ke mulmultaj homoj supervivis, kaj
ke estis kiuj farigis la bazon de ĉi tiu nova societo... Ĝi estas nur la
impreso, kiun mi havas, vivinte ĉi du historiajn etapojn antagonismajn.
La subpremanta klaso ne imagis, kio okazu: ili pensis pri sia stabileco
en la sistemo, pri la konsumo de aĉetajoj, pri ĝoji ene de si mem,
senzorge, pri kiu okazis cirkaŭ ili; ne estis grava por ili, ke por vivi
tiamaniere ili devis detrui la vivon, ili devis labori pli, ili devis
administri la morton.

Foje estas malfacile por mi kredi, ke ĉion kion mi vidis kaj vivis vere
okazis. Defalis sistemo, kiu ŝajnis senfina. Mi ĝojas, ke mi
helpis finigi ĝin, kaj ke mi ege kontribuis por krei ĉi tiun novan
socion. Mi esperas, ke ĉi tiu rakonto, kiun mi nun al vi diras, ne
estu forgesita, kaj utilu por ke vi ne faru la pasintajn erarojn.

[^*]: DRM estas la angla mallongigo de *Digital Rights Management*,
cifereca administrado rajtoj. Ĝi signifas la teknologioj uzitaj per
malebligi, ke la scio estis atingita kaj havigita. Por kio, iuj homoj
Esperante nomis ironie al sistemo cifereca administrado de limigoj.
