Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-03-27 17:16
Lang: es
Slug: la-Union-Europea-quiere-analizar-las-comunicaciones-antes-de-que-sean-cifradas
Tags: privacidad, Unión Europea, vigilancia
Title: La Unión Europea quiere analizar las comunicaciones antes de que sean cifradas

La Comisión Europea podría obligar a analizar los mensajes de servicios
de mensajería antes de ser cifrados y enviados. Esta ley, cuyos detalles
aún se desconocen y que será presentada el 30 de marzo, pretende
combatir el abuso infantil. En la práctica esto equivaldría a que se
abrieran las cartas antes de ser enviadas y a que todo el mundo fuera
considerado sospechoso de abuso infantil.

Hay quienes consideran que la ley puede conducir a una vigilancia masiva
y al fin del derecho al secreto de las comunicaciones.
