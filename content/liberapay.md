Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2017-11-10 11:08
Image: <img src="/wp-content/uploads/2017/11/liberapay.png" alt="Liberapay es una plataforma de donaciones recurrentes. Ayudamos a financiar a los creadores y a los proyectos que aprecias." width="1256" height="594" srcset="/wp-content/uploads/2017/11/liberapay.png 1256w, /wp-content/uploads/2017/11/liberapay-628x297.png 628w" sizes="(max-width: 1256px) 100vw, 1256px">
Lang: es
Slug: liberapay
Tags: donaciones recurrentes, Liberapay, página web, software libre
Title: Liberapay

Financiar el procomún no es una tarea sencilla en una sociedad
capitalista. Para la cultura libre y el <i lang="en">software</i> libre es posible
aplicar el enfoque de empresas, cooperativas u organizaciones sin ánimo
de lucro.

Cualquiera que sea la forma de organizarse, un proyecto necesita dinero
o voluntarias para llevarse a cabo. En este sentido es útil contar con
donaciones recurrentes. ¿Pero cómo? Hay muchas formas de lograr
donaciones recurrentes; en este artículo hablaré de una de ellas:
el uso de Liberapay.

<!-- more -->

Liberapay es una plataforma de donaciones recurrentes que proporciona un
servicio para dar dinero a equipos, organizaciones y personas.
Liberapay está desarrollado bajo el marco de una asociación sin ánimo de
lucro ubicada en Francia, así que no existen tasas por el uso de su
servicio. El trabajo y los costes del proyecto se financian [a través
del propio Liberapay](https://liberapay.com/Liberapay/). Las únicas
tasas que existen vienen de su procesador de pagos, se cobran únicamente
al añadir dinero a Liberapay.

Para donar dinero a proyectos de Liberapay solo hace falta crear una
cuenta, añadirle dinero (mediante pago domiciliado, tarjeta de crédito o
transferencia bancaria) y elegir la cantidad a donar a los proyectos de
Liberapay que nos gusten.

Aceptar donaciones es también sencillo. Solamente debemos crear una
cuenta de Liberapay y redactar información sobre el proyecto o los
proyectos que desarrollamos. Las cuentas de equipo son un poco
diferentes. Las donaciones hechas a equipos se reparten entre sus
integrantes, que deciden cuánto tomar de ese dinero. El dinero restante
que no es reclamado por ninguna integrante se acumula para futuras
semanas (los pagos en Liberapay son semanales).

Ahora mismo Liberapay se limita a euros, pero en un futuro soportará
dólares americanos, bitcoins y otras monedas.

Hay muchos [equipos que están recibiendo dinero con
Liberapay](https://liberapay.com/explore/teams), desde <i lang="en">software</i> hasta
cómics. Pero Liberapay también tiene sus limitaciones: por ejemplo, no
permite donar algo que no sea dinero ni permite hacer pública la
procedencia de la donación.

En el futuro Liberapay cambiará. Lo más probable es que muchas de las
limitaciones explicadas en este artículo se resuelvan. Como el código
fuente es <i lang="en">software</i> libre, cualquiera puede proponer una nueva
funcionalidad o solucionar un problema o limitación.
