Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2022-11-19 17:22
Lang: en
Slug: los-estúpidos-códigos-qr-en-restaurantes
Save_as: the-stupid-QR-codes-in-restaurants/index.html
URL: the-stupid-QR-codes-in-restaurants/
Tags: QR code, restaurants
Title: The stupid QR codes in restaurantes
Image: <img src="/wp-content/uploads/2022/11/hombre-chupando-móvil.png" alt="Man licking a phone" width="960" height="1200" srcset="/wp-content/uploads/2022/11/hombre-chupando-móvil.png 960w, /wp-content/uploads/2022/11/hombre-chupando-móvil-480x600.png 480w" sizes="(max-width: 960px) 100vw, 960px">

Before, you could go to a restaurant, look at the menu and order:
it was as simple as that. Now many places don't even have a physical
menu anymore; they assume the customer has a “smart” phone and an
internet connection. If this is the case, the customer is expected to
use the camera and scan the
[QR code](https://en.wikipedia.org/wiki/QR_code), which takes them to a
web page that does not respect their privacy, often takes a long time to
load and, in many cases, is not very intuitive.

## It is inefficient, it pollutes more

Loading a web page with images from a remote server, for every customer,
is polluting. With a physical menu, no electricity is wasted, people can
reuse the menu indefinitely... If there is no Internet or you have no
battery, how do you read the menu with the QR?

## Without privacy

When we visit a website we leave a digital footprint. If we use a QR
codes to consult the menu, there are companies, governments, etc., that
can know that we have read the menu of a specific restaurant at a
specific time.

Customers also lose their privacy when they pay by card instead of using
cash, but that's another issue.

<a href="/wp-content/uploads/2022/11/no-thanks-QR-restaurants.png">
<img src="/wp-content/uploads/2022/11/no-thanks-QR-restaurants.png" alt="" width="1200" height="626" srcset="/wp-content/uploads/2022/11/no-thanks-QR-restaurants.png 1200w, /wp-content/uploads/2022/11/no-thanks-QR-restaurants-600x313.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

## Better without QR-codes

I don't have a “smart” phone and I don't like restaurants. If I
eat in a restaurant, I ask for the physical menu. If they don't give it
to me, they have to tell me the menu, because I have no way of seeing the QR
code. Most restaurant food is unhealthy, the workers are often
exploited, a lot of food is wasted, there are few vegan options, and so
on. The restaurant industry has many problems. The use of QR codes
for menus is just another step in the wrong direction, but it's very
easy to combat by refusing to use a “smart” phone to scan a stupid QR
code.
