Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2022-11-06 15:55
Lang: eo
Slug: los-estúpidos-códigos-qr-en-restaurantes
Save_as: la-stultaj-QR-kodoj-en-restoracioj/index.html
URL: la-stultaj-QR-kodoj-en-restoracioj/
Tags: QR-kodo, restaurantes
Title: La stultaj QR-kodoj en restoracioj
Image: <img src="/wp-content/uploads/2022/11/hombre-chupando-móvil.png" alt="Hombre chupando móvil" width="960" height="1200" srcset="/wp-content/uploads/2022/11/hombre-chupando-móvil.png 960w, /wp-content/uploads/2022/11/hombre-chupando-móvil-480x600.png 480w" sizes="(max-width: 960px) 100vw, 960px">

Antaŭe kutimis iri al restoracio, legi la menuon kaj mendi: tiel simple.
Nun multaj ejoj eĉ ne havas fizikan menuon; ili antaŭsupozas, ke
la kliento havas «saĝtelefonon» kaj Interretan konekton. Se tio okazas,
oni atendas, ke la kliento uzu la fotilon kaj skanu la
[QR-kodon](https://eo.wikipedia.org/wiki/Rapidresponda_kodo), kiu
kondukas al retejo, kiu ne respektas la privatecon, kutimas malfrue
ŝarĝiĝi kaj, en multaj okazoj, estas malmulte ergonomia.

## Ĝi estas malrendimeta, poluas pli

Ŝarĝi retejon kun bildoj el fora servilo, por ĉiu kliento, poluas. Per
fizika menuo oni ne malŝparas elektron, la homoj povas daŭre reuzi la
menuon... Se ne estas Interreto aŭ via baterio malplenas, kiel vi vidas
la menuon per la QR?

## Sen privateco

Kiam ni vizitas retejon, ni lasas ciferecan spuron. Se ni uzas la
QR-kodojn por vidi la menuon, estas firmaoj, registaroj, ktp., kiuj
povas scii je kioma preciza horo ni legis la menuon de preciza
restoracio.

La klientoj ankaŭ [perdas sian privatecon](/eo/neniam-pagu-per-karto/#sen-privateco), kiam ili pagas per kreditkarto
anstataŭ uzi kontantan monon, sed tio estas alia temo.

<a href="/wp-content/uploads/2022/11/ne-dankon-QR-restoracioj.png">
<img src="/wp-content/uploads/2022/11/ne-dankon-QR-restoracioj.png" alt="" width="1200" height="626" srcset="/wp-content/uploads/2022/11/ne-dankon-QR-restoracioj.png 1200w, /wp-content/uploads/2022/11/ne-dankon-QR-restoracioj-600x313.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

## Pli bone sen QR

Mi ne havas «saĝtelefonon», nek mi ŝatas restoraciojn. Se mi manĝas en
restoracio, mi petas la fizikan menuon. Se ili ne donas ĝin al mi, ili
devas diri ĝin al mi, ĉar mi neniel povas skani la QR-kodon. La
plejparto de la manĝaĵo de la restoracioj estas malsana, la laboristoj
kutime estas ekspluatitaj, oni forĵetas multe da manĝaĵo, estas
malmultaj veganaj opcioj, ktp. La restoracia industrio havas multe da
problemoj. La uzado de la QR-kodo por la menuo estas nur unu paŝo pli al
malbona direkto, sed pli facile batalebla neante uzi «saĝtelefonon» por
skani stultan QR-kodon.
