Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2022-11-06 13:00
Modified: 2024-01-11 10:32
Lang: es
Slug: los-estúpidos-códigos-qr-en-restaurantes
Tags: código QR, restaurantes
Title: Los estúpidos códigos QR en restaurantes
Image: <img src="/wp-content/uploads/2022/11/hombre-chupando-móvil.png" alt="Hombre chupando móvil" width="960" height="1200" srcset="/wp-content/uploads/2022/11/hombre-chupando-móvil.png 960w, /wp-content/uploads/2022/11/hombre-chupando-móvil-480x600.png 480w" sizes="(max-width: 960px) 100vw, 960px">

Antes era común ir a un restaurante, ojear el menú y pedir: así de
simple. Ahora en muchos sitios ya ni tienen un menú físico; asumen que
el cliente tiene un móvil «inteligente» y conexión a Internet. Si se
cumple el caso, se espera que el cliente use la cámara de fotos y
escanee el código QR, que le lleva a una página web que no respeta la
privacidad, suele tardar tiempo en cargar y, en muchos casos, es poco
intuitiva.

## Es ineficiente, contamina más

Cargar una página web con imágenes en un servidor remoto, por cada
cliente, es contaminante. Con un menú físico, no se gasta electricidad,
la gente puede reutilizar el menú indefinidamente... Si no hay Internet
o no tienes batería, ¿cómo consultas el menú con el QR?

## Sin privacidad

Cuando visitamos una página web dejamos una huella digital. Si usamos
los códigos QR para consultar el menú, hay empresas, gobiernos, etc.,
que pueden saber que a tal hora concreta hemos consultado el menú de un
restaurante concreto.

Los clientes también [pierden su privacidad cuando pagan con tarjeta](/jamás-pagues-con-tarjeta/#falta-de-privacidad) en
vez de usar dinero en efectivo, pero eso es otro tema.

<a href="/wp-content/uploads/2022/11/no-gracias-QR-restaurantes.png">
<img src="/wp-content/uploads/2022/11/no-gracias-QR-restaurantes.png" alt="Menú con QR, no gracias" width="1200" height="626" srcset="/wp-content/uploads/2022/11/no-gracias-QR-restaurantes.png 1200w, /wp-content/uploads/2022/11/no-gracias-QR-restaurantes-600x313.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

## Mejor sin QR

No tengo un móvil «inteligente» ni me gustan los restaurantes. Si como
en un restaurante, pido el menú físico. Si no me lo dan, me lo tienen
que decir, porque no tengo manera de ver el código QR. La mayoría de la
comida de los restaurantes es insana, los trabajadores suelen estar
explotados, se desperdicia mucha comida, hay pocas opciones veganas,
etc. La industria de la hostelería tiene muchos problemas. El uso del
código QR para los menús es solo un paso más en la dirección errónea,
pero muy fácil de combatir negándose a usar un móvil «inteligente» para
escanear un estúpido código QR.
