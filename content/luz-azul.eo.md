Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2023-01-31 15:50
Lang: eo
Slug: luz-roja
Save_as: ruĝa-lumo-poemo/index.html
URL: ruĝa-lumo-poemo/
Tags: verso
Title: Ruĝa lumo
JS: quiere-morir.js (bottom)

Merlo, fiksu sur mi vian rigardon.  
Kaŝtanarbo, fiksu sur mi viajn ekinojn.  
Kunprenu min for de la blua lumo.

Estas loko, kie la luno lumas,  
kie flustras vian nomon la nokto,  
kiu petas al ni, ke ni kunigu niajn korpojn.  
Ĉu vi sentas la ombrojn, kiuj brulas?  
Ĉu vi aŭdas la kanton de la arbaro?

Ruĝan lumon vivigu.  
Bluan lumon mortigu.
