Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2022-05-05 20:00
Lang: es
Slug: manifiesto-por-la-guerrilla-del-acceso-abierto
Tags: acceso abierto
Title: Manifiesto por la guerrilla del acceso abierto
Image: <img src="/wp-content/uploads/2022/05/swartz-en-un-árbol-manifiesto-por-la-guerrila-del-acceso-abierto.png" alt="">

La información es poder. Pero como todo poder, hay quienes quieren
preservarlo solo para ellos. Todo el patrimonio cultural y científico
del mundo, publicado durante siglos en libros y publicaciones, está
siendo digitalizado y cerrado por un puñado de empresas privadas.
¿Quieres leer publicaciones que presentan los resultados científicos más
conocidos? Tendrás que enviarle un montón de dinero a editoriales
como Reed Elsevier.

Hay quienes luchan por cambiar esto. El movimiento por el acceso
abierto ha luchado valientemente para asegurarse de que los científicos
no cedan su derecho de autor, sino que en su lugar se aseguren de que su
trabajo se publique en Internet, bajo términos que permitan su acceso a
cualquiera. Pero incluso en los mejores escenarios, su trabajo solo
servirá para cosas que se publiquen en el futuro. Todo lo que existe
hasta este momento se habrá perdido.

Ese es un precio muy alto por el que pagar. ¿Obligar a los académicos a
pagar dinero para leer el trabajo de sus colegas? ¿Escanear
bibliotecas enteras y solo permitir leerlas a la gente en Google?
¿Proporcionar artículos científicos a quienes están en universidades
selectas en el primer mundo y no a los niños del sur global? Es
indignante e inaceptable.

«Estoy de acuerdo», dicen muchos, «¿pero qué podemos hacer? Las
empresas tienen los derechos de autor, ganan enormes cantidades de
dinero cobrando por el acceso, y es completamente legal &mdash;no hay
nada que podamos hacer para detenerlas&mdash;». Pero sí hay algo que
podemos hacer, algo que ya se está haciendo: podemos contraatacar.

Vosotros con acceso a estos recursos &mdash;estudiantes, bibliotecarios,
científicos&mdash;, os han dado un privilegio. Podéis alimentaros de
este banquete del conocimiento mientras el resto del mundo no puede
entrar. Pero no es necesario &mdash;de hecho, moralmente no
podéis&mdash; que mantengáis este privilegio solo para vosotros. Tenéis
el deber de compartirlo con el mundo. Y lo habéis hecho: intercambiando
contraseñas con colegas, rellenando solicitudes de descarga para amigos.

Mientras tanto, quienes han sido bloqueados no están de brazos cruzados.
Os habéis colado por agujeros sigilosamente y habéis trepando vallas,
liberando la información encerrada por las editoriales y compartiéndola
con vuestros amigos.

Pero todas estas acciones se llevan a cabo en la oscura y oculta
clandestinidad. Las llaman robo o piratería, como si compartir
la riqueza del conocimiento fuera el equivalente moral de saquear un
barco y asesinar a su tripulación. Pero compartir no es inmoral
&mdash;es un imperativo moral&mdash;. Solo quienes están cegados
por la codicia se negarían a que un amigo hiciera una copia.

Las grandes empresas, por supuesto, están cegadas por la codicia. Las
leyes bajo las que operan lo exigen &mdash;sus accionistas se
rebelarían por menos que eso&mdash;. Y los políticos a los que han
sobornado las respaldan, aprobando leyes que les dan el poder exclusivo
de decidir quién puede hacer copias.

No hay justicia en el cumplimiento de leyes injustas. Es hora de salir a
la luz y, siguiendo la noble tradición de la desobediencia civil,
declarar nuestra oposición a este robo privado de la cultura pública.

Necesitamos tomar la información, dondequiera que esté guardada, hacer
nuestras copias y compartirlas con el mundo. Necesitamos tomar las cosas
que están libres del derecho de autor y añadirlas a este archivo.
Necesitamos comprar bases de datos secretas y ponerlas en la Red.
Necesitamos descargar revistas científicas y subirlas a redes de
intercambio de archivos. Necesitamos pelear por el acceso abierto de
guerrilla.

Con suficientes de nosotros, alrededor del mundo, no solo enviaremos un
mensaje fuerte que se oponga a la privatización del conocimiento;
haremos que sea una cosa del pasado. ¿Te unes a nosotros?

Aaron Swartz

Julio de 2008, Eremo, Italia

