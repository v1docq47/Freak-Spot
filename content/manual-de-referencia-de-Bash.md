Author: Jorge Maldonado Ventura
Category: Bash
Date: 2019-09-01
Image: <img src="/wp-content/uploads/2019/09/Gnu-bash-logo.svg" alt="The Bourne-Again Shell">
Lang: es
Slug: manual-de-referencia-de-Bash
Tags: Bash, cultura libre, documentación, Libremanuals, libro, PDF, traducción
Title: Manual de referencia de Bash

Hace tiempo inicié un [proyecto de traducción](https://notabug.org/jorgesumle/bashrefes)
de la [documentación oficial de Bash](https://www.gnu.org/software/bash/manual/) al castellano.

Pongo a vuestra disposición los frutos de ese trabajo: podéis
[descargar o consultar la documentación](/programas/docs/bash/)
en varios formatos. También podéis [comprar el libro impreso](http://libremanuals.net/bash.html)
que comercializa el proyecto Libremanuals.

<p><small>El <a href="/wp-content/uploads/2019/09/Gnu-bash-logo.svg">logotipo oficial de Bash</a> ha sido publicado por la
<a href="https://www.fsf.org/">Free Software Foundation</a> bajo la licencia <a href="http://artlibre.org/licence/lal/en/" rel="license">Free Art License 1.3</a></small>.</p>
