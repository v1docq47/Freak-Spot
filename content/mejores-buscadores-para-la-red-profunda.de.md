Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2023-02-25 16:16
Lang: de
Slug: mejores-buscadores-para-la-internet-profunda-de-tor-deep-web
Tags: Suchmaschine, dark web, .onion, versteckte Dienst, Tor Browser
Save_as: die-besten-Suchmaschinen-für-das-versteckte-Web-von-Tor/index.html
URL: die-besten-Suchmaschinen-für-das-versteckte-Web-von-Tor/
Title: Die besten Suchmaschinen für das versteckte Web von Tor
Image: <img src="/wp-content/uploads/2023/02/cebollas-moradas.jpeg" alt="" width="1920" height="1280" srcset="/wp-content/uploads/2023/02/cebollas-moradas.jpeg 1920w, /wp-content/uploads/2023/02/cebollas-moradas.-960x640.jpg 960w, /wp-content/uploads/2023/02/cebollas-moradas.-480x320.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Die Suche nach versteckten Diensten für Tor kann ohne die Hilfe einer
Suchmaschine schwierig sein. Glücklicherweise gibt es mehrere davon. Die
Links zu diesen Suchmaschinen funktionieren nur auf Browsern, die den
Zugang zu versteckten Diensten erlauben, wie [Tor
Browser](https://www.torproject.org/de/download/) und Brave.

## 1. [Ahmia](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/)

Diese Suchmaschine ist [freie Software](https://github.com/ahmia) und
hat mehr als 53&nbsp;000 versteckte Dienste indexiert<!-- more -->[^1].
Im Gegensatz zu anderen Suchmaschinen indexiert sie keine
Kinderpornografie[^2].

<a href="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png">
<img src="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png" alt="" width="1000" height="950" srcset="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png 1000w, /wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia-500x475.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

## 2. [Torch](http://torchdeedp3i2jigzjdmfpn5ttjhthh5wbmda2rr3jvqjg5p77c54dqd.onion/)

Diese Suchmaschine behauptet, sie sei die älteste und am längsten
laufende Suchmaschine im versteckten Web. Sie finanziert sich mit
Werbung und hat keine Skrupel Kinderpornografie und andere illegale
Inhalte zu indexieren.

## 3. [Iceberg](http://iceberget6r64etudtzkyh5nanpdsqnkgav5fh72xtvry3jyu5u2r5qd.onion/)

Sie indexiert Inhalte aller Art, wird durch Werbung finanziert, erlaubt
Webseiten zu indexieren und zeigt eine Seitenbewertung an, die auf der
Anzahl der Erwähnungen und der Sicherheit der Nutzer basiert ist.

<figure>
<a href="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png">
<img src="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png" alt="" width="1920" height="1040" srcset="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png 1920w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-960x520.png 960w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-480x260.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption>Bewertung <a href="http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/">des versteckten Dienstes dieser Website</a> auf Iceberg.</figcaption>
</figure>

## 4. [TorLanD](http://torlgu6zhhtwe73fdu76uiswgnkfvukqfujofxjfo7vzoht2rndyhxyd.onion/)

Sie indexiert Inhalte aller Art, wird durch Werbung finanziert und
ermöglicht Webseiten zu indexieren.

<figure>
<a href="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png">
<img src="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png" alt="" width="1000" height="579" srcset="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png 1000w, /wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD-500x289.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">Indexierung des versteckten Dienstes dieser Webseite auf TorLanD.</figcaption>
</figure>

## 5. [Haystak](http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/)

Sie indexieren keine Inhalte, die sie für unmoralisch oder illegal
halten, wie z. B. Kindesmissbrauch und Menschenhandel.  Sie behaupten,
dass sie mehr als 1,5 Milliarden Seiten von 260.000 versteckten Diensten
indexiert haben. Sie haben eine Adresse für Spenden und die Möglichkeit,
für zusätzliche Funktionen zu bezahlen.

<figure>
<a href="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png">
<img src="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png" alt="" width="1507" height="606" srcset="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png 1507w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-753x303.png 753w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-376x151.png 376w" sizes="(max-width: 1507px) 100vw, 1507px">
</a>
    <figcaption class="wp-caption-text">Bezahlte Funktionen von Haystak</figcaption>
</figure>

## 6. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion)

Sie haben eine Seite mit allgemeinen Nutzungsbedingungen, in der die
illegale Nutzung verurteilt wird. Sie finanzieren sich durch Werbung und
das Angebot von Hosting für versteckte Dienste.

## 7. [Ourrealm](http://orealmvxooetglfeguv2vp65a3rig2baq2ljc7jxxs4hsqsrcemkxcad.onion/)

Sie wird durch Werbung finanziert. Ihr Motto lautet "Suche jenseits der
Zensur"[^3].

## 8. [Ondex](http://ondexcylxxxq6vcc62l3r2m6rypohvvymsvqeadhln5mjo73pf6ksjad.onion/)

It is funded by ads and indexes all types of content.

## 9. [Deep Search](http://search7tdrcvri22rieiwgi5g46qnwsesvnubqav2xakhezv4hjzkkad.onion/)

Sie zeigt auf ihrer Startseite versteckte Dienste, die im Trend sind,
indexiert alle Arten von Inhalten und wird durch Anzeigen finanziert.

## 10. [Find Tor](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/)

Sie indexiert alle Arten von Inhalten, ist werbefinanziert und
[behauptet, käuflich zu sein](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/buy).

[^1]: Die Liste ist auf <https://ahmia.fi/onions/> oder
    <http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/onions/> verfügbar.
[^2]: Sie haben eine [öffentliche schwarze Liste von Seiten, die sie nicht indexieren](https://ahmia.fi/documentation/).
    [public blacklist of pages that they don't index](https://ahmia.fi/documentation/).
[^3]: «<i lang="en">Search beyond censorship</i>» auf Englisch.

## Andere Methoden, um Inhalt zu finden

Es gibt Listen von versteckten Diensten, Foren usw. Dieser Artikel listet nur
einige der populärsten Suchmaschinen auf.
