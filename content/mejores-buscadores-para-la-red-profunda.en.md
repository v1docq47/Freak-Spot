Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2023-02-24 15:14
Lang: en
Slug: mejores-buscadores-para-la-internet-profunda-de-tor-deep-web
Tags: search engine, dark web, .onion, hidden service, Tor Browser
Save_as: best-search-engines-for-Tors-dark-web/index.html
URL: best-search-engines-for-Tors-dark-web/
Title: Best search engines for Tor's dark web
Image: <img src="/wp-content/uploads/2023/02/cebollas-moradas.jpeg" alt="" width="1920" height="1280" srcset="/wp-content/uploads/2023/02/cebollas-moradas.jpeg 1920w, /wp-content/uploads/2023/02/cebollas-moradas.-960x640.jpg 960w, /wp-content/uploads/2023/02/cebollas-moradas.-480x320.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Finding hidden services for Tor can be tricky without the help of a
search engine. Fortunately, there are quite a few. Links to these search
engines work only for browsers that allow access to hidden services,
such as the [Tor Browser](https://www.torproject.org/download/) and
Brave.

## 1. [Ahmia](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/)

This search engine is [free software](https://github.com/ahmia) and has
more than 53&nbsp;000 hidden services indexed<!-- more -->[^1]. Unlike
other search engines, it does not index child pornography[^2].

<a href="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png">
<img src="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png" alt="" width="1000" height="950" srcset="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png 1000w, /wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia-500x475.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

## 2. [Torch](http://torchdeedp3i2jigzjdmfpn5ttjhthh5wbmda2rr3jvqjg5p77c54dqd.onion/)

This search engine claims to be the oldest and longest running search
engine on the deep web. It is ad-supported and has no qualms about
indexing child pornography and other illegal content.

## 3. [Iceberg](http://iceberget6r64etudtzkyh5nanpdsqnkgav5fh72xtvry3jyu5u2r5qd.onion/)

It indexes content of all kinds, is funded by advertisements, allows you
to index web pages and displays a page score based on the number of
mentions and user safety.

<figure>
<a href="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png">
<img src="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png" alt="" width="1920" height="1040" srcset="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png 1920w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-960x520.png 960w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-480x260.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption>Score <a href="http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/">of the hidden service of this website</a> on Iceberg.</figcaption>
</figure>

## 4. [TorLanD](http://torlgu6zhhtwe73fdu76uiswgnkfvukqfujofxjfo7vzoht2rndyhxyd.onion/)

It indexes content of all kinds, is funded by ads and allows you to
index web pages.

<figure>
<a href="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png">
<img src="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png" alt="" width="1000" height="579" srcset="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png 1000w, /wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD-500x289.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">Indexing this site's hidden service on TorLanD.</figcaption>
</figure>

## 5. [Haystak](http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/)

They don't index content they consider immoral or illegal, such as child
abuse and human trafficking. They claim to have indexed more than 1.5
billion pages from 260,000 hidden services. They have an address to
receive donations and the option to pay for additional features.

<figure>
<a href="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png">
<img src="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png" alt="" width="1507" height="606" srcset="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png 1507w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-753x303.png 753w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-376x151.png 376w" sizes="(max-width: 1507px) 100vw, 1507px">
</a>
    <figcaption class="wp-caption-text">Haystak's paid features</figcaption>
</figure>

## 6. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion)

They have a terms and conditions page condemning illegal uses. They
finance themselves with ads and by offering hosting for hidden services.

## 7. [Ourrealm](http://orealmvxooetglfeguv2vp65a3rig2baq2ljc7jxxs4hsqsrcemkxcad.onion/)

It's funded by ads. Its motto is «search beyond censorship».

## 8. [Ondex](http://ondexcylxxxq6vcc62l3r2m6rypohvvymsvqeadhln5mjo73pf6ksjad.onion/)

It is funded by ads and indexes all types of content.

## 9. [Deep Search](http://search7tdrcvri22rieiwgi5g46qnwsesvnubqav2xakhezv4hjzkkad.onion/)

It displays trending hidden services on its homepage, it indexes all types
of content and is funded by ads.

## 10. [Find Tor](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/)

It indexes all types of content, is funded by ads and [claims to be for
sale](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/buy).

[^1]: The list is available at <https://ahmia.fi/onions/> or
    <http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/onions/>.
[^2]: It has a
    [public blacklist of pages that they don't index](https://ahmia.fi/documentation/).

## Other options for finding content

There are lists of hidden services, forums etc. This article only lists
some of the most popular search engines.
