Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2023-02-15 00:46
Lang: eo
Slug: mejores-buscadores-para-la-internet-profunda-de-tor-deep-web
Tags: serĉilo, profunta Interreto, .onion, kaŝita servo, Tor Browser
Save_as: la-plej-bonaj-serĉiloj-de-la-profunda-Interreto-de-Tor/index.html
URL: la-plej-bonaj-serĉiloj-de-la-profunda-Interreto-de-Tor/
Title: La plej bonaj serĉiloj de la profunda Interreto de Tor
Image: <img src="/wp-content/uploads/2023/02/cebollas-moradas.jpeg" alt="" width="1920" height="1280" srcset="/wp-content/uploads/2023/02/cebollas-moradas.jpeg 1920w, /wp-content/uploads/2023/02/cebollas-moradas.-960x640.jpg 960w, /wp-content/uploads/2023/02/cebollas-moradas.-480x320.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Trovi kaŝitajn servojn por Tor povas esti komplika afero sen la helpo de
serĉilo. Bonŝance estas kelkaj. La ligiloj al ĉi tiuj serĉiloj nur
funkcias en retumiloj, kiuj ebligas aliri al kaŝitajn servojn, kiel [Tor
Browser](https://www.torproject.org/download/) kaj Brave.

## 1. [Ahmia](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/)

Ĉi tiu serĉilo estas [libera](https://github.com/ahmia) kaj havas pli ol
53&nbsp;000 kaŝitajn servojn <!-- more --> indeksitajn[^1]. Male al
aliaj serĉiloj, ĝi ne indeksas infanan pornografion[^2].

<a href="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png">
<img src="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png" alt="" width="1000" height="950" srcset="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png 1000w, /wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia-500x475.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

## 2. [Torch](http://torchdeedp3i2jigzjdmfpn5ttjhthh5wbmda2rr3jvqjg5p77c54dqd.onion/)

Ĉi tiu serĉilo asertas esti la plej serĉilo malnova kaj tiu, kiu dum
plej da tempo funkcias en la profunda Interreto. Ĝi estas financita per
reklamoj kaj ne hontas indeksi infanan pornografion kaj aliajn specojn de
kontraŭleĝaj enhavoj.

## 3. [Iceberg](http://iceberget6r64etudtzkyh5nanpdsqnkgav5fh72xtvry3jyu5u2r5qd.onion/)

Ĝi indeksas ĉian enhavon. Ĝi estas financita per reklamoj. Ĝi ebligas al
vi indeksi retejojn kaj montras poentojn de la paĝo bazite sur la nombro
de mencioj kaj sur la sekureco por la uzanto.

<figure>
<a href="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png">
<img src="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png" alt="" width="1920" height="1040" srcset="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png 1920w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-960x520.png 960w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-480x260.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption>Poentoj <a href="http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/">de la kaŝita servo de ci tiu retejo</a> en Iceberg.</figcaption>
</figure>

## 4. [TorLanD](http://torlgu6zhhtwe73fdu76uiswgnkfvukqfujofxjfo7vzoht2rndyhxyd.onion/)

Ĝi indeksas ĉian enhavon. Ĝi financas sin per reklamoj. Ĝi ebligas al vi
indeksi retejojn.

<figure>
<a href="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png">
<img src="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png" alt="" width="1000" height="579" srcset="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png 1000w, /wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD-500x289.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">Indeksante la kaŝitan servon de
    ĉi tiu retejo en TorLanD.</figcaption>
</figure>

## 5. [Haystak](http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/)

Ili ne indeksas enhavon, kiun ili konsideras malmoralan kaj
kontraŭleĝan, kiel mistraktadon de infanoj kaj homan ŝakradon. Ili
asertas, ke ili indeksis pli ol 1,5 miliardoj da paĝoj de 260&nbsp;000
kaŝitaj servoj. Ili havas adreson por ricevi donacojn kaj la opcion pagi
por kromaj funkcioj.

<figure>
<a href="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png">
<img src="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png" alt="" width="1507" height="606" srcset="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png 1507w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-753x303.png 753w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-376x151.png 376w" sizes="(max-width: 1507px) 100vw, 1507px">
</a>
    <figcaption class="wp-caption-text">Pagendaj funkcioj de Haystak</figcaption>
</figure>

## 6. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion)

Ili havas paĝon de kondiĉoj pri uzo, en kiu ili kondamnas la
kontraŭleĝajn uzojn. Ili financas sin oferante gastigadon de kaŝitaj
servoj kaj per reklamoj.

## 7. [Ourrealm](http://orealmvxooetglfeguv2vp65a3rig2baq2ljc7jxxs4hsqsrcemkxcad.onion/)

Ĝi estas financita per reklamoj. Ĝia devizo estas «serĉu trans la cenzuro»[^3].

## 8. [Ondex](http://ondexcylxxxq6vcc62l3r2m6rypohvvymsvqeadhln5mjo73pf6ksjad.onion/)

Ĝi estas financita per reklamoj. Ĝi indeksas ĉian enhavon.

## 9. [Deep Search](http://search7tdrcvri22rieiwgi5g46qnwsesvnubqav2xakhezv4hjzkkad.onion/)

Ĝi montras tendencajn kaŝitajn servojn en la ĉefa paĝo. Ĝi indeksas ĉian
enhavon. Ĝi estas financita per reklamoj.

## 10. [Find Tor](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/)

Ĝi indeksas ĉian enhavon. Ĝi estas financita per reklamoj kaj [anoncas, ke ĝi aĉeteblas](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/buy).

[^1]: Oni povas konsulti la liston en <https://ahmia.fi/onions/> aŭ
    <http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/onions/>.
[^2]: Ili havas
    [publikan nigran liston de paĝoj, kiujn ili ne indeksas](https://ahmia.fi/documentation/).
[^3]: <i lang="en">Search beyond censorship</i> en la angla.

## Aliaj opcioj por trovi enhavon

Estas listoj de kaŝitaj servoj, diskutejoj, ktp. Ĉi tiu artikolo nur
prezentas liston de iuj el la plej popularaj serĉiloj.
