Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2023-02-22 10:00
Lang: pt
Slug: mejores-buscadores-para-la-internet-profunda-de-tor-deep-web
Tags: buscador, Internet obscura, .onion, serviço oculto, Tor Browser
Save_as: Os-melhores-buscadores-da-Internet-obscura-do-Tor/index.html
URL: Os-melhores-buscadores-da-Internet-obscura-do-Tor/
Title: Os melhores buscadores da Internet obscura do Tor
Image: <img src="/wp-content/uploads/2023/02/cebollas-moradas.jpeg" alt="" width="1920" height="1280" srcset="/wp-content/uploads/2023/02/cebollas-moradas.jpeg 1920w, /wp-content/uploads/2023/02/cebollas-moradas.-960x640.jpg 960w, /wp-content/uploads/2023/02/cebollas-moradas.-480x320.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Encontrar serviços ocultos para o Tor pode ser complicado sem a ajuda de
um motor de busca. Felizmente, há vários. As ligações a estes motores de
busca funcionam apenas para navegadores que permitem o acesso a serviços
ocultos, tais como [o Tor Browser](https://www.torproject.org/pt-BR/download/) e o Brave.

## 1. [Ahmia](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/)

Este motor de busca [é livre](https://github.com/ahmia) e tem mais de
53&nbsp;000 serviços ocultos indexados<!-- more -->[^1]. Ao contrário de outros
motores de busca, não indexa pornografia infantil[^2].

<a href="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png">
<img src="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png" alt="" width="1000" height="950" srcset="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png 1000w, /wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia-500x475.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

## 2. [Torch](http://torchqsxkllrj2eqaitp5xvcgfeg3g5dr3hr2wnuvnj76bbxkxfiwxqd.onion/)

Este motor de busca afirma ser o motor de busca mais antigo e o que mais
tempo tem estado a funcionar na rede profunda. É apoiado por anúncios e
não tem escrúpulos em indexar pornografia infantil e outros conteúdos
ilegais.

## 3. [Iceberg](http://iceberget6r64etudtzkyh5nanpdsqnkgav5fh72xtvry3jyu5u2r5qd.onion/)

Indexa conteúdos de todos os tipos. É financiado por anúncios. Permite
indexar páginas web e exibe uma pontuação de página com base no número
de menções e na segurança do utilizador.

<figure>
<a href="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png">
<img src="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png" alt="" width="1920" height="1040" srcset="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png 1920w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-960x520.png 960w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-480x260.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption>Pontos <a href="http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/">do serviço oculto deste sítio eletrónico</a> no Iceberg.</figcaption>
</figure>

## 4. [TorLanD](http://torlgu6zhhtwe73fdu76uiswgnkfvukqfujofxjfo7vzoht2rndyhxyd.onion/)

Indexa conteúdos de todos os tipos. É financiado por anúncios.
Permite-te indexar páginas web.

<figure>
<a href="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png">
<img src="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png" alt="" width="1000" height="579" srcset="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png 1000w, /wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD-500x289.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">Indexação do serviço oculto
    deste sítio eletrónico no TorLanD.</figcaption>
</figure>

## 5. [Haystak](http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/)

Não indexam conteúdos que consideram imorais ou ilegais, tais como abuso
de crianças e tráfico de seres humanos. Afirmam ter indexado mais de 1,5
mil milhões de páginas de 260&nbsp;000 serviços ocultos. Têm um endereço
para receber donativos e a opção de pagar por funcionalidades
adicionais.

<figure>
<a href="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png">
<img src="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png" alt="" width="1507" height="606" srcset="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png 1507w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-753x303.png 753w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-376x151.png 376w" sizes="(max-width: 1507px) 100vw, 1507px">
</a>
    <figcaption class="wp-caption-text">Funcionalidades pagas do
    Haystak</figcaption>
</figure>

## 6. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion)

Têm uma página de termos e condições que condena os usos ilegais.
Financiam-se oferecendo alojamento de serviços ocultos e com anúncios.

## 7. [Ourrealm](http://orealmvxooetglfeguv2vp65a3rig2baq2ljc7jxxs4hsqsrcemkxcad.onion/)

É financiado por anúncios. O seu lema é «procura para além da censura».

## 8. [Ondex](http://ondexcylxxxq6vcc62l3r2m6rypohvvymsvqeadhln5mjo73pf6ksjad.onion/)

É financiado por publicidade. Indexa todos os tipos de conteúdos.

## 9. [Deep Search](http://search7tdrcvri22rieiwgi5g46qnwsesvnubqav2xakhezv4hjzkkad.onion/)

Apresenta as tendências dos serviços ocultos na página inicial. Indexa
todos os tipos de conteúdo. É financiado por anúncios.

## 10. [Find Tor](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/)

Indexa todos os tipos de conteúdos. É financiado por anúncios e [afirma
estar à venda](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/buy).

[^1]: A lista está disponível em <https://ahmia.fi/onions/> ou
    <http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/onions/>.
[^2]: Eles têm [uma lista negra pública de páginas que não indexam](https://ahmia.fi/documentation/).
[^3]: <i lang="en">Search beyond censorship</i> em inglês.

## Outras opções para encontrar conteúdo

Existem listas de serviços ocultos, fóruns, etc. Este artigo lista
apenas alguns dos motores de busca mais populares.
