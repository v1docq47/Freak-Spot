Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2023-12-15 21:57
Lang: eo
Slug: menos-mal
Save_as: fek/index.html
URL: fek/
Tags: rakonto, prozo
Title: F<em>ek</em>

Multaj homoj konis Johanon, malriĉan laboriston, kiu laboris en fabriko.
Li loĝis en malgranda domo, kiun li luis. Oni malpliigis al li multege
la salajron, kaj li apenaŭ havis monon por pagi la multekostan lupagon
kaj manĝi. Aldone li helpis sian fraton pagi la hipotekon de la domo,
kie li loĝis, ĉar li estis maldungita.

Tamen ĉiutage Johano vekiĝis por labori dankema pro tio, ke li havis la
ŝancon bone manĝi la antaŭan nokton. Malgraŭ liaj ekonomiaj problemoj,
li ĉiam sentis sin kontenta kaj feliĉa. Li neniam hezitis helpi sian
fraton, kvankam tio signifis ne manĝi de tempo al tempo.

Riĉa komercisto loĝis en la urbo de Johano. Ambicia ekde junaĝo, por li
la profitoj neniam sufiĉis. Tio igis lin vivi kun daŭra anksio kaj
streso serĉante grandajn profitojn, ĉar ĉiuj liaj negocoj farindas en la
ĝusta momento por maksimumi la enspezon.

Tiom da tempo li dediĉis al siaj entreprenoj, ke li apenaŭ ripozis: la
plejparto de la noktoj li ne dormis pro maltrankvileco aŭ ĉar li daŭre
laboris ĝis malfrue.

En sia libera tempo li lukse vivis, sed neniam sufiĉis kaj li
malfeliĉis. Daŭre li bedaŭris pro tio, ke li ne gajnis pli da mono en
siaj negocoj, ĉar, kvankam li estis milionulo, li freneze elspezis ĉion.
Li ĉiam trovis pli bonan aŭton, pli bonan jakton, pli bonan palacon, pli
bonan helikopteron… kompare al la aferoj, kiujn li jam havis. Pro tio li
baldaŭ enuis je siaj kapricoj: li ne povis ĝui ilin sciinte, ke ekzistis
nova kaj pli bona modelo, kiun li ne povis aĉeti. Tamen, kiam li akiris
la aĵon, kiun li deziris, baldaŭ ĝi enuigis lin. Li bezonis multege da
mono por financi sian luksan vivnivelon.

Unu tagon li rimarkis, ke li ne estis feliĉa. La tagoj pasis, kaj li
daŭrigis sian rutinon meditante kaj observante sian ĉirkaŭaĵon. Ĉiuj
ŝajnis esti kiel li, krom gaja viro, kiu ĉiam montris ridegon. Strangis
al li vidi tiom da feliĉo en malriĉa kaj simpla laboristo. La tagoj
pasis, kaj li ĉiam renkontis tiun feliĉan homon.

Li ne komprenis kial li estis tiel feliĉa. Tial unu tagon li decidis
eltrovi la sekreton de Johano. Li invitis unu el liaj kunlaborantoj, kiu
ankaŭ estis amiko lia, trinki ion kaj promesis al li, ke li rekompencus
lin finance, se li rakontu al li pri la vivo de tiu homo. La laboristo
tion faris. Atente aŭskultinte tion, kion li diris, la milionulo ankoraŭ
ne komprenis, kiel persono kun tia malĝoja vivo povis esti tiel feliĉa.

La sekva tago estis dimanĉo. Johano promenis tra la urbo; li feliĉe kaj
senzorge piediris. Subite venis la riĉa entreprenisto, kiu serĉinte
kontentigan respondon diris al li: «Juna Johano, oni rakontis al mi la
malfeliĉan historion de via vivo, kaj mi ne povas kompreni kial vi ĉiam
estas feliĉa kaj amikema. Mi ŝatus, ke vi diru al mi vian sekreton. Kiel
vi sukcesas esti feliĉa? Tiam Johano haltis kaj respondis: «Neniam
forgesu viajn principojn. Vi devas agi laŭ viaj valoroj kaj fari tion,
kion vi opinias ĝusta. Kiam vi renkontu malfacilaĵon, lukte reagu, pretu
por solvi tion kaj, samtempe, estu serena kaj ne ĉagreniĝu. Faru bonajn
aferojn kaj helpu la aliulojn. Tion mi faras kaj mi feliĉas».

Liaj vortoj ege seniluziigis la entrepreniston, kiu opiniis lin freneza
kaj stulta. Li pensis, ke per tiuj agadoj Johano neniam estus feliĉa;
oni ĉiam ekspluatus lin. Nenion li povos ĝui. Se li malfeliĉis estante
riĉa kaj pova, li ne povis eĉ imagi kiel tiu malriĉulo povis vivi. Pro
tio li ne kredis Johanon, kiam li diris, ke li estis feliĉa.

Li elokvente provis konvinki lin, ke li ŝanĝu sian agmanieron, forgesu
siajn principojn kaj zorgu pri si mem; sed Johano ne volis forlasi siajn
principojn. Unuafoje en sia vivo la entreprenisto sentis malĝojon pro
iu. Johano estis mizera idioto bonkora. Strange lia kompato igis lin
fari iun malavaran agon, kiun li neniam faris.

— Kvankam mi ne samopinias, mi dankas vin pro la konsilo. Oni rakontis
al mi vian historion, kaj mi kompatas vian mizeron. Mi ŝatus helpi vin,
bonan homon, igi vian vivon pli ĝoja kaj malpeza. Mi donos al vi unu el
miaj aŭtoj, por ke vi povu veture promeni kaj trankviliĝi de tempo al
tempo. Estas bonega aŭto — li diris dum la laboristo mire rigardis lin.
— Ĝi ankaŭ utilos al vi por iri al via laborejo. Sed atentu — daŭrigis
la parolema kaj vanta entreprenisto —, ĉi tiu aŭto havas propraĵon: por
antaŭeniri vi devas diri «ek» kaj por bremsi vi devas diri «halt’». La
aliaj komandoj logikas: «glacoviŝilo» aktivas la glacoviŝilon, «radio»
ŝaltas la radion, «pli laŭte» laŭtigas la ludilon, «malpli laŭte»
mallaŭtigas la ludilon. Estas manlibro en la gantujo. Vi rapide lernos
ĉion. Ĝoju!

La entreprenisto donis al li sian aŭton. Johano ne volis akcepti lin kaj
diris, ke li ne bezonis ĝin; sed fine pro la insistado de la
entreprenisto li akceptis ĝin.

Johano sentis sin vere mizera kompare al la entreprenisto, kiu ŝajnis
scii ĉion kaj havi ĉion; li nenion havis. Kiam li ricevis la ŝlosilojn,
eniris li la aŭton kaj diris «ek».

Konfuzite, li ne sciis kien iri kaj kion fari, do li veturis al najbara
urbo per sia nova aŭto. Ĝi estis tre multekosta, do li konsideris vendi
ĝin; li iris al la laborejo bicikle kaj ĉiam, kiam li eliris el la urbo,
li uzis aŭtobuson, trajnon aŭ alian transportilon. Li ne bezonis ĝin.

Sed dum mallonga momento li pensis pri la vortoj de tiu komercisto.
Neniam li meditis pri sia mizera situacio. Dum la veturo li
rimarkis, ke pro tiom protekti la aliulojn li forgesis helpi sin
mem. Eble li estis tro altruista. «Ekde nun mi pensos nur pri mi
mem, kiel rekomendis al mi la entreprenisto». Li pensis, ke bonŝance
li povus iĝi kiel li konsideris tiun entrepreniston: saĝa, feliĉa
kaj riĉa.

Dum la veturo li sentis la emocion stiri aŭton ege kvalitan. Johano
havis la impreson, ke li estis kiel la aŭto; li sentis sin rapida,
nehaltigebla. Dum momento li forgesis pri la tempo, pri siaj devoj
kaj pri siaj pensoj. Li estis tiom distrita en siaj pensoj, ke li ne
vidis signalon, kiu avertis, ke la vojo estis blokita. Iom
malproksime oni estis rekonstruinta la ponton, kiu falis dum la
violenta ŝtormo de la antaŭa semajno. Johano antaŭeniris tra la
ŝajne senfina ponto, kiam li ekrimarkis, ke ĝi kondukis al abismo.
Ekvidis li la rompitan ponton, trafikan barilon kaj la krutaĵon
antaŭ la abismo. Tuj li reagis kriinte «halt’».

La aŭto transiris la trafikan barilon; ĝi haltis tre proksime de la
abismo… Johano forviŝis la ŝviton de sia frunto, trankviliĝe
elspiris. La telefono sonoris. Estis lia frato. «Jes… Bone…», Johano
respondis, «mi revenos en d<strong>ek</strong> minutoj». La aŭto
komencis antaŭeniri.  «F<strong>ek</strong>!», Johano ekkriis, kaj la
aŭto pli rapide moviĝis. Kiam Johano diris «halt’», estis tro malfrue…

