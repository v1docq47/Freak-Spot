Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2016-07-05 07:35
Lang: es
Modified: 2019-05-19 15:10
Slug: menos-mal
Status: published
Tags: cuento, prosa
Title: Menos mal

## I {#i style="text-align: center;"}

Mucha gente conocía al pobre obrero Juan, que trabajaba en la industria.
Vivía en una pequeña casa cuyo alquiler tenía que pagar cada mes. Le
redujeron  muchísimo el sueldo, y apenas tenía dinero para pagar el caro
alquiler y comer. Además, ayudaba a un vecino desempleado y a su propio
hermano a pagar la hipoteca de la casa en la que vivía, pues también
perdió su trabajo.

Sin embargo, cada día se levantaba para trabajar agradecido por haber
tenido la posibilidad de comer bien la noche anterior. A pesar de sus
problemas económicos, siempre se sentía satisfecho y feliz. Nunca dudaba
en ayudar a su vecino y a su hermano a subsistir ni rehusaba en ello,
aunque supusiera pasar algún día sin alimento.

## II {#ii style="text-align: center;"}

Un rico empresario vivía en la ciudad de Juan. Desde joven había vivido
para cosechar fortuna; para él los beneficios nunca eran suficientes.
Esto hacía de su vida una ansiedad continua marcada por un angustioso
estrés en busca de intereses, pues todos los negocios debía realizarlos
en el momento adecuado para maximizar sus ganancias.

Tanto era el tiempo que dedicaba a sus negocios, que no encontraba
momento de descanso: la mayoría de las noches no dormía por inquietud o
porque se quedaba trabajando hasta tarde.

En su tiempo libre, este vivía a todo tren, pero jamás tenía suficiente
y no era feliz. Continuamente, se arrepentía de no haber conseguido más
dinero en sus negocios, pues, aunque era millonario, se lo gastaba todo
como un loco. Siempre encontraba un coche mejor, un yate mejor, una
mansión mejor, un helicóptero mejor... de los que tenía. Por eso se
aburría pronto de sus caprichos: no podía disfrutarlos sabiendo que
había un nuevo y mejor modelo que no podía adquirir. Sin embargo, cuando
conseguía el objeto que deseaba, pasaba poco tiempo hasta que le
aburría. Necesitaba ingentes cantidades de dinero para financiar su
lujoso nivel de vida.

## III {#iii style="text-align: center;"}

Un día se dio cuenta de que no era feliz. Pasaron los días y siguió su
rutina reflexionando y observando a su alrededor. Todos parecían estar
como él, excepto un alegre hombre que siempre mostraba una sonrisa de
oreja a oreja. Le extrañó ver tanta felicidad en un pobre y simple
obrero. Pasaban los días, y siempre se cruzaba con ese feliz hombre.

No comprendió por qué era tan alegre. Por eso, un día se propuso
averiguar el secreto de Juan. Invitó a uno de los compañeros del
trabajo, y amigo, del obrero a tomar algo y le dijo que le recompensaría
económicamente si le contaba la vida de tal hombre. El trabajador hizo
así. Después de escuchar atentamente lo que dijo, el millonario seguía
sin entender cómo un personaje con una vida tan triste podía ser tan
feliz.

El día siguiente era domingo. Juan iba caminando por la ciudad; paseaba
feliz y despreocupado. De repente, se acercó el rico empresario, que
buscando una respuesta satisfactoria, le dijo: «Joven Juan, me han
contado la desgraciada historia de tu día a día, y no logro comprender
por qué siempre estás alegre y amable. Me gustaría que me contaras tu
secreto. ¿Cómo consigues ser feliz?». Entonces, Juan se paró y le dijo:
«No olvides jamás tus principios. Tienes que ser tú mismo y hacer lo que
consideres correcto. Cuando encuentres una adversidad reacciona con
combatividad, resuelto a solucionarla, y, a la vez, con serenidad, sin
alterarte..., haciendo el bien y ayudando a los demás. Eso hago yo y soy
feliz».

Sus palabras decepcionaron enormemente al empresario, que lo tomó por
loco e idiota. Veía que no comprendía que con esa actitud —de hacer
siempre el bien y ayudar sus allegados— nunca sería feliz: siempre se
aprovecharían de él. No tendría nunca nada que disfrutar. Si él era
triste siendo rico y poderoso, no podía siquiera imaginar cómo viviría
ese pobre hombre. Así pues, no le creyó cuando le dijo que era feliz.

Intentó convencerle elocuentemente de que depusiera su actitud,
olvidándose de sus principios, y se preocupara de sí mismo; pero Juan no
quería traicionar sus principios. Por primera vez en su vida, el
empresario se sintió triste por alguien. Juan era el primer desgraciado
idiota que había conocido. Extrañamente, su compasión le llevó a
realizar una acción generosa nada propia de él.

—Aunque no lo comparto, te doy las gracias por el consejo. Me han
contado tu historia, y siento pena por tu miseria. Me gustaría ayudarte,
buen hombre, a hacerte la vida más divertida y pasadera. Te regalaré uno
de mis coches para que puedas darte una vuelta y despejarte de vez en
cuando. Es un gran coche —decía mientras el proletario le miraba
atónito—. Te servirá también para ir al trabajo. Pero ten cuidado
—continuó el charlatán y petulante empresario—, este coche tiene una
peculiaridad: para avanzar tienes que decir «menos mal» (antes era
«menos más» \[de menos a más velocidad\], pero el micrófono captaba
mejor lo otro y lo cambié)...; para frenar, «mal menos» (fácil de
recordar). Las demás órdenes son lógicas: «parabrisas» activa el
limpiaparabrisas, «radio» enciende la radio, «menos» baja el volumen,
«más» sube el volumen... Hay un manual en la guantera. Lo aprenderás
rápido. Diviértete.

El empresario le cedió su coche. Juan no quiso aceptarlo y le dijo que
no lo necesitaba; aunque, finalmente, ante la insistencia del
empresario, acabó aceptándolo.

## IV {#iv style="text-align: center;"}

Juan se sintió realmente miserable ante el empresario, que parecía
saberlo todo y tenerlo todo; él no tenía nada. Cuando hubo recibido las
llaves, se subió al coche y dijo: «Menos mal».

Desconcertado, no sabía adónde ir ni qué hacer, así que paseó por la
ciudad con su nuevo coche. El coche era muy caro, así que pensó en
venderlo; iba a trabajar en bicicleta y siempre que salía de la ciudad
cogía el autobús, el tren o cualquier otro transporte. No lo necesitaba.

Pero durante un momento pensó en las palabras de aquel hombre. Nunca se
había planteado su miserable situación, siempre se sintió afortunado de
poder ayudar a su hermano y poder comer cada día..., quizás estaba
siendo demasiado altruista. Durante el trayecto se dio cuenta de que, de
tanto proteger a los demás, se había olvidado de ayudarse a sí mismo. A
partir de ahora pensaría solo en sí mismo, como le recomendó el
empresario. Pensó, ignorante de que ya era feliz, que con suerte
llegaría a ser como él veía a aquel empresario: sabio, feliz y rico.

Mientras conducía, sintió la emoción de conducir un vehículo de
excepcional calidad. Juan se identificó con el coche, se sentía veloz,
imparable. Por un momento se olvidó del tiempo, de sus obligaciones y de
sus pensamientos; salió incluso de la ciudad. Estaba tan distraído en
sus pensamientos que no vio una señal que alertaba que la carretera
estaba cortada. Más allá estaban reconstruyendo el puente que se
desplomó tras la violenta tormenta de la semana anterior. Juan subía una
empinada carretera que parecía no tener fin, cuando se dio cuenta de que
esta conducía hacia un abismo. Observó el destrozado puente, la obra y
el precipicio que había bajo él. Tardó un poco en reaccionar, pero
consiguió gritar a tiempo: «Mal menos».

El coche atravesó la valla de la obra; se quedó a un palmo del abismo...
Juan se secó el sudor de su frente, resopló aliviado y pronunció un
«menos mal». El coche aceleró y cayó por el profundo barranco; al rato
se oyó el eco de un grito en el fondo del despeñadero.
