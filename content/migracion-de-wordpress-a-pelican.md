Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2017-02-28 17:31
Lang: es
Modified: 2017-03-03 09:01
Slug: migracion-de-wordpress-a-pelican
Tags: Wordpress, Pelican, migración
Title: Migración de Wordpress a Pelican

En el artículo [Mejor sin Wordpress](/mejor-sin-wordpress/)
dije que tenía pensado migrar este blog, hecho con Wordpress, a Pelican.
Pues bien, ya lo he conseguido. No he cambiado apenas la apariencia del
blog. El objetivo principal de la migración era mejorar el rendimiento
y tener la posibilidad de gestionar el blog con un sistema de control de
versiones usando Git. En este artículo explico resumidamente cómo lo he
hecho.<!-- more -->[^1]

Primero hice una copia de seguridad del contenido del servidor y de la
base de datos de Wordpress. Después instalé
[Pelican](https://blog.getpelican.com/) y
[Markdown](https://pypi.python.org/pypi/Markdown) (ya que los artículos
los pensaba escribir en
[Markdown](https://es.wikipedia.org/wiki/Markdown)) con la instrucción
`pip install pelican markdown` y creé la estructura básica para la
página web ejecutando `pelican-quickstart`.

<figure>
  <a href="/wp-content/uploads/2017/02/pelican-quickstart.png">
    <img src="/wp-content/uploads/2017/02/pelican-quickstart.png" alt="pelican-quickstart" class="size-fill" width="896" height="561" srcset="/wp-content/uploads/2017/02/pelican-quickstart.png 896w, /wp-content/uploads/2017/02/pelican-quickstart-448x280.png 448w" sizes="(max-width: 896px) 100vw, 896px">
  </a>
  <figcaption class="wp-caption-text">Creación de la base de la página web con pelican-quickstart</figcaption>
</figure>

Tras hacer esto se crearon los archivos básicos de Pelican:

- **`content/`**. Directorio donde se ubica el contenido del sitio web
  (artículos, páginas...). Inicialmente está vacío.
- **`fabfile.py`**. Sirve para automatizar el proceso de generación,
  publicación, servir la página generada en <http://localhost:8000> y
  publicar la página mediante
  [SSH](https://es.wikipedia.org/wiki/Secure_Shell). Requiere el
  programa [Fabric](http://www.fabfile.org/).
- **`Makefile`**. Contiene directivas para la automatización de la
  generación del sitio web. `make html` genera la página, `make publish`
  genera la página preparada para su publicación...
- **`output/`**. Directorio donde se genera la página por defecto.
  Inicialmente está vacío.
- **`pelicanconf.py`**. Contiene la configuración del sitio web.
- **`publishconf.py`**. Contiene la configuración que se aplica solo cuando
  se quiere publicar el sitio web.

Más adelante en el desarrollo, borré el archivo `fabfile.py` porque no
lo encontré útil. También [añadí el archivo `Makefile` y el directorio
`output` al archivo
`.gitignore`](https://notabug.org/Freak-Spot/Freak-Spot/commit/a2116df99da768e797b9fff8a2789e8a64f0eedb)
del repositorio Git del sitio web. Lo hice porque el archivo `Makefile`
contiene información sensible como mi servidor y usuario FTP, la carpeta
`output` simplemente no tiene sentido mantenerla en el control de
versiones, pues va a cambiar cada vez que se genere el sitio web.

Una vez creada la estructura básica del sitio web, transformé el
contenido de Wordpress a Pelican. Afortunadamente, Pelican te permite
recuperar archivos desde el formato XML de Wordpress a Markdown. ¿Qué es
el formato XML de Wordpress? Desde el menú Exportar de Wordpress se
puede exportar un archivo XML con el contenido de la base de datos (es
decir, los artículos, las páginas, etc.).

<a href="/wp-content/uploads/2017/02/exportar-xml-de-wordpress.png">
  <img src="/wp-content/uploads/2017/02/exportar-xml-de-wordpress.png" alt="Exportar XML de Wordpress" class="size-fill" width="1409" height="774" srcset="/wp-content/uploads/2017/02/exportar-xml-de-wordpress.png 1409w, /wp-content/uploads/2017/02/exportar-xml-de-wordpress-704x387.png 704w" sizes="(max-width: 1409px) 100vw, 1409px">
</a>

Para transformar ese contenido a archivos escritos en Markdown que pueda
utilizar Pelican, ejecuté `pelican-import --wpfile --markup markdown
--output content/ wordpress.xml`[^2] en la misma carpeta donde ejecuté
`pelican-quickstart`. Para probar si funcionaba todo bien ejecuté
`pelican content` para generar el sitio web, entonces [aparecieron
varios errores durante el
procesamiento](https://notabug.org/Freak-Spot/Freak-Spot/issues/1).
Cuando fui al lugar en <http://localhost> donde tenía ubicado la carpeta
`output` del proyecto, encontré también que la apariencia de la página
generada no era muy agradable.

<a href="/wp-content/uploads/2017/02/Apariencia-de-Freak-Spot-en-Pelican-al-principio.png">
  <img src="/wp-content/uploads/2017/02/Apariencia-de-Freak-Spot-en-Pelican-al-principio.png" alt="Apariencia de Wordpress" class="size-fill" width="1920" height="1044" srcset="/wp-content/uploads/2017/02/Apariencia-de-Freak-Spot-en-Pelican-al-principio.png 1920w, /wp-content/uploads/2017/02/Apariencia-de-Freak-Spot-en-Pelican-al-principio-960x522.png 960w, /wp-content/uploads/2017/02/Apariencia-de-Freak-Spot-en-Pelican-al-principio-480x261.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

Sin embargo, vi que el contenido de los artículos se mostraba
correctamente, excepto por los *Shortcodes*[^3] de los complementos de
Wordpress y algunos otros problemas que ocurrieron con la transformación
a Markdown.

A partir de entonces comencé a migrar el tema de Wordpress y los
complementos de Wordpress a su alternativa en Pelican. Durante el
desarrollo también hice algunas modificaciones y pequeñas mejoras.

A la hora de migrar las imágenes y otros recursos, descargué la carpeta
`wp-content` de Wordpress y [la ubique dentro de la carpeta `content` de
Pelican](https://notabug.org/Freak-Spot/Freak-Spot/commit/0d4560a67f6ec0c92e183424619df88d901e0600),
especificando en `pelicanconf.py` que la carpeta `wp-content` contenía
contenido estático, y no artículos y páginas. Esto me permitió preservar
las URLs antiguas y agilizar el proceso de migración. También conservé
las URLs antiguas de Wordpress, añadiendo las siguientes líneas a
`pelicanconf.py`:

    :::python
    ARTICLE_URL = '{slug}'  # Cómo aparece el nombre de los artículos en la URL
    ARTICLE_SAVE_AS = '{slug}/index.html'  # Donde se guardan los artículos

    # URL de cada página
    PAGINATION_PATTERNS = (
        (1, '{base_name}/', '{base_name}/index.html'),
        (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
    )

Además, tuve que reorganizar las categorías, pues Pelican solo permite
que un artículo se encuentre en una categoría y en Wordpress varios
artículos estaban en categorías distintas. En mi opinión, el menú de
categorías era un poco confuso y desordenado en Wordpress, así que lo
cambié para que fuera compatible con Pelican. Ahora cada artículo solo
forma parte de una categoría y el menú de categorías es menos
complicado.

Para encontrar más detallado el proceso de migración consulta los
commits del repositorio Git <https://notabug.org/Freak-Spot/Freak-Spot>,
anteriores al commit
[52aaf6c528367871471733450455f51411e34f15](https://notabug.org/Freak-Spot/Freak-Spot/commit/52aaf6c528367871471733450455f51411e34f15).

[Heckyel](https://conocimientoslibres.tuxfamily.org/) me ayudó a
solucionar algunos problemas. Y otros amigos también me ayudaron y me
dieron consejos. Agradezco a todos ellos su ayuda. El resultado de la
migración ha sido una página web mucho más ligera y eficiente, con una
mayor facilidad de desarrollo y de colaboración.

[^1]:
    La migración la realicé desde la versión 4.7.2 de Wordpress a la
    versión 3.7.1 de Pelican.
[^2]:
    El nombre del archivo XML de Wordpress varía según el nombre de la
    página web. Hace falta reempazar en esta instrucción el nombre
    `wordpress.xml` por el nombre el archivo XML de Wordpress importado.
[^3]:
    Los *Shortcodes* son códigos que utiliza Wordpress para añadir
    funciones adicionales a una página. Suelen ser utilizados por
    complementos de Wordpress.
