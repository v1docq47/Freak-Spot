Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2023-12-12 16:57
Lang: es
Slug: millones-de-legiones-a-nuestra-espalda
Tags: GNU/Linux, historia
Title: Millones de legiones a nuestra espalda
Image: <img src="/wp-content/uploads/2023/12/millones-de-legiones.png" alt="" width="1426" height="711" srcset="/wp-content/uploads/2023/12/millones-de-legiones.png 1426w, /wp-content/uploads/2023/12/millones-de-legiones-713x355.png 713w" sizes="(max-width: 1426px) 100vw, 1426px">

La invención del fuego, de la filosofía, de la electricidad, del
ordenador, de GNU/Linux... Detrás de cada tecnología está el legado de
millones de personas que contribuyeron para hacerlo posible.
Las tecnologías actuales son el fruto del trabajo colectivo acumulado a
lo largo de la historia.

Las contribuciones que hacemos al <i lang="en">software</i> libre, a la
cultura libre, a la descentralización de la economía o a cualquier otro
proyecto serán el legado que dejaremos a la próxima generación.

Un ejemplo de ese legado es el proyecto GNU, iniciado en 1983, que consiguió crear un
sistema operativo completamente libre. Al principio, quienes
soñaron esa idea fueron ridiculizados y marginados por la sociedad. Sin
embargo, su empeño dio como fruto numerosos programas, comunidades,
empresas y el marco legal
para proteger las creaciones informáticas de la privatización y el
inevitable olvido &mdash;lo que les sucede a los programas privativos que dejan de ser
rentables&mdash;. Hoy en día, GNU/Linux (insignificante en sus
orígenes) se usa en todos los superordenadores, tecnología aeroespacial, en
la abrumadora mayoría de servidores, en millones y millones de
ordenadores personales...

Este proyecto sigue creciendo y evolucionando, sigue adquiriendo más
usuarios y más colaboradores. Tú y yo y las demás personas estamos
dejando los cimientos de las generaciones futuras, nos sumaremos a las
millones de legiones que hay a nuestra espalda para iluminar el futuro.

Gracias a todas las personas que contribuyeron a estos proyectos, puedo
disfrutar de muchas comodidades inexistentes en el pasado. Hoy
contribuyo a proyectos liberadores de todo tipo para dejar mi granito de
arena. Muchas personas me dan las gracias. Yo les doy también las
gracias, sois personas increíbles. Juntos escribiremos un futuro mejor.
