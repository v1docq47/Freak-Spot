Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2023-12-15 12:57
Lang: pt
Slug: millones-de-legiones-a-nuestra-espalda
Tags: GNU/Linux, história
Save_as: milhões-de-legiões-atrás-de-nós/index.html
URL: milhões-de-legiões-atrás-de-nós/
Title: Milhões de legiões atrás de nós
Image: <img src="/wp-content/uploads/2023/12/millones-de-legiones.png" alt="" width="1426" height="711" srcset="/wp-content/uploads/2023/12/millones-de-legiones.png 1426w, /wp-content/uploads/2023/12/millones-de-legiones-713x355.png 713w" sizes="(max-width: 1426px) 100vw, 1426px">

A invenção do fogo, da filosofia, da eletricidade, do computador, do
GNU/Linux? Por trás de cada tecnologia está o legado de milhões de
pessoas que contribuíram para torná-la possível. As tecnologias de hoje
são fruto do trabalho coletivo acumulado ao longo da história.

As contribuições que dermos ao <i lang="en">software</i> livre, à cultura livre, à
descentralização da economia ou a qualquer outro projeto serão o legado
que deixaremos à geração seguinte.

Um exemplo desse legado é o projeto GNU, iniciado em 1983, que conseguiu
criar um sistema operativo totalmente livre. No início, os que sonharam
com essa ideia foram ridicularizados e marginalizados pela sociedade. No
entanto, os seus esforços deram origem a inúmeros programas,
comunidades, empresas e ao quadro jurídico que protege as criações
informáticas da privatização e do inevitável esquecimento &mdash; que
acontece aos programas proprietários quando deixam de ser rentáveis.
Hoje, o GNU/Linux (insignificante nas suas origens) é utilizado em todos
os supercomputadores, na tecnologia aeroespacial, na esmagadora maioria
dos servidores, em milhões e milhões de computadores pessoais...

Este projeto continua a crescer e a evoluir, continua a adquirir mais
utilizadores e mais contribuidores. Tu e eu e outros estamos a lançar
os alicerces para as gerações futuras, e juntar-nos-emos aos milhões de
legiões que estão atrás de nós para iluminar o futuro.

Graças a todas as pessoas que contribuíram para esses projectos, posso
usufruir de muitos confortos que não existiam no passado. Hoje contribuo
para projectos libertadores de todos os tipos para fazer a minha parte.
Muitas pessoas agradecem-me. Eu também vos agradeço, vocês são pessoas
incríveis. Juntos escreveremos um futuro melhor.
