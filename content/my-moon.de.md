Author: Jorge Maldonado Venturtheir
Category: Kino
Date: 2023-02-13 12:00
Image: <img src="/wp-content/uploads/2017/04/My-Moon-poster.jpg" alt="El astronauta del cortometraje My Moon" class="attachment-post-thumbnail wp-post-image">
Lang: de
Slug: my-moon
Tags: Blender, kurzes Video, freie Kultur, Inkscape, Krita, OpenMPT, Video, YouTube
Title: <cite>My Moon</cite>: kurzes Video mit freier Software erstellt

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2017/04/My-Moon-poster.jpg">
  <source src="/temporal/My_Moon_(Blender_short_film).mp4" type="video/mp4">
  <p>Entschuldigung, dein Browser unterstützt nicht HTML 5. Bitte wechsle
  oder aktualisiere deinen Browser</p>
</video>

Dieses Video, das ursprünglich auf YouTube hochgeladen wurde und den
Titel <cite>My Moon (Blender short film)</cite> trägt, wurde von Nik Prodanov im Rahmen seines Abschlusses mit kaum finanziellen Mitteln und unter Verwendung freier Software erstellt. Es ist lizenziert unter <a href="https://creativecommons.org/licenses/by/4.0/deed.de"><abbr title="Creative Commons Namensnennung 4.0 International">CC BY 4.0</abbr></a>.
