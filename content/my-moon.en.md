Author: Jorge Maldonado Venturtheir
Category: Cinema
Date: 2023-02-12 23:00
Image: <img src="/wp-content/uploads/2017/04/My-Moon-poster.jpg" alt="El astronauta del cortometraje My Moon" class="attachment-post-thumbnail wp-post-image">
Lang: en
Slug: my-moon
Tags: Blender, short film, free culture, Inkscape, Krita, OpenMPT, video, YouTube
Title: <cite>My Moon</cite>: short film made with free software

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2017/04/My-Moon-poster.jpg">
  <source src="/temporal/My_Moon_(Blender_short_film).mp4" type="video/mp4">
  <p>Sorry, your browser doesn't support HTML 5. Please change
  or update your browser</p>
</video>

This video, originally uploaded to YouTube and titled <cite>My Moon (Blender
short film)</cite>, was made by Nik Prodanov as part of their graduation, with
hardly any financial resources and using free software. It is licensed
under <a href="https://creativecommons.org/licenses/by/4.0/"><abbr title="Creative Commons Attribution 4.0 International">CC BY 4.0</abbr></a>.
