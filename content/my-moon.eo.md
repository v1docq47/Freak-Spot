Author: Jorge Maldonado Ventura
Category: Kino
Date: 2023-02-13 00:10
Image: <img src="/wp-content/uploads/2017/04/My-Moon-poster.jpg" alt="El astronauta del cortometraje My Moon" class="attachment-post-thumbnail wp-post-image">
Lang: eo
Slug: my-moon
Tags: Blender, filmeto, libera kulturo, Inkscape, Krita, OpenMPT, video, YouTube
Title: <cite>My Moon</cite>: filmeto farita per liberaj programoj

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2017/04/My-Moon-poster.jpg">
  <source src="/temporal/My_Moon_(Blender_short_film).mp4" type="video/mp4">
<p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>

Ĉi tiu video, origine alŝutita al YouTube kaj titolita <cite>My Moon (Blender
short film)</cite>, estis farita de Nik Prodanok por ria diplomiĝo, per
neniom da monaj rimedoj kaj uzante liberajn programojn. Ĝi estas
sub la <a
href="https://creativecommons.org/licenses/by/4.0/deed.eo"><abbr
title="Creative Commons Atribuite 4.0 Tutmonda">CC BY
4.0</abbr></a>-permesilo.
