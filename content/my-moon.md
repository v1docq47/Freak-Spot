Author: Jorge Maldonado Ventura
Category: Cine
Date: 2017-04-13 15:38
Image: <img src="/wp-content/uploads/2017/04/My-Moon-poster.jpg" alt="El astronauta del cortometraje My Moon" class="attachment-post-thumbnail wp-post-image">
Lang: es
Slug: my-moon
Tags: Blender, cortometraje, cultura libre, Inkscape, Krita, OpenMPT, video, YouTube
Title: <cite>My Moon</cite>: un cortometraje hecho con <i lang="en">software</i> libre

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2017/04/My-Moon-poster.jpg">
  <source src="/temporal/My_Moon_(Blender_short_film).mp4" type="video/mp4">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Este vídeo, originalmente subido a YouTube y titulado *My Moon (Blender
short film)*, fue hecho por Nik Prodanov como parte de su graduación,
sin apenas recursos económicos y usando <i lang="en">software</i> libre. Se encuentra
bajo la licencia <a
href="https://creativecommons.org/licenses/by/4.0/"><abbr
title="Creative Commons Attribution 4.0 International">CC BY
4.0</abbr></a>.
