Author: Jorge Maldonado Venturtheir
Category: Cinema
Date: 2023-02-13 12:00
Image: <img src="/wp-content/uploads/2017/04/My-Moon-poster.jpg" alt="El astronauta del cortometraje My Moon" class="attachment-post-thumbnail wp-post-image">
Lang: pt
Slug: my-moon
Tags: Blender, vídeo curto, cultura livre, Inkscape, Krita, OpenMPT, vídeo, YouTube
Title: <cite>My Moon</cite>: vídeo curto feito com programas livres

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2017/04/My-Moon-poster.jpg">
  <source src="/temporal/My_Moon_(Blender_short_film).mp4" type="video/mp4">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

Este vídeo, originalmente carregado no YouTube e intitulado <cite>My Moon
(Blender short film)</cite>, foi feito por Nik Prodanov como parte da sua
graduação, com quase nenhum recurso financeiro e utilizando programas
livres. Está licenciado sob <a href="https://creativecommons.org/licenses/by/4.0/deed.pt"><abbr title="Creative Commons Atribuição 4.0 Internacional">CC BY 4.0</abbr></a>.
