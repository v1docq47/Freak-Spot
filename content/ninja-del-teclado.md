Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2020-04-09
Modified: 2023-03-04 13:51
Lang: es
Slug: combinaciones-de-teclado-y-tecla-Componer-GNU/Linux
Tags: ajustes, Componer, GNU/Linux, MATE, teclado, Trisquel, Trisquel 8, Unicode
Title: Escribir cualquier carácter con el teclado en GNU/Linux, rápido
Image: <img src="/wp-content/uploads/2020/04/ninja-del-teclado.jpg" alt="">

Por tu trabajo o lo que sea tienes que escribir algunas veces caracteres
que no encuentras dibujados en el teclado (¹, «, &mdash;, ä, ĉ, ß,
¢, etc.) o tienes alguna tecla rota. ¿Qué haces?

1. Buscar el carácter en Internet, copiarlo y pegarlo.
2. Usar funciones del editor de texto para insertar caracteres
   especiales.
3. Buscar el código de Unicode en Internet e introducirlo con la
   combinación de teclas de tu sistema operativo[^1].
4. Aprender cómo se escriben esos caracteres raros usando combinaciones
   de teclado fáciles de recordar y recurrir a ellas en el futuro.

Si tu respuesta es la 1, la 2 o la 3, deberías seguir leyendo; la mejor
solución es la 4 si ya has tenido varias veces la necesidad de
introducir caracteres extraños.

<!-- more -->

Para que te hagas una idea, te muestro aquí una pequeña lista de
combinaciones que yo uso en el teclado español en GNU/Linux[^2]:

- <kbd>Alt derecho</kbd> + <kbd>z</kbd> = «
- <kbd>Alt derecho</kbd> + <kbd>x</kbd> = »
- <kbd>Mayús</kbd> + <kbd>^</kbd>, luego un número = ¹
- <kbd>Componer</kbd> + <kbd>_</kbd>, luego un número = ₁
- <kbd>Alt derecho</kbd> + <kbd>v</kbd> = “
- <kbd>Alt derecho</kbd> + <kbd>b</kbd> = ”
- <kbd>Alt derecho</kbd> + <kbd>Mayús</kbd> + <kbd>v</kbd> = ‘
- <kbd>Alt derecho</kbd> + <kbd>Mayús</kbd> + <kbd>b</kbd> = ’
- <kbd>Componer</kbd>, luego <kbd>-</kbd>, luego <kbd>-</kbd>, luego <kbd>-</kbd> = &mdash;

Si buscabas cómo escribir alguno de estos caracteres, igual ya has
resuelto el problema. «¡Pero espera!» quizás te preguntas...

- ¿Cómo puedo saber esas combinaciones?
- ¿Cuál es la tecla Componer?

## Ver las combinaciones de teclas

Desde el menú de distribuciones de teclado, al menos en el entorno
escritorio MATE, encuentras una opción para mostrar el mapa de teclas.
Consulta el artículo [Cambiar entre distribuciones de teclado
rápidamente en MATE](/multiples-mapas-de-teclas/) para
hacerte una idea. Hurgando un poco en los ajustes de teclado encontrarás
el mapa de teclado. También está el comando `xmodmap -pke`.

<figure>
<a href="/wp-content/uploads/2020/04/distribución-de-teclado-español-GNU-Linux.png">
<img src="/wp-content/uploads/2020/04/distribución-de-teclado-español-GNU-Linux.png" alt="" width="1020" height="345" srcset="/wp-content/uploads/2020/04/distribución-de-teclado-español-GNU-Linux.png 1020w, /wp-content/uploads/2020/04/distribución-de-teclado-español-GNU-Linux-510x172.png 510w" sizes="(max-width: 1020px) 100vw, 1020px">
</a>
    <figcaption class="wp-caption-text">Distribución del teclado «Español» en Trisquel 8 MATE</figcaption>
</figure>

Vemos en la imagen que una tecla tiene hasta cuatro caracteres. En el
teclado físico que tienes delante probablemente no hay tantos dibujados.
Vamos a fijarnos en una tecla concreta, la que corresponde a la letra v.
Vemos que en esa tecla aparecen varios caracteres, así se escribe cada uno:

- *Abajo a la izquierda (**v**)*. Pulsa simplemente esa tecla
- *Arriba a la izquierda (**V**)*. <kbd>Mayús</kbd> + *tecla*
- *Abajo a la derecha (**“**)*. <kbd>Alt derecho</kbd> + *tecla*
- *Arriba a la derecha (**‘**)*. <kbd>Alt derecho</kbd> + <kbd>Mayús</kbd> + *tecla*

## La tecla Componer

El problema aparece cuando no está la tecla que queremos escribir en
nuestra distribución de teclado, quizá se trata de un símbolo como
&mdash;, → o cualquier otro.

En este caso, debemos definir la [tecla Componer](https://en.wikipedia.org/wiki/Compose_key)
si nuestro teclado no la tiene o no está asignada. Para ello podemos
configurarla en los ajustes de teclado de nuestra distribución o
modificar el archivo `/etc/default/keyboard` añadiendo a la variable
`XKBOPTIONS` la tecla que queremos usar (`lalt`, para Alt izquierdo; `rwin`, [tecla
super](https://es.wikipedia.org/wiki/Windows_(tecla)) derecha; `lwin`,
tecla Inicio izquierda...). Si quisiera usar la tecla Alt derecho como
tecla Componer, haría este cambio en el archivo `/etc/default/keyboard`:

    :::diff
    - XKBOPTIONS=""
    + XKBOPTIONS="compose:ralt"

Una vez definida, prueba una combinación como *<kbd>Componer</kbd> luego
<kbd>-</kbd> luego <kbd>></kbd>* (debería escribir →). Podemos
buscar los caracteres predefinidos con sus respectivas combinaciones en
el directorio `/usr/share/X11/locale/` &mdash;con una búsqueda recursiva
con grep (`grep -R símbolo`) es sencillo&mdash;. El formato de
definición lo encontramos en la página de manual [Compose(5)](https://man.archlinux.org/man/Compose.5). Si no
existe una combinación para el carácter que queremos escribir, podemos
definirla creando un archivo `~/.XCompose`.

## Hay mucho más...

No he querido entrar en detalle sobre teclas muertas y otros aspectos
sobre los teclados. Con estos conocimientos seguro que te será más
sencillo encontrar más información si te ha sabido a poco.

Espero que con lo que he enseñado en este artículo puedas escribir
muchos caracteres extraños con la agilidad de un ninja. ☺

[^1]: En el sistema operativo GNU/Linux con el sistema de ventanas X,
hay que mantener pulsados <kbd>Ctrl</kbd>+<kbd>Mayús</kbd>, escribir
<kbd>u</kbd>, soltar <kbd>Ctrl</kbd> y <kbd>Mayús</kbd>, escribir de
código hexadecimal y, finalmente, pulsar <kbd>Entrar</kbd>.
[^2]: El símbolo + significa que se deben pulsar las teclas al mismo
tiempo. Cuando digo «luego», se deben pulsar de forma sucesiva. Detrás del
= aparece el producto de la combinación de teclas.
