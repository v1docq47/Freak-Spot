Author: Alyssa Rosenzweig
Category: Privacidade
Date: 2023-06-17 17:40
Lang: pt
Slug: no-más-celulares-a-partir-de-ahora
Save_as: nada-de-telemóveis-a-partir-de-agora/index.html
URL: nada-de-telemóveis-a-partir-de-agora/
Tags: conselho, telemóvel, educação, software livre, Tor
Title: Nada de telemóveis a partir de agora

*Este artigo é uma tradução do artigo [«No Cellphones Beyond This
Point»](https://web.archive.org/web/20181026102419/https://rosenzweig.io/blog/no-cellphones.html)
publicado pela Alyssa Rosenzweig sob a licença <a
href="https://creativecommons.org/licenses/by-sa/4.0/deed.pt">CC BY-SA
4.0</a>*.

Recuso-me a andar com um telemóvel &mdash; para os meus amigos confusos
na nossa sociedade obcecada pela tecnologia &mdash; eis a razão. Alguns
de vocês já me perguntaram qual é o meu número para me enviarem
mensagens. Talvez tenha sido um professor de uma das minhas aulas a
pedir-me para utilizar um <i lang="pt">software</i> proprietário na aula. Talvez fosse um
membro da família, preocupado com o facto de, numa situação de
insegurança, eu não poder pedir ajuda.

Há quatro níveis de raciocínio associados à minha recusa de ter um
telemóvel, apesar de ser uma utilizadora ativa da Internet. Por ordem da
menor para a maior importância:

Primeiro, a eletrónica dos telemóveis é desconfortável para mim. O meu
tempo nos computadores é maioritariamente gasto em escrita, programação
e arte; para mim, estas tarefas requerem teclados de tamanho normal ou
<i lang="en">tablets</i> de desenho. Esta não é uma razão ética para
evitar telemóveis e <i lang="en">tablets</i>, claro, e reconheço que
muitas pessoas têm utilizações mais adequadas ao formato pequeno.

Em segundo lugar, os utilizadores de telemóveis criam a cultura do
telemóvel. Numa fração de tempo da vida de um adulto, os telemóveis
passaram de inexistentes a socialmente aceitáveis para serem usados
quando se fala com alguém na vida real. Esta cultura não é inevitável
para a eletrónica digital &mdash; muitas pessoas utilizam a tecnologia de
forma responsável, pelo que as aplaudo &mdash;, mas continua a ser
deprimentemente comum. Se eu tivesse um telemóvel à frente do nariz
enquanto fingisse estar a falar com os meus amigos, continuaria a
perpetuar a noção de que *este comportamento é aceitável*. Como receio
poder vir a tornar-me alguém que utiliza a tecnologia desta forma, evito
andar com um telemóvel para evitar o risco ético.

Em terceiro lugar, os telemóveis constituem um grave risco para a
liberdade e a privacidade. A grande maioria dos telemóveis no mercado
utilizam sistemas operativos proprietários, como o iOS, e estão repletos
de <i lang="en">software</i> proprietário. Além disso, ao contrário da maioria dos
computadores portáteis e de secretária, muitos destes sistemas
operativos executam verificações de assinatura. Ou seja, é
criptograficamente impossível e, em alguns casos, ilegal substituir o
sistema por <i lang="en">software</i> livre. Este facto, por si só, é
uma razão para se recusar a tocar nestes dispositivos.

A situação real é, infelizmente, pior. Na eletrónica convencional,
existe um único chip principal no seu interior, a UCP [unidade central
de processamento]. A UPC corre o sistema operativo, como o
[GNU/Linux](https://gnu.org/), e tem o controlo total da máquina. Nos
telemóveis não é assim; estes dispositivos têm dois chips principais
&mdash; a UCP e a banda base. O primeiro tem o conjunto habitual de
problemas de liberdade; o segundo é uma caixa negra ligada à Internet
com um conjunto assustador de capacidades. No mínimo, devido à conceção
das redes de telemóveis, sempre que o telefone estiver ligado à rede (ou
seja, a banda de base estiver em linha), a localização do utilizador
pode ser rastreada através da triangulação das torres de telemóveis. O
risco já é inaceitável para muitas pessoas. As operações de telefonia
tradicionais são vulneráveis à vigilância e à manipulação, uma vez que
nem as chamadas nem as mensagens de texto são encriptadas. E, para
agravar ainda mais a situação, poucos telemóveis possuem um isolamento
aceitável da banda de base. Ou seja, a UCP, que pode executar <i lang="en">software</i>
livre, não controla a banda de base, o que, para efeitos práticos, faz
com que seja ilegal executar <i lang="en">software</i> livre nos Estados
Unidos. Pelo contrário, em muitos casos, a banda de base controla a UCP.
Não importa se são utilizadas mensagens encriptadas através de XMPP se a
banda de base pode simplesmente tirar uma captura de ecrã sem o
conhecimento nem o consentimento do sistema operativo do lado da UCP. Em
alternativa, mais uma vez, dependendo da forma como a banda de base
esteja ligada ao resto do sistema, pode ter a capacidade de ativar
remotamente o microfone e a câmara. Com 33 anos de atraso, um mundo em
que todos têm um telemóvel ultrapassa os pesadelos de George Orwell.
Talvez não tenha «nada a esconder», mas eu continuo a preocupar-me com a
minha privacidade. Os telemóveis são assustadores. Não contem comigo.

Por último, tendo em conta as graves implicações para a sociedade e a
liberdade, recuso-me a perpetuar este sistema. Poderia decidir andar com
um telemóvel mesmo assim, decidindo que, como pessoa aborrecida, posso
sacrificar a liberdade em nome de uma conveniência instantaneamente
gratificante. Mas, ao ser complacente, apenas acrescentaria uma pessoa à
dimensão do problema, um pesado fardo ético quando a utilização da rede
de telemóveis contribui para o
[efeito de rede](https://pt.wikipedia.org/wiki/Efeito_de_rede), como o nome sugere.

Se eu tivesse o meu telemóvel na frente dos outros, estaria a dar a
entender que «os telemóveis não fazem mal». Se alguém me admirar do
ponto de vista ético, também poderá continuar a usar o telemóvel.

Se eu permitisse que os meus amigos me enviassem mensagens de texto em
vez de utilizarem meios de comunicação mais éticos, estaria a dar a
entender que «não há problema em enviar mensagens de texto» e que «é
razoável esperar que as pessoas enviem mensagens de texto». Se eles
estivessem indecisos quanto à ética e à necessidade de ter um telemóvel,
isto poderia levá-los a ficar com ele.

Se eu utilizasse um telemóvel para actividades na aula, estaria a dar a
entender que «os alunos do século XXI devem ter um telemóvel». Preferia
ser o último a resistir na turma para os lembrar de que não se trata de
um pressuposto ético.

Se eu receber um olhar perplexo dos meus conhecidos, confidentes e
professores, tenho agora a oportunidade de os educar sobre <i lang="en">software</i>
livre e privacidade. Poucas pessoas estão cientes dos riscos desses
«dispositivos portáteis de vigilância», como escreveria Richard
[Stallman](https://stallman.org/). Esses «momentos embaraçosos» são
oportunidades perfeitas para ajudá-los a tomar uma decisão mais
informada.

Ao andar com um telemóvel, estaria a perpetuar algo mau. Ao recusar-me
ativamente a andar com um, estou a fazer algo bom.

Então, em vez de utilizar um telemóvel, quais são as minhas alternativas?

Para a maioria das tarefas digitais, incluindo escrever este artigo,
utilizo um computador portátil com
<a href="https://pt.wikipedia.org/wiki/Software_livre"><i lang="en">software</i> livre</a>.
Como suplemento, para me ligar à Internet, utilizo uma
[placa Wi-Fi](https://en.wikipedia.org/wiki/ath9k) que corre <i
lang="en">firmware</i> livre!

Para falar com os meus amigos, utilizo, sempre que possível, protocolos
descentralizados e de especificação aberta. Em particular, estou
disponível por correio eletrónico, XMPP e Mastodon. Em alguns casos em
que isso não é possível devido ao efeito de rede, uso sistemas
centralizados gratuitos como o IRC. Ocasionalmente, uso sistemas
proprietários que foram objeto de engenharia reversa para serem usados
com <i lang="en">software</i> livre, como o Discord <ins>[o projeto de engenharia reversa a
que ela ligou já não existe]</ins>. Sempre que possível, utilizo encriptação
forte implementada com <i lang="en">software</i> livre, como GPG e OTR, para proteção
extra contra ameaças à privacidade. Se a privacidade local for um
problema, ligo-me através do [Tor](https://tor.org/). Qualquer uma destas medidas é um
grande passo em relação as chamadas telefónicas, mensagens de texto,
o Whatsapp ou o Snapchat. Todas elas juntas protegem-te da maioria dos
adversários.

Para me ligar enquanto estou fora de casa, procuro redes Wi-Fi públicas,
que podem ser seguras quando combinadas com encriptação e Tor. Se esta
não for uma opção, talvez tenha de pedir a outra pessoa que me empreste os
seus aparelhos electrónicos &mdash; isto é lamentável, mas enquanto o efeito
de rede estiver em jogo, é eticamente aceitável explorá-lo. Na maior
parte das vezes, evito ligar-me à Internet fora de casa; sou mais
produtivo desligado!

Por isso, sim, consigo viver sem telemóvel. Nem sempre é cómodo, mas a
produtividade, a liberdade e o comportamento ético são sempre mais
importantes do que a conveniência.

Encorajo-te a fazer o mesmo.
