Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2023-11-24 16:45
Lang: eo
Slug: numerar-líneas-de-poema-cada-verso-y-cada-5-versos
Tags: GNU/Linukso, Perl, sed, poezio, nl
Title: Numeri poemajn liniojn, po unu ĉiujn kvin versojn
Save_as: numeri-poemajn-liniojn/index.html
URL: numeri-poemajn-liniojn/
Image: <img src="/wp-content/uploads/2023/11/poemo-mia-biblioteko-de-Raymond-Schwartz-numerita.png" alt="">

Versoj de poemoj, kiujn ni trovas en la Interreto, malofte estas
numeritaj. Numerado ne necesas por legado, sed kiam oni analizas kaj
komentas longan poemon, tio ege utilas. En ĉi tiu artikolo mi lernigas
kiel numeri tekstdosieron (tie ni devas alglui poemon kopiita el la
Interreto).

Kiel [ekzemplon de dosiero](/wp-content/uploads/2023/11/Mia-biblioteko-Raymond-Schwartz.txt) mi uzas la poemon *Mia biblioteko* de
Raymond Schwartz. Se ni volas numeri ĉiujn ĝiajn liniojn, sufiĉas
plenumi la sekvan komandon:

    :::bash
    nl Mia-biblioteko-Raymond-Schwartz.txt

Jen la rezulto:

    :::text
         1	Kiel bravaj grenadiroj,
         2	Vicigitaj flank'-ĉe-flanko,
         3	Sur bretar' de l' libroŝranko
         4	Ili staras en spaliroj.

         5	Admirinda kern-falango,
         6	Ĉiam preta, kun rapiroj
         7	De l' spirit', al militiroj
         8	Kontraŭ la tirana Manko.

         9	Botelvic' da eliksiroj
        10	El la plej nobela sango
        11	Donas spriton al la lango

        12	Kaj inspiron al la liroj
        13	Pli ol lernolibrodiroj!
        14	Al Botel' do estu danko!

<!-- more -->

Konvene `nl`[^1] implicite ne numeras malplenajn liniojn.

Kion fari, se ni volas numeri ĉiun kvin liniojn? Ni forigas la nombron,
kiuj ne estas kvin-obloj. Ni povas doni tiun laboron al Perl[^2]:

    :::bash
    nl Mia-biblioteko-Raymond-Schwartz.txt | perl -pe 's/(^ *[0-9]*[12346789]\b)/" " x length($1)/gei'

Jen la rezulto:

    :::text
           	Kiel bravaj grenadiroj,
           	Vicigitaj flank'-ĉe-flanko,
           	Sur bretar' de l' libroŝranko
           	Ili staras en spaliroj.

          5	Admirinda kern-falango,
           	Ĉiam preta, kun rapiroj
           	De l' spirit', al militiroj
           	Kontraŭ la tirana Manko.

           	Botelvic' da eliksiroj
         10	El la plej nobela sango
           	Donas spriton al la lango

          	Kaj inspiron al la liroj
          	Pli ol lernolibrodiroj!
          	Al Botel' do estu danko!

La antaŭa regula esprimo anstataŭas la numerojn, kiuj ne estas
kvin-obloj, en la linia maldekstro per spacetoj. Ĉar `nl` metas kelkajn
spacetojn en la linia maldekstro, ni uzas `^ *`. Por eviti signa
misaranĝo, ni kalkulas la longecon de la elekto, kiun ni anstataŭigas per
spacetoj (`" " x length($1)`).

Eblas pli simple fari tion per `sed`, sed estiĝus eta signa misaranĝo;
tamen, ĉar `nl` implicite uzas tabojn post numeroj, ĝi ne videblas.

    :::bash
    nl Mia-biblioteko-Raymond-Schwartz.txt | sed 's/^ *[[:digit:]]*[1|2|3|4|6|7|8|9]\b/   /'


[^1]: Ĝi apartenas al la pako `coreutils`, kiu jam estas instalita en
    multaj distribuaĵoj de GNU/Linukso.
[^2]: Rulu `sudo apt install perl`, se ĝi ne estas instalita en via
    komputilo (por distribuaĵoj bazitaj sur Debiano).
