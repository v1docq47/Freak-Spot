Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2020-04-08
Lang: es
Slug: numerar-líneas-de-poema-cada-verso-y-cada-5-versos
Tags: GNU/Linux, Perl, sed, poesía, nl
Title: Numerar líneas de poema, cada verso y cada 5 versos
Image: <img src="/wp-content/uploads/2020/04/Canción-del-pirata-numerada-cada-cinco-versos.png" alt="Canción del pirata numerada cada 5 versos">

Pocas veces están ya numerados los versos de los poemas que encontramos
en Internet. Para la lectura no es necesaria la numeración, pero cuando
se realizan análisis y comentarios de un poema largo, esta resulta muy
útil. En este artículo enseño cómo numerar un archivo de texto (en el
que se supone que habremos pegado el poema copiado de Internet).

Como [archivo de ejemplo](/wp-content/uploads/2020/04/Canción-del-pirata.txt){:download}
uso la *Canción del pirata*. Si queremos numerar todas sus líneas, basta
con ejecutar la siguiente instrucción:

    :::bash
    $ nl Canción-del-pirata.txt

Este es el resultado:

    :::text
         1	Con diez cañones por banda,
         2	viento en popa, á toda vela,
         3	no corta el mar, sino vuela,
         4	mi velero bergantín:
         5	Bajel pirata que llaman,
         6	por su bravura, el Temido,
         7	en todo mar conocido,
         8	del uno al otro confin.

         9	La luna en el mar riela,
     [...]

<!-- more -->

Convenientemente `nl`[^1] no numera por defecto las líneas en blanco.

¿Y si solo queremos numerar cada cinco líneas? Pues eliminamos los
números que no son múltiplos de cinco. Podemos mandarle hacer ese
trabajo a Perl[^2].

    :::bash
    nl Canción-del-pirata.txt | perl -pe 's/(^ *[0-9]*[12346789]\b)/" " x length($1)/gei'

El resultado es...

    :::text
           	Con diez cañones por banda,
          	viento en popa, á toda vela,
          	no corta el mar, sino vuela,
          	mi velero bergantín:
         5	Bajel pirata que llaman,
          	por su bravura, el Temido,
          	en todo mar conocido,
          	del uno al otro confin.

          	La luna en el mar riela,
        10	en la lona gime el viento,
    [...]

Lo que hace la expresión regular anterior es reemplazar los números que
no son múltiplos de cinco que hay a principio de línea por caracteres en
blanco. Como `nl` introduce varios caracteres en blanco a principio de
línea, usamos `^ *`. Para que no haya un desajuste de caracteres,
calculamos la longitud de la selección, que la cambiamos por espacios
(`" " x length($1)`).

Se puede hacer de forma más sencilla con `sed`, pero habría un pequeño
desajuste de caracteres; aunque como `nl` usa tabulaciones por defecto
después de los números, no se nota.

    :::bash
    nl Canción-del-pirata.txt | sed 's/^ *[[:digit:]]*[1|2|3|4|6|7|8|9]\b/   /'


[^1]: Pertenece al paquete `coreutils`, que se encuentra ya instalado en
las diferentes distribuciones de GNU/Linux.
[^2]: Ejecuta `sudo apt install perl` si no lo tienes instalado (para
distribuciones derivadas de Debian)
