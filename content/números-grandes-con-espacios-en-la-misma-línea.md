Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2020-12-09 02:20
Lang: es
Slug: números-grandes-con-espacios-en-la-misma-línea
Title: Escribir números grandes con espacios en la misma línea
Tags: Componer, espacio duro, HTML, formato, ortografía, páginas web, números, recomendación, RAE, Unicode
Image: <img src="/wp-content/uploads/2020/12/como-no-separar-digitos-de-números-grandes-espacios-en-blanco.png" alt="" width="1029" height="434" srcset="/wp-content/uploads/2020/12/como-no-separar-digitos-de-números-grandes-espacios-en-blanco.png 1029w, /wp-content/uploads/2020/12/como-no-separar-digitos-de-números-grandes-espacios-en-blanco-514x217.png 514w" sizes="(max-width: 1029px) 100vw, 1029px">

Esto es un solo número y no debería cortarse para continuar en otra
línea: 2 000 000 000 000 000. En muchos sitios estoy viendo los efectos
de la recomendación de la [RAE](https://es.wikipedia.org/wiki/RAE) que
hace que los números grandes se dividan en dos líneas distintas...

<figure>
<a href="/wp-content/uploads/2020/12/como-no-separar-digitos-de-números-grandes-espacios-en-blanco.png">
<img src="/wp-content/uploads/2020/12/como-no-separar-digitos-de-números-grandes-espacios-en-blanco.png" alt="" width="1029" height="434" srcset="/wp-content/uploads/2020/12/como-no-separar-digitos-de-números-grandes-espacios-en-blanco.png 1029w, /wp-content/uploads/2020/12/como-no-separar-digitos-de-números-grandes-espacios-en-blanco-514x217.png 514w" sizes="(max-width: 1029px) 100vw, 1029px">
</a>
    <figcaption class="wp-caption-text">RAE, vuestra página no da buen
    ejemplo.</figcaption>
</figure>

Se debe introducir un [espacio
duro](https://es.wikipedia.org/wiki/Espacio_duro) para evitar problemas
de formato como el anterior. [Con una combinación de teclas podemos
escribir ese
carácter](/combinaciones-de-teclado-y-tecla-Componer-GNU/Linux/). Yo, en
GNU/Linux, pulso la [tecla
Componer](https://en.wikipedia.org/wiki/Compose_key) y luego dos veces
espacio. El código de [Unicode](https://es.wikipedia.org/wiki/Unicode)
del espacio duro es `0xA0`. En
[HTML](https://es.wikipedia.org/wiki/HTML) también se puede introducir
este carácter escribiendo `&nbsp;` (de
<i><strong>n</strong>on-<strong>b</strong>reaking
<strong>sp</strong>ace</i>).

Que tú veas un número bien en tu ordenador al escribir un texto no
quiere decir que alguien con una resolución menor vaya a ver lo mismo.
Tenlo en cuenta.
