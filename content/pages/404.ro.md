CSS: tipuesearch.css
Date: 2021-01-20
Lang: ro
Slug: 404
Save_as: 404/index.html
URL: 404/
Status: hidden
Title: Pagina nu a fost gasita

Se pare că nu este nimic în acest loc. Ce se întâmplă dacă încercați o căutare?

<noscript>Ne pare rău, motorul de căutare nu funcționează fără JavaScript.</noscript>

<form id="search_form" action="../../buscar.php">
<div class="tipue_search"><input type="search" name="q" id="tipue_search_input" pattern=".{3,}" title="cel puțin 3 caractere" required>
<img alt="icoană de lupă" src="/wp-content/uploads/2017/02/tipue_search.png" class="tipue_search_icon">
</div>
</form>
<div id="tipue_search_content"></div>

<script>
// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
$(document).ready(function() {
    // Si JavaScript está activado, utiliza la versión con JavaScript
     $('#search_form').attr('action', '/eo/serĉi.html');

     $('#tipue_search_input').tipuesearch({
          'mode': 'json',
          'contentLocation': '../../tipuesearch_content.json'
     });
     $('.tipue_search').click(function(e) {
        if (e.target.tagName.toUpperCase() == 'INPUT') {
            e.preventDefault();
        } else {
            $('#search_form').submit();
        }
     });
});
// @license-end
</script>
