Author: Jorge Maldonado Ventura
Date: 2020-04-01 00:11
Modified: 2023-12-10 23:21
Lang: de
Slug: acerca-de
Save_as: über/index.html
URL: über/
Title: Über Freak Spot

Wir schreiben über Freie Software und Freie Kultur mit einer
unabhängigen Perspektive. Das Problem mit vielen Webseiten über Freie
Software ist, dass sie von den Werbentreibenden abhängen. Und solange
sie das Image von manchen Firmen beschönigen, werden sie nicht in der
Lage sein, die Freie Software zu verteidigen weder einen unparteiischen
Standpunkt zu stellen.

Gegründet in Juni 2016, versucht diese Website die andere Seite der
Medaille zu sein. Die ist von und für die Gemeinschaft erstellt, darum
[akzeptieren wir natürlich Beiträge](https://notabug.org/Freak-Spot/Freak-Spot/src/master/README.markdown#colaboraci%C3%B3n).
