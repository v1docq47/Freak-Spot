Author: Jorge Maldonado Ventura
Date: 2016-07-28 17:43
Lang: es
Modified: 2023-12-10 23:22
Slug: acerca-de
Status: published
Title: Sobre Freak Spot

Escribimos sobre <i lang="en">software</i> libre y cultura libre con un punto de vista
independiente. El problema de muchos sitios web sobre <i lang="en">software</i> libre es
que dependen de los anunciantes. No están en condiciones de defender el <i lang="en">software</i> libre ni de
aportar una perspectiva imparcial, ya que requieren ejecutar código
fuente privativo de anuncios y lavan la imagen de ciertas empresas.

Fundado en junio de 2016, este sitio web trata de ser la otra cara de la
moneda. Está hecho por y para la comunidad, así que naturalmente
[admitimos colaboraciones](https://notabug.org/Freak-Spot/Freak-Spot/src/master/README.markdown#colaboraci%C3%B3n).
