Author: Jorge Maldonado Ventura
CSS: tipuesearch.css
Date: 2020-09-18
JS: tipuesearch_set.eo.js (top), tipuesearch.js (top)
Lang: eo
Save_as: serĉi.html
Slug: buscar
Status: hidden
Title: Serĉi
Url: serĉi.html

<form id="search_form" action="../buscar.php">
<div class="tipue_search"><input type="search" name="q" id="tipue_search_input" pattern=".{3,}" title="Almenaŭ 3 signoj" required>
<img alt="bildsimbolo de lupeo" src="/wp-content/uploads/2017/02/tipue_search.png" class="tipue_search_icon">
</div>
</form>
<div id="tipue_search_content"></div>

<script>
// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
$(document).ready(function() {
     // Si JavaScript está activado, utiliza la versión con JavaScript
     $('#search_form').attr('action', 'serĉi.html');

     $('#tipue_search_input').tipuesearch({
          'mode': 'json',
          'contentLocation': 'tipuesearch_content.json'
     });
     $('.tipue_search').click(function(e) {
        if (e.target.tagName.toUpperCase() == 'INPUT') {
            e.preventDefault();
        } else {
            $('#search_form').submit();
        }
     });
});
// @license-end
</script>
