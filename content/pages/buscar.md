Author: Jorge Maldonado Ventura
CSS: tipuesearch/tipuesearch.css
Date: 2017-02-21 09:08
JS: tipuesearch/tipuesearch_set.js (top), tipuesearch/tipuesearch.js (top)
Lang: es
Modified: 2017-04-22 20:34
Save_as: buscar.html
Slug: buscar
Status: hidden
Title: Buscar
URL: buscar.html

<form id="search_form" action="buscar.php">
<div class="tipue_search"><input type="search" name="q" id="tipue_search_input" pattern=".{3,}" title="Al menos 3 caracteres" required>
<img alt="icono de una lupa" src="/wp-content/uploads/2017/02/tipue_search.png" class="tipue_search_icon">
</div>
</form>
<div id="tipue_search_content"></div>

<script>
// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
$(document).ready(function() {
     // Si JavaScript está activado, utiliza la versión con JavaScript
     $('#search_form').attr('action', 'buscar.html');

     $('#tipue_search_input').tipuesearch({
          'mode': 'json',
          'contentLocation': 'tipuesearch_content.json'
     });
     $('.tipue_search').click(function(e) {
        if (e.target.tagName.toUpperCase() == 'INPUT') {
            e.preventDefault();
        } else {
            $('#search_form').submit();
        }
     });
});
// @license-end
</script>
