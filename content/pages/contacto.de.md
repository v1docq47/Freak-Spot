Author: Jorge Maldonado Ventura
Date: 2018-03-17 19:04
Lang: de
Modified: 2023-04-23 15:00
Slug: contacto
Status: published
Title: Kontakt

Mein Email ist
[jorgesumle@freakspot.net](mailto:jorgesumle@freakspot.net). Nutz
meinen
<abbr title="GNU Privacy Guard">GPG</abbr> öffentlichen Schlüssel
([4BF5 360D 50C8 4085 5648 644E 1200 84A8 5F2F 6B50](/Jorge_jorgesumle@freakspot.net-0x120084A85F2F6B50-pub.asc)),
damit die Nachricht nicht von anderen Personen gelesen werden kann.
