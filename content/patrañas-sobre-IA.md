Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2023-09-05 15:00
Lang: es
Slug: patrañas-sobre-IA
Tags: burbuja, capitalismo, consejos, IA
Title: Patrañas sobre IA

Hay una burbuja del aprendizaje automático, pero esta tecnología ha
llegado para quedarse. Cuando la burbuja explote, el mundo *habrá*
cambiado a causa del aprendizaje automático. Pero probablemente será
peor, no mejor.

A diferencia de lo que esperan los catastrofistas de la IA, el mundo no
desaparecerá más rápido gracias a la IA. Los avances actuales en el
aprendizaje automático no nos acercan a la IAF, y, como señaló Randall
Monroe en 2018:

<a href="/wp-content/uploads/2023/09/futuro-robot.png">
<img src="/wp-content/uploads/2023/09/futuro-robot.png" alt="" width="1223" height="412" srcset="/wp-content/uploads/2023/09/futuro-robot.png 1223w, /wp-content/uploads/2023/09/futuro-robot-611x206.png 611w" sizes="(max-width: 1223px) 100vw, 1223px">
</a>

Lo que sucederá con la IA es el viejo y aburrido capitalismo. Su
capacidad de permanecer en el poder consistirá en reemplazar a humanos
competentes y caros con robots cutres y baratos. Los modelos de lenguaje
de gran tamaño son un gran avance respecto a las cadenas de Markov, y
Stable Diffusion puede generar imágenes que solo son un poco extrañas
manipulando un poco los comandos. Los programadores mediocres usarán
GitHub Copilot para escribir código trivial y plantillas (el código
trivial es tautológicamente de poco interés), y el aprendizaje de
máquina probablemente seguirá siendo útil para escribir cartas de
presentación por ti. Los coches autónomos podrían llegar En Cualquier
Momento™, lo que será genial para los entusiastas de la ciencia ficción
y los tecnócratas, pero mucho peor en todos los aspectos que, por
ejemplo, [construir más trenes](https://yewtu.be/watch?v=0dKrUE_O0VE).

Los mayores cambios duraderos del aprendizaje automático serán más bien
los siguientes:

- Una reducción de la mano de obra para trabajos creativos cualificados.
- La total eliminación de humanos en puestos de atención al cliente.
- Contenidos basura y <i lang="en">phishing</i> más convincentes,
  estafas más escalables.
- Granjas de contenidos que dominarán con ardides los resultados de búsqueda.
- Granjas de libros (tanto electrónicos como en papel) saturarán el
  mercado.
- Los contenidos generados por IA saturarán las redes sociales.
- Propaganda y campañas artificiales generalizadas, tanto en política
  como en publicidad.

  Las empresas de IA seguirán generando residuos y emisiones de
  CO<sub>2</sub> a gran escala, ya que extraen agresivamente todo el
  contenido de Internet que pueden encontrar, externalizando los costes
  a la infraestructura digital mundial, y nutren con ese acopio las granjas
  de GPUs para generar sus modelos. Puede que los humanos tengan poder
  de decisión ayudando a etiquetar el contenido, para lo que buscarán
  los mercados más baratos con las leyes laborales más débiles con el
  objetivo de construir fábricas que explotan a trabajadores para
  alimentar al monstruo de datos que es la IA.

Nunca confiarás en otra reseña de un producto. Nunca volverás a hablar
con un humano en la empresa que te proporciona Internet. El mundo
digital que te rodea se llenará de contenidos insípidos y lacónicos. La
tecnología creada para las granjas de interacciones &mdash;esos vídeos
editados por IA con la chirriante voz de máquina que has visto
últimamente en los canales que sigues&mdash; será comercializada como una marca
blanca y usada para promover productos e ideologías a escala masiva con
un coste mínimo desde cuentas de redes sociales que son llenadas con
contenido de IA, cultivan una audiencia y se venden al por mayor y en
regla con el algoritmo.

Todas estas cosas ya están sucediendo e irán a peor. El futuro de los
medios es una regurgitación insípida y sin alma de todos los medios
anteriores a la era de la IA, y el destino de todos los nuevos medios
creativos es ser subsumidos en el amasijo enturbiante de matemáticas.

Esto será increíblemente rentable para los barones de la IA, y para
asegurar su inversión están desplegando una inmensa y cara campaña de
propaganda mundial. Para el público, las capacidades actuales y
futuras posibles de la tecnología están siendo exageradas en promesas
altisonantes ridículamente improbables. En reuniones a puerta cerrada se
hacen promesas mucho más realistas de reducir los costes a la mitad.

La propaganda también se apoya en el canon místico de la IA de ciencia
ficción: la amenaza de ordenadores inteligentes con poder para acabar
con el mundo, el encanto prohibido de un nuevo Proyecto Manhattan y
todas sus consecuencias, la tan profetizada singularidad. La tecnología
ni se acerca a este nivel, un hecho bien sabido por expertos y los
propios barones, pero la ilusión es mantenida con el intereses de
presionar a los legisladores para que ayuden a los barones a erigir
una muralla alrededor de su nueva industria.

Por supuesto, la IA representa una amenaza de violencia, pero como
señala Randall, no proviene de la propia IA, sino de las personas que la
emplean. El ejército de EE.&nbsp;UU. está probando drones controlados
por IA, que no van a ser autoconscientes, pero aumentarán a gran escala
los errores humanos (o la malicia humana) hasta que mueran personas
inocentes. Las herramientas de IA ya se están usando para imponer
fianzas y condiciones de libertad condicional &mdash;pueden meterte en
la cárcel o mantenerte allí&mdash;. La policía está usando la IA para
reconocimiento facial y «actuaciones policiales predictivas».
Naturalmente, todos estos modelos acaban discriminando a las minorías,
privándolas de libertad y, a menudo, matándolas.

La IA se caracteriza por un capitalismo agresivo. La burbuja
propagandística ha sido creada por inversores y capitalistas que
invierten en ella, y los beneficios que esperan de esa inversión van a
salir de tu bolsillo. No se acerca la singularidad, sino que las
promesas más realistas de la IA van a empeorar el mundo. La revolución
de la IA ya está aquí, y no me gusta nada.

<details style="margin-bottom: 1em; cursor: pointer;">
  <summary>Mensaje provocativo</summary>
<p>Redacté la primera versión de un artículo mucho más incendiario bajo
el título «ChatGPT es el nuevo sustituto tecnoateísta de Dios». Hace
algunas comparaciones bastante pertinentes entre el culto a las
criptomonedas y el culto al aprendizaje automático, y entre la
religiosa, inquebrantable
y en gran medida ignorante fe en ambas tecnologías como precursoras del progreso.
Fue divertido escribirlo, pero este es probablemente un artículo
mejor.</p>
<p>Encontré este comentario en Hacker News y lo cité en el borrador
original: «Probablemente vale la pena hablar con GPT4 antes de buscar
ayuda profesional [para tratar la depresión]».
<p>En caso de que necesites oírlo: <a href="https://es.euronews.com/next/2023/04/01/un-hombre-se-suicida-despues-de-que-un-chat-de-ia-le-invitara-a-hacerlo">no</a> (advertencia: suicidio) acudas a los
servicios de OpenAI para combatir tu depresión. Encontrar y concertar
una cita con un terapeuta puede ser difícil para mucha gente &mdash;es
normal que sientas que es difícil&mdash;. Habla con tus amigos y pídeles
que te ayuden a encontrar el tratamiento adecuado para tus
necesidades.</p>
</details>

*Este artículo es una traducción del artículo «[AI crap](https://drewdevault.com/2023/08/29/2023-08-29-AI-crap.html)»
publicado por Drew Devault bajo la licencia [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.es)*.
