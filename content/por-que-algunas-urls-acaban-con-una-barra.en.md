Author: Jorge Maldonado Ventura
Category: Web development
Date: 2023-02-12 22:40
Lang: en
Slug: por-que-algunas-urls-acaban-con-una-barra
Status: published
Save_as: why-some-URLs-end-with-a-slash/index.html
URL: why-some-URLs-end-with-a-slash/
Tags: web addresses, http, https, URL, main page, website, protocol, URI, url, URLs
Title: Why do some URLs end with a slash?

You have probably come across URLs that end with a slash (such as
<https://freakspot.net/en/> or [/](/), the server's root directory) and
others that don't (such as this one:
<https://www.gnu.org/gnu/gnu.html>).
What's the difference? Is it important?

A [URL](https://en.wikipedia.org/wiki/URL) is basically an address to a
resource. URLs do not only refer to web pages, but also to other types
of resources. Examples of URL schemes are `http`, `https`, `ftp`, `telnet`, `data`
and `mailto`. In this article I am referring to web pages that use
either the `http` or the `https` scheme.

**URLs that end with a slash refer to a directory; URLs that do not end
with a slash refer to a file**. When you click on the link
<https://freakspot.net/en>, the server realises that the requested
address is not a file and goes to <https://freakspot.net/en/>. There it
finds a main file called `index.html` or *index with another extension*
and displays its contents.

Consequently, the page loads faster when we use links to main pages
ending with slashes (e.g. [/](/)) or when we link to the filename (e.g.
<https://www.example.com/index.html>).
