Author: Jorge Maldonado Ventura
Category: Reta programado
Date: 2023-01-16 10:00
Lang: eo
Slug: por-que-algunas-urls-acaban-con-una-barra
Status: published
Save_as: kial-kelkaj-URL-finiĝas-per-suprenstreko/index.html
URL: kial-kelkaj-URL-finiĝas-per-suprenstreko/
Tags: retadresoj, http, https, Unuforma Rimeda Lokindiko, ĉefa paĝo, retejo, protokolo, URI, url, URL-j
Title: Kial kelkaj URL-j finiĝas per suprenstreko?

Eble vi trovis URL-jn, kiuj finiĝas per suprenstreko (kiel
[https://freakspot.net/eo/](https://freakspot.net/eo/) aŭ [/](/),
servila dosieruja radiko), kaj aliajn, kiuj ne finiĝas per suprenstreko
(kiel ĉi tiu : <https://www.gnu.org/gnu/gnu.html>). Kio estas la
diferenco? Ĉu gravas?

[URL](https://eo.wikipedia.org/wiki/URL) estas esence adreso al
afero. La URL-j ne nur aludas retejojn, sed ankaŭ aliajn specojn de
aferoj. Kelkaj ekzemploj de URL-skemoj estas `http`, `https`, `ftp`,
`telnet`, `data` kaj `mailto`. En ĉi tiu artikolo mi parolas pri
retejoj, kiuj uzas la `http`- aŭ la `https`-skemon.

**URL-adresoj, kiuj finiĝas per suprenstreko, referencas dosierujon;
tiuj, kiuj ne, referencas dosieron**. Kiam vi klakas la ligilon
<https://freakspot.net/eo>, la servilo rimarkas, ke la bezonata adreso
ne estas dosiero, kaj iras al <https://freakspot.net/eo/>. Ĉi tie ĝi
trovas ĉefan dosieron nomitan `index.html` aŭ *index kun alia finaĵo*
kaj montras ĝian enhavon.

Sekve la ŝarĝo de retejo estas pli rapida, kiam ni uzas ligilojn al
ĉefaj retejoj, kiuj finiĝas per suprenstrekoj (ekzemple [/](/)) aŭ kiam
ni ligas al la dosiernomo (ekzemple
<https://freakspot.net/eo/arba-strukturo-per-CSS-kaj-HTML/index.html>).
