Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2016-09-10 19:33
Image: <img src="/wp-content/uploads/2016/09/solidaridad.jpeg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" width="500" height="375" alt="Presa política sale después de 8 años en la cárcel por un hurto" srcset="/wp-content/uploads/2016/09/solidaridad.jpeg 500w, /wp-content/uploads/2016/09/solidaridad-300x225.jpeg 300w" sizes="(max-width: 500px) 100vw, 500px">
Lang: es
Modified: 2017-02-25 17:34
Slug: presa-politica-sale-despues-de-8-anos-en-la-carcel-por-un-supuesto-hurto
Status: published
Tags: anarquista, cárceles, España, presos políticos
Title: Presa política sale después de 8 años en la cárcel por un hurto

La presa política anarquista Noelia Cotelo sale hoy de la cárcel tras 8
años de vejaciones en las instituciones penitenciarias españolas. En
este programa de radio, su madre relata las injusticias que sufrió en
prisión: <https://archive.org/details/19OctZgzNoeliaCotelo>

Ha sido una presa política valiente que ha resistido innumerables
torturas en prisión. Fue condenada por un hurto un año y medio, que se
ha prolongado por denunciar las injusticias que sufría en la cárcel:
abusos sexuales, humillación, palizas, amenazas de muerte...

¡Solidaridad con todos los incontables presos políticos!
