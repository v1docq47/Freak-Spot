Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Bash
Date: 2017-03-29 04:55
Image: <img src="/wp-content/uploads/2017/03/mi-bash-git-prompt.png" alt="git-bash-prompt">
JS: asciinema-player.js (top)
Lang: es
Modified: 2021-11-02
Slug: prompt-para-git-en-bash
Tags: Bash, .bashrc, configuración de Bash, Git, interfaz de línea de órdenes, repositorios
Title: Prompt para Git en Bash

Si eres programador y has trabajado con Git, seguramente habrás
comprobado alguna vez si el repositorio en el que estás trabajando está
actualizado, la rama en la que te encuentras, etc. Pero cada vez que
haces esto tienes que ejecutar alguna instrucción de Git. ¿No sería más
cómodo tener siempre esa información a simple vista?

<!-- more -->

Puedes modificar las [variables de
entorno](https://es.wikipedia.org/wiki/Variable_de_entorno) de tu shell
para que esa información se muestre automáticamente cuando te encuentres
en un repositorio. Por suerte, ya hay proyectos de <i lang="en">software</i> libre que se
han encargado de ello. Yo conozco dos:
[bash-git-prompt](https://github.com/magicmonty/bash-git-prompt) y
[git-status-prompt](https://notabug.org/bill-auger/git-status-prompt).
Hay muchos más, estos dos son simplemente los que yo he usado. En este
artículo os enseñaré a instalar y configurar bash-git-prompt.

La motivación que llevó a crear bash-git-prompt se encuentra en [este
artículo](http://sebastiancelis.com/2009/11/16/zsh-prompt-git-users/)
escrito por [Sebastian Celis](http://sebastiancelis.com/). Básicamente,
el objetivo era crear un programa rápido y que mostrará la mayor
información posible utilizando el menor número de caracteres posible.
Originalmente se creó este programa para la shell
<abbr title="Z shell">zsh</abbr>, y más tarde nació bash-git-prompt para
Bash, como una migración de la versión para
<abbr title="Z shell">zsh</abbr> que se encuentra en
<https://github.com/olivierverdier/zsh-git-prompt>.

Para instalar bash-git-prompt, clona el repositorio
[bash-git-prompt](https://github.com/magicmonty/bash-git-prompt):

`git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1`

Y añade al archivo `~/.bashrc` las siguientes líneas:

    ::bash
    # bash-git-prompt
    GIT_PROMPT_ONLY_IN_REPO=1
    source ~/.bash-git-prompt/gitprompt.sh

Hay muchas variables de configuración que se pueden añadir además de
`GIT_PROMPT_ONLY_IN_REPO`. Lee atentamente el README del repositorio
bash-git-prompt para conocer todas las variables de configuración
existentes.

![Bash Git prompt](/wp-content/uploads/2017/03/bash-git-prompt-original.png)

Si [recargamos la configuración de
Bash](/recargar-la-configuracion-de-bash-bashrc/)
y entramos en el directorio de un repositorio, podremos ver la nueva
apariencia de la interfaz de línea de órdenes que trae bash-git-prompt
por defecto. Cada símbolo tiene un significado diferente. Una vez
aprendas lo que significa cada símbolo, podrás saber de un vistazo si el
repositorio está actualizado, el número de archivos que se han
modificado...

Se puede personalizar muy fácilmente. A mí no me resulta muy útil que me
muestre la hora actual y un salto de línea, así que he creado un nuevo
diseño que no tiene esas características.

A continuación, dejo un vídeo grabado con
[asciinema](https://asciinema.org/) en el que enseño cómo instalar
bash-git-prompt desde cero y cómo crear un tema personalizado.

<asciinema-player src="../asciicasts/bash-git-prompt.json" poster="npt:2:53">
Lo siento, asciinema-player no funciona sin JavaScript.
</asciinema-player>
