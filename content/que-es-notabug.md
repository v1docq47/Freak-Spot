Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2016-12-29 09:55
Image: <img src="/wp-content/uploads/2016/12/NotABug.png" width="220" height="202" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="¿Qué es NotABug?">
Lang: es
Modified: 2017-03-20 14:25
Slug: que-es-notabug
Status: published
Tags: alojamiento de código, code hosting, Git, Gogs, NotABug, NotABug.org, programación, repositorios
Title: ¿Qué es NotABug?

NotABug o [NotABug.org](https://notabug.org/) es una plataforma que
permite gestionar repositorios de código con
[Git](https://es.wikipedia.org/wiki/Git). La plataforma utiliza un
programa llamado [Gogs](https://gogs.io/), que es <i lang="en">software</i> libre, lo que
permite que pueda ser instalado localmente o en un servidor.

Entre sus características destacan las siguientes:

-   Alojamiento para proyectos públicos y privados.
-   Repositorios Git, [Wiki](https://es.wikipedia.org/wiki/Wiki) y
    [sistema de seguimiento de
    errores](https://es.wikipedia.org/wiki/Sistema_de_seguimiento_de_errores)*.*
-   Privacidad para los usuarios y visitantes: funciona también en la
    red Tor y tiene todo el código JavaScript alojado en su
    propio servidor.
-   Interfaz gráfica sencilla y muy rápida.
-   Alojamiento gratuito para proyectos de <i lang="en">software</i> libre.

Estoy registrado en NotABug desde el 26 de julio de 2016, y es la única
plataforma que utilizo actualmente para desarrollar mis propios
proyectos. También he colaborado con proyectos interesantes en NotABug
como [Awesome gamedev](https://notabug.org/Calinou/awesome-gamedev),
[LibreVideoJS](https://notabug.org/Heckyel/LibreVideoJS),
[LibreVideoJS-wp](https://notabug.org/Heckyel/librevideojs-html5-player),
[Résumér](https://notabug.org/SylvieLorxu/Resumer),
[mkblog.sh](https://notabug.org/SylvieLorxu/mkblog.sh) y [la página web
de la comunidad Peer](https://peers.community/). Puedes encontrar más
información sobre los proyectos en los que estoy involucrado visitando
[mi perfil de NotABug](https://notabug.org/jorgesumle/).

Hasta ahora NotABug es la mejor plataforma que conozco para desarrollar
programas usando Git. Es fácil de usar y de realizar proyectos
colaborativos. Además, la interfaz gráfica es muy sencilla y ágil, a
diferencia de otras alternativas como [Gitlab](https://gitlab.com/).
