Author: Jorge Maldonado Ventura
Category: Uncategorized
Date: 2017-09-22 18:23
Image: <img src="/wp-content/uploads/2017/09/reddit-reemplazado-por-raddle.png" alt="Face of the mascot of Reddit covered by the mascot of Raddle">
Lang: en
Slug: raddle-como-respuesta-a-reddit
Tags: censorship, corporations, debate, discussion, webpages, Reddit, Raddle, voting,
Title: Raddle in response to Reddit

[Raddle](https://raddle.me/) is here as an answer to the decisions
contrary to the community of the Reddit corporation, which [recently
decided to stop publishing its source code as free
software](https://www.reddit.com/r/changelog/comments/6xfyfg/an_update_on_the_state_of_the_redditreddit_and/).
In addition, it's raising some privacy concerns:

- The cookies from Reddit [can track you accross different domains](https://www.reddit.com/help/privacypolicy?v=b51465c2-761d-11e7-9160-0e433a32d3b8).
- As Reddit is located in the
  <abbr title="United States of America">USA</abbr>,
  <a href="https://www.siliconrepublic.com/life/reddit-warrant-canary-disappearance">the <abbr title="Federal Bureau of Investigation">FBI</abbr> has obtained</a>
  and could obtain in the future all the information they want.

The main reason for the creation of Raddle seems to be, however, the
censorship and the deletion of accounts from leftist groups, according
to the explanation on the
[wiki page "History"](https://raddle.me/wiki/history) from Raddle.

## What changes make Raddle different to Reddit?

<a href="/wp-content/uploads/2017/09/Raddle-foros.png">
<img src="/wp-content/uploads/2017/09/Raddle-foros.png" alt="The overview of Raddle is simple" width="1004" height="814" srcset="/wp-content/uploads/2017/09/Raddle-foros.png 1004w, /wp-content/uploads/2017/09/Raddle-foros-502x407.png 502w" sizes="(max-width: 1004px) 100vw, 1004px">
</a>

<!-- more -->

Raddle is free software, collaborative and doesn't track its users. It
doesn't have any shadowy economic interest, as it's not controlled by a
private company, but conceived as a community space. In addition, it has
a Tor hidden service: <http://c32zjeghcp5tj3kb72pltz56piei66drc63vkhn5yixiyk4cmerrjtid.onion/>.

Being a collaborative project, new features can be included after they
are accepted by the community in the
[meta forum](https://raddle.me/f/meta).


Originally, Raddle was called Raddit, but [its name was changed](https://raddle.me/f/meta/6779/we-ve-rebranded-to-raddle-me) to
avoid being compared with Reddit and prevent possible problems with
domain registrars.

## How does Raddle work?

<figure>
    <a href="/wp-content/uploads/2017/09/discusión-en-Raddle.png">
    <img src="/wp-content/uploads/2017/09/discusión-en-Raddle.png" alt="" width="1335" height="849" srcset="/wp-content/uploads/2017/09/discusión-en-Raddle.png 1335w, /wp-content/uploads/2017/09/discusión-en-Raddle-667x424.png 667w" sizes="(max-width: 1335px) 100vw, 1335px">
    </a>
    <figcaption class="wp-caption-text">Debate from Raddle</figcaption>
</figure>

Raddle is a place to share articles and to create discussions and
votings. The content is organized in forums. One can subscribe to
different forums to be informed about its news. We can use different
filters to view the content that interests us: by the most recent, most
voted, by negative votes, by activity.

Some information about the service itself can be found in the wiki of
Raddit, which can be edited by anyone, except some blocked pages (such
as the privacy policy).

The appearance of Raddle can be customized very easily choosing some of
the existing themes or creating a custom one. Raddle is translated into
different languages.

<a href="/wp-content/uploads/2017/09/usando-Raddle.png">
<img src="/wp-content/uploads/2017/09/usando-Raddle.png" alt="In Raddle you can upvote and downvote" width="1000" height="868" srcset="/wp-content/uploads/2017/09/usando-Raddle.png 1000w, /wp-content/uploads/2017/09/usando-Raddle-500x434.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
