Author: Jorge Maldonado Ventura
Category: Senkategoria
Date: 2020-10-07 14:00
Image: <img src="/wp-content/uploads/2017/09/reddit-reemplazado-por-raddle.png" alt="Vizaĝo de la besto de Reddit kovrita per la besto de Raddle">
Lang: eo
Slug: raddle-como-respuesta-a-reddit
Save_as: raddle-kiel-respondo-al-reddit/index.html
URL: raddle-kiel-respondo-al-reddit/
Tags: cenzuro, firmoj, debato, disputo, retejoj, Reddit, Raddle, voĉdonoj
Title: Raddle kiel respondo al Reddit

[Raddle](https://raddle.me/) estas ĉi tie kiel respondo al la decidoj
de la firmo Reddit kontraŭ la komunumo, kiu [decidis ĉesi afiŝi sian fontokodon kiel libera programaro](https://www.reddit.com/r/changelog/comments/6xfyfg/an_update_on_the_state_of_the_redditreddit_and/).
Krome ĝi havas kelkajn privatecajn problemojn:

- La kuketoj de Reddit [povas sekvi vin trans diversaj domajnoj](https://www.reddit.com/help/privacypolicy?v=b51465c2-761d-11e7-9160-0e433a32d3b8).
- Ĉar Reddit troviĝas en Usono, <a href="https://www.siliconrepublic.com/life/reddit-warrant-canary-disappearance">la <abbr title="Federacia Buroo de Enketado, angle «Federal Bureau of Investigation»">FBI</abbr> prenis</a>
  kaj povus repreni ĉiun informon, kiun ili volas.

  La ĉefa kialo por la estigo de Raddle ŝajnas tamen esti la cenzurado
  kaj la forigo de kontoj de maldekstraj aroj, kiel klarigas la [vikia
  paĝo "History"](https://raddle.me/wiki/history) de Raddle.

## Kio ŝanĝas Raddle rilate al Reddit?

<a href="/wp-content/uploads/2020/10/Raddle-diskutejoj.png">
<img src="/wp-content/uploads/2020/10/Raddle-diskutejoj.png" alt="La superrigardo ĝenerala estas simpla" width="1002" height="1004" srcset="/wp-content/uploads/2020/10/Raddle-diskutejoj.png 1002w, /wp-content/uploads/2020/10/Raddle-diskutejoj-501x502.png 501w" sizes="(max-width: 1002px) 100vw, 1002px">
</a>

<!-- more -->

Raddle estas libera programaro, kunlabora kaj ne sekvas ĝiajn uzantojn.
Ĝi ne havas ian aĉan monan intereson, ĉar ĝi ne estas per privata
korporacio kontrolita, sed estas pensita kiel komunuma ejo. Ankaŭ ĝi
havas kaŝitan Tor-servon: <http://c32zjeghcp5tj3kb72pltz56piei66drc63vkhn5yixiyk4cmerrjtid.onion/>.

Ĉar ĝi estas kunlabora projekto, oni povas aldoni novajn funkciojn, post
kiam ili estas akceptitaj de la komunumo en la [«meta»
diskutejo](https://raddle.me/f/meta).

Origine, Raddle nomiĝis Raddit, sed [ĝia nomo estis ŝanĝita](https://raddle.me/f/meta/6779/we-ve-rebranded-to-raddle-me), por ke ĝi ne estu komparita kun Reddit kaj por eviti eblajn problemojn kun la registranto de domajnoj.

## Kiel funkcias Raddle?

<figure>
    <a href="/wp-content/uploads/2020/10/diskuto-en-Raddle.png">
    <img src="/wp-content/uploads/2020/10/diskuto-en-Raddle.png" alt="" width="1002" height="1004" srcset="/wp-content/uploads/2020/10/diskuto-en-Raddle.png 1002w, /wp-content/uploads/2020/10/diskuto-en-Raddle-501x502.png 501w" sizes="(max-width: 1002px) 100vw, 1002px">
    </a>
    <figcaption class="wp-caption-text">Diskuto de Raddle</figcaption>
</figure>

Raddle estas ejo por disdoni artikolojn kaj krei diskutojn kaj voĉdoni.
La enhavo estas organizita per diskutejoj. Oni povas aboni la
diversajn diskutejojn por esti informita de iliaj novaĵoj. Eblas uzi
diversajn filtrilojn por vidi la enhavon, pri kiu ni havas intereson:
laŭ novaj, jesaj voĉdonoj, neaj voĉdonoj, aktiveco.

Iom da informo pri la servo mem povas troviĝas en la vikio de Raddle,
kiun ĉiu ajn povas redakti, ekcepte iuj blokitaj paĝoj (kiel la regularo
pri privateco).

La aspekto de Raddle povas esti facilege agordita elektante kelkajn de
la disponeblaj etosoj aŭ kreante propran. Raddle estas tradukita al
kelkaj lingvoj.

<a href="/wp-content/uploads/2020/10/uzado-de-Raddle.png">
<img src="/wp-content/uploads/2020/10/uzado-de-Raddle.png" alt="En Raddle oni povas voĉdoni supren kaj malsupren" width="1002" height="1004" srcset="/wp-content/uploads/2020/10/uzado-de-Raddle.png 1002w, /wp-content/uploads/2020/10/uzado-de-Raddle-501x502.png 501w" sizes="(max-width: 1002px) 100vw, 1002px">
</a>
