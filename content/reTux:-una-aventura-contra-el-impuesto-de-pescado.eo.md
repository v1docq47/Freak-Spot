Author: Jorge Maldonado Ventura
Category: Videludoj
Date: 2018-08-19 15:18
Modified: 2022-08-10 16:54
Image: <img src="/wp-content/uploads/2018/08/ReTux-neĝa-malamiko.png" alt="Bildo pri Tukso en kastelo kontraŭ fina malamiko">
Lang: eo
Save_as: retux-aventuro-kontraŭ-fiŝa-imposto/index.html
Slug: retux-una-aventura-contra-el-impuesto-de-pescado
Tags: 2D, libera kulturo, GNU/Linukso, platforma, ReTux, Tukso, videludo, Vindozo
Title: <cite>ReTux</cite>: aventuro kontraŭ la fiŝa imposto
Url: retux-aventuro-kontraŭ-fiŝa-imposto/

En la videludo ReTux, Tukso estas la ĉefrolulo de aventuro, en kiu li
devos venki la Neĝan Reĝon, kiu klopodas trudi imposton pri fiŝoj. Neniu
pingveno ŝatas, ke oni forprenu riajn fiŝojn.

<!-- more -->

ReTux estas platforma videludo programare kaj
[kulture](http://freedomdefined.org/Definition/Eo) libera por komputilo.
Oni povas elŝuti ĝin senpage el [ĝia oficiala
retejo](https://retux-game.github.io/).

La ludo havas similan trajton ol
[SuperTux](https://www.supertux.org/), ankaŭ kelkajn bruojn,
bildojn kaj muzikon. Sed ankaŭ havas proprajn trajtojn. Unu el miaj
favorataj estas, ke Tukso povas preni, ĵeti kaj malpreni objektojn. Se vi
bezonas rompi lignan blokon, kiu staras ekster atingodistanco de Tukso,
vi povas preni kaj ĵeti glacian blokon supren por rompi ĝin. La ebloj
estas grandegaj, kaj oni povas esti tre krea.

<a href="/wp-content/uploads/2018/08/ReTux-kaŝita-pingveno.png">
<img src="/wp-content/uploads/2018/08/ReTux-kaŝita-pingveno.png" alt="Kaŝita pingveno en ReTux" width="800" height="448" srcset="/wp-content/uploads/2018/08/ReTux-kaŝita-pingveno.png 800w, /wp-content/uploads/2018/08/ReTux-kaŝita-pingveno-400x224.png 400w" sizes="(max-width: 800px) 100vw, 800px">
</a>

Kiam vi venkas la Neĝan Reĝon, vi povas peni trovi ĉiujn kaŝitajn
pingvenojn.  En ĉiu nivelo estas kaŝita pingveno. Mi avertas, ke kelkaj
estas tre malfacila trovi. En kelkaj niveloj vi trovos konsiletojn, kiuj
vin helpos trovi ilin.

<a href="/wp-content/uploads/2018/08/ReTux-konsileto.png">
<img src="/wp-content/uploads/2018/08/ReTux-konsileto.png" alt="Konsileto por trovi kaŝitan pingvenon" width="800" height="448" srcset="/wp-content/uploads/2018/08/ReTux-konsileto.png 800w, /wp-content/uploads/2018/08/ReTux-konsileto-400x224.png 400w" sizes="(max-width: 800px) 100vw, 800px">
</a>

Mi ĝojis tre la ludon, kaj mi rekomendas al homoj el ĉiu aĝo, speciale
al amantoj de plataĵaj videludoj. ReTux estas simpla kaj amuza ludo, al
kiu ĉiam vi povos dediĉi kelkajn minutojn.

Ĉar video diras pli ol mil vortojn, rigardu la antaŭprezentaĵon kaj
ludu.

<video controls preload="none" poster="/wp-content/uploads/2017/04/ReTux-poster-trailer.png" data-setup="{}">
  <source src="https://b2aeaa58a57a200320db-8b65b95250e902c437b256b5abf3eac7.ssl.cf5.rackcdn.com/media_entries/9356/ReTux_Trailer.webm" type="video/webm">
  <p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>
