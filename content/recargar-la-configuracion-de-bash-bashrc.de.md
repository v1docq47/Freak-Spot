Author: Jorge Maldonado Ventura
Category: Bash
Date: 2019-09-09
Modified: 2022-11-02 23:00
Lang: de
Slug: recargar-la-configuracion-de-bash-bashrc
Tags: .bashrc, Bash, Konfiguration von Bash, GNU/Linux, Befehlszeile, source
Title: Die Konfiguration von Bash neu laden (<code>.bashrc</code>)
Save_as: die-Konfiguration-von-Bash-neuladen/index.html
URL: die-Konfiguration-von-Bash-neuladen/

Wenn du die Konfigurationsdatei von Bash (`~/.bashrc`) änderst, muss du
an- und abmelden, um die Änderung zu bewirken. Du kannst auch den Befehl
`source ~/.bashrc` oder `. ~/.bashrc` (sie sind gleichwertig) ausführen.
