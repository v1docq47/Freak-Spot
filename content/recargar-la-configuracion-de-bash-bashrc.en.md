Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-11-02 23:00
Lang: en
Slug: recargar-la-configuracion-de-bash-bashrc
Tags: .bashrc, Bash, Bash configuration, GNU/Linux, command-line, source
Title: Reload Bash configuration (<code>.bashrc</code>)
Save_as: reload-Bash-configuration/index.html
URL: reload-Bash-configuration/

If you change the Bash configuration file (`~/.bashrc`), you must logout
and login for the changes to take effect. You can also execute the
command `source ~/.bashrc` or `. ~/.bashrc` (they are equivalent).
