Author: Jorge Maldonado Ventura
Category: Bash
Date: 2019-04-20 15:58
Modified: 2022-11-02 23:00
Lang: eo
Slug: recargar-la-configuracion-de-bash-bashrc
Tags: .bashrc, Bash, agordaro de Bash, GNU/Linukso, komandlinia interfaco, source
Title: Reŝarĝi la agordaron de Bash (<code>.bashrc</code>)
Save_as: reŝarĝi-la-agordaron-de-Bash/index.html
URL: reŝarĝi-la-agordaron-de-Bash/

Kiam vi redaktas la agordodosieron de Bash (`~/.bashrc`), vi devas
elsaluti kaj ensaluti por efektiĝi la ŝanĝoj. Vi ankaŭ povas uzi la
komandon `source ~/.bashrc` aŭ `. ~/.bashrc` (ili estas ekvivalentaj).
