Author: Jorge Maldonado Ventura
Category: Bash
Date: 2016-07-14 01:48
Modified: 2022-11-02 23:55
Lang: es
Slug: recargar-la-configuracion-de-bash-bashrc
Status: published
Tags: .bashrc, Bash, configuración Bash, GNU/Linux, source, interfaz de línea de órdenes
Title: Recargar la configuración de Bash (<code>.bashrc</code>)

Cuando modificas el archivo de configuración de Bash (`~/.bashrc`),
debes cerrar e iniciar una nueva sesión para que se hagan efectivos los
cambios. También puedes utilizar la instrucción `source ~/.bashrc` o
`. ~/.bashrc` (son equivalentes).
