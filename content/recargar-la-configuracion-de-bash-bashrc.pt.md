Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-11-02 23:39
Lang: pt
Slug: recargar-la-configuracion-de-bash-bashrc
Tags: .bashrc, Bash, configuração do Bash, GNU/Linux, interface da linha de comandos, source
Title: Recarregar a configuração do Bash (<code>.bashrc</code>)
Save_as: recarregar-a-configuração-do-Bash/index.html
URL: recarregar-a-configuração-do-Bash/

Quando editas o ficheiro de configuração do Bash (`~/.bashrc`), tens de
sair e fazer <i lang="en">login</i> para tornar efetivos as mudanças.
Também podes usar o comando `source ~/.bashrc` ou `. ~/.bashrc` (eles
são equivalentes).
