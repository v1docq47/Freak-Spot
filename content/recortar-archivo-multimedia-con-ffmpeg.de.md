Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-12-27 17:00
Lang: de
Slug: recortar-archivo-multimedia-con-FFmpeg
Save_as: Multimediadatei-mit-FFmpeg-beschneiden/index.html
URL: Multimediadatei-mit-FFmpeg-beschneiden/
Tags: Klang, ffmpeg, Video
Title: Multimediadatei mit FFmpeg beschneiden
Image: <img src="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg" alt="" width="1200" height="826" srcset="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg 1200w, /wp-content/uploads/2022/06/pinchadiscos-ffmpeg.-600x413.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Wenn du eine Multimediadatei nur bearbeiten willst, um ihren Anfang,
ihr Ende oder beides zu beschneiden, ist die schnellste Option die
Verwendung von [FFmpeg](https://de.wikipedia.org/wiki/FFmpeg). FFmpeg
kann in Debian-basierten Verteilungen mit `sudo apt install ffmpeg`
installiert werden.

Wenn wir die ersten 10 Sekunden einer Multimediadatei entfernen wollen,
müssen wir nur FFmpeg wie folgt ausführen:

    :::text
    ffmpeg -i Musik.mp3 -ss 10 Musik2.mp3

Nach `-i` musst du die Datei angeben, die du bearbeiten willst
(`Musik.mp3`); `-ss` gefolgt von `10` gibt die Sekunden an, die wir
entfernen wollen; schließlich kannst du den Namen der neuen Datei
angeben, `Musik2.mp3`.

Wenn wir sowohl den Anfang als auch das Ende entfernen wollen, können
wir das Argument `-to` hinzufügen:

    :::text
    ffmpeg -i Musik.mp3 -ss 15 -to 04:10 Musik2.mp3

Nach `-to` muss eine Position stehen, in diesem Fall Minute 4 und
Sekunde 10 (`04:10`). Es gibt auch die Möglichkeit, `-t` zu verwenden,
was auf diese Weise verwendet würde, um das gleiche Ergebnis zu
erzielen:

    :::text
    ffmpeg -i Musik.mp3 -ss 15 -t 235 Musik2.mp3

`-t` bedeutet, dass die Aufzeichnung in der neuen Datei erfolgt, bis 235
Sekunden vergangen sind. In diesem Fall werden diese neuen 235 Sekunden
aufgezeichnet, nachdem FFmpeg die ersten `15` Sekunden übersprungen hat.
