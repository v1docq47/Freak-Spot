Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-11-19 10:00
Lang: en
Slug: recortar-archivo-multimedia-con-FFmpeg
Save_as: crop-multimedia-file-with-FFmpeg/index.html
URL: crop-multimedia-file-with-FFmpeg/
Tags: sound, ffmpeg, video
Title: Crop multimedia file with FFmpeg
Image: <img src="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg" alt="" width="1200" height="826" srcset="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg 1200w, /wp-content/uploads/2022/06/pinchadiscos-ffmpeg.-600x413.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

If you only want to edit a multimedia file to crop its beginning, end or
both, the fastest option is to use
[FFmpeg](https://en.wikipedia.org/wiki/FFmpeg). FFmpeg can be installed
in Debian-based distributions with `sudo apt install ffmpeg`.

If we want to remove the 10 first seconds of a multimedia file, we only
have to execute FFmpeg like this:

    :::text
    ffmpeg -i music.mp3 -ss 10 music2.mp3

After `-i` you have to specify the file that you want to edit
(`music.mp3`); `-ss` followed by `10` indicates the seconds that we want
to remove; finally, you can specify the name of the new file,
`music2.mp3`.

If we want to remove both the beginning and the end, we can add the
`-to` argument:

    :::text
    ffmpeg -i music.mp3 -ss 15 -to 04:10 music2.mp3

After `-to` there must be a position, in this case minute 4 and second 10
(`04:10`). There is also the possibility of using `-t`, which to get the
same result would be used like this:

    :::text
    ffmpeg -i music.mp3 -ss 15 -t 235 music2.mp3

`-t` indicates that it will record into the new file until `235` seconds
have passed . In this case, those new 235 seconds will be recorded after
skipping the first `15` seconds.
