Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2022-10-26 23:58
Lang: eo
Slug: recortar-archivo-multimedia-con-FFmpeg
Save_as: eltondi-el-plurmedia-dosiero-per-FFmpeg/index.html
URL: eltondi-el-plurmedia-dosiero-per-FFmpeg/
Tags: sono, ffmpeg, video
Title: Eltondi el plurmedia dosiero per FFmpeg
Image: <img src="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg" alt="" width="1200" height="826" srcset="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg 1200w, /wp-content/uploads/2022/06/pinchadiscos-ffmpeg.-600x413.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Se vi nur volas redakti plurmedian dosieron por eltondi ĝian komencon,
finon aŭ ambaŭ, la plej rapida maniero estas uzi FFmpeg. Oni povas instali
FFmpeg-n en distribuoj bazitaj sur Debiano per `sudo apt install
ffmpeg`.

Se ni volas forigi la 10 unuajn sekundojn de plurmedia dosiero, sufiĉas
plenumi FFmpeg tiel:

    :::text
    ffmpeg -i muzikaĵo.mp3 -ss 10 muzikaĵo2.mp3

Post `-i` oni specifas la dosieron, kiun ni volas redakti
(`muzikaĵo.mp3`); `10` post `-ss` indikas la sekundojn, kiujn ni
volas forigi; fine, oni specifas la nomon de la nova dosiero
`muzikaĵo2.mp3`.

Se ni volas forigi kaj la komencon kaj la finon, ni povas aldoni la
`-to` argumenton:

    :::text
    ffmpeg -i muzikaĵo.mp3 -ss 15 -to 04:10 muzikaĵo2.mp3

Post `-to` devas esti pozicio, en ĉi tiu ekzemplo la 4-a minuto kaj la
10-a sekundo (`04:10`). Ankaŭ eblas uzi `-t`, kiun oni uzus tiel por
akiri la saman rezulton:

    :::text
    ffmpeg -i muzikaĵo.mp3 -ss 15 -t 235 muzikaĵo2.mp3

`-t` indikas, ke estos registrita ĝis post 235 sekundoj sur novan
dosieron. Tiuokaze tiuj 235 novaj sekundoj estos registritaj post
preterpasi la 15 unuajn sekundojn.
