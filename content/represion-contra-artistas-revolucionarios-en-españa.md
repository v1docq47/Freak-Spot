Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2017-02-23 12:18
Lang: es
Modified: 2017-03-21 20:22
Slug: represión-contra-artistas-revolucionarios-en-españa
Tags: represión, España, presos políticos, cárceles, libertad de expresión
Title: Represión contra artistas revolucionarios en España

Hoy he leído [una noticia que me ha sorprendido bastante](https://archivo.kaosenlared.net/marcaespana-y-libertad-de-expresion-el-regimen-espanol-condena-a-tres-anos-y-medio-de-carcel-al-rapero-valtonyc-por-su-tema-sobre-el-rey-juan-carlos/).
Se trata de la condena a un rapero (conocido como Valtonyc) de 3 años y
6 meses de cárcel por hacer canciones contra la monarquía. Fue acusado
por injurias al Rey y por enaltecimiento del terrorismo.

El artísta Valtonyc detenido en está ocasión decía en Twitter:

> Esta se suma a las recientes condenas y denuncias contra artístas
> revolucionarios. Entre fiscal y el policia que me detuvo, presente en juicio, repitieron
> 17 veces que soy de izquierdas y 9 que rapeo en catalán. ¿Es ilegal?

Como estas situaciones hay muchas más. Algunas de las más conocidas son
la condena a [un año de prisión a César
Stawberry](https://m.manerasdevivir.com/noticias/60443/cesar-strawberry-condenado-carcel-inhabilitacion)
(cantante de [Def Con Dos](https://es.wikipedia.org/wiki/Def_Con_Dos))
por enaltecimiento del terrorismo y la condena a [dos años de cárcel al
rapero Pablo
Hasél](https://www.hhgroups.com/noticias/dos-anos-de-carcel-para-pablo-hasel-comunicado-5499/)
también por enaltecimiento del terrorismo.

Otra situación bastante alarmante fue la detención de 13 músicos
pertenecientes al colectivo musical La Insurgencia en noviembre del año
pasado. Según informó el Diario Octubre:

> El martes 8 de noviembre, la policía nacional abordó en sus respectivas
> ciudades a cada uno de los 13 músicos pertenecientes al colectivo
> musical La Insurgencia, robándoles todo dispositivo electrónico que
> portasen encima y citándolos para el día 17 de noviembre a las 10:30
> horas en la Audiencia Nacional de Madrid para declarar en calidad de investigados por presuntos delitos de enaltecimiento del terrorismo, de incitación al odio, de asociación ilícita y contra las instituciones del Estado.

Una de las más esperpénticas fue la detención el año pasado de [dos
titiriteros](https://www.noticiasdegipuzkoa.eus/opinion/2016/02/10/titiriteros-carcel-4098413.html)
por interpretar una obra de títeres llamada [*La Bruja y Don Cristóbal*](http://titeresdesdeabajo.blogspot.com.es/p/la-bruja-y-don-cristobal.html). ¿Adivinad por qué fue?
Por un supuesto enaltecimiento del terrorismo.

La situación de los artistas revolucionarios en el estado español es
alarmante. Se está llevando a cabo una persecución a cualquier tipo de
oposición al régimen español. Y todas estas condenas y persecuciones se
producen en un tiempo en el que no hay atentados terroristas en España.

La reciente condena de Valtonyc irónicamente se contrapone con el trato
que tienen los miembros de la Casa Real frente a la «Justicia». Es muy
curioso el [caso Noós](https://es.wikipedia.org/wiki/Caso_N%C3%B3os), en
el que la infanta Cristina de Borbón (la hermana del rey) y su marido
defraudaron más de quince millones a la Hacienda pública. En este caso,
la infanta Cristina de Borbón no fue condenada (parece ser que la
Infanta no se enteraba de nada, ni siquiera de lo que firmaba), y su
marido fue condenado a seis años de prisión.
