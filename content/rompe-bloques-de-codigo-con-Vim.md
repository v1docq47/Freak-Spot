Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Videojuegos
Date: 2018-01-22 16:19
Image: <img src="/wp-content/uploads/2018/01/VimGameCodeBreak.png" alt="">
JS: asciinema-player.js (top)
Lang: es
Slug: rompe-bloques-de-codigo-con-vim
Tags: complemento, Neovim, Vim
Title: Rompepalabras con Vim

Hay un juego para Vim basado en
[Arkanoid](https://es.wikipedia.org/wiki/Arkanoid) llamado
[VimGameCodeBreak](https://github.com/johngrib/vim-game-code-break), se
puede instalar fácilmente como cualquier otro complemento. Para aprender
a instalar complementos con Vim, lee la sección de ayuda correspondiente
(`:h plugins`). Yo lo instalé con
[Vundle](https://github.com/VundleVim/Vundle.vim) sin problemas.

<!-- more -->

He aquí una demostración:

<asciinema-player src="/asciicasts/vim-game-code-break.json">
Lo siento, asciinema-player no funciona sin JavaScript.
</asciinema-player>

Estos son los controles:

<table style="table-layout: inherit">
<thead>
<tr>
<th>h</th>
<th>l</th>
<th>barra espaciadora</th>
<th>`</th>
<th>]</th>
<th>[</th>
<th>q</th>
<th>Q</th>
</tr>
</thead>
<tbody>
<tr>
<td>←</td>
<td>→</td>
<td>nueva bola</td>
<td>tecla de truco</td>
<td>modo DIOS</td>
<td>modo humano</td>
<td>acaba partida</td>
<td>sale y cierra el juego</td>
</tr></tbody>
</table>

Algunos bloques desprenden letras, que tienen un efecto en la pala.
Sirven para añadir una nueva bola, hacer la pala más larga, etc.
¡Descúbrelo todo por ti misma y disfrútalo!
