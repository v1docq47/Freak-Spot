Author: Jorge Maldonado Ventura
Category: Ohne Kategorie
Date: 2023-01-08 19:18
Modified: 2023-03-08 09:00
Lang: de
Slug: saltar-muros-de-pago-de-periódicos
Save_as: Bezahlschranken-in-Zeitungen-überwinden/index.html
URL: Bezahlschranken-in-Zeitungen-überwinden/
Tags: Bezahlschank, Trick
Title: Bezahlschranken in Zeitungen überwinden
Image: <img src="/wp-content/uploads/2022/05/saltando-muro.jpeg" alt="Hombre saltando un muro" width="2250" height="1500" srcset="/wp-content/uploads/2022/05/saltando-muro.jpeg 2250w, /wp-content/uploads/2022/05/saltando-muro.-1125x750.jpg 1125w, /wp-content/uploads/2022/05/saltando-muro.-562x375.jpg 562w" sizes="(max-width: 2250px) 100vw, 2250px">

In vielen Zeitungen gibt es
[Bezahlschranken](https://de.wikipedia.org/wiki/Paywall), die
verhindern, dass wir den vollen Inhalt von Artikeln sehen können. Es
gibt jedoch einige Tricks, um sie zu vermeiden.

Eine nützliche Browsererweiterung, mit der du diese Bezahlschranken
umgehen kannst, ist [Bypass Paywalls Clean](https://gitlab.com/magnolia1234/bypass-paywalls-firefox-clean#installation). Diese Erweiterung
funktioniert für beliebte Websites, und weitere können leicht
hinzugefügt werden. Wie funktioniert sie? Im Wesentlichen nutzt die
Erweiterung Tricks wie die Deaktivierung von JavaScript, die
Deaktivierung von Cookies oder der Wechsel des
[Benutzeragenten](https://de.wikipedia.org/wiki/User_Agent) zu dem eines
bekannten [WebCrawlers](https://de.wikipedia.org/wiki/Webcrawler) (wie
[Googlebot](https://de.wikipedia.org/wiki/Googlebot)).

Es ist nicht notwendig, die oben genannte Erweiterung zu installieren,
wenn du das nicht willst. Lese weiter, um im Detail zu erfahren, mit
welchen Tricks die meisten Bezahlschranken umgangen werden können.

<!-- more -->

## 1. Javascript deaktivieren

Manchmal stellt die Zeitung die Bezahlschranke mit JavaScript auf. Die
[Deaktivierung von JavaScript](/en/disable-JavaScript-in-Firefox/)
würde ausreichen, um die Paywall zu entfernen.

## 2 Cookies löschen

Manchmal zeigen Zeitungen nach dem Lesen einiger Artikel Bezahlschranken an.
Dies geschieht, weil sie neue Besucher nicht mit einer Bezahlschranke
abschrecken wollen. Um sie glauben zu lassen, dass du ein neuer
Besucher bist, [lösche einfach deine Cookies](/elimina-las-cookies-en-navegadores-derivados-de-firefox/).

## 3. Ändern Sie den Benutzeragenten

Viele Zeitungen wollen den Zugang für Nutzer beschränken, nicht aber für
Webcrawler, da dies ihre Platzierung in den Suchmaschinen
beeinträchtigen würde. Es gibt Erweiterungen, um den Benutzeragenten auf
einfache Weise zu ändern: eine davon ist [User-Agent Switcher and
Manager](https://addons.mozilla.org/de/firefox/addon/user-agent-string-switcher/).
Du musst dich als Webcrawler wie Googlebot oder Bingbot ausgeben.

## 4. Blockiere Elemente

Manchmal funktioniert es, Elemente von Webseiten zu entfernen. Wir
können dies tun, indem wir den Elementinspektor des Webbrowsers öffnen
und die Elemente entfernen, die zur Bezahlschranke gehören, aber es ist
einfacher, [dies mit uBlock Origin zu tun](/bloqueo-de-elementos-con-ublock-origin/).

## 5. Piraten-Repositorien

Es gibt Piraten-Repositorien wie
[Sci-Hub](https://de.wikipedia.org/wiki/Sci-Hub) oder [Library
Genesis](https://de.wikipedia.org/wiki/Library_Genesis). Ich weiß nicht,
ob es ein spezielles Repository für Internet-Zeitungen gibt.
