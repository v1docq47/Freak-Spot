Author: Jorge Maldonado Ventura
Category: Ruzo
Date: 2023-01-06 20:05
Modified: 2023-03-08 09:00
Lang: eo
Slug: saltar-muros-de-pago-de-periódicos
Save_as: transsalti-pagmurojn-de-ĵurnaloj/index.html
URL: transsalti-pagmurojn-de-ĵurnaloj/
Tags: pagmuro, ruzo
Title: Transsalti pagmurojn de ĵurnaloj
Image: <img src="/wp-content/uploads/2022/05/saltando-muro.jpeg" alt="Hombre saltando un muro" width="2250" height="1500" srcset="/wp-content/uploads/2022/05/saltando-muro.jpeg 2250w, /wp-content/uploads/2022/05/saltando-muro.-1125x750.jpg 1125w, /wp-content/uploads/2022/05/saltando-muro.-562x375.jpg 562w" sizes="(max-width: 2250px) 100vw, 2250px">

Multaj ĵurnaloj montras pagmurojn, kiuj malhelpas, ke ni vidu
la tutan enhavon de la artikoloj. Ekzistas tamen kelkaj ruzoj por eviti
ilin.

Utila kromaĵo por la retumilo, kiu ebligas al ni salti tiujn pagmurojn,
estas [Bypass Paywalls
Clean](https://gitlab.com/magnolia1234/bypass-paywalls-firefox-clean#installation).
Ĉi tiu kromaĵo funkcias en popularaj retejoj kaj oni povas facile aldoni
aliajn. Kiel ĝi funkcias? Simple la kromaĵo uzas ruzojn kiel malaktivigi
la Ĝavoskripton, malaktivigi la kuketojn aŭ ŝanĝi la nomon de klienta
aplikaĵo (angle <i lang="en">user agent</i>) al tiu de konita ret-araneo
(kiel Googlebot).

Ne necesas instali la antaŭan kromaĵon, se vi ne volas. Legu plu por
detale scii la ruzojn, kiujn oni povas uzi por eviti plejparton de la
pagmuroj.
<!-- more -->

## 1. Malaktivigi Ĝavoskripton

Kelkfoje la ĵurnalo metas la pagmuron per Ĝavoskripto. [Malaktivigi
Ĝavoskripton](/eo/malebligi-Ĝavoskripton-en-Firefox-kaj-ĝiaj-derivitaj-retumiloj/)
sufiĉus por forigi la pagmuron.

## 2. Forigi kuketojn

Kelkfoje la ĵurnaloj montras la pagmurojn, post kiam vi vidis kelkajn
artikolojn. Ĉi tio okazas, ĉar ili ne volas timigi novajn vizitantojn
per pagmuro. Por ke ili pensu, ke vi estas nova vizitanto, sufiĉas
[forigi la kuketojn](/elimina-las-cookies-en-navegadores-derivados-de-firefox/).

## 3. Ŝanĝi la nomon de klienta aplikaĵo

Multaj ĵurnaloj volas limigi la aliron al la uzantoj, sed ne al la
ret-araneoj, ĉar tio influus ilian pozicion en serĉiloj. Ekzistas
kromaĵoj por facile ŝanĝi la nomon de klienta aplikaĵo: unu el tiuj
estas [User-Agent Switcher and Manager](https://addons.mozilla.org/es/firefox/addon/user-agent-string-switcher/).
Oni devus ŝajnigi sin ret-araneo kiel Googlebot aŭ Bingbot.

## 4. Bloki elementojn

Kelkfoje funkcias demeti retejajn elementojn. Ni povas fari tion malfermante
la retumilan inspektilon kaj forigante la elementojn, kiuj apartenas al
la pagmuro, sed pli facilas [fari tion per uBlock Origin](/bloqueo-de-elementos-con-ublock-origin/).

## 5. Pirataj deponejoj

Estas pirataj deponejoj kiel
[Sci-Hub](https://en.wikipedia.org/wiki/Sci-Hub) aŭ [Library
Genesis](https://en.wikipedia.org/wiki/Library_Genesis). Mi ne scias, se
ekzistas iu deponejo speciala por Interretaj ĵurnaloj.
