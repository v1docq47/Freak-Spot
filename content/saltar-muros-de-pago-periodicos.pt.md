Author: Jorge Maldonado Ventura
Category: Truques
Date: 2023-02-03 11:54
Modified: 2023-03-08 09:00
Lang: pt
Slug: saltar-muros-de-pago-de-periódicos
Save_as: como-evitar-o-acesso-pago-de-jornais/index.html
URL: como-evitar-o-acesso-pago-de-jornais/
Tags: acesso pago, truque
Title: Como evitar o acesso pago de jornais
Image: <img src="/wp-content/uploads/2022/05/saltando-muro.jpeg" alt="Hombre saltando un muro" width="2250" height="1500" srcset="/wp-content/uploads/2022/05/saltando-muro.jpeg 2250w, /wp-content/uploads/2022/05/saltando-muro.-1125x750.jpg 1125w, /wp-content/uploads/2022/05/saltando-muro.-562x375.jpg 562w" sizes="(max-width: 2250px) 100vw, 2250px">

Muitos jornais têm [acesso pago](https://pt.wikipedia.org/wiki/Acesso_pago) que nos impede de ver o conteúdo
completo dos artigos. Existem, no entanto, alguns truques para os evitar.

Uma extensão útil do navegador que nos permite contornar esses muros de
pagamento é [Bypass Paywalls
Clean](https://gitlab.com/magnolia1234/bypass-paywalls-firefox-clean#installation).
Esta extensão funciona para sítios web populares, e outros podem ser
facilmente adicionados. Como funciona? Basicamente, a extensão utiliza
truques tais como desactivar o JavaScript, desactivar os <i lang="en">cookies</i> ou alterar
o agente do utilizador para o de um [rastreador web](https://pt.wikipedia.org/wiki/Web_crawler)
conhecido (como [o Googlebot](https://pt.wikipedia.org/wiki/Googlebot)).

Não há necessidade de instalar a extensão anterior se não queres.
Continua a ler para descobrir em detalhe os truques que podes utilizar
para evitar a maior parte dos muros de pagamento.

<!-- more -->

## 1. Desativar o JavaScript

Por vezes, o jornal coloca o muro de pagamento usando JavaScript. A [desactivação
do JavaScript](/desahabilitar-javascript-comodamente-firefox-y-derivados/) seria suficiente para remover o acesso pago.

## 2. Eliminar <i lang="en">cookies</i>

Às vezes, os jornais exibem muros de pagamento depois de ver alguns
artigos. Isto acontece porque eles não querem assustar os novos
visitantes com o acesso pago. Para os fazer pensar que és um novo
visitante basta [apagar as tuas <i lang="en">cookies</i>](/elimina-las-cookies-en-navegadores-derivados-de-firefox/).

## 3. Mudar o agente do utilizador

Muitos jornais querem restringir o acesso aos utilizadores, mas não aos
rasteadores web, pois isso afectaria a sua classificação nos motores
de busca. Existem extensões para alterar o agente do utilizador de uma
forma simples: uma delas é o [User-Agent Switcher and Manager](https://addons.mozilla.org/pt-PT/firefox/addon/user-agent-string-switcher/).
Terias de fazer-te passar por um rastreador web como o Googlebot ou o
Bingbot.

## 4. Remover elementos

Por vezes funciona para remover elementos de páginas web. Podemos fazer
isto abrindo o inspetor de elementos do navegador e removendo os
elementos que pertencem ao muro de pagamento, mas é mais fácil de
[o fazer com o uBlock Origin](/bloqueo-de-elementos-con-ublock-origin/).

## 5. Repositórios piratas

Existem repositórios piratas como o
[Sci-Hub](https://pt.wikipedia.org/wiki/Sci-Hub) ou a
[Library Genesis](https://pt.wikipedia.org/wiki/Library_Genesis). Não sei se
existe um repositório especializado para os jornais da Internet.
