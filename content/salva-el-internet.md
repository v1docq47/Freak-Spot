Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2016-06-29 12:07
Lang: es
Slug: salva-el-internet
Status: published
Tags: Internet, libertad, neutralidad de la Red, Unión Europea
Title: Salva el Internet

Los reguladores europeos están a punto de decidir si darle a las grandes
corporaciones de telecomunicaciones el poder de decidir sobre lo que
podemos y no podemos hacer en Internet.

La neutralidad de la Red está en peligro. La discriminación de datos en
base a su origen, destino o tipo de dato supondría un peligro para la
libertad de expresión, para la libre competencia entre empresas y para
muchas otras libertades.

Por estas razones, se ha puesto en marcha una campaña para salvar el
Internet: <https://www.savetheinternet.eu/es/>. En dicha página, que ya
ha sido traducida a varios idiomas, se explican los peligros de las
regulaciones que se quieren aprobar y cómo ayudar a pararlas. Todavía
nos quedan 19 días para tomar acción y salvar nuestra libertad.
