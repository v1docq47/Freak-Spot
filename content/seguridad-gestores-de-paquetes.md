Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-04-14 22:00
Lang: es
Slug: otro-ataque-a-la-cadena-de-suministro-en-npm-eventsource-polyfill
Tags: gestor de paquetes, npm, Rusia, seguridad, Ucrania
Title: Otro ataque a la cadena de suministro en npm: EventSource polyfill
Image: <img src="/wp-content/uploads/2022/04/npmalware.png" alt="npmalware">

Por si npm no había demostrado ser un gestor de paquetes inseguro
&mdash;recientemente con el [*malware* de node-ipc](/ciberguerra-contra-inocentes/)&mdash;, otra
biblioteca muy utilizada, descargada más de
600 000 veces por semana con npm, llamada [EventSource
polyfill](https://github.com/Yaffle/EventSource) [ha añadido mensajes propagandísticos
escritos en
ruso](https://github.com/Yaffle/EventSource/commit/de137927e13d8afac153d2485152ccec48948a7a)
a favor del régimen de Ucrania dirigidos a quienes tienen su zona
horaria configurada como una región de Rusia, llegando a abrir una
ventana en el navegador con la URL de una recogida de firmas contra la
guerra.

Muestra mensajes falsos como «El 91 % de los ucranianos apoya plenamente
a su presidente Volodímir Zelenski», cuando la participación política
en las elecciones de 2019 fue del 49,84 % y el partido de este político
consiguió en total 6 307 793 votos (un 43,16 %). También recomienda
acceder al periódico de la BBC (un periódico estatal británico) mediante
Tor (pues está censurado en Rusia) como fuente de información fiable.
