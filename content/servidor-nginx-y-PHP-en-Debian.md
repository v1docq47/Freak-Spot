Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-04-15 18:40
Lang: es
Slug: instalar-servidor-nginx-con-php-en-debian
Tags: Debian, Debian 11, Nginx, PHP
Title: Instalar servidor Nginx con PHP en Debian 11

En este artículo enseño cómo instalar un servidor Nginx que pueda
ejecutar programas de PHP en Debian 11.

Primero hay que instalar los siguientes paquetes:

    :::bash
    sudo apt install nginx php php-fpm

A continuación, hay que descomentar las siguientes líneas del archivo de
configuración predeterminado de Nginx
(`/etc/nginx/sites-available/default`):

    :::text
	#location ~ \.php$ {
	#	include snippets/fastcgi-php.conf;
	#
	#	# With php-fpm (or other unix sockets):
	#	fastcgi_pass unix:/run/php/php7.4-fpm.sock;
	#	# With php-cgi (or other tcp sockets):
	#	fastcgi_pass 127.0.0.1:9000;
	#}

Quedando así<!-- more -->[^1]:

    :::text
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;

		# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/run/php/php7.4-fpm.sock;
		# With php-cgi (or other tcp sockets):
		#fastcgi_pass 127.0.0.1:9000;
	}

Luego hay que comprobar si la sintaxis del archivo de configuración es
correcta con `sudo nginx -t`. Si no da error, iniciamos el servicio
de<!-- more -->
PHP-FPM[^1] y recargamos la configuración de Nginx:

    :::text
    sudo systemctl enable php7.4-fpm
    sudo systemctl start php7.4-fpm
    sudo systemctl reload nginx


[^1]: Si la versión de PHP del servicio de PHP-FPM que has instalado no
  es la misma, ajústala.

Finalmente, cambiamos los permisos para que nuestro usuario pueda
acceder a la carpeta del servidor local:

    :::text
    sudo chown -R $USER:www-data /var/www/html

Podremos comprobar si PHP se ejecuta en nuestro servidor creando un
programa de prueba llamado `prueba.php` en `/var/www/html`, por ejemplo:

    :::php
    <?php
    echo 'PHP se ejecuta en este servidor.';

Al abrir la dirección <http://localhost/prueba.php> en el navegador
debería mostrarse una página con el mensaje anterior.
