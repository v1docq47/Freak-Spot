Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2022-04-15 15:00
Lang: eo
Save_as: snap-oj-en-Ubuntu-aŭtomataj-ĝisdatigoj/index.html
URL: snap-oj-en-Ubuntu-aŭtomataj-ĝisdatigoj/
Slug: snaps-en-ubuntu-actualizaciones-automaticas
Tags: GNU/Linux, pako-administrilo, sekureco, Snap, Ubuntu
Title: Snap-oj en Ubuntu: malpli da sekureco kaj aŭtomataj ĝisdatigoj
Image: <img src="/wp-content/uploads/2022/04/snap-ubuntu.jpg" alt="">

Per sia versio 22.04, kiu estos eldonita la 21an de aprilo, Ubuntu
igos, ke programoj uzu Snap-pakojn anstataŭ la `.deb`-aj. Tiuj pakoj
aŭtomate ĝisdatiĝas sen iri tra prova fazo kiel okazas kun la pakoj de
Debiano kaj aliaj distribuoj. En la kazo de la pako de Firefox, estas la
skipo de Mozilla (ne Ubuntu), kiu decidas kiel kaj kiam la retumilo
ĝisdatiĝas.

Firefox estas libera programaro, sed ĝi enhavas proprieterojn
kiel Pocket. Mozilla povas per Snap aldoni aliajn similajn erojn kaj
malagrablajn funkciojn.

La Snap-oj havas iujn avantaĝojn: ili permesas paki programon kun ĉiuj
iliaj dependaĵoj, ili funkcias en ĉiuj distribuoj, ktp. Tamen ili
malrapidigas la startigan procezon; ili estas multe pli malrapidaj, kiam
ili estas plenumitaj unuafoje; ili okupas pli da spaco, ĉar ili enhavas
bibliotekojn, kiuj povus esti uzitaj de pluraj programoj; ilia defaŭlta
deponejo («aĉetejo») estas proprieta; ili postulas la uzon de systemd,
ktp.

Se la uzo de la Snap-oj estus nedeviga, ne estus tiom da polemiko, sed
Ubuntu trudis ilin por kelkaj pakoj, por kiuj ne plu estas `.deb`-a
alternativo.
