Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-04-15 13:00
Lang: es
Slug: snaps-en-ubuntu-actualizaciones-automaticas
Tags: gestor de paquetes, GNU/Linux, seguridad, Snap, Ubuntu
Title: Snaps en Ubuntu: menos seguridad y actualizaciones automáticas
Image: <img src="/wp-content/uploads/2022/04/snap-ubuntu.jpg" alt="">

Con su nueva versión 22.04, que será publicada el 21 de abril, Ubuntu
hará que más programas usen paquetes
[Snap](https://es.wikipedia.org/wiki/Snap_(gestor_de_paquetes)) en vez
de los `.deb`. Estos paquetes se actualizan de forma automática sin
pasar por una fase de prueba como sucede con los paquetes de Debian y
otras distribuciones. En el caso del paquete de Firefox, es el equipo de
Mozilla (no Ubuntu) quien decide cómo y cuándo se actualiza el
navegador.

Firefox es <i lang="en">software</i> libre, pero incluye componentes
privativos como [Pocket](https://es.wikipedia.org/wiki/Pocket). Mozilla
puede mediante Snap añadir otros componentes parecidos y funcionalidades
desagradables.

Los Snaps tienen algunas ventajas: permiten empaquetar un programa con
todas sus dependencias, funcionan en cualquier distribución, etc. Sin
embargo, ralentizan el proceso de arranque, son mucho más lentos cuando
se ejecutan por primera vez, ocupan más espacio (pues contienen en ellos
bibliotecas que podrían usarse por varios programas), su repositorio
predeterminado («tienda») es privativo, requiere el uso de
[systemd](https://es.wikipedia.org/wiki/Systemd), etc.

Si el uso de los Snaps fuera opcional en Ubuntu, no habría tanta
controversia, pero Ubuntu los ha impuesto para varios paquetes, para los
que ya no existe una alternativa `.deb`.
