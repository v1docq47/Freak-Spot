Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2021-11-08 19:50
Modified: 2021-11-11
Lang: es
Slug: la-libertad-de-software
Tags: licencias, privacidad, software libre
Title: La libertad de <i lang="en">software</i> no es cuestión de licencias: es de poder

*Este artículo es una traducción del inglés del artículo «[Software
freedom isn’t about licenses – it’s about
power.](https://rosenzweig.io/blog/software-freedom-isnt-about-licenses-its-about-power.html)»
publicado por Alyssa Rosenzweig bajo la licencia <a
href="https://creativecommons.org/licenses/by-sa/4.0/deed.es"><abbr
title="Creative Commons Atribución-CompartirIgual 4.0 International">CC
BY-SA 4.0</abbr></a>*.

Un acuerdo de licencia de usuario final restrictivo es una manera de que
una compañía pueda ejercer poder sobre el usuario. Cuando el movimiento
del <i lang="en">software</i> libre se fundó hace treinta años, estas
licencias restrictivas eran la principal dinámica de poder hostil al
usuario, así que las licencias permisivas y <i lang="en">copyleft</i>
surgieron como sinónimos de la libertad de <i lang="en">software</i>.
Usar licencias es importante; la autonomía del usuario se pierde con
modelos de suscripción, licencias revocables, <i lang="en">software</i>
solo binario y clausulas legales onerosas. Sin embargo, estos problemas
aplicables a los programas de escritorio son solo la punta del iceberg
de las actuales dinámicas de poder digitales.

Hoy en día, las empresas ejercen poder sobre sus usuarios mediante
rastreo, vendiendo datos, manipulación psicológica, anuncios
molestos, obsolescencia programada y programas de Gestión de «Derechos»
Digitales (DRM [en inglés]). Estos problemas afectan a
cada usuario digital, con inclinación técnica o sin ella, tanto en
escritorio como en móviles inteligentes.

El movimiento del <i lang="en">software</i> libre prometió enmendar
estos males mediante licencias libres en el código fuente, con
adherentes que argumentaban que las licencias libres proporcionan
inmunidad frente a estas formas de programas maliciosos, ya que los
usuarios podrían modificar el código. Desafortunadamente, la mayoría de
usuarios carece de los recursos para hacerlo. Mientras las violaciones
más flagrantes de la libertad del usuario vengan de empresas que
publican programas privativos, estos males pueden seguir sin corrección
incluso en programas de código abierto, y no todos los programas
privativos presentan estos problemas. El navegador web es nominalmente
un programa libre [se refiere a Firefox y a Chromium] que contiene el
trío de telemetría, anuncios y DRM; un videojuego antiguo es un programa
privativo, pero relativamente inocuo.

Así pues, no es suficiente mirar la licencia. Ni siquiera es suficiente
considerar la licencia y un conjunto fijo de problemas endémicos a los
programas privativos; el contexto importa. El <i lang="en">software</i>
no está aislado. De la misma forma que los programas privativos tienden
a integrarse con otros programas privativos, los programas libres
tienden a integrarse con otros programas libres. La libertad de <i
lang="en">software</i> <em>en contexto</em> exige un leve empujón hacia
intereses del usuario, no hacia intereses corporativos.

¿Entonces cómo deberíamos conceptualizar la libertad de <i
lang="en">software</i>?

Consideremos los tres partidarios del <i lang="en">software</i> libre y
el código abierto: aficionados, empresas y activistas. A los aficionados
individuales les importa trastear con el programa de su elección,
enfatizando el código fuente licenciado libremente. Estas preocupaciones
no les afectan a quienes no convierten modificar código en un
pasatiempo. No hay <em>nada malo</em> en esto, pero nunca será un
problema doméstico.

Por su parte, las grandes empresas afirman amar «el código abierto». No,
no les preocupa el movimiento social, solo la reducción de costes
lograda aprovechándose de programas licenciados permisivamente. Este
énfasis corporativo en las licencias se hace normalmente en detrimento
de la libertad de <i lang="en">software</i> en el contexto más amplio.
De hecho, es esta ironía la que motiva la libertad de <i
lang="en">software</i> más allá de la licencia.

Es el espíritu del activista el que se debe aplicar a todo el mundo sin
importar su habilidad técnica o estatus económico. No hay escasez de
programas de código abierto, normalmente de origen empresarial, pero
esto es insuficiente &mdash;es la dinámica de poder lo que debemos
combatir&mdash;.

No estamos solos. La libertad de <i lang="en">software</i> está ligada a
problemas sociales contemporáneos, que incluyen la reforma del derecho
de autor, la privacidad, la sostenibilidad y la adicción a Internet.
Cada problema surge como una dinámica de poder hostil entre un autor de
<i lang="en">software</i> corporativo y el usuario, con interacciones
complicadas con las licencias de <i lang="en">software</i>. Desligar
cada problema de las licencias proporciona un entorno de trabajo para
tratar cuestiones complejas de reformas políticas en la era digital.

La reforma del derecho de autor generaliza los enfoques de licencias de
los movimientos del <i lang="en">software</i> libre y de la cultura
libre. En efecto, las licencias libres nos empoderan para usar, adaptar,
mezclar y compartir tanto obras como <i lang="en">software</i>. Sin
embargo, las licencias privativas que gestionan hasta el mínimo detalle
el núcleo de la comunidad y creatividad humanas están condenadas a
fracasar. Las licencias privativas han tenido poco éxito evitando la
proliferación de las obras creativas que buscan «proteger», y los
derechos de adaptar y mezclar obras han sido ejercidos desde hace mucho
tiempo por aficionados dedicados de obras privativas, produciendo
volúmenes de ficción y arte de fans. La misma observación se aplica al
<i lang="en">software</i>: los acuerdos de licencia privativos para
usuarios finales no han parado ni la compartición de archivos ni la
ingeniería inversa. De hecho, ha surgido una afición única y creativa
alrededor del <i lang="en">software</i> privativo en las comunidades de
modificaciones de videojuegos. Independientemente de los problemas
legales, la imaginación humana y el espíritu de compartir persisten. Así
pues, no debemos juzgar a nadie por los programas y las obras privativas
en su vida; en su lugar, debemos  trabajar por la reforma del derecho de
autor y las licencias libres para protegerlos de los excesos del
derecho de autor.

Las preocupaciones por la privacidad son también tradicionales en el
discurso de la libertad de <i lang="en">software</i>. Los programas
verdaderamente seguros de comunicaciones nunca pueden ser privativos,
dada la posibilidad de puertas traseras y la imposibilidad de auditorías
transparentes. Desgraciadamente, lo inverso falla: hay programas
licenciados libremente que inherentemente comprometen la privacidad del
usuario. Considera clientes de terceros para sistemas de mensajería
centralizados y no cifrados. Aunque dos usuarios de un cliente así que
se envíen mensajes el uno al otro en privado estén utilizando solo <i
lang="en">software</i> libre, si sus mensajes están siendo procesados
por los datos, todavía hay peligro. Una vez más se subraya la necesidad
del contexto.

La sostenibilidad es una preocupación emergente, que se vincula a la
libertad del <i lang="en">software</i> a través de la crisis de los
residuos electrónicos. En el mundo de los móviles, donde la
obsolescencia de los móviles al cabo de unos pocos años es la norma y
las baterías de litio se acumulan indefinidamente en los vertederos,
vemos la paradoja de un sistema operativo con licencia libre con un
pésimo historial social. Una implicación curiosa es la necesidad de
controladores de dispositivos libres. Mientras los controladores
privativos llevan a los aparatos a la obsolescencia después de que el
fabricante los abandone en favor de un nuevo producto, los controladores
libres permiten un mantenimiento a largo plazo. Como antes, la licencia
no es suficiente; el código también debe estar mantenido y aceptar
modificaciones. El simple hecho de publicar el código fuente es no es
suficiente para resolver el problema de los residuos electrónicos, pero
es un prerrequisito. En riesgo está el derecho del propietario de
continuar usando un aparato que ya ha comprado, incluso después de que
el fabricante ya no quiera darle soporte. Deseado por los activistas
climáticos y los conscientes con el consumo, no podemos dejar que el <i
lang="en">software</i> anule este derecho.

Más allá de los derechos de autor, la privacidad y la sostenibilidad,
ningún <i lang="en">software</i> puede ser realmente «libre» si la
propia tecnología nos encadena, nos atonta y nos lleva a encolerizarnos
por los clics. Gracias a la cultura televisiva que se extiende a
Internet, el ciudadano típico tiene menos que temer de las escuchas del
gobierno que de sí mismo. Por cada mensaje cifrado que descifra una
agencia de inteligencia, miles de mensajes se difunden voluntariamente
al público, buscando una gratificación instantánea. ¿Por qué iba a
molestarse una empresa o un gobierno en husmear en nuestra vida privada,
si se la ponemos en bandeja de plata? En efecto, las implementaciones
populares de código abierto de tecnología corrupta no constituyen un
éxito, una cuestión personificada por las respuestas del <i
lang="en">software</i> libre a las redes sociales. No, incluso sin <i
lang="en">software</i> privativo, centralización o cruel manipulación
psicológica, la proliferación de las redes sociales sigue poniendo en
peligro a la sociedad.

En general, centrarse en cuestiones concretas sobre la libertad del
<i lang="en">software</i> da cabida a los matices, en lugar de la
tradicional visión binaria. Los usuarios finales pueden tomar decisiones
más informadas, al ser conscientes de las soluciones intermedias de las
tecnologías más allá de la licencia. Los desarrolladores de
<i lang="en">software</i> obtienen un marco para entender cómo su
<i lang="en">software</i> encaja en el panorama general, ya que una
licencia libre es necesaria, pero no suficiente, para garantizar la
libertad del software a día de hoy. Los activistas pueden dividir y
conquistar.

Muchos fuera de nuestra esfera inmediata entienden y se preocupan por
estos problemas; el éxito a largo plazo requiere estos aliados.
Las afirmaciones de superioridad moral mediante licencias son
infundadas y necias; no hay éxito apuñalando por la espalda a nuestro
amigos. En su lugar, un enfoque matizado amplía nuestro alcance. Aunque
las filosofías morales abstractas pueden ser intelectualmente válidas,
son inaccesibles a todos excepto a los académicos y los partidarios más
dedicados. Las abstracciones están siempre al margen de la política,
pero estas cuestiones concretas ya son comprendidas por el público en
general. Asimismo, no podemos limitarnos a audiencias técnicas; entender
la topología de una red no puede ser un prerrequisito para
conversaciones privadas. Enfatizar demasiado el papel del código fuente
y subestimar la dinámica de poder en juego es una estrategia condenada
al fracaso; durante décadas lo hemos intentado y hemos fracasado. En un
mundo post-Snowden hay demasiado en juego para más fracasos. Reformar
las cuestiones específicas allana el camino hacia la libertad del
<i lang="en">software</i>. Después de todo, el cambio social es más
difícil que escribir código, pero con una reforma social incremental,
las licencias se convierten en la parte fácil.

El análisis matizado ayuda incluso a los activistas individuales de la
libertad del <i lang="en">software</i>. Los intentos puristas de rechazar
categóricamente la tecnología no libre son loables, pero fuera de una
comunidad cerrada ir en contra de la corriente conduce al agotamiento
del activista. Durante el día, los empleadores y las escuelas exigen
invariablemente <i lang="en">software</i> privativo, a veces utilizado
para facilitar la vigilancia. Por la noche, los pasatiempos populares y
las conexiones sociales de hoy en día están mediadas por programas
cuestionables, desde el DRM de un videojuego hasta la vigilancia de un
chat con un grupo de amigos. Cortar los lazos con los amigos y abandonar
el autocuidado como <i lang="en">prerrequisito</i> para luchar contra
las organizaciones poderosas parece noble, pero es inútil. Incluso sin
la política, sigue habiendo desafíos técnicos con usar solo <i
lang="en">software</i> libre. Añadir otras preocupaciones, o tal vez
renunciar a un teléfono móvil, solo amplía el riesgo de agotamiento en
la lucha por la libertad del <i lang="en">software</i>.

Como aplicación, este enfoque de la libertad del <i
lang="en">software</i> saca a la luz problemas dispares con la web
moderna que hacen saltar la alarma en la comunidad del <i lang="en">software</i> libre.
El problema tradicional es el JavaScript privativo, una cuestión de
licencias, aunque considerar solo las licencias de JavaScript provoca
conclusiones imprecisas e inexactas sobre las «aplicaciones» web. Se
suman a la lista probremas más profundos como la publicidad y el rastreo
desenfrenado; Internet es la mayor red de vigilancia de la historia de
la humanidad, en gran medida con fines comerciales. Hasta cierto punto,
estos problemas se ven mitigados por los bloqueadores de <i
lang="en">scripts</i>, anuncios y rastreadores; estos pueden ser
preinstalados en un navegador web para reducir el daño en busca de una
web más gentil. Sin embargo, el defecto fatal de la web es aún más
fundamental. Por su diseño, cuando un usuario navega a una URL, su
navegador ejecuta *cualquier* código que se haya introducido en el
cable. En efecto, la web implica una actualización automática,
independientemente de la licencia del código. Incluso si el código es
benigno, sigue siendo cada año más caro de ejecutar, lo que obliga a un
ciclo de actualización del <i lang="en">hardware</i> que deja de
funcionar si la web no estuviera hinchada por los intereses
corporativos. Un punto más sutil es la «economía de la atención» ligada
a la web. Mientras que es difícil volverse adicto a la lectura en un
navegador de solo texto, hacer maratones de televisión con DRM es una
historia diferente. Avances poco ambiciosos como el «Modo de Lectura» se
ven limitados por la irónica distribución de documentos a través de una
tienda de aplicaciones. En la web, cuestiones dispares de DRM,
actualización automática forzada, privacidad, sostenibilidad y patrones
psicológicos oscuros convergen en un único y peor escenario para la
libertad del <i lang="en">software</i>.  Las licencias fueron solo el principio.

Sin embargo, hay motivos para el optimismo. Adecuadamente
contextualizada, la lucha por la libertad del <i lang="en">software</i>
se puede ganar. Para luchar por la libertad del
<i lang="en">software</i>, lucha por la privacidad. Lucha por la
reforma del derecho de autor. Lucha por la sostenibilidad. Resiste los
patrones psicológicos oscuros. En el centro de cada lucha hay una
batalla por la libertad del <i lang="en">software</i> &mdash;sigue
luchando y podremos ganar&mdash;.
