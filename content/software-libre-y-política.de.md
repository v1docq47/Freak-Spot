Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2022-12-30 15:00
Lang: de
Slug: software-libre-y-política
Tags: Anarchismus, Kapitalismus, Kommunismus, GNU/Linux, Ideologie, Politik, freie Software
Save_as: Freie-Software-und-Politik/index.html
URL: Freie-Software-und-Politik/
Title: Freie Software und Politik
Image: <img src="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg" alt="" width="1200" height="800" srcset="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg 1200w, /wp-content/uploads/2020/11/software-libre-e-ideología.-600x400.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Ist freie Software anarchistisch oder kapitalistisch? Manche nennen es
kommunistisch, andere sagen, es sei kapitalistisch, anarchistisch... Wer
hat Recht? Sind Kommentare wie die folgenden des ehemaligen
Microsoft-CEO Steve Ballmer sinnvoll?<!-- more -->

> Linux ist ein harter Konkurrent. Es gibt kein Unternehmen mit dem Namen
> Linux, es gibt kaum einen Fahrplan. Aber Linux wurde geboren, als wäre
> es aus dem Boden gewachsen. Und es hatte, wissen Sie, die Merkmale des
> Kommunismus, die die Menschen so sehr mögen. Ich meine, es ist
> kostenlos.[^1]

## Kapitalismus

Proprietäre Software begünstigt Monopole von Unternehmen, die fast den
gesamten Markt kontrollieren. Es ist unmöglich, mit proprietärer
Software allein eine gute Marktposition zu erreichen. Deshalb müssen viele
Unternehmen freie Software einsetzen, um konkurrenzfähig zu sein.
Heutzutage ist es schwierig, Technologieunternehmen zu finden, die nicht
in erheblichem Umfang auf freie Software zurückgreifen.

Natürlich gibt es unterschiedliche kapitalistische Strömungen. Freie
Software hat auf jeden Fall einen Platz in dieser Art von Gesellschaft,
solange es eine Nachfrage nach ihr gibt oder ihre Verwendung einen
Wettbewerbsvorteil bietet.

## Anarchismus

Freie Software [beendet die ungerechte Macht der Programmierer über die
Nutzer](https://www.gnu.org/philosophy/free-software-even-more-important.de.html).
Da es im Anarchismus darum geht, die dem Individuum aufgezwungene
Autorität zu beenden, sind die durch freie Software gewährten Freiheiten
eine Befreiung.

## Andere politische Systeme

Freie Software wird in einer Vielzahl von politischen Systemen
eingesetzt. Wo liegt das Problem? Nordkorea hat zum Beispiel eine
GNU/Linux-Verteilung namens [Red Star
OS](https://de.wikipedia.org/wiki/Red_Star_OS) entwickelt.

## Fazit

Ich denke, es ist absurd, freie Software in ein bestimmtes politisches
System einzuordnen. Sie ist zweifelsohne effizienter und sicherer als
proprietäre Software, und zahlreiche politische Modelle können von ihrer
Einführung profitieren. [Proprietäre Software ist wie Alchemie](/de/Freie-Software-ist-besser-als-Alchemie/), während
freie Software wie Wissenschaft ist. Kein Wunder, dass [fast alle
Supercomputer](https://de.wikipedia.org/wiki/Supercomputer#Betriebssystem_und_Programmierung) und Webserver mit freier Software arbeiten.

[^1]: Aus dem Artikel in The Register [MS' Ballmer: Linux is communism](https://www.theregister.com/Print/2000/07/31/ms_ballmer_linux_is_communism/).
