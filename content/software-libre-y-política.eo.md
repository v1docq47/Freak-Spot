Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2020-11-10
Lang: eo
Slug: software-libre-y-política
Tags: anarkiismo, kapitalismo, komunismo, GNU/Linukso, ideologio, politiko, libera programaro
Save_as: libera-programaro-kaj-politiko/index.html
URL: libera-programaro-kaj-politiko/
Title: Liberaj programoj kaj politiko
Image: <img src="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg" alt="" width="1200" height="800" srcset="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg 1200w, /wp-content/uploads/2020/11/software-libre-e-ideología.-600x400.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Ĉu la libera programado estas anarkiisma aŭ kapitalista? Iuj nomas ĝin
komunisma, iuj diras, ke estas kapitalisma, anarkiisma... Kiu
pravas?<!-- more --> Ĉu havas sencon komentoj kiel la diritaj de la
antaŭa ĝenerala direktoro de Microsoft Steve Ballmer?:

> Linukso estas malmola konkuranto. Ne estas neniu firmao nomita
> Linukso; preskaŭ ne estas programada plano. Tamen Linukso naskiĝas
> kiel planto el la tero. Kaj ĝi havis, vi scias, la komunismajn
> trajtojn, kiujn la homoj tiom ŝatas. Nome estas libera.[^1]

## Kapitalismo

La proprieta programaro favoras monopolojn de firmaoj, kiuj kontrolas
preskaŭ tutan merkaton. Ne estas eble atingi bonan ejon en la merkato
nur per proprieta programaro, do multaj firmaoj devas uzi liberan
programaro por konkuri. Nun estas malfacile trovi ciferajn firmaojn,
kiuj ne ege uzas liberan programaron.

Kompreneble estas malsamaj kapitalismaj subsistemoj. Ĉiukaze la libera
programaro ekzistas en tiaj socioj ĉiam, kiam estas mendado aŭ kiam ĝia
uzo supozas konkuran avantaĝon.

## Anarkiismo

La libera programaro [finas la malĝustan povon, kiun havas la
programistoj super la uzantoj](https://www.gnu.org/philosophy/free-software-even-more-important.html).
Ĉar la anarkiismo klopodas ĉesi la devigitan estradon super la
individuo, la liberecoj, kiujn donas la libera programaro signifas
liberadon.

## Aliaj politikaj sistemoj

Liberaj programoj estas uzitaj en tre malsamaj politikaj sistemoj. Kiu
problemo estas? Ekzemple Nordkoreio programis GNU/Linuksan distribuon
nomita [Red Star OS](https://en.wikipedia.org/wiki/Red_Star_OS).

## Konkludo

Mi rigardas kiel stulte rilati la liberan programaron al konkreta
sistema politiko. Sendube ĝi estas pli rendimenta kaj sekura ol la
proprieta programaro. [La proprieta programaro estas kiel la
alĥemio](/eo/liberaj-programoj-estas-pli-bonaj-ol-alkemio/), dum la libera
programaro estas kiel la scienco. Estas kialo, pro kiu [preskaŭ ĉiuj
superkomputiloj](https://en.wikipedia.org/wiki/Supercomputer#Operating_systems)
kaj retaj serviloj funkcias per libera programaro.

[^1]: Tradukita de la artikolo de The Register [MS' Ballmer: Linux is communism](https://www.theregister.com/Print/2000/07/31/ms_ballmer_linux_is_communism/).
