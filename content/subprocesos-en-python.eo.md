Author: Jorge Maldonado Ventura
Category: Python
Date: 2018-09-04 18:20
Lang: eo
Modified: 2018-09-06 07:45
Save_as: fadenoj-en-Python/index.html
Slug: subprocesos-en-python
Status: published
Tags: kunkuro, Python 3, Python 3.5.2, hazarda, fadenoj, threading, time
Title: Fadenoj en Python
URL: fadenoj-en-Python/

La komputaj fadenoj permesas al ni fari taskojn samtempe. En Python ni
povas uzi la modulon
[threading](https://docs.python.org/3.5/library/threading.html), sed
estas multaj aliaj.

Ni kreos kelkajn simplajn fadenojn (*threads*).

    :::python
    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    import threading
    import time
    import random

    def sleeper(name, s_time):
        print('{} komencita je {}.'.format(
            name, time.strftime('%H:%M:%S', time.gmtime())))

        time.sleep(s_time)

        print('{} finiĝis je {}.'.format(
            name, time.strftime('%H:%M:%S', time.gmtime())))


    for i in range(5):
        thread = threading.Thread(target=sleeper, args=(
            str(i + 1) + 'a fadeno', random.randint(1, 9)))

        thread.start()

    print('Mi finiĝis, sed la aliaj fadenoj ne.')

Unue ni importis la bezonatajn modulojn: `time`, `random` kaj
`threading`. Por krei je *threads* ni nur bezonas la lastan. Je `time`
ni uzis por simuli takson kaj akiri ĝian tempo de komenco kaj fino;
`random` havigas al nia takso hazardan daŭron.

La funkcio *sleeper* «dormas» (faras nenion) dum la tempo kiu ni
specifas, diras al ni kiam komencis «dormi» kaj kiam finis «dormi».
Kiel parametroj ni donas la nomon, kiu ni volas doni al la fadeno, kaj
la tempon, dum kiu «dormos» la funkcion.

Poste ni kreas "por"-iteracion, kiu kreas 5 fadenojn, kiuj plenumas la
`sleeper`-funkcion. En la konstruilo (`threading.Thread`) ni devas diri
la funkcion por plenumi (`target=sleeper`) kaj la parametrojn, kiujn ni
volas doni al ĝi
(`args=('str(i + 1) + 'a fadeno', random.randint(1, 9))`).

    1a fadeno komencita je 15:17:07.
    2a fadeno komencita je 15:17:07.
    3a fadeno komencita je 15:17:07.
    4a fadeno komencita je 15:17:07.
    5a fadeno komencita je 15:17:07.
    Mi finiĝis, sed la aliaj fadenoj ne.
    3a fadeno finiĝis je 15:17:13.
    5a fadeno finiĝis je 15:17:13.
    2a fadeno finiĝis je 15:17:14.
    4a fadeno finiĝis je 15:17:14.
    1a fadeno finiĝis je 15:17:16.

La resulto de la plenumo estas hazarda: ni ne scias kiu fadeno finos
unue.
