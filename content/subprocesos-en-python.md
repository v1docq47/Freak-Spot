Author: Jorge Maldonado Ventura
Category: Python
Date: 2016-08-03 21:22
Lang: es
Modified: 2018-09-04 03:11
Slug: subprocesos-en-python
Status: published
Tags: concurrencia, Python 3, Python 3.5.2, random, subprocesos, threading, threads, time
Title: Subprocesos en Python

Los subprocesos o [hilos de
ejecución](https://es.wikipedia.org/wiki/Hilo_de_ejecuci%C3%B3n) nos
permiten realizar tareas concurrentemente. En Python podemos utilizar el
módulo [`threading`](https://docs.python.org/3.5/library/threading.html),
aunque hay muchos otros.

Vamos a crear varios subprocesos (*threads*) sencillos.

    :::python
    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    import threading
    import time
    import random

    def sleeper(name, s_time):
        print('{} iniciado a las {}.'.format(
            name, time.strftime('%H:%M:%S', time.gmtime())))

        time.sleep(s_time)

        print('{} finalizado a las {}.'.format(
            name, time.strftime('%H:%M:%S', time.gmtime())))


    for i in range(5):
        thread = threading.Thread(target=sleeper, args=(
            'Proceso ' + str(i + 1), random.randint(1, 9)))

        thread.start()

    print('Yo he terminado, pero los otros subprocesos no.')

Primero, hemos importado los modulos necesarios: `time`, `random` y
`threading`. Para crear *threads* solo necesitamos el último. `time` lo
hemos utilizado para simular una tarea y obtener su tiempo de inicio y
fin; `random`, para hacer que nuestro proceso tenga una duración
aleatoria.

La función *sleeper* «duerme» (no hace nada) durante el tiempo que le
especifiquemos, nos dice cuándo ha empezado a «dormir» y cuando ha
terminado de «dormir». Como parámetros le pasamos el nombre que le
queremos dar al subproceso y el tiempo que va a «dormir» la función.

Luego, creamos un bucle for que crea 5 subprocesos que ejecutan la
función `sleeper`. En el constructor (`threading.Thread`), debemos
indicar la función a ejecutar (`target=sleeper`) y los parámetros que
queremos pasarle
(`args=('Proceso ' + str(i + 1), random.randint(1, 9)`).

    Proceso 1 iniciado a las 21:19:23.
    Proceso 2 iniciado a las 21:19:23.
    Proceso 3 iniciado a las 21:19:23.
    Proceso 4 iniciado a las 21:19:23.
    Proceso 5 iniciado a las 21:19:23.
    Yo he terminado, pero los otros subprocesos no.
    Proceso 1 finalizado a las 21:19:25.
    Proceso 5 finalizado a las 21:19:26.
    Proceso 4 finalizado a las 21:19:27.
    Proceso 2 finalizado a las 21:19:32.
    Proceso 3 finalizado a las 21:19:32.

El resultado de la ejecución es aleatorio: no sabemos cuál proceso
finalizará primero.
