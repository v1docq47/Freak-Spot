Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2023-04-04 00:20
Lang: en
Slug: tiktok-¿por-qué-esa-mierda-tiene-tanto-éxito
Save_as: tiktok-why-is-that-shit-so-popular/index.html
URL: tiktok-why-is-that-shit-so-popular/
Tags: Instagram, YouTube, TikTok, addiction, social network
Title: TikTok: why is that shit so popular?
Image: <img src="/wp-content/uploads/2023/03/pastillas-adicción.jpeg" alt="" width="1880" height="1253" srcset="/wp-content/uploads/2023/03/pastillas-adicción.jpeg 1880w, /wp-content/uploads/2023/03/pastillas-adicción.-940x626.jpg 940w, /wp-content/uploads/2023/03/pastillas-adicción.-470x313.jpg 470w" sizes="(max-width: 1880px) 100vw, 1880px">

Imagine a drug that increases your stress, anxiety and leaves you
mentally exhausted; a drug that not only hooks you, but leaves you
drained of energy every day. Who would use such a drug? Almost everyone.
That drug is called TikTok.

Feeling lonely? Don't worry, in the **Trending** and challenges
community you'll have "friends" eager to see you acting like a retard or
making a fool of yourself with an embarrassing dance.

<a href="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp">
<img src="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp" alt="" width="1000" height="530" srcset="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp 1000w, /wp-content/uploads/2023/03/tiktok-adictivo-para-niños-500x265.webp 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

Teenagers, children and adults act like idiots for social approval. But
that doesn't hurt anyone, right? There are challenges like [pouring
bleach in your eyes to see if they change
colour](https://www.iheartradio.ca/100-3-the-bear/trending/new-internet-challenge-involving-teens-and-bleach-great-1.9097980),
[putting on orange mould
make-up](https://www.iheartradio.ca/100-3-the-bear/trending/new-internet-challenge-involving-teens-and-bleach-great-1.9097980),
tripping someone while they jump, as in the following video:

<video controls poster="/wp-content/uploads/2023/03/desafío-rompecráneos-de-TikTok.jpg">
  <source src="/video/desafío-rompecráneos-de-TikTok.webm" type="video/webm">
  <p>Sorry, your browser doesn't support HTML 5. Please change
  or update your browser</p>
</video>

[People have died doing stupid challenges](https://www.theverge.com/2022/7/7/23199058/tiktok-lawsuits-blackout-challenge-children-death). Who's to blame? TikTok
doesn't force you to use the platform, but in an extremely stressful
world of anxiety and dopamine overdose, with people desperate for social
approval, some seek refuge in a toxic environment. There are people who
feel the need to prove to other people that they are happy, that they
have a fantastic life? The reality is quite different.

Faced with family conflicts, problems at school or at work, people take
refuge in TikTok, which at first glance does not look like a drug, to
forget their problems. However, TikTok acts on the mesolimbic pathway,
activating the brain's reward system. If watching stupid videos makes
you forget about your problems and also gives you pleasure, you will
watch stupid videos, and your brain will want to do it again, so you
watch another video and your brain releases dopamine. Over time, the
dopaminergic system adapts: at the beginning it will be enough to watch
one video, but then you need to watch two, three, four... To reach the
same level of pleasure as at the beginning, you will have to watch more
and more videos.

On the other hand, behaviours that increase dopamine in a healthy way,
such as sports and healthy eating, are tedious. These activities cannot
be compared to what the big companies offer: they release your dopamine
to the maximum without hardly any effort on your part, until you end up
overweight, exhausted...

With social networks like TikTok you don't have to choose what you want
to watch, because the algorithm chooses for you the kind of shit you
like, giving it to you without you having to lift a finger. It does this
for free because **you are the product**: their business is to keep you
hooked in front of the screen so that you watch as many ads as possible.
How do they do that? By gradually changing your behaviours and your way
of thinking, in other words, making you dependent without you even
realising it. You can't see how the algorithm that has been
hyper-optimised to hook you works, but you can watch the following video
and do things that encourage other people to spend more time on the
platform, like commenting and liking. **You are a used, not a user**.

That's why you've spent whole days doing practically nothing and you are
always so tired, bored, discouraged, stressed, sad, unmotivated,
irritated, weakened... **hooked**.

<a href="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png">
<img src="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png" alt="" width="1200" height="579" srcset="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png 1200w, /wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura-600x289.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

Is it TikTok that is making people sick? TikTok is a reflection of our
society, a sick society, deluded by the false happiness of social media,
fed up with the world. Social networks know many things about you, but
you don't know anything about them, because they are [proprietary
programs](https://www.gnu.org/proprietary/). There are free social
networks, but [they can also be quite toxic](https://drewdevault.com/2022/07/09/Fediverse-toxicity.html).

<a href="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp">
<img src="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp" alt="" width="1200" height="1200" srcset="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp 1200w, /wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles-600x600.webp 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

To get out of addiction you can try dopamine detoxification, that is,
reducing your use of social media and engaging in activities that
require effort and do not give you immediate pleasure. This can be done
gradually to make it easier.

Of course, social networks can also be useful, but they should be used
in moderation. Ideally, they should be free and respectful of privacy.
They should not encourage addiction, which is exactly what TikTok,
Instagram, YouTube and other social networks do.
