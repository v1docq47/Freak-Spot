Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2023-03-29 17:40
Lang: es
Slug: tiktok-¿por-qué-esa-mierda-tiene-tanto-éxito
Tags: Instagram, YouTube, TikTok, adicción, redes sociales
Title: TikTok: ¿por qué esa mierda tiene tanto éxito?
Image: <img src="/wp-content/uploads/2023/03/pastillas-adicción.jpeg" alt="" width="1880" height="1253" srcset="/wp-content/uploads/2023/03/pastillas-adicción.jpeg 1880w, /wp-content/uploads/2023/03/pastillas-adicción.-940x626.jpg 940w, /wp-content/uploads/2023/03/pastillas-adicción.-470x313.jpg 470w" sizes="(max-width: 1880px) 100vw, 1880px">

Imagínate una droga que aumenta tu estrés, tu ansiedad y que te
deja agotado mentalmente; una droga que no solo engancha, sino que todos
los días te deja sin energía. ¿Quién usaría esa droga? Casi todo el
mundo. Esa droga se llama TikTok.

¿Te sientes solo? No te preocupes, en la comunidad de
**Tendencias** y de desafíos tendrás «amigos» deseando verte actuando
como un retrasado o haciendo el ridículo con un baile que da vergüenza
ajena.

<a href="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp">
<img src="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp" alt="" width="1000" height="530" srcset="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp 1000w, /wp-content/uploads/2023/03/tiktok-adictivo-para-niños-500x265.webp 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

Adolescentes, niños y adultos hacen el imbécil por aprobación social.
Pero eso no le hace daño a nadie, ¿no? Hay desafíos como [echarse lejía
en los ojos para ver si cambian de color](https://www.iheartradio.ca/100-3-the-bear/trending/new-internet-challenge-involving-teens-and-bleach-great-1.9097980), [maquillarse con moho de
naranja](https://www.airedesantafe.com.ar/viral/nuevo-peligroso-desafio-tiktok-maquillarse-moho-naranja-n438593),
hacer la zancadilla a alguien mientras salta en el aire, como en el
siguiente vídeo:


<video controls poster="/wp-content/uploads/2023/03/desafío-rompecráneos-de-TikTok.jpg">
  <source src="/video/desafío-rompecráneos-de-TikTok.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

[Han muerto personas realizando retos estúpidos](https://www.muyinteresante.es/salud/21660.html).
¿Quién tiene la culpa? TikTok no te obliga a usar la plataforma, pero en
un mundo extremamente estresante que provoca ansiedad y una sobredosis
de dopamina, con gente desesperada por tener aprobación social, hay
gente que se refugia en un ambiente tóxico. Hay quienes sienten la
necesidad de demostrar a otras personas que son felices, que tienen una
vida fantástica... La realidad es bastante diferente.

Ante conflictos familiares, problemas en la escuela o el trabajo, las
personas se refugian en TikTok, que a primera vista no parece una droga,
para olvidarse de sus problemas. Sin embargo, TikTok actúa en la vía
mesolímbica, activando el sistema de recompensa cerebral. Si ver vídeos
estúpidos hace que te olvides de tus problemas y además te da placer,
vas a ver vídeos estúpidos, y tu cerebro va a querer hacerlo de nuevo,
así que ves otro vídeo y tu cerebro libera dopamina. Con el tiempo, el
sistema dopaminérgico se va adaptando: al principio te va a bastar con
ver un vídeo, pero luego necesitas ver dos, tres, cuatro... Para llegar
al mismo nivel de placer que al principio vas a tener que ver más y más
vídeos.

Por otro lado, los comportamientos que aumentan la dopamina de forma
saludable, como hacer deporte y comer sano, resultan
tediosos. Estas actividades no se pueden comparar a lo que ofrecen las
grandes empresas, que liberan al máximo tu dopamina sin apenas esfuerzo
por tu parte, hasta que acabas con sobrepeso, exhausto...

Con redes sociales como TikTok no hace falta que elijas lo que quieres
ver, pues el algoritmo elige por ti el tipo de mierda que te gusta,
dándotelo sin que tengas que mover un dedo. Lo hace de forma
gratuita porque **tú eres el producto**: su negocio consiste en
mantenerte enganchado delante de la pantalla para que veas el máximo
número de anuncios posible. ¿Cómo hacen eso?  Cambiando gradualmente tus
comportamientos y tu forma de pensar, en otras palabras, volviéndote
dependiente sin que te des cuenta. No puedes ver cómo funciona el
algoritmo que ha sido hiperoptimizado para engancharte, pero puedes
ver el siguiente vídeo y hacer cosas que inciten a otras personas a
pasar más tiempo en la plataforma, como comentar y darle a «me gusta».
**Eres un usado, no un usuario**.

Por eso te has pasado días enteros sin hacer prácticamente nada y
siempre estás tan cansado, aburrido, desanimado, estresado, triste,
desmotivado, irritado, debilitado... **enganchado**.

<a href="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png">
<img src="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png" alt="" width="1200" height="579" srcset="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png 1200w, /wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura-600x289.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>


¿Es TikTok quien está volviendo a las personas enfermas? TikTok es un
reflejo de nuestra sociedad, una sociedad enferma, engañada
con la falsa felicidad de las redes sociales, harta del mundo. Las redes
sociales saben muchas cosas de ti, pero tú no sabes nada de ellas,
porque son [programas privativos](https://www.gnu.org/proprietary/proprietary.es.html).
Existen redes sociales [libres](https://es.wikipedia.org/wiki/Software_libre), pero [también pueden ser bastante tóxicas](/el-fediverso-puede-ser-bastante-tóxico/).

<a href="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp">
<img src="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp" alt="" width="1200" height="1200" srcset="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp 1200w, /wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles-600x600.webp 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

Para salir de la adicción puedes probar la desintoxicación de
dopamina, es decir, reducir el uso de redes sociales y practicar
actividades que supongan un esfuerzo y no te den un placer inmediato.
Para que resulte más fácil se puede hacer de forma gradual.

Claro, las redes sociales también pueden resultar útiles, pero deben
usarse con moderación. Lo ideal sería que fueran libres y respetuosas
con la privacidad. No deben fomentar una adicción, que es precisamente
lo que TikTok, Instagram, YouTube y otras redes sociales hacen.
