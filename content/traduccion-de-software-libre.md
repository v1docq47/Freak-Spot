Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2017-10-08 02:59
Modified: 2021-11-02
Lang: es
Slug: traduccion-de-software-libre
Tags: colaboración, localización, internacionalización, regionalización, software libre, traducción
Title: Traducción de <i lang="en">software</i> libre

La traducción de <i lang="en">software</i> es una tarea que puede realizar cualquiera que
sepa el idioma del texto origen del programa y cualquier otro idioma a
la perfección (p. ej., la lengua materna). No hacen falta grandes
conocimientos técnicos para traducir <i lang="en">software</i>. En esta publicación
expongo los pasos a seguir que yo recomiendo a la hora de traducir
<i lang="en">software</i> libre.

<!-- more -->

## 1. Encuentra el proyecto

Has visto una página web, videojuego, aplicación de escritorio... que
deseas traducir, pero no sabes cómo hacerlo. Primero, asegúrate de que
la última versión del programa que estás usando no está traducida. Si
sigue sin estar traducida a tu idioma o la traducción está incompleta,
puedes contribuir.

Busca en la página web del proyecto o en la sección de ayuda del
programa enlaces a un correo electrónico o a un proyecto en un servicio
web de control de versiones. Los proyectos de <i lang="en">software</i> libre se suelen
coordinar en servicios web de control de versiones como
[Gitlab](https://es.wikipedia.org/wiki/Gitlab) o
[GNU Savannah](https://es.wikipedia.org/wiki/GNU_Savannah).

## 2. Avisa

Es recomendable avisar de alguna forma a las desarrolladoras del
proyecto, ya que nunca sabes si hay otra persona trabajando en la
traducción; incluso puede que mientras estés realizando la traducción a
alguien más se le ocurra traducir. Si ya hay otra persona traduciendo,
puedes preguntarle si necesita ayuda o si le parece bien repartir el
trabajo.

## 3. Aprende cómo traducir el proyecto

Es normal encontrar información sobre cómo colaborar en el
[README](https://es.wikipedia.org/wiki/README) del proyecto.

Algunos proyectos tienen una guía de traducción que sirve para
orientarnos sobre cómo realizar la traducción. Hay programas que
utilizan <i lang="en">software</i> de traducción en línea (como
[Weblate](https://weblate.org/es/)), otros trabajan usando
[gettext](https://es.wikipedia.org/wiki/Gettext), otros usan unos
métodos más rudimentarios. Si te han remitido a la guía de traducción,
léela detalladamente.

Si no tiene guía, usa el sentido común y pregunta a las desarrolladoras
del programa. Si hay otras traducciones hechas, puedes mirar cómo son e
intentar crear un archivo de traducción para tu idioma con la misma
estructura. Si encuentras archivos con la extensión `.po`, puedes usar
[Poedit](https://es.wikipedia.org/wiki/Poedit) o cualquier otro programa
para realizar traducciones que soporte gettext.

## 4. Traduce

Aplicando las pautas de traducción del proyecto, ponte a traducir. Una
vez hayas terminado de traducir, revísalo todo. Comprueba también si al
usar el programa todo se ve bien; puede que haya símbolos en tu idioma
que no se vean bien, que la traducción no quepa completamente en el
lugar donde debería estar, etc.

Si no tienes idea de cómo compilar el código fuente de la última versión
para probar que todo se ve correctamente, explica a las desarrolladoras
tu problema. Lo más seguro es que te digan que envíes la traducción
directamente. Antes de aceptar tus traducción en el programa, otra
persona comprobará que todo está correcto; pero cerciorarte de que todo
se ve bien antes de enviar la traducción es una buena práctica.

## 5. Pide la inclusión de la traducción

Finalmente, envía la traducción para que puedan revisarla y aceptarla.
Puede que debas usar un sistema de control de versiones como Git o
incluso puede que no tengas que hacer nada, ya que aplicaciones web como
Weblate avisan automáticamente de los cambios y permiten que una
administradora los acepte o requiera correcciones. Si te dicen que hay
algo mal, corrígelo y vuelve a pedir la inclusión de la traducción en el
programa.
