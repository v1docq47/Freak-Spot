Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2018-04-22 13:16
Image: <img src="/wp-content/uploads/2018/04/trisquel-mini-8-flidas.png" alt="" width="1026" height="524" srcset="/wp-content/uploads/2018/04/trisquel-mini-8-flidas.png 1026w, /wp-content/uploads/2018/04/trisquel-mini-8-flidas-513x262.png 513w" sizes="(max-width: 1026px) 100vw, 1026px">
Lang: es
Slug: publicado-trisquel-8
Tags: Flidas, Trisquel, Trisquel 8, Trisquel Flidas, publicación
Title: Publicado Trisquel 8, actualicemos

Ya
[ha sido publicada la versión 8 de Trisquel](https://trisquel.info/es/trisquel-80-lts-flidas),
llamada Trisquel Flidas. Ha pasado un año y medio desde que
[lanzaron la primera versión alfa](/impresiones-sobre-la-version-alfa-de-trisquel-8-flidas/).
Alguna gente ya llevaba tiempo usando la versión en desarrollo cuando se
publicó la versión final, así que parece que promete estabilidad.

El desarrollo de Trisquel ha sido un poco lento esta vez, pues llega dos
años después de Ubuntu Xenial Xerus (16.04 LTS), la versión de Ubuntu en
que estaba basada. Las desarrolladoras dicen haber solucionado los
problemas que causaron tal retraso y ya están haciendo los [preparativos
para trabajar en la nueva versión de
Trisquel](https://trisquel.info/es/planes-de-desarrollo-para-trisquel-90),
que se llamará Etiona, siguiendo la tradición de diosas celtas.

Si tienes Trisquel 7 y quieres actualizarlo a la nueva versión, aquí te
comento el proceso de actualización que hay que seguir. No hay razón
para hacerlo ahora, pues Trisquel 7 tiene actualizaciones hasta abril de
2019. En este caso, os voy a mostrar cómo se hace con la versión
Trisquel Mini, apenas hay diferencias estéticas con la versión estándar;
el proceso es equivalente.

<!-- more -->

<figure>
    <a href="/wp-content/uploads/2018/04/actualizar-a-trisquel-8.png">
    <img src="/wp-content/uploads/2018/04/actualizar-a-trisquel-8.png" alt="">
    </a>
    <figcaption class="wp-caption-text">Primero abrimos el programa Actualización de software</figcaption>
</figure>

A continuación, podremos realizar las actualizaciones que tenemos
pendientes en Trisquel 7. Recuerda que siempre que se trate de aplicar
actualizaciones, Trisquel nos preguntará la contraseña.

<a href="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_2.png">
<img src="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_2.png" alt="Le damos a Instalar ahora para realizar las actualizaciones">
</a>

Una vez terminadas las actualizaciones pendientes de Trisquel 7, seremos
informadas de que el <i lang="en">software</i> está actualizado pero hay una versión
nueva de Trisquel disponible. Para comenzar la actualización a Trisquel
8, le damos a **Actualizar...**.

<a href="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_3.png">
<img src="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_3.png" alt="">
</a>

A continuación, nos aparecen las notas de la versión, que aparecen en
inglés. Supongo que se olvidaron de traducir esto. No explica grandes
cosas, solo el anuncio de la nueva versión y los canales para colaborar
y seguir las novedades del proyecto.

<a href="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_4.png">
<img src="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_4.png" alt="Notas de la versión">
</a>

Le damos de nuevo a **Actualizar**. Ahora se instalarán y actualizarán
los paquetes de la nueva versión de Trisquel.

<a href="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_5.png">
<img src="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_5.png" alt="Realizando la actualización a Trisquel 8">
</a>

Para rematar, Trisquel nos informa con detalle de los paquetes que se
van a actualizar, eliminar, instalar y los que ya no son necesarios.
Al pulsar en **Iniciar la actualización** se actualizará y tendremos
Trisquel 8 al reiniciar el ordenador. Es importante asegurarse que el
ordenador tenga batería o acceso a electricidad durante todo el proceso
de instalación.

<a href="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_6.png">
<img src="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_6.png" alt="Comenzando la actualización de Trisquel 8.">
</a>

<a href="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_7.png">
<img src="/wp-content/uploads/2018/04/actualizar-a-trisquel-8_7.png" alt="Podemos conservar paquetes obsoletos o desinstalarlos.">
</a>

<img src="/wp-content/uploads/2018/04/trisquel-mini-8-flidas.png" alt="Captura de pantalla de Trisquel 8." width="1026" height="524" srcset="/wp-content/uploads/2018/04/trisquel-mini-8-flidas.png 1026w, /wp-content/uploads/2018/04/trisquel-mini-8-flidas-513x262.png 513w" sizes="(max-width: 1026px) 100vw, 1026px">

Trisquel es una de las distribuciones de GNU/Linux completamente libres
más sencilla para principiantes. Esperemos que siga siendo un éxito.
