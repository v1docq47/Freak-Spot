Author: Jorge Maldonado Ventura
Category: Reta programado
Date: 2020-10-22
Lang: eo
Slug: trovi-rompitajn-ligilojn-en-retejo
Tags: HTML, libera programaro, ligiloj, Perl, retejo, W3C Link Checker
Title: Trovi rompitajn ligilojn en retejo
CSS: asciinema-player.css
JS: asciinema-player.js (top)

«Paĝo ne trovita», «eraro 404», ktp.: tio ĝenas. Do, reta programisto,
serĉu kaj reparu la rompitajn ligilojn de via retejo de tempo al tempo.

## Trovi ilin

Ekzistas multaj retaj iloj; mia prefera estas [W3C Link
Checker](https://validator.w3.org/checklink). Eblas ankaŭ instali ĝin en
via komputilo, ĉar ĝi estas libera programaro.

La instruoj pri ĝia uzado troviĝas en [ĝia
deponejo](https://github.com/w3c/link-checker). Mi montras rapide al vi
en la jena video, kiel instali kaj uzi ĝin en via komputilo:

<!-- more -->

<asciinema-player src="/asciicasts/link-checker.cast">
Pardonu, asciinema-player ne funkcias sen Ĝavoskripto.
</asciinema-player>

Kiel montrita en la video, por uzi ĝin, vi nur devas doni al ĝi la bazan
retan URL-n. Se via retejo estas granda, vi devos atendi iom da tempo,
sed la fina eligo resumas la problemojn.

Estas rekomendinda okaze fari tion. Ĉar mia retejo ne estas tre granda,
mi pensas, ke jare estas bone ripari la rompitajn ligilojn en retejo.
Estas ankaŭ bone havi facile troveblan retadreson kaj komentan sistemon,
ĉar tiel la uzantoj povas diri al vi, se io estas rompita.

## Ripari ilin

Se la ligitaj paĝoj simple estis movitaj, estas simple ripari ilin.
Tamen se la paĝo jam ne ekzistas, vi eble devas serĉi en reta arĥivo
kiel [Internet Archive](https://eo.wikipedia.org/wiki/Internet_Archive).

Se vi ankoraŭ ne povas trovi ĝin, eble vi devas anstataŭigi la ligilon
per simila aŭ forigi ĝin, se necesas, aldonante klarigon.
