Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-12-04 13:00
Modified: 2020-12-05
Lang: es
Slug: bloqueo-de-elementos-con-ublock-origin
Tags: Abrowser, Chromium, extensión, Firefox, filtros cosméticos, Google Chrome, Microsoft Edge, Opera, tutorial, uBlock Origin
Title: Bloquear elementos molestos: uBlock Origin
Image: <img src="/wp-content/uploads/2020/12/bloquear-elementos-molestos-con-uBlock-Origin.png" alt="" width="995" height="572" srcset="/wp-content/uploads/2020/12/bloquear-elementos-molestos-con-uBlock-Origin.png 995w, /wp-content/uploads/2020/12/bloquear-elementos-molestos-con-uBlock-Origin-497x286.png 497w" sizes="(max-width: 995px) 100vw, 995px">

Consulta el [artículo
introductorio](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/)
si aún no has instalado uBlock Origin.

<!-- more -->
<video controls poster="/wp-content/uploads/2020/12/bloquear-elementos-molestos-con-uBlock-Origin.png" src="/video/eliminar-elementos-molestos-con-uBlock-Origin.webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Básicamente se pueden bloquear elementos de forma permanente haciendo
clic derecho en el elemento que queramos eliminar y luego clicando en
**Bloquear elemento**. El elemento a eliminar estará marcado en rojo,
y en la esquina inferior derecha aparecerá un menú con varias opciones:

- **Vista previa**: mira cómo quedaría la página antes de aplicar los
  cambios.
- **Crear**: confirma la eliminación del elemento seleccionado.
- **Elegir**: elige otro elemento con el ratón.
- **Salir**: no bloquea nada.

Si te interesa profundizar más sobre esta extensión, en el [siguiente
artículo](/uso-avanzado-de-ublock-origin/) explico las funciones más
avanzadas de uBlock Origin.
