Author: Jorge Maldonado Ventura
Category: Webentwicklung
Date: 2020-08-25 11:30
Lang: de
Save_as: HTML-efektiv-validieren/index.html
URL: HTML-efektiv-validieren/
Slug: validar-HTML-de-forma-efectiva
Tags: GitLab, GitLab CI, HTML, html5validator, Kontinuierliche Integration, Java, Python, Validierung, WHATWG
Title: HTML efektiv validieren
Image: <img src="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png" alt="" width="1077" height="307" srcset="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png 1077w, /wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI-538x153.png 538w" sizes="(max-width: 1077px) 100vw, 1077px">

Die <abbr title="HyperText Markup Language">HTML</abbr>-Sprache hält den
Standard [WHATWG](https://html.spec.whatwg.org/) ein. Weil HTML eine
[Auszeichnungssprache](https://de.wikipedia.org/wiki/Auszeichnungssprache)
ist, bricht ein Fehler nicht die Webseite, sondern das Browser zeigt die
so gut es kann.

HTML-Fehler sind problematisch, denn sie können unerwartete und schwer
reproduzierbare Probleme verursachen, vor allem wenn sie nur in einem
Browser auftritt. Deswegen ist es sehr wichtig, gültige HTML zu
schreiben.

Trotzdem ist es sehr einfach Fehler zu machen und zu übersehen. Darum
ist es empfehlenswert den HTML-Code zu validieren; das heißt, die Fehler
herauszufinden und korrigieren. Dafür existieren Programmen, die
normalerweise einfach die Fehler zeigen. Das aktuellste und
empfehlenswerteste Programm ist [The Nu Html
Checker](https://validator.github.io/validator/). Das W3C betreibt eine
Instanz dieses Programms, die uns HTML-Dokumente mit unserem Browser
zu validieren erlaubt, [durch Einführung einer
URL](https://validator.w3.org/nu/), [durch Hochladen einer
Datei](https://validator.w3.org/nu/#file) oder [durch Einfügen des
HTML-Codes in ein Formular](https://validator.w3.org/nu/#textarea).
Weil dieses Programm Freie Software ist, kannst du es in deinem Computer
einfach installieren.

<figure>
    <a href="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png">
    <img src="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png" alt="" width="1364" height="712" srcset="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png 1364w, /wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org-682x356.png 682w" sizes="(max-width: 1364px) 100vw, 1364px">
    </a>
    <figcaption class="wp-caption-text">Online Validierung der Webseite
    <a href="https://gnu.org/">https://gnu.org/</a>, von GNU.</figcaption>
</figure>

Das online-Programm funktioniert gut, wenn du nur von Zeit zu Zeit
einige wenige Webseiten validieren müsst, aber es ist nicht gut, um eine
ganze Webseite zu validieren. Hierfür empfehle ich die Verwendung der
Version von Nu Html Checker, die im Terminal ausgeführt wird. Die
befindet sich im Datei `vnu.jar` (man müsst Java installiert haben).

Was mich betrifft, nutze ich das
[html5validator](https://pypi.org/project/html5validator/)-Paket, denn
ich arbeite hauptsächlich mit Python und es bedeutet keine zusätzliche
Abhängigkeit. Um dieses Paket in eine auf Debian basierte
GNU/Linux-Distribution zu installieren, muss man nur Folgendes
ausführen:

    :::bash
    sudo apt install default-jre
    sudo pip3 install html5validator

Am Ende der Installierung haben wir ein Programm namens html5validator,
das wir vom Terminal ausführen können:

    :::bash
    html5validator index.html

Ein großartiges Argument ist `--root`, das uns jede Datei in einem
Ordner validieren lasst, und die des Ordners im Ordner..., und so weiter
bis alles validiert ist. Mit diesem Programm gebe ich das
Hauptverzeichnis meiner Webseite an, und so validiere ich die ganze
Webseite in wenigen Sekunden.

    :::bash
    html5validator --root webseite/

Im Idealfall nutzt du eine Art von [Kontinuierliche
Integration](https://de.wikipedia.org/wiki/Kontinuierliche_Integration),
damit du den vorherigen Befehl nicht ausführen musst, jedes Mal, wenn du
etwas in der Webseite änderst. Darum nutzte ich [GitLab
CI](https://docs.gitlab.com/ce/ci/yaml/README.html). So behalte ich
diese Webseite und viele andere ohne HTML-Fehler, und wenn ich etwas
breche, erfahre ich bald.

<figure>
<a href="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png">
<img src="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png" alt="" width="1077" height="307" srcset="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png 1077w, /wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI-538x153.png 538w" sizes="(max-width: 1077px) 100vw, 1077px">
</a>
    <figcaption class="wp-caption-text">Dieser Test von GitLab CI zeigt,
    dass die Webseite mit Erfolgt und ohne HTML-Fehler generiert hat.
    </figcaption>
</figure>
