Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2020-08-24 14:20
Modified: 2021-11-02
Lang: es
Slug: validar-HTML-de-forma-efectiva
Tags: GitLab, GitLab CI, HTML, html5validator, integración continua, Java, Python, validación, WHATWG
Title: Validar HTML de forma efectiva
Image: <img src="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png" alt="" width="1077" height="307" srcset="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png 1077w, /wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI-538x153.png 538w" sizes="(max-width: 1077px) 100vw, 1077px">

El lenguaje <abbr title="HyperText Markup Language">HTML</abbr> se
adhiere al estándar [WHATWG](https://html.spec.whatwg.org/). Como es un
[lenguaje de
marcado](https://es.wikipedia.org/wiki/Lenguaje_de_marcado), un error en
HTML no hace que la página web deje de funcionar, sino que el navegador
la muestra lo mejor que puede.

Tener errores en HTML es problemático, ya que puede producir fallos
inesperados y difíciles de reproducir, sobre todo cuando solo ocurren en
un navegador. Así pues, es vital escribir un HTML válido.

Sin embargo, es muy fácil cometer errores y pasarlos por alto. Por eso
es recomendable validar el código HTML; es decir, encontrar los fallos y
corregirlos. Para eso existen los validadores, que, por lo general,
simplemente muestran los errores. El más actualizado y recomendable
es [The Nu Html Checker](https://validator.github.io/validator/). La W3C
mantiene una instancia de ese validador que nos permite validar
documentos HTML desde el navegador, ya sea [introduciendo una
URL](https://validator.w3.org/nu/), [subiendo un
archivo](https://validator.w3.org/nu/#file) o [introduciendo el código
HTML en un formulario](https://validator.w3.org/nu/#textarea). Como este
validador es libre, puedes instalarlo en tu ordenador fácilmente.

<figure>
    <a href="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png">
    <img src="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png" alt="" width="1364" height="712" srcset="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png 1364w, /wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org-682x356.png 682w" sizes="(max-width: 1364px) 100vw, 1364px">
    </a>
    <figcaption class="wp-caption-text">Validación en línea de la página
    web de GNU <a href="https://gnu.org/">https://gnu.org/</a>.</figcaption>
</figure>

El validador en línea funciona bien si solo tienes que validar unas
pocas páginas web de vez en cuando, pero no sirve para validar un sitio
web entero. Para ello recomiendo usar la versión de The Nu Html
Checker que se ejecuta en terminal. Esta se encuentra en el archivo
`vnu.jar` (hace falta tener Java instalado).

En mi caso, yo utilizo el paquete
[html5validator](https://pypi.org/project/html5validator/), ya que
trabajo principalmente con Python y no supone una dependencia adicional.
Para instalar este paquete en una distribución de GNU/Linux basada en
Debian solo hay que ejecutar...

    :::bash
    sudo apt install default-jre
    sudo pip3 install html5validator

Al terminar la instalación tenemos un programa llamado html5validator
que podemos ejecutar desde la terminal:

    :::bash
    html5validator index.html

Un argumento súper útil es `--root`, que permite validar todos los
archivos de un directorio, y del directorio dentro del directorio...,
así hasta que haya validado todo. Yo lo uso especificando el directorio
raíz de mi sitio web, validando así el sitio web completo en unos
segundos.

    :::bash
    html5validator --root sitio-web/

Lo ideal es usar algún tipo de [integración
continua](https://es.wikipedia.org/wiki/Integraci%C3%B3n_continua) para
no tener que ejecutar manualmente la anterior instrucción cada vez que
cambias algo en la página web. Para ello yo uso
[GitLab CI](https://docs.gitlab.com/ce/ci/yaml/README.html). De este
modo, mantengo este sitio web y muchos otros sin errores de HTML, y
cuando rompo algo, me entero pronto.

<figure>
<a href="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png">
<img src="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png" alt="" width="1077" height="307" srcset="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png 1077w, /wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI-538x153.png 538w" sizes="(max-width: 1077px) 100vw, 1077px">
</a>
    <figcaption class="wp-caption-text">Esta prueba de GitLab CI muestra
    que el sitio web se ha generado con éxito y sin errores de HTML.</figcaption>
</figure>
