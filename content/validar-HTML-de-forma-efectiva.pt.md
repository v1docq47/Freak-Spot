Author: Jorge Maldonado Ventura
Category: Programação web
Date: 2023-01-21 20:00
Lang: pt
Slug: validar-HTML-de-forma-efectiva
Save_as: validar-o-HTML-eficientemente/index.html
URL: validar-o-HTML-eficientemente/
Tags: GitLab, GitLab CI, HTML, html5validator, integração continua, Java, Python, validação, WHATWG
Title: Validar o HTML eficientemente

O <abbr title="HyperText Markup Language">HTML</abbr> adere à norma
[WHATWG](https://html.spec.whatwg.org/). Como é uma linguagem de
marcação, um erro em HTML não faz com que a página web deixe de
funcionar, mas o navegador apresenta-a o melhor que pode.

Ter erros em HTML é problemático, pois pode produzir falhas inesperadas e
difíceis de reproduzir, especialmente quando estas ocorrem apenas num
navegador. É portanto vital escrever HTML válido.

No entanto, é muito fácil cometer erros e ignorá-los. É por isso que é
aconselhável validar o código HTML, ou seja, encontrar os erros e
corrigi-los. Para este fim, existem validadores, que normalmente
simplesmente exibem os erros. O mais actualizado e recomendado é [The Nu
Html Checker](https://validator.github.io/validator/). O W3C mantém uma
instância deste validador que nos permite validar documentos HTML a
partir do navegador, quer [introduzindo um
URL](https://validator.w3.org/nu/), [carregando um
ficheiro](https://validator.w3.org/nu/#file) ou [introduzindo o código
HTML num formulário](https://validator.w3.org/nu/#textarea). Como este
validador é livre, podes instalá-lo facilmente no teu computador.

<figure>
    <a href="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png">
    <img src="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png" alt="" width="1364" height="712" srcset="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png 1364w, /wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org-682x356.png 682w" sizes="(max-width: 1364px) 100vw, 1364px">
    </a>
    <figcaption class="wp-caption-text">Validação em linha do sítio web do GNU <a href="https://gnu.org/">https://gnu.org/</a>.</figcaption>
</figure>

O validador em linha funciona bem se precisar de validar apenas algumas
páginas web de vez em quando, mas não é adequado para validar um sítio
web inteiro. Para isso recomendo a utilização da versão terminal do Nu
Html Checker. Isto pode ser encontrado no ficheiro `vnu.jar` (Java deve
ser instalado).

No meu caso, utilizo o pacote [html5validator](https://pypi.org/project/html5validator/), uma vez que trabalho
principalmente com Python e não requer qualquer dependência adicional.
Para instalar este pacote numa distribuição GNU/Linux baseada em Debian,
só precisas de correr...

    :::bash
    sudo apt install default-jre
    sudo pip3 install html5validator

Quando a instalação estiver concluída, temos um programa chamado
html5validator que podemos executar a partir do terminal:

    :::bash
    html5validator index.html

Um argumento super útil é `--root`, que nos permite validar todos os
ficheiros de um directório, e o directório dentro do directório..., até
ter validado tudo. Utilizo-o especificando o directório raiz do meu
sítio web, validando todo o sítio web em poucos segundos.

    :::bash
    html5validator --root sítio-web/

Idealmente, deverás utilizar algum tipo de [integração
contínua](https://pt.wikipedia.org/wiki/Integra%C3%A7%C3%A3o_cont%C3%ADnua)
para que não tenhas de executar manualmente o comando acima sempre que
mudar alguma coisa na página web. Eu utilizo [o GitLab
CI](https://docs.gitlab.com/ee/ci/yaml/index.html) para isto. Desta
forma, mantenho este sítio web e muitos outros livres de erros HTML, e
quando quebro algo, o descubro cedo.

<figure>
<a href="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png">
<img src="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png" alt="" width="1077" height="307" srcset="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png 1077w, /wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI-538x153.png 538w" sizes="(max-width: 1077px) 100vw, 1077px">
</a>
    <figcaption class="wp-caption-text">Este teste GitLab CI mostra que
    o website foi gerado com sucesso e sem erros HTML.</figcaption>
</figure>
