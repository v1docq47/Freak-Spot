Author: Jorge Maldonado Ventura
Category: Videospiele
Date: 2022-11-17 17:47
Lang: de
Slug: videludo-Super-Bombinhas
Save_as: Videospiel-Super-Bombinhas/index.html
URL: Videospiel-Super-Bombinhas/
Tags: freie Kultur, GNU/Linux, Platform, Videospiel, Windows
Title: Super Bombinhas (Videospiel)
Image: <img src="/wp-content/uploads/2021/11/Super-Bombinhas.png" alt="">

Die Hauptfiguren dieses Plattformspiels sind lebende Bomben. Bomba Azul
ist die einzige Bombe, die nicht von dem bösen Gaxlon gefangen genommen
wurde. Du musst die anderen Bomben und den König Aldan retten. Jede
Bombe hat eine besondere Fähigkeit: Bomba Amarela kann schnell laufen
und höher springen als die anderen, Bomba Verde kann explodieren, der
König Aldan kann die Zeit anhalten usw. Nach der Rettung einer Bombe,
kannst du im Laufe des Abenteuers zu ihr wechseln.

Im Spiel gibt es 7 sehr unterschiedliche Regionen. Jede von ihnen hat
einen Endgegner. Du musst jede Bombe klug einsetzen, um weiterzukommen.

Das Spiel hat auch einen Level-Editor, um neue Level zu erstellen.

![Level-Editor](/wp-content/uploads/2021/11/Super-Bombinhas-level-editor.gif)

Denn ein Bild sagt mehr als tausend Worte, ich zeige nun ein kurzes
Video:

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2021/11/Super-Bombinhas.png" data-setup="{}">
  <source src="/video/super-bombinhas-en.webm" type="video/webm">
  <p>Entschuldigung, dein Browser unterstützt nicht HTML 5. Bitte wechsle
  oder aktualisiere deinen Browser.</p>
</video>

Du kannst das Spiel auf GNU/Linux und Windows installieren. Um es auf
Debian-basierte Verteilungen zu installieren, musst du die
neueste `.deb`-Datei von der [Releases-Seite](https://github.com/victords/super-bombinhas/releases)
herunterladen und ausführen.
