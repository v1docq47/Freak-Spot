Author: Jorge Maldonado Ventura
Category: Videojogos
Date: 2021-11-10
Lang: pt
Slug: videludo-Super-Bombinhas
Save_as: videojogo-Super-Bombinhas/index.html
URL: videojogo-Super-Bombinhas/
Tags: cultura livre, GNU/Linux, plataforma, videjogo, Windows
Title: Videojogo <cite>Super Bombinhas</cite>
Image: <img src="/wp-content/uploads/2021/11/Super-Bombinhas.png" alt="">

Umas bombas viventes são as protagonistas deste jogo eletrónico de
plataforma. A Bomba Azul é a única bomba que não foi capturada pelo
maléfico Gaxlon. Deves salvar as outras bombas e o rei Aldan. Cada bomba
tem uma habilidade especial: a Bomba Amarela pode correr depressa e
saltar mais do que as outras, a Bomba Verde pode explodir, o rei Aldan
pode parar o tempo, etc. Depois de salvar uma bomba podes mudar para ela
durante a aventura.

No jogo existem 7 regiões muito diferentes. Cada uma delas tem um
inimigo final. Deves usar todas as bombas sabiamente para avançar.

O jogo também tem um editor de níveis para criar novos níveis.

![Editor de níveis](/wp-content/uploads/2021/11/Super-Bombinhas-level-editor.gif)

Como uma imagem vale mais que mil palavras, aqui está um pequeno vídeo:

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2021/11/Super-Bombinhas.png" data-setup="{}">
  <source src="/video/super-bombinhas-pt.webm" type="video/webm">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

Podes instalar o jogo em GNU/Linux e Windows. Para o instalar em
distribuições baseadas em Debian, deves descarregar o último ficheiro
`.deb` a partir da [página de
lançamentos](https://github.com/victords/super-bombinhas/releases) e
o correr.

