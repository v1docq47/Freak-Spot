Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2023-02-23 23:19
Image: <img src="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png" alt="">
Lang: eo
Save_as: Vidi-la-kodajn-modifojn-en-Vim-per-vim-gitgutter/index.html
URL: Vidi-la-kodajn-modifojn-en-Vim-per-vim-gitgutter/
Slug: viendo-las-modificaciones-de-codigo-en-vim-con-vimgitgutter
Tags: kromaĵo, diff, tekstoprilaboro sen aranĝo, Git, modificaciones, Neovim, Vim, vim-gitgutter
Title: Vidi la kodajn modifojn en Vim per <code>vim-gitgutter</code>

Kelkfoje, kiam ni redaktas tekston aŭ fontkodon, utilas vidi la ŝanĝojn,
kiujn ni faris rilate al la antaŭa versio. Per
[`vim-gitgutter`](https://github.com/airblade/vim-gitgutter) eblas
aŭtomate fari tion sen eliri el la redaktilo. `vim-gitgutter` estas
kromaĵo por Vim, kiu montras la lastajn ŝanĝojn faritajn en dosiero de
Git-deponejo.

<!-- more -->

La kromaĵo montras la tipon de modifo, kiu estis farita, en la kolumno
ĉe la linia maldekstro. Implicite estas uzataj tri simboloj kun malsamaj
koloroj:

<a href="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png">
<img src="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png" alt="Bildo, kiu montras la simbolojn de vim-gutter">
</a>

- <span style="color: red">–</span> indikas, ke oni forigis la enhavon
  inter la liniojn, kie troviĝas tiu simbolo.
- <span style="color: green">+</span> indikas, ke la linio estas nova.
- <span style="color: yellow">~</span> signifas, ke la linio estis
  modifita.

Por instali la kromaĵon ni necesas havi Vim-on kun subteno por simboloj,
vi povas kontroli tion plenumante `:echo has('signs')`. Se la antaŭa
komando liveras <samp>1</samp>, vi povas instali `vim-gitgutter` kiel
ĉiun ajn alian kromaĵon (plenumu `:h plugin` en Vim por pli da informo
pri kromaĵoj). Vi ankaŭ necesas havi Git-on instalita.

Por provi la kromaĵon sufiĉas modifi dosieron, kiu troviĝu en Git-a
deponejo. La simboloj aperos dum oni modifas la dosieron. Kiam oni faras
novan enmeton, la simboloj malaperas, ĝis kiam ni denove faras modifojn.

La kromaĵo havas multajn opciojn kaj estas tre agordebla por adapti ĝin
al niaj bezonoj. Por pli da informo konsultu ĝian dokumentaron
plenumante `:h gitgutter`.
