Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Textbearbeitung
Date: 2023-02-21 12:46
JS: asciinema-player.js (top)
Lang: de
Slug: vim-surround
Tags: Bearbeitung von einfachem Text, Editor, Erweiterung, Neovim, PHP, Vim, vim-surround
Title: <code>surround.vim</code>

<!-- more -->

<asciinema-player id="video" src="../../asciicasts/89325.json">Entschuldigung,
asciinema-player funktioniert nicht ohne JavaScript.
</asciinema-player>

Beim Programmieren muss ich manchmal Zeichen oder Tags (in
Auszeichnungssprachen) ändern, die Befehle umgeben. In Python zum
Beispiel muss ich manchmal doppelte Anführungszeichen in einfache
Anführungszeichen umwandeln. Das ist etwas umständlich, weil man zu den
Anfangs- und Endanführungszeichen gehen und sie einzeln ersetzen muss.

Für dieses Problem gibt es eine Lösung: die `surround.vim`-Erweiterung für
die Editoren Vim und Neovim. Mit ihr kann ich die Zeichen oder Tags sehr
einfach ersetzen. Um `print("Hallo, Welt!")` durch
`print('Hallo, Welt!')` zu ersetzen, drück einfach `cs"'` im
Normalmodus innserhalb der Anführungszeichen. Ich kann auch
Anführungszeichen von Auszeichnungssprachen wie HTML ersetzen und
umgekehrt. Zum Beispiel kann ich `'Hallo, Welt!'` durch `<h1>Hallo,
Welt!</h1>` ersetzen, indem ich `cs'<h1>` eingebe.

Der Ersetzungsbefehl mag schwer zu merken sein, ist es aber nicht, wenn
du Englisch kannst und denkst, dass `cst'` für <i lang="en">change
surrounding tag [with] "</i> und `cs'` für <i lang="en">change
surrounding " with '</i> steht.

Mit dieser Erweiterung kannst du auch um die Stelle herum, wo du dich
befindest, löschen. Wenn wir z. B. die Anführungszeichen in
`print('Hallo, Welt!')` löschen wollen, müssen wir nur `ds'` eingeben.

Wir können auch HTML-Tags um ein Wort oder mehrere Wörter hinzufügen.
Die Eingabe von `ysiw<em>` im Normalmodus mit dem Cursor auf dem Wort
«hallo» setzt das Wort zwischen `em`-Tags: `<em>hallo</em>`. `yss)`
setzt eine Zeile, in der sich der Cursor befindet, in Klammern.
Um mehr Kontrolle über den zu umschließenden Text zu haben, kannst du
den visuellen Modus verwenden. Wenn du einen Text ausgewählt hast,
drück <kbd>S</kbd> und gib das Zeichen, die Bezeichnung oder die Zeichen
ein, die den Text umgeben sollen.

Was aber, wenn du einen Tag für PHP hinzufügen willst (seine Syntax ist
`<?php Code ?>`)? Um dies zu tun, musst du die folgende Zeile zu unserer
Vim-Konfigurationsdatei (`.vimrc`) hinzufügen: `autocmd FileType php let
b:surround_45 = "<?php ?>`. Damit können wir mehrere Codezeilen mit
PHP-Tags umgeben, indem wir einen Text im visuellen Modus auswählen und
`S-` drücken, falls wir mit einer in PHP geschriebenen Datei arbeiten. Wir
können eine Zeile auch im Normalmodus umschließen, indem wir `yss-`
drücken.

Nachfolgend gibt es einige weitere Beispiele dafür, was du mit `surround.vim`
machen kannst. Das in der Originaltextspalte markierte Zeichen stellt
die Cursorposition dar. Denk daran, dass der Cursor zwischen den
Anführungszeichen oder Tags stehen muss, die du ersetzen willst. Du
kannst sie [in dem Video, das ich dir am Anfang dieses Artikels gezeigt
habe](#video), in Aktion sehen.

Originaltext                            | Befehl       | Ergebnis
----------------------------------------|--------------|--------------------------------------
"Look ma, I'm <mark>H</mark>TML!"       | cs"&lt;q&gt; | &lt;q&gt;Look
ma, I'm HTML!&lt;/q&gt;
if <mark>x</mark>&gt;3 {                | ysW(         | if ( x&gt;3 ) {
my $str = <mark>w</mark>hee!;           | vllllS'      | my $str = 'whee!';
&lt;div&gt;Yo!<mark>&lt;</mark>/div&gt; | dst          | Yo!
Hallo W<mark>e</mark>lt!                | yssB         | \{Hallo Welt!\}
