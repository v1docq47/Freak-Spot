Author: Jorge Maldonado Ventura
Category: Ediçao de textos
Date: 2021-10-27
Modified: 2022-05-29 17:38
Lang: pt
Slug: virgulilla-portugues
Save_as: til-no-GNU-Linux/index.html
URL: til-no-GNU-Linux/
Tags: espanhol, GNU/Linux, português, teclado, til
Title: Til (ã, õ) no teclado espanhol do GNU/Linux

Para escrever um til utiliza-se a combinação
<kbd>AltGr</kbd>+<kbd>4</kbd>. Contudo, se quisermos adicionar o til,
por exemplo, ao a (ã) ou ao e (õ), como acontece em português, não
podemos usando essa combinação, pois apenas o símbolo til (~) aparece
imediatamente. Existe a opção de pressionar a combinação
<kbd>AltGr</kbd>+<kbd>¡</kbd>, soltar e pressionar <kbd>a</kbd> para
escrever ã, mas se preferes fazê-lo com a tecla <kbd>4</kbd>, aqui te
explico como.

Para podermos escrever ã e õ temos de modificar o ficheiro
`/usr/share/X11/xkb/symbols/es` (são necessárias permissões de
administrador). Na linha da tecla 4 temos de trocar `asciitilde`
por `dead_tilde`; isto é, trocar isto...


    :::text
        key <AE04>	{ [         4,     dollar,   asciitilde,       dollar ]	};

por isto...

    :::text
        key <AE04>	{ [         4,     dollar,   dead_tilde,       dollar ]	};

Uma vez guardada a modificação, é necessário reiniciar o sistema de
entrada do núcleo (`sudo udevadm trigger --subsystem-match=input
--action=change`) ou simplemente reiniciar o computador para que as
alterações tenham efeito.

Agora podes pressionar <kbd>AltGr</kbd>+<kbd>4</kbd>, soltar as teclas e
pressionar <kbd>a</kbd> para escrever ã. Se queres apenas digitar
o til (~), podes apertar <kbd>AltGr</kbd>+<kbd>ñ</kbd>,
<kbd>AltGr</kbd>+<kbd>4</kbd> duas vezes ou
<kbd>AltGr</kbd>+<kbd>4</kbd> e <kbd>espaço</kbd>.
