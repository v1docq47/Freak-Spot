Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2018-07-16 15:40
Lang: eo
Save_as: enketo-pri-la-somera-tempo-en-la-EU-nur-se-vi-donas-informon-al-Guglo/index.html
Slug: votación-sobre-horario-de-verano-en-la-ue-solo-si-le-das-información-a-google
Tags: Google, JavaScript, página web, política internacional, privacidad, reCAPTCHA, Unión Europea
Title: Enketo pri la somera tempo en la EU... Nur se vi donas informon al Guglo
Url: enketo-pri-la-somera-tempo-en-la-EU-nur-se-vi-donas-informon-al-Guglo/

La Eŭropa Unio estis konsultinta al ĉiuj civitanoj de la Eŭropa
Komisiono pri la ŝanĝo de horo, farita por adapti la horloĝojn al la
sunlumo. La konsulto funkcias pere de [retenketo](https://ec.europa.eu/eusurvey/runner/2018-summertime-arrangements).

Tamen, oni ne povas plenumi ĝin sen ŝargado de ekstera kaj proprieta
Ĝavoskripta kodo de Guglo, la programo
[reCAPTCHA](/eo/etikedo/recaptcha/). En la
[deklaro pri privateco](https://ec.europa.eu/info/law/better-regulation/specific-privacy-statement_es)
ligilita el la retpaĝo, tamen, oni ne diras, ke oni sendas datumoj al Guglo.

Oni esperas multan partoprenon en la konsulto. Por Guglo ĝi estas
bonege, ke milionoj da uzuloj laboru senpage enkomputiligantaj
proprietajn kartojn aŭ ekzercantaj la armean algoritmon (ekzemple).
