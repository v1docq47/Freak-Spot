Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2018-07-16 15:40
Lang: es
Slug: votación-sobre-horario-de-verano-en-la-ue-solo-si-le-das-información-a-google
Tags: Google, JavaScript, página web, política internacional, privacidad, reCAPTCHA, Unión Europea
Title: Votación sobre horario de verano en la UE... Solo si le das información a Google

La Comisión Europea está consultando a todas las ciudadanas de la Unión
Europa sobre el cambio de horario, realizado para adaptar los relojes a
la luz solar. La consulta se realiza a través de
[una encuesta en Internet](https://ec.europa.eu/eusurvey/runner/2018-summertime-arrangements?surveylanguage=ES).

Sin embargo, no se puede completar sin cargar código JavaScript externo
y [privativo](https://es.wikipedia.org/wiki/Software_propietario) de Google,
el programa [reCAPTCHA](/etiqueta/recaptcha/). En el
[aviso de privacidad enlazado](https://ec.europa.eu/info/law/better-regulation/specific-privacy-statement_es)
desde la página de la encuesta, sin embargo, no se avisa de que se
enviarán datos a Google.

Se espera una alta participación en la encuesta. Para Google es genial
que millones de usuarias
[trabajen gratis](/como-explota-Google-con-CAPTCHAs/)
digitalizando mapas privativos o entrenando su
[algoritmo militar](/al-completar-un-recaptcha-de-google-ayudas-a-matar/)
(por ejemplo).


