Author: Jorge Maldonado Ventura
Category: Bash
Date: 2018-12-21 00:55
Modified: 2022-11-04 22:24
Lang: eo
Slug: wget-descargar-todos-los-archivos-de-web-según-expresión-regular
Tags: elŝuti, regula esprimo, wget
Title: Wget: elŝuti ĉiujn dosierojn de retejo laŭ regula esprimo
Save_as: wget-elŝuti-ĉiujn-dosierojn-de-retejo-laŭ-regula-esprimo/index.html
URL: wget-elŝuti-ĉiujn-dosierojn-de-retejo-laŭ-regula-esprimo/

Ni trovis retejon, kiu havigas multe da dosieroj en PNG-aranĝo,
MP3-aranĝo aŭ ajne nomita kaj ni volas elŝuti ĉiujn. Se estas multaj,
fari tion per muso ne estas rapida.

Per [GNU wget](https://www.gnu.org/software/wget/) ni povas facilege
solvi ĉi tiun problemon. Ni simple povas plenumi komandon kiel ĉi tiu:

<!-- more -->

    :::bash
    wget -c -A '*.mp3' -r -l 1 -nd https://www.esperantofre.com/kantoj/kantomr1.htm

La agordoj havas la sekvajn signifojn:

- `-c`: daŭras la elŝuton de dosiero parte elŝutita.
- `-A`: nur akceptas dosierojn, kiuj kongruas kun regula esprimo
  `*.mp3`.
- `-r`: rikure.
- `-l 1`: unu nivelo de profundeco (t.e., nur la dosieroj ligitaj
   de la skribita retejo).
- `-nd`: ne kreas dosierstrukturon; nur elŝutas ĉiujn dosierojn en la
  nunan dosierujon.
