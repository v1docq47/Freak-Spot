Author: Jorge Maldonado Ventura
Category: Bash
Date: 2016-07-08 06:09
Modified: 2022-11-06 03:50
Lang: es
Slug: xdg-open
Status: published
Tags: abrir cualquier archivo desde la línea de órdenes, xdg-open
Title: <code>xdg-open</code>

Una instrucción bastante útil es `xdg-open`. Con ella podemos abrir
cualquier programa o URL desde la línea de órdenes. Si ejecutara
`xdg-open https://freakspot.net`, se abriría la página principal de
este sitio web con Abrowser (mi navegador por defecto) y luego podría
ejecutar otra instrucción. Una desventaja que tiene es que solo podemos
pasarle un parámetro, por lo que para abrir dos páginas web habría que
ejecutar `xdg-open` dos veces.
